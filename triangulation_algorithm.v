From mathcomp Require Import ssreflect ssrbool ssrnat eqtype ssrfun seq tuple.
From mathcomp Require Import choice path finset finfun fintype bigop fingraph.
From mathcomp Require Import ssralg ssrnum matrix mxalgebra.
(* From mathcomp Require Import finmap. *)
From mathcomp Require Import zmodp.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Section num_computations.

Variable P : Type.

Variable R : unitRingType.

Variables coords : P -> R * R.

Definition mkf3 (T : Type) (a b c : T) :=
  [ffun i : 'I_3 => if val i == 0 then a else if val i == 1 then b else c].

Notation "<< a ; b ; c >>" := (mkf3 a b c).

Definition project_p (p : R * R) : R ^ 3 :=
  [ffun i => if val i == 0 then 1%R else if val i == 1 then p.1 else p.2].

Definition surface_mx (t : (R * R)%type ^ 3) :=
  (\matrix_(i < 3, j < 3) project_p (t i) j)%R.

End num_computations.

Section num_computations_comparison.

Variable P : Type.

Variable R : realDomainType.

Variable coords : P -> R * R.

Definition ccw0 (a b c : R * R) := (0 < \det (surface_mx (mkf3 a b c)))%R.

Definition ccw (a b c : P) := ccw0 (coords a) (coords b) (coords c).

End num_computations_comparison.

Section abstract_over_types.

Variable P : finType.

Variable R : realDomainType.

Variable coords : P -> R * R.

Variable p0 : P.

Variable pick_set : {set P} -> option P.

Variable pick_triangle : {set {set P}} -> P -> option {set P}.

Notation "<< a ; b ; c >>" := (mkf3 a b c).

Notation ccw := (ccw coords).

Definition separated (t : {set P}) (q p : P) :=
  (q \in t) &&
  if pick [pred x | x \in t :\ q] is Some p1 then
    if pick [pred x | x \in t :\ q :\ p1] is Some p2 then
      (ccw p1 p2 q && ccw p2 p1 p) || (ccw p2 p1 q && ccw p1 p2 p)
    else
      false
  else
    false.

Definition inside_triangle p (t : {set P}) :=
    [forall q in t, ~~separated t q p] && (p \notin t) && (#|t| == 3).

Definition boundary_edge (tr : {set {set P}}) :=
 [set e : {set _} | (#|e| == 2) && (#|[set t in tr | e \subset t]| == 1)].

Definition three_points (s : {set P}) :=
  let p1 := if pick [pred p | p \in s] is Some p1 then p1 else p0 in
  let p2 := if pick [pred p | p \in s :\ p1] is Some p2 then p2 else p0 in
  let p3 :=
    if pick [pred p | p \in s :\ p1 :\ p2] is Some p3 then p3 else p0 in
  if ccw p1 p2 p3 then
    << p1; p2; p3 >>
  else
    << p1; p3; p2 >>.

Fixpoint naive_triangulate n (s : {set P}) : {set {set P}} :=
  match n, pick_set s with
    S m, Some p =>
    if #|s| <= 3 then [set s]
    else
      let tr := naive_triangulate m (s :\ p) in
        if pick_triangle tr p is Some t then
              (tr :\ t) :|: [set p |: t :\ q | q in t]
        else
          tr :|: \bigcup_(t in tr)
          [set p |: t :\ q | q in t & separated t q p &&
                (t :\ q \in boundary_edge tr)]
  | _, _ => [set s]
  end.

Definition new_path (f : {ffun P -> P}) a b np :=
  finfun [fun x => f x with a |-> np, np |-> b].

Fixpoint find_next_purple p f side n b :=
  match n with
    0 => b
  | S k => if (ccw b (f b) p) == side then
             b
           else find_next_purple p f side k (f b)
  end.

Definition find_purple_points p f (e : P) : P * P :=
  let n := fingraph.order f e in
  if ccw e (f e) p then
   let pp1 := find_next_purple p f false n (f e) in
     (pp1, find_next_purple p f true n (f pp1))
  else
   let pp2 := find_next_purple p f true n (f e) in
     (find_next_purple p f false n (f pp2), pp2).

Definition cycle3 (f : {ffun 'I_3 -> P}) : {ffun P -> P} :=
  finfun [fun y => f 0%R with f 0%R |-> f 1%R, f 1%R |-> f (-1)%R].

Fixpoint naive2 n (s : {set P}) : {set {set P}} * {ffun P -> P} * P :=
  match n, pick_set s with
    S m, Some p =>
    if #|s| <= 3 then
      ([set s], cycle3 (three_points s), three_points s ord0)
    else
      let '(tr, chf, bp) := naive2 m (s :\ p) in
      if pick_triangle tr p is Some t then
              ((tr :\ t) :|: [set p |: t :\ q | q in t],
               chf, bp)
      else
        let (a, b) := find_purple_points p chf bp in
          (tr :|: \bigcup_(x in orbit chf bp | ccw p (chf x) x)
             [set [set p; chf x; x]], new_path chf a b p, p)
  | _, _ => ([set s], cycle3 (three_points s), p0)
  end.

Fixpoint naive3 (s : seq P) : {set {set P}} * {ffun P -> P} * P :=
  match  s with
    nil =>  (set0, [ffun x => x], p0)
  | p :: tl => 
    if size s <= 3 then
      let s' := [set y in s] in
      ([set [set y in s]], cycle3 (three_points s'),
          three_points s' ord0)
    else 
      let '(tr, chf, bp) := naive3 tl in
      if pick_triangle tr p is Some t then
              ((tr :\ t) :|: [set p |: t :\ q | q in t],
               chf, bp)
      else
        let (a, b) := find_purple_points p chf bp in
          (tr :|: \bigcup_(x in orbit chf bp | ccw p (chf x) x)
             [set [set p; chf x; x]], new_path chf a b p, p)
  end.

Definition inside_triangle_seq (t : seq P) (p : P) : bool :=
  inside_triangle p [set y in t] && (size t == 3).
 
Fixpoint pick_triangle_seq (l : seq (seq P)) (p : P) : 
    option (seq P * seq (seq P)) :=
  match l with
    nil => None
  | t::l' =>
    if inside_triangle_seq t p then
      Some (t, l')
    else
      let r := pick_triangle_seq l' p in
        match r with
          Some (t', l2) => Some (t',  t::l2)
        | None => None
        end
  end.

Fixpoint add_red_triangles p (l : seq P) tr :=
  match l with
  | a :: (b :: _) as l' => 
    if ccw b a p then
      add_red_triangles p l' ([:: b; a; p] :: tr)
    else
      tr
  | _ => tr
  end.

Fixpoint naive4 (s : seq P) : seq (seq P) * {ffun P -> P} * P :=
   match  s with
    nil =>  (nil, [ffun x => x], p0)
  | p :: tl => 
    if size s <= 3 then
      let s' := [set y in s] in
      ([:: [:: three_points s' 0%R; three_points s' 1%R;
               three_points s' (-1)%R]], 
       cycle3 (three_points s'),
          three_points s' ord0)
    else 
      let '(tr, chf, bp) := naive4 tl in
      if pick_triangle_seq tr p is Some (t, trmt) then
        let s' := [set y in t] in
        ([:: three_points s' 0%R; three_points s' 1%R; p] ::
         [:: three_points s' 1%R; three_points s' (-1)%R; p] ::
         [:: three_points s' (-1)%R; three_points s' 0%R; p] ::  trmt,
         chf, bp)
      else
        let (a, b) := find_purple_points p chf bp in
          (add_red_triangles p ((orbit chf a) ++ [:: a]) tr,
           new_path chf a b p, p)
  end.

Section maps.

Variables (map : Type) (map0 : map) (evalmap : map -> P -> P)
  (update : map -> P -> P -> map) (remove : map -> P -> map)
  (mapsize : map -> nat).

Hypothesis eval_update1 :
  forall m y x,  evalmap (update m x y) x = y.

Hypothesis eval_update_dif :
  forall m x x' y, x <> x' -> evalmap (update m x y) x' = evalmap m x'.

Hypothesis eval_remove :
  forall m x x', x <> x' -> evalmap (remove m x) x' = evalmap m x'.

Hypothesis mapsizeP : 
  forall m p, fingraph.order (evalmap m) p <= mapsize m.

Definition seq3 (s : seq P) :=
  match s with
    [:: a; b; c] => if ccw a b c then s else [:: a; c; b]
  | _ => s
  end.

Definition inside_triangles (s : seq P) (p : P) :=
  match s with
    [:: a; b; c] => [:: [:: a; b; p]; [:: b; c; p]; [:: c; a; p]]
  | _ => nil
  end.

Fixpoint find_next_purple_map p map side n b :=
  match n with
    0 => b
  | S k => if (ccw b (evalmap map b) p) == side then
             b
           else find_next_purple_map p map side k (evalmap map b)
  end.

Definition find_purple_points_map p m (e : P) : P * P :=
  let n := mapsize m in
  if ccw e (evalmap m e) p then
   let pp1 := find_next_purple_map p m false n (evalmap m e) in
     (pp1, find_next_purple_map p m true n (evalmap m pp1))
  else
   let pp2 := find_next_purple_map p m true n (evalmap m e) in
     (find_next_purple_map p m false n (evalmap m pp2), pp2).

Definition map_cycle3 s :=
  match s with
    [:: a; b; c] => update (update (update map0 c a) b c) a b
  | _ => map0
  end.

Fixpoint map_orbit (m : map) (e stopper : P) (fuel : nat) trailer :
  seq P :=
  match fuel with
  | O => trailer
  | S k =>
    if evalmap m e == stopper then
      e::trailer
    else
       e::map_orbit m (evalmap m e) stopper k trailer
  end.

Definition new_map (m : map) (a b p : P) :=
  update (update m p b) a p.

Fixpoint naive5 (s : seq P) : seq (seq P) * map * P :=
   match  s with
    nil =>  (nil, map0, p0)
  | p :: tl => 
    if size s <= 3 then
      let tr1 := seq3 s in
      ([:: tr1], map_cycle3 tr1, head p0 tr1)
    else 
      let '(tr, chf, bp) := naive5 tl in
      if pick_triangle_seq tr p is Some (t, trmt) then
       (inside_triangles t p ++ trmt, chf, bp)
      else
        let (a, b) := find_purple_points_map p chf bp in
          (add_red_triangles p (map_orbit chf a a (mapsize chf) [:: a]) tr,
           new_map chf a b p, p)
  end.

End maps.

Definition naive_triangulate' (s : {set P}) := naive_triangulate #|s| s.

Definition flip_edge (t1 t2 : {set P}) :=
  [set x |: (t1 :\: t2) :|: (t2 :\: t1) | x in t1 :&: t2].

Definition in_circle_l (p : R * R) : R ^ 4 :=
  [ffun i : 'I_4 =>
   if val i < 3 then
     project_p p (inord i)
   else
    (p.1 ^+ 2 + p.2 ^+ 2)%R].

Definition in_circle_mx (t : P ^ 4) :=
  (\matrix_(i < 4, j < 4) in_circle_l (coords (t i)) j)%R.



Definition in_circle_tmx (q : P) (t : P ^ 3) : 'M[R]_4 :=
  in_circle_mx
    [ffun i : 'I_4 => if val i < 3 then t (inord i) else q].

Definition in_circle_fun q t : R := (\det (in_circle_tmx q t))%R.

Definition in_circle q t := (in_circle_fun q (three_points t) < 0)%R.

Definition illegal_edge (tr : {set {set P}}) t1 t2 :=
 ([set t1; t2] \subset tr) && (#|t1 :&: t2| == 2) &&
 [forall x in t1 :\: t2, in_circle x t2].

Fixpoint regularize (n : nat) (tr : {set {set P}}) :=
  match n, pick [pred e : {set P} * {set P} | illegal_edge tr e.1 e.2] with
    S m, Some (t1, t2) =>
    let tr' := tr :\: [set t1; t2] :|: flip_edge t1 t2 in
    regularize m tr'
  | _, _ => tr
  end.

Definition regularize' (tr : {set {set P}}) :=
  regularize #|{set {set {set P}}}| tr.

Definition Delaunay (s : {set P}) :=
  regularize' (naive_triangulate' s).

Definition walk_step (tr : {set {set P}}) (t : {set P}) (p : P) :=
  [set t' | t' in tr & (#|t :&: t'| == 2) && 
     [forall x in t :\: t', separated t x p]].

Fixpoint walk n (tr : {set {set P}}) (t : {set P}) (p : P) :=
  match n, pick [pred x | x \in (walk_step tr t p)] with
    S m, Some t' => walk m tr t' p
  | _, _ => [set t]
  end.

Definition walk' (tr : {set {set P}}) t p := walk #|tr| tr t p.

Fixpoint triangulation_w_walk n (s : {set P}) :=
  match n, pick [pred x | x \in s] with
    S m, Some p =>
    if #|s| <= 3 then [set s]
    else
      let tr := regularize' (triangulation_w_walk m (s :\ p)) in
      if pick [pred x | x \in tr] is Some t then
        if pick [pred x | x \in walk' tr t p] is Some t' then
          if inside_triangle p t' then
            (tr :\ t) :|: [set t :\ q :|: [set p] | q in t]
          else
            tr :|: \bigcup_(t in tr)
              [set p |: t :\ q | q in t & separated t q p &&
                  (t :\ q \in boundary_edge tr)]
        else
          tr
      else
        tr
  | _, _ => [set s]
  end.

Definition illegal_edge_at (tr : {set {set P}}) (t : {set P}) (p : P) :=
  if pick [pred x : {set P} | (x \in tr) && (x :&: t == t :\ p)] 
  is Some t' then
     if pick [pred y : P | y \in t' :\: t] is Some q then
       if in_circle q t then Some (t', q) else None
     else
       None
  else
    None.

Fixpoint reg_w_worklist n (tr : {set {set P}}) (wl : {set {set P} * P}) :=
  match n, pick [pred x | x \in wl] with
    S m, Some (t, p) =>
    match pick [pred a | a \in t :\ p] with
      Some q =>
      match illegal_edge_at tr t q with
        Some q' =>
        let new_triangles := [set ((q'.2 |: t :\ a), a) | a in [set p; q]] in
          reg_w_worklist m
            (tr :\ t :\ (q |: t :\ p) :|: [set v.1 | v in new_triangles])
            (wl :\ (t, p) :|: new_triangles)
      | None =>
        match pick [pred a | a \in t :\ p :\ q] with
          Some q =>
          if illegal_edge_at tr t q is Some q' then
            let new_triangles := [set (q'.2 |: t :\ a, a) | a in [set p; q]] in
              reg_w_worklist m
                (tr :\ t :\ (q |: t :\ p) :|: [set v.1 | v in new_triangles])
                (wl :\ (t, p) :|: new_triangles)
           else reg_w_worklist m tr (wl :\ (t, p))
          | None => tr
          end
      end
    | None => tr
    end
  | _, _ => tr
  end.

Definition reg_w_worklist' (tr : {set {set P}}) wl :=
  reg_w_worklist #|tr| tr wl.

Definition pair_triangle_ext_point (t : {set P}) (q p : P) :=
  if pick [pred a | a \in t :\ q] is Some q' then
    (p |: t :\ q, q')
  else
    (t, q).


Fixpoint triangulate_w_worklist n (s : {set P}) :=
  match n, pick [pred x | x \in s] with
    S m, Some p =>
    if #|s| <= 3 then [set s]
    else
      let tr := (triangulate_w_worklist m (s :\ p)) in
      if pick [pred x | x \in tr] is Some t then
        if pick [pred x | x \in walk' tr t p] is Some t' then
          if inside_triangle p t' then
            let work_list :=
              [set pair_triangle_ext_point t q p | q in t] in
            reg_w_worklist' ((tr :\ t) :|: [set v.1 | v in work_list])
                   work_list
          else
            regularize'
              (tr :|: \bigcup_(t in tr)
                           [set p |: t :\ q | q in t & separated t q p &&
                                  (t :\q \in boundary_edge tr)])
        else
          tr
      else
        tr
  | _, _ => [set s]
  end.



Definition general_position (s : {set P}) :=
  [forall a : P, forall b : P, forall c : P,
    (#|[set a; b; c]| == 3) && ([set a; b; c] \subset s) ==>
       ((ccw a b c) || (ccw b a c))].

Definition Delaunay_criterion (tr : {set {set P}}) :=
  [forall p : {set P} * {set P}, ~~illegal_edge tr p.1 p.2].

End abstract_over_types.


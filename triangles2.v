From mathcomp Require Import all_ssreflect.
From mathcomp Require Import all_algebra.
From mathcomp Require Import fingroup perm.
(* From mathcomp Require Import finmap. *)
From mathcomp Require Import zmodp.

Require Import Psatz.

Require Import triangulation_algorithm Knuth_axiom5 triangles.
Require Import fingraph_complements.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Import GRing.Theory Num.Theory.

Section variables.

Variables (P : finType) (R : realDomainType) (coords : P -> R * R) (p0 : P).

Variable (pick_triangle : {set {set P}} -> P -> option {set P}).

Notation inside_triangle := (inside_triangle coords).

Hypothesis pick_triangle_in : forall tr p,
  match pick_triangle tr p with
  | Some x => (x \in tr) && inside_triangle p x
  | _ => true end.

Hypothesis pick_triangleP : forall (tr : {set {set P}}) p,
 [exists x in tr, inside_triangle p x] = (pick_triangle tr p != None).

Section fixed_pick_set.

Variable (pick_set : {set P} -> option P).

Hypothesis pick_setP : forall (s : {set P}),
  reflect (exists2 x, x \in s & pick_set s = Some x) (s != set0).

Hypothesis pick_set_in : forall (s : {set P}) x,
  pick_set s = Some x -> x \in s.

Notation naive2 := (naive2 coords p0 pick_set pick_triangle).

Definition pick_seq_set (s : seq P) (s' : {set P}) : option P :=
  if (find (mem s') s < size s)%N then
    Some (nth p0 s (find (mem s') s))
  else pick_set s'.

Definition naive2' s n s' :=
  triangulation_algorithm.naive2 coords p0
    (pick_seq_set s) pick_triangle n s'.

Notation convex_hull_path := (convex_hull_path coords).

Notation naive3 := (naive3 coords p0 pick_triangle).

Notation naive3' := (naive3 coords p0).

Notation naive :=
  (naive_triangulate coords pick_set pick_triangle).

Notation three_points := (three_points coords p0).

Notation surface_mx := (surface_mx coords).

Notation "<< a ; b ; c >>" := (mkf3 a b c).

Notation general_position := (general_position coords).

Notation ccw := (ccw coords).

Lemma cycle3_add1 i (s : {set P}) : #|s| = 3 ->
  cycle3 (three_points s) (three_points s i) = three_points s (i + 1).
Proof.
move=> s3.
by elim/elimI3: i; rewrite /cycle3 ffunE
            /= ?(eqxx, (inj_eq (three_points_inj s3))) ?addNr.
Qed.

Lemma orbit_cycle3 (s : {set P}) i : #|s| = 3 ->
  boundary_edge [set s] =
  [set [set a; cycle3 (three_points s) a] |
  a in orbit (cycle3 (three_points s)) (three_points s i)].
Proof.
move=> s3.
rewrite (boundary_edge_base coords p0 s3).
have cycle3_add1 := fun i => cycle3_add1 i s3.
apply/setP=> e; apply/imsetP/imsetP=> [[it it1 it2] | ].
  exists (three_points s it);[ | rewrite cycle3_add1 //].
  rewrite -(subrK i it).
  elim/elimI3: (it - i) {it1 it2}.
      by rewrite add0r -fconnect_orbit connect0.
    rewrite (addrC 1) -fconnect_orbit (connect_trans (fconnect1 _ _)) //.
    by rewrite cycle3_add1 connect0.
  rewrite -fconnect_orbit (connect_trans (fconnect1 _ _)) //.
  rewrite cycle3_add1 (connect_trans (fconnect1 _ _)) //.
  by rewrite cycle3_add1 (addrC (-1)) -addrA.
rewrite /orbit.
generalize (fingraph.order (cycle3 (three_points s)) (three_points s i)).
move=> n; elim: n i => [ | n IH] i //=.
  by move=> [x]; rewrite in_nil.
move=> [x]; rewrite in_cons => /orP [/eqP -> -> | ].
  by exists i => //; rewrite cycle3_add1.
rewrite cycle3_add1; move => xi1 edef; apply (IH (i + 1)).
by exists x.
Qed.

Lemma convex_hull_path_cycle3 (s : {set P}) : #|s| = 3 -> general_position s ->
  convex_hull_path s (boundary_edge [set s]) (cycle3 (three_points s)).
Proof.
move=> s3 gp.
apply/forallP=> x; apply/implyP => xb.
have [i xi] : exists i, x = three_points s i.
  have/bigcupP [e e1 e2]:= xb; move: e1; rewrite (boundary_edge_base coords p0 s3).
  by case/imsetP => [j j1 j2]; move: e2; rewrite j2 !inE => /orP [/eqP -> | /eqP ->];
    [exists j | exists (j + 1)].
apply/andP; split; [apply/andP; split | ].
    rewrite xi cycle3_add1 //; apply/bigcupP.
    exists [set three_points s i; three_points s (i + 1)]; last first.
      by rewrite !inE eqxx ?orbT.
    by rewrite (boundary_edge_base coords p0) //; apply/imsetP; exists i.
  apply/forallP=> q; apply/implyP=> qs; apply/implyP=> /andP [qnx qnfx].
  move: qs; rewrite {1}(three_points_eq coords p0 s3) => /imsetP [j _ qj].
  move : qnx qnfx; rewrite xi ?cycle3_add1 // qj -(subrK i j); elim/elimI3: (j - i);
  rewrite ?add0r ?(addrC 1) ?eqxx // (addrC (-1)) => _ _.
  by apply: three_points_oriented.
apply/eqP; rewrite (orbit_cycle3 i s3); rewrite xi; apply/setP => e.
by apply/imsetP/imsetP; move=> [p A ?]; exists p => //; move: A; rewrite inE.
Qed.

Lemma find_next_purple_side p chf side bp pp witness :
  witness \in orbit chf bp -> (ccw witness (chf witness) p) = side ->
  find_next_purple coords p chf side (fingraph.order chf bp) bp = pp ->
  ccw pp (chf pp) p = side.
Proof.
move=> wio wis.
suff : forall i, (i <= findex chf bp witness)%N ->
       find_next_purple coords p chf side (fingraph.order chf bp - i)
         (iter i chf bp) = pp -> ccw pp (chf pp) p = side.
  by move=> /(_ 0%N); rewrite subn0; apply; rewrite leq0n.
set w := findex _ _ _; move=> i iw.
rewrite -(subKn iw).
have : (w - i <= w)%N by rewrite leq_subr.
have wlto : (w < fingraph.order chf bp)%N.
  by apply: findex_max; rewrite fconnect_orbit.
elim: (w - i)%N => [_ | k IH].
  rewrite subn0 iter_findex; last by rewrite fconnect_orbit.
  move: wlto; rewrite -subn_gt0; case: (_ - w)%N => [// | k _ /=].
  by rewrite wis eqxx => <-.
move=> kw.
have -> /= : (fingraph.order chf bp - (w - k.+1) =
          (fingraph.order chf bp - (w - k)).+1)%N.
  rewrite !subnBA //; last by rewrite ltnW.
  by rewrite addnS subSn // ltnW  // (leq_trans wlto) // leq_addr.
case: ifP => [eqside | neqside]; first by move=> <-; apply/eqP.
by rewrite -iterS -subSn //; apply/IH/ltnW.
Qed.

Lemma find_next_purple_fconnect p chf side bp k :
  fconnect chf bp (find_next_purple coords p chf side k bp).
Proof.
elim: k bp => [ | k IH /=] bp; first by apply: connect0.
case: ifP => _; first by apply: connect0.
apply: connect_trans (IH _); apply: fconnect1.
Qed.

Lemma find_next_purple_not_side p chf side bp k i :
  (i < findex chf bp (find_next_purple coords p chf side k bp))%N ->
  ccw (iter i chf bp) (chf (iter i chf bp)) p = ~~side.
Proof.
elim: k i bp => /= [ | k IH] i bp; first by rewrite findex0 ltn0.
case: ifP => [is_side | not_side]; first by rewrite findex0 ltn0.
have [/eqP -> |notbp ] :=
  boolP (find_next_purple coords p chf side k (chf bp) == bp).
  by rewrite findex0 ltn0.
set pp := (X in findex chf bp X).
have step_pp : findex chf bp pp = (findex chf (chf bp) pp).+1.
  apply: findex_step notbp; apply:(connect_trans (fconnect1 _ _)).
  by apply:find_next_purple_fconnect.
case: i => [ | i]; first by move: not_side; case: (ccw _ _ _); case side.
by rewrite step_pp ltnS !iterSr; apply: IH.
Qed.

Lemma find_next_purpleP b p f x y w :
  fcycle f (orbit f x) ->  w \in orbit f x ->  ccw x (f x) p = ~~ b ->
  ccw w (f w) p = b ->
  find_next_purple coords p f b (fingraph.order f x) (f x) = y ->
  ccw (finv f y) y p = ~~ b /\ ccw y (f y) p = b.
Proof.
move=> cyco win xP winP yq.
have winf : w \in orbit f (f x).
  by rewrite (cycle_orbit_mem cyco) // -fconnect_orbit fconnect1.
have orderf : fingraph.order f (f x) = fingraph.order f x.
  apply: (cycle_orbit_order_eq cyco).
  by rewrite -fconnect_orbit fconnect1.
have := find_next_purple_side winf winP.
rewrite orderf=>/(_ _ yq) ->; split; [ | by []].
have := leq0n (findex f (f x) y); rewrite leq_eqVlt eq_sym.
move=>/orP [| igt0].
  rewrite findex_eq0 => /eqP <-.
  by rewrite (cycle_orbit_finv_f_in cyco) -?fconnect_orbit ?connect0 ?xP.
have := find_next_purple_fconnect p f b (f x) (fingraph.order f x).
rewrite yq => /iter_findex => <-.
rewrite -iterSr iterS (cycle_orbit_finv_f_in cyco); last first.
  by rewrite -fconnect_orbit fconnect_iter.
rewrite -(prednK igt0) iterSr.
set j := findex _ _ _.
have := @find_next_purple_not_side p f b (f x) (fingraph.order f x) j.-1.
by rewrite yq -/j; rewrite prednK // leqnn; apply.
Qed.

Lemma find_purple_points_correct p chf bp pp1 pp2 red1
  blue1 :
  fcycle chf (orbit chf bp) ->
  red1 \in orbit chf bp ->  ~~ ccw red1 (chf red1) p ->
  blue1 \in orbit chf bp -> ccw blue1 (chf blue1) p ->
  find_purple_points coords p chf bp = (pp1, pp2) ->
  pp1 \in orbit chf bp /\ pp2 \in orbit chf pp1 /\
  ~~ ccw pp1 (chf pp1) p /\ ccw (finv chf pp1) pp1 p /\
  ~~ ccw (finv chf pp2) pp2 p /\ ccw pp2 (chf pp2) p.
Proof.
move=> cyco red1o red1P blue1o blue1P ppdef; set ob := fingraph.order chf bp.
have [pp1in pp2in] : pp1 \in orbit chf bp /\ pp2 \in orbit chf bp.
  suff : pp1 \in orbit chf (chf bp) /\ pp2 \in orbit chf (chf bp).
    rewrite -!fconnect_orbit =>[[c1 c2]].
    by rewrite !(connect_trans (fconnect1 _ bp)) //.
  rewrite -!fconnect_orbit; move: ppdef; rewrite /find_purple_points.
  case: ifP => csign; move => [pp1q pp2q].
    set A := (X in X /\ _); have : A; rewrite /A {A}.
      by rewrite -pp1q; apply: find_next_purple_fconnect.
    move=> cpp1; rewrite cpp1; split;[ by [] | ].
    rewrite (connect_trans cpp1) // (connect_trans (fconnect1 _ _)) //.
    by rewrite -pp2q pp1q; apply: find_next_purple_fconnect.
  set A := (X in _ /\ X); have : A; rewrite /A {A}.
    by rewrite -pp2q; apply: find_next_purple_fconnect.
  move=> cpp2; rewrite cpp2; split;[ | by [] ].
  rewrite (connect_trans cpp2) // (connect_trans (fconnect1 _ _)) //.
  by rewrite -pp1q pp2q; apply: find_next_purple_fconnect.
split;[by [] | ].
have pp2pp1 : pp2 \in orbit chf pp1.
  by rewrite (cycle_orbit_mem cyco).
split;[by [] | ].
have chfin : chf bp \in orbit chf bp.
  by rewrite -fconnect_orbit fconnect1.
move : ppdef; rewrite /find_purple_points; case:ifP => csign [pp1q pp2q].
  have := find_next_purpleP cyco red1o => /(_ false p pp1).
  rewrite (negbTE red1P) csign => /(_ erefl erefl pp1q) [-> pp1red].
  have cyco1 : fcycle chf (orbit chf pp1).
    by apply: (cycle_orbit_cycle cyco).
  have blue1o1: blue1 \in orbit chf pp1.
    by rewrite (cycle_orbit_mem cyco pp1in).
  have := find_next_purpleP cyco1 blue1o1 _ blue1P => /(_ _ pp1red).
  rewrite (cycle_orbit_order_eq cyco pp1in) -pp1q => /(_ _ pp2q) [or1 or2].
  by rewrite or1 or2 pp1q pp1red.
have := find_next_purpleP cyco blue1o _ blue1P pp2q.
rewrite csign => /(_ erefl) [-> pp2blue].
have cyco2 : fcycle chf (orbit chf pp2).
  by apply: (cycle_orbit_cycle cyco).
have red1o2 : red1 \in orbit chf pp2.
    by rewrite (cycle_orbit_mem cyco pp2in).
move: pp1q; rewrite pp2q -(cycle_orbit_order_eq cyco pp2in) => pp1q.
have := find_next_purpleP cyco2 red1o2 _ (negbTE red1P) pp1q.
rewrite pp2blue => /(_ erefl) [or1 or2].
by rewrite or1 or2.
Qed.

Lemma find_purple_points_in_o p (f : {ffun P -> P}) b :
   fst (find_purple_points coords p f b) \in orbit f b /\
   snd (find_purple_points coords p f b) \in orbit f b.
Proof.
rewrite /find_purple_points.
case: ifP => _.
  set X := (X in X /\ _); have : X; rewrite /X {X}.
    rewrite -fconnect_orbit /=.
    by apply/(connect_trans (fconnect1 _ _))/find_next_purple_fconnect.
  rewrite -!fconnect_orbit => fact1; rewrite fact1; split;[by [] | ].
  apply: (connect_trans fact1).
  by apply/(connect_trans (fconnect1 _ _))/find_next_purple_fconnect.
set X := (X in _ /\ X); have : X; rewrite /X {X}.
  rewrite -fconnect_orbit /=.
  by apply/(connect_trans (fconnect1 _ _))/find_next_purple_fconnect.
rewrite -!fconnect_orbit => fact1; rewrite fact1; split;[ | by [] ].
apply: (connect_trans fact1).
by apply/(connect_trans (fconnect1 _ _))/find_next_purple_fconnect.
Qed.

Lemma convex_hull_cycle' (s : {set P}) (s' : {set {set P}}) f :
  (3 <= #|s|)%N -> cover s' \subset s ->
  convex_hull_path s s' f ->
  {in cover s', forall x, fcycle f (orbit f x)}.
Proof.
move=> s3 ss' ch x xin.
apply: (cycle_orbit_in _ _ xin).
  by apply: convex_hull_stable ch.
by apply: convex_hull_path_inj s3 ss' ch.
Qed.

Lemma mem_orbit1 (T : finType) (f : T -> T) (x y : T) :
  y \in orbit f x -> f y \in orbit f x.
Proof.
by rewrite -!fconnect_orbit => A; apply/(connect_trans A)/fconnect1.
Qed.

Lemma naive_naive2 n (s : {set P}) :
  (3 <= #|s| <= n)%N -> general_position s ->
  exists chf b,
    naive2 n s = (naive n s, chf, b) /\
    convex_hull_path s (boundary_edge (naive n s)) chf /\
    b \in cover (boundary_edge (naive n s)).
Proof.
elim: n s => [ /= | n IH /=] s /andP [s3 sn] gp.
  by move: (leq_trans s3 sn).
have s0 : (0 < #|s|)%N by apply: leq_trans s3.
move: (s0); rewrite card_gt0 => /pick_setP [p pin ->].
have [s3' | s3' ] := boolP (#|s| <= 3)%N.
  have s3q : #|s| = 3 by apply: anti_leq; rewrite s3 s3'.
  exists (cycle3 (three_points s)), (three_points s 0); split => //.
  split; first by apply/(convex_hull_path_cycle3 s3q gp).
  by rewrite (cover_boundary_base coords p0) // three_points_in.
have sn' : (#|s :\ p| <= n)%N.
  rewrite cardsD (setIidPr (_ : [set p] \subset s)); last by rewrite subE.
  by rewrite cards1 -(leq_add2r 1) subnK ?addn1.
have cardint : (2 < #| s :\ p | <= n)%N.
  apply/andP; split; last by [].
  rewrite cardsD (setIidPr (_ : [set p] \subset s)); last by rewrite subE.
  rewrite cards1 -(leq_add2r 1) subnK // addn1 ltn_neqAle s3 andbT eq_sym.
  by apply/negP=> /eqP A; case/negP: s3'; rewrite A.
have gp' : general_position (s :\ p).
  by apply/(general_position_sub gp); rewrite subsetDl.
have smp3 : (2 < #| s :\ p |)%N by case/andP: cardint.
have/IH/(_ gp') [f' [b']] := cardint.
case : (naive2 n (s :\ p)) => [[tr chf] bp] [[trq <- <-]].
rewrite -trq => [[chfp bpino]] {f' b'}.
have := all_triangles_in p0 pick_setP pick_set_in pick_triangle_in
   pick_triangleP sn' smp3 gp'; rewrite -trq => [[all3 [coversmp cht]]] {trq}.
have bsub : cover (boundary_edge tr) \subset s :\ p.
  apply/subsetP=> q /bigcupP [e]; rewrite inE=> /andP [_] /cards1P [u U].
  move: (set11 u); rewrite -U inE=> /andP [uin esubu] qe.
  rewrite -coversmp; apply/bigcupP; exists u => //.
  by apply/(subsetP esubu).
have bn0 : boundary_edge tr != set0.
  by apply/set0Pn; move: bpino=> /bigcupP [x x1 x2]; exists x.
have hullrec : forall q, q \in s :\ p ->
  in_hull coords (cover (boundary_edge tr)) chf q.
  move=> q qin; apply/forallP => x; apply/implyP => xin; apply/implyP => /andP.
  move=> [qnx qnf].
  move: chfp => /forallP/(_ x); rewrite xin implyTb => /andP [ /andP [_] ].
  by move=> /forallP/(_ q); rewrite qin qnx qnf !implyTb.
have pnotin : p \notin s :\ p by rewrite !inE negb_and negbK eqxx.
have beo : boundary_edge tr = [set [set a; chf a] | a in orbit chf bp].
  move: chfp; rewrite /convex_hull_path =>/forallP/(_ bp).
  by rewrite bpino implyTb=> /andP[_ /eqP <-].
have orbit_b : orbit chf bp =i cover (boundary_edge tr).
  move=> y; apply/idP/bigcupP; rewrite beo.
    move=> yin; exists [set y; chf y]; rewrite ?subE //; apply/imsetP.
    by exists y.
  move=> [e /imsetP [x A ->]]; rewrite !inE => /orP[] /eqP -> //. 
  by apply: mem_orbit1.
have bsub' : [set y in orbit chf bp] \subset s :\ p.
  by apply/subsetP => y; rewrite inE orbit_b; apply/(subsetP bsub).
have [ | ] := (boolP [exists x in tr, inside_triangle p x]).
  rewrite pick_triangleP; case tq : (pick_triangle tr p) => [t | ]; last first.
    by rewrite eqxx.
  exists chf; exists bp; split => //.
  have := pick_triangle_in tr p; rewrite tq => /andP [tint pinst].
  have beP : boundary_edge (tr :\ t :|: [set p |: t :\ q | q in t]) =
   [set [set a; chf a] | a in orbit chf bp].
    by rewrite (add_point_in_new_boundary coords p0 all3 coversmp pnotin tint).
  split.
    rewrite -{1}(setD1K pin) beP -beo.
    apply: (add_point_in_convex_hull_path p0 smp3 bsub chfp all3 coversmp erefl
            _ tint pinst bn0) => //.
    by rewrite setD1K.
  rewrite beP; apply/bigcupP; exists [set bp; chf bp].
    by apply/imsetP; exists bp; rewrite ?subE // -fconnect_orbit connect0.
  by rewrite !subE.
move=> no_t.
move: (no_t); rewrite pick_triangleP negbK => /eqP ->.
case fpp_q : (find_purple_points coords p chf bp) => [pp1 pp2].
exists (new_path chf pp1 pp2 p), p; split.
  congr (_ :|: _, _, _).
  apply/setP=> i;apply/idP/bigcupP; last first.
    case=> [t tint /imsetP [q]]; rewrite inE=> /andP[qint /andP [sep]].
    have qins : q \in s :\ p.
      by rewrite -coversmp; apply/bigcupP; exists t.
    move=> be iq; move:be.
    rewrite beo => /imsetP [x xo tmqx].
    have xbe : x \in cover (boundary_edge tr) by rewrite -orbit_b.
    have tq : t = q |: t :\ q by rewrite setD1K.
    have [qnx qnchf] : q != x /\ q != chf x.
      have : q \notin t :\ q by rewrite !inE negb_and negbK eqxx.
      by rewrite tmqx !inE negb_or => /andP.
    apply/bigcupP; exists x.
      rewrite xo andTb.
      move: sep; rewrite tq tmqx setUC -separated_immediate.
        by rewrite Knuth_1.
      move: (chfp) => /forallP/(_ x); rewrite xbe implyTb => /andP[ /andP [_]].
      by move/forallP/(_ q); rewrite qins qnx qnchf !implyTb.
    by rewrite inE iq tmqx (setUC [set x]) setUA.
  move/bigcupP => [x /andP [xino pfx]]; rewrite inE => /eqP iq.
  have xfb : [set x; chf x] \in boundary_edge tr.
    by rewrite beo; apply/imsetP; exists x.
  have xinb : x \in cover (boundary_edge tr) by rewrite -orbit_b.
  move: (xfb); rewrite inE => /andP [e2 /cards1P [t] tq].
  move: (set11 t); rewrite -tq inE => /andP [tint xfsubt].
  have t3 : #|t| = 3 by apply: all3.
  have /cards1P [q qq] : (#|t :\: [set x; chf x]| == 1)%N.
    by rewrite cardsD (setIidPr xfsubt) (eqP e2) t3.
  have tdec : t = [set x; chf x; q] by apply: set3_D2_dec.
  exists t => //.
  have tmq : t :\ q = [set x; chf x].
    by rewrite -qq setDDr setDv set0U (setIidPr xfsubt).
  have qint : q \in t by rewrite tdec !inE eqxx !orbT.
  apply/imsetP; exists q; last by rewrite tmq (setUC [set x]) setUA.
  rewrite inE qint andTb tmq xfb andbT.
  rewrite tdec -separated_immediate; first by rewrite Knuth_1.
  have [qnx qnchf] : q != x /\ q != chf x.
    have : q \notin t :\ q by rewrite !inE negb_and negbK eqxx.
    by rewrite tmq !inE negb_or => /andP.
  move: (chfp) => /forallP/(_ x); rewrite xinb implyTb => /andP [/andP []] _.
  have qins : q \in (s :\ p) by rewrite -coversmp; apply/bigcupP; exists t.
  by move => /forallP/(_ q); rewrite qins qnx qnchf !implyTb.
have gp_o_bp : general_position [set y in orbit chf bp].
  by apply/(general_position_sub (general_position_sub _ (subsetDl _ _)) bsub').
have [red1 red1_o_bp /Knuth_2 red1P'] :
  exists2 red1, red1 \in orbit chf bp & ccw (chf red1) red1 p.
  move/andP: (cht) => [_ ]; rewrite /covers_triangulation => /forallP/(_ chf).
  rewrite chfp => /forallP/(_ p); rewrite pnotin setD1K // gp !implyTb implybE.
  rewrite (negbTE no_t) orbF /in_hull negb_forall => /existsP [] red1.
  rewrite !negb_imply => /andP [A /andP [B]].
  have /forallP/(_ p)/forallP/(_ red1)/forallP/(_ (chf red1)) := gp.
  rewrite cards3 (andbAC (p != red1)) -(eq_sym p) B andTb.
  have := ch_path_no_fixpoint smp3 bsub chfp A; rewrite eq_sym => [[-> _]].
  rewrite andTb !subE pin.
  have -> : red1 \in s.
    have : red1 \in s :\ p by rewrite (subsetP bsub).
    by rewrite inE => /andP [].
  have -> : chf red1 \in s.
    have : chf red1 \in s :\ p.
      rewrite (subsetP bsub) //.
      by have/forallP/(_ red1):= chfp; rewrite A => /andP[/andP []].
    by rewrite inE => /andP [].
  rewrite !implyTb -Knuth_1 => /orP [-> // | red1P].
  exists red1; first by move: A; rewrite -orbit_b.
  by rewrite -Knuth_1.
have pp1in : pp1 \in cover (boundary_edge tr).
  rewrite -orbit_b -[pp1]/((pp1, pp2).1) -fpp_q.
  by have [-> ] := find_purple_points_in_o p chf bp.
have beo1 : [set [set x; chf x] | x in orbit chf pp1] = boundary_edge tr.
  move/forallP: chfp => /(_ pp1); rewrite pp1in implyTb => /andP []/andP[] _ _.
  by move /eqP =>/setP it; apply /setP => y; rewrite -it; apply/imsetP/imsetP;
  case=> [z z1 z2]; exists z; move: z1; rewrite // inE.
have cyco : fcycle chf (orbit chf bp).
  by apply: (convex_hull_cycle' smp3 bsub chfp bpino).
have gp_dec : general_position (p |: s :\ p) by rewrite setD1K.
have /negP noh : ~ in_hull coords (cover (boundary_edge tr)) chf p.
  move: (cht) => /andP [ _ ] /forallP /(_ _) /implyP /(_ chfp) /forallP /(_ p).
  by move=> B A; move: B; rewrite gp_dec pnotin !implyTb A (negbTE no_t).
have red1b : red1 \in cover (boundary_edge tr).
  by rewrite -orbit_b.
have := partition_red_blue smp3 bsub chfp pnotin gp_dec noh red1b.
move=> /(_ red1P') [pp [i [ppin [ilto [reds blues]]]]].
have ppo : pp \in orbit chf bp by rewrite orbit_b.
have ppblue : ccw (iter i chf pp) (chf (iter i chf pp)) p.
  by rewrite -iterS; apply: blues; move: ilto => /andP [] _ ->; rewrite leqnn.
have /andP [igt0 ilo] := ilto.
have ppbo := convex_hull_path_iter_in i smp3 bsub chfp ppin.
have := find_purple_points_correct cyco red1_o_bp red1P' _ ppblue fpp_q.
rewrite orbit_b=> /(_ ppbo) [pp1o [pp2o [pp1red' [pp1blue [pp2red pp2blue']]]]].
have pnb : p \notin orbit chf bp.
  rewrite orbit_b; apply/negP => A.
  suff : p \in s :\ p by rewrite !inE eqxx.
  by rewrite (subsetP bsub).
have chfnofix : forall q, q \in orbit chf bp -> q != chf q.
  move=> q; rewrite orbit_b => /ch_path_no_fixpoint.
  by move=> /(_ _ _ _ _ smp3 bsub chfp) []; rewrite eq_sym.
have pno : forall z, z \in orbit chf bp -> p != z.
  by move=> z zin; apply/negP => /eqP A; case/negP: pnb; rewrite A.
have chfo : forall z, z \in orbit chf bp -> chf z \in orbit chf bp.
  move=> z zin; rewrite -fconnect_orbit (connect_trans _ (fconnect1 _ _)) //.
  by rewrite fconnect_orbit.
have pp1red : ccw (chf pp1) pp1 p.
  have/general_position_split: general_position [set pp1; chf pp1; p].
    apply: general_position_sub (_ : _ \subset s) => //.
    rewrite subE andbC subE pin (subset_trans _ (subsetDl s [set p])) //.
    by rewrite (subset_trans _ bsub') // !subE pp1o mem_orbit1.
  rewrite chfnofix // pno // eq_sym pno ?chfo // => /(_ isT isT isT).
  by move=> /orP [] //; rewrite (negbTE pp1red').
have pp1ino: pp1 \in orbit chf pp by rewrite (cycle_orbit_mem cyco ppo).
have cycopp : fcycle chf (orbit chf pp) by apply: (cycle_orbit_cycle cyco).
have iterm1 : forall j, (0 < j)%N ->
     iter j.-1 chf pp = finv chf (iter j chf pp).
  move=> j jgt0; rewrite -[in RHS](prednK jgt0) iterS.
  by rewrite (cycle_orbit_finv_f_in cycopp) // -fconnect_orbit fconnect_iter.
have pp1pp : pp1 = pp.
  have [j jq jlto] : exists2 j,
       pp1 = iter j chf pp & (j < fingraph.order chf pp)%N.
    exists (findex chf pp pp1).
      by move: pp1ino; rewrite -fconnect_orbit => /iter_findex.
    by rewrite findex_max // fconnect_orbit.
  have jlti : (j < i)%N.
    rewrite ltnNge; apply/negP => ilej.
    move: (blues j); rewrite size_orbit ilej jlto // => /(_ isT).
    by rewrite iterS -jq (negbTE pp1red').
  have jle0 : (j <= 0)%N.
    rewrite leqNgt; apply/negP => jgt0.
    move: (reds j.-1); rewrite (ltn_trans _ jlti) prednK // => /(_ isT).
    by rewrite -jq iterm1 // -jq => /Knuth_2; rewrite pp1blue.
  by move: jle0; rewrite jq leqn0 => /eqP ->.
set k := findex chf pp1 pp2.
have cp12 : fconnect chf pp1 pp2.
  by rewrite fconnect_orbit.
have kqi : k = i.
  apply: anti_leq; apply/andP; split.
    rewrite leqNgt; apply/negP => iltk.
    move: (blues k.-1); rewrite size_orbit -ltnS (ltn_predK iltk) iltk.
    have := pp2o; rewrite -fconnect_orbit => /findex_max; rewrite pp1pp andTb.
    move=> tmp; rewrite (leq_trans _ tmp); last by rewrite -pp1pp leqnSn.
    move=> /(_ isT); rewrite iterm1 -?pp1pp //; last first.
      by apply: (ltn_trans _ iltk).
    by rewrite iter_findex // (negbTE pp2red).
  rewrite leqNgt; apply/negP=> klti.
  move: (reds _ klti).
    by rewrite iterS -pp1pp iter_findex // => /Knuth_2; rewrite pp2blue'.
have [j [pp2q [jlto [reds' blues']]]]
 : exists j, pp2 = iter j chf pp1 /\ (j < fingraph.order chf pp1)%N /\
         (forall k, (k < j)%N ->
            ccw (iter k.+1 chf pp1) (iter k chf pp1) p) /\
         (forall k, (j <= k < fingraph.order chf pp1)%N ->
           ccw (iter k chf pp1) (iter k.+1 chf pp1) p).
  exists i.
  rewrite -kqi iter_findex //; split; first by [].
  split; first by rewrite findex_max.
  split; first by rewrite kqi pp1pp.
  by rewrite kqi pp1pp -size_orbit.
have jint : (0 < j < fingraph.order chf pp1)%N.
  rewrite jlto andbT.
  rewrite lt0n; apply/negP => /eqP A; move: pp1red'.
  by move: pp2q; rewrite A /= => <- /negP; apply.
have new_b := (orbit_new_path_boundary_edge smp3 bsub chfp all3
        coversmp erefl pnotin pp1in pp1red jint reds' blues' beo1).
split.
rewrite -new_b pp2q.
by have := (add_point_out_new_convex_hull_path smp3 bsub chfp pnotin pp1in
             pp1red jint reds' blues'); rewrite setD1K // chfq'.
rewrite -new_b; apply/bigcupP.
exists [set p; (new_path chf pp1 (iter j chf pp1) p p)].
  by apply/imsetP; exists p; rewrite ?subE // -fconnect_orbit connect0.
by rewrite !subE.
Qed.

Lemma pick_seq_set_cat_cons s0 p s : 
  uniq (s0 ++ p :: s) ->
  pick_seq_set (s0 ++ p :: s) [set y in p :: s] = Some p.
Proof.
move=> us0ps.
move: (us0ps); rewrite uniq_catC cat_uniq => /and3P [ups A us0].
rewrite /pick_seq_set find_cat.
  have pops : (mem [set y in p :: s] =1 mem (p :: s)).
    by move=> z; rewrite 2!inE.
  rewrite (eq_has pops) (negbTE A) size_cat ltn_add2l -cat1s find_cat /= !inE.
  by rewrite eqxx /= nth_cat addn0 (ltnn _) subnn.
Qed.

Lemma naive2_naive3 (s : seq P) :
  (3 <= size s)%N -> uniq s -> naive3 s = naive2' s (size s) [set y in s].
Proof.
suff : forall s0, (3 <= size s)%N -> uniq (s0 ++ s) ->
    naive3 s = naive2' (s0 ++ s) (size s) [set y in s].
  by move=> main; apply: (main [::]).
elim: s => [// | p s IH s0 s3 us /=].
have -> : #|[set y in p :: s]| = #|p :: s|.
  by apply/eq_card => z; rewrite !inE.
move: (us); rewrite uniq_catC cat_uniq => /and3P [ups disj us0].
rewrite pick_seq_set_cat_cons // (card_uniqP ups) /=.
move: (ups); rewrite cons_uniq => /andP [pnotin us'].
case: ifP => s3' //.
have -> // : [set y in p :: s] :\ p = [set y in s].
  apply/setP=> z; apply/idP/idP; rewrite !inE.
    by move=> /andP [/negbTE -> /= ].
  move=> zs; rewrite zs orbT; apply/andP; split => //.
  by apply/negP => /eqP A; move: pnotin; rewrite -A zs .
have us0ps : (uniq ((rcons s0 p) ++ s)) by rewrite -cats1 -catA /=.
have sgt2 : (2 < size s)%N.
  by rewrite ltnNge -ltnS s3'.
by rewrite (IH (rcons s0 p) sgt2 us0ps) -cats1 -catA.
Qed.

End fixed_pick_set.

Section pick_set2.

Notation convex_hull_path := (convex_hull_path coords).

Definition pick_set (s : {set P}) : option P :=
  pick [pred x | x \in s].

Lemma pick_setP (s : {set P}) 
  : reflect (exists2 x, x \in s & pick_set s = Some x) (s != set0).
Proof.
rewrite /pick_set /=; case: pickP => /=.
  move=> x xin; have /set0Pn -> : exists x, x \in s by exists x.
  by apply: ReflectT; exists x.
move=> none; have /set0Pn -> : ~(exists x, x \in s).
  by move=> [x]; rewrite -[x \in s]/([pred x in s] x) none.
by apply: ReflectF => [] [x].
Qed.

Lemma pick_set_in (s : {set P}) x : pick_set s = Some x -> x \in s.
Proof.
by rewrite /pick_set /=; case: pickP => //= y yin [<-].
Qed.

Lemma pick_seq_setP (l : seq P) (s : {set P}) :
    reflect (exists2 x, x \in s & pick_seq_set pick_set l s = Some x)
        (s != set0).
Proof.
rewrite /pick_seq_set; case t : (_ < _)%N; last by apply: pick_setP.
move: (t); rewrite -has_find => /hasP exw.
have -> : s != set0.
  by apply/set0Pn; case: exw => w wl win; exists w; exact win.
apply/ReflectT; exists (nth p0 l (find (mem s) l)) => //.
by apply/nth_find/hasP.
Qed.

Lemma pick_seq_set_in (l : seq P) (s : {set P}) x :
  pick_seq_set pick_set l s = Some x -> x \in s.
Proof.
rewrite /pick_seq_set; case t : (_ < _)%N; last by apply: pick_set_in.
by move=> [] <-; apply: nth_find; move: (t); rewrite -has_find.
Qed.

Notation pick_seq_set := (pick_seq_set pick_set).

Notation naive3 := (naive3 coords p0 pick_triangle).

Notation naive3' := (naive3 coords p0).

Notation naive :=
  (naive_triangulate coords pick_set pick_triangle).

Notation three_points := (three_points coords p0).

Notation surface_mx := (surface_mx coords).

Notation "<< a ; b ; c >>" := (mkf3 a b c).

Notation general_position := (general_position coords).

Notation ccw := (ccw coords).

(*
Lemma inside_triangle_seqP (l : seq P) p :
  inside_triangle_seq coords l p = inside_triangle p [set y in l].
Proof.  by []. Qed.
*)

Lemma pick_triangle_seq_size (l l' : seq (seq P)) p t :
 (forall t, t \in l -> size t = 3) ->
 pick_triangle_seq coords l p = Some (t, l') ->
 size t = 3 /\ (forall t, t \in l' -> size t = 3).
Proof.
elim: l l' => [ | a l1 IH] l' //= alls.
have alls' : forall t, t \in l1 -> size t = 3.
  by move=> t' tin; apply: alls; rewrite inE tin orbT.
case: ifP => [pina | pnina].
  move=> [a_t l1l']; rewrite -a_t -l1l'; split => //.
  by apply: alls; rewrite !subE.
case h : (pick_triangle_seq coords l1 p) => // [[t2 l2]] [t2t l2q].
move: (IH l2 alls'); rewrite -t2t -l2q => /(_ h) [] st2 others.
split => // t'; rewrite !inE => /orP [/eqP -> | tin].
  by apply: alls; rewrite inE eqxx.
by apply: others.
Qed.

Lemma pick_triangle_seq_some (l l' : seq (seq P)) p t :
  (forall t, t \in l -> size t = 3) ->
  uniq (map (fun t => [set x in t]) l) ->
  pick_triangle_seq coords l p = Some (t, l') ->
  (inside_triangle p [set x in t]) /\
  t \in l /\ 
  [set t in map (fun y => [set x in y]) l'] =
  [set t  in map (fun y => [set x in y]) l] :\ [set x in t] /\
  uniq (map (fun t' => [set y in t']) l').
Proof.
elim: l l' => [ | a l1 IH] l' //= alls /andP [anot ul1].
case: ifP => [pina | pnina].
  move=> vq; move: vq anot ul1 pina => [-> ->] tnot ul' pint.
  split; first by move/andP: pint => [].
  rewrite !inE eqxx; split; first by []; split => //.
  apply/setP => t'; rewrite !inE.
  have [/eqP tt' | tnt'] := boolP (_ == _); last first.
    by rewrite ?andFb ?andTb ?orFb.
  by rewrite tt' (negbTE tnot).
case rv : (pick_triangle_seq coords l1 p) => [ [t2 l2]|] //.
move=> [t2_t al2_l'].  
move: rv; rewrite t2_t => rv.
have alls' : forall t,  t \in l1 -> size t = 3.
  by move=> t' tin; apply alls; rewrite !inE tin orbT.
have [pint [tin1 [setq ul']]] := IH l2 alls' ul1 rv.
split; first by [].
rewrite !inE tin1 orbT; split; first by [].
rewrite -al2_l' map_cons cons_uniq ul' andbT.
have : [set x in a] \notin [set z in [seq [set x in y] | y <- l2]].
  by rewrite setq !inE negb_and anot orbT.
rewrite inE => ->; split; last by [].
apply/setP=> z; rewrite !inE.
have -> : z \in [seq [set x in y] | y <- l2] = 
       (z \in [set t in [seq [set x in y] | y <- l2]]).
  by rewrite inE.
rewrite setq !inE.
have [/eqP za | ] := boolP (z == [set x in a]).
  rewrite !orTb za andbT; apply/esym/negP => /eqP a_t.
  move: pnina; rewrite /inside_triangle_seq a_t pint.
  by rewrite (alls a) // inE eqxx.
by rewrite !orFb.
Qed.

Lemma pick_triangle_seq_none (l : seq (seq P)) p t :
  pick_triangle_seq coords l p = None ->
  t \in l -> ~~ inside_triangle_seq coords t p.
Proof.
elim: l => [ | a l IH] //=.
case: ifP => //; case A : (pick_triangle_seq _ _) => [ [? ?] | ] //.
rewrite inE; move=> pnina _ /orP [/eqP -> | ].
  by rewrite pnina.
by apply: IH.
Qed.

Lemma set_three_points_remove1 (t : {set P}) j :
  #|t| = 3 ->
  t :\ three_points t j = [set three_points t (j + 1); three_points t (j - 1)].
Proof.
move=> t3; rewrite (three_points_eqi coords p0 j t3); apply/setP=> z.
rewrite !inE !andb_orr -(three_points_eqi coords p0 j t3) andNb orFb.
by congr (_ || _); rewrite andbC; 
  (apply/andP/idP;[case=> -> // | move/eqP => ->; apply/andP];
   rewrite eqxx (inj_eq (three_points_inj t3)) //
   eq_sym ?addm1_dif -?(addrC 1) ?add1_dif).
Qed.

Definition fina := (add0r, p1p1, m1m1, addNr, addrN, subrr).

Lemma inside_triangles_eq (t : {set P}) (p : P) :
    #|t| = 3 ->
    [set p |: t :\ q | q in t] =i
    [seq [set y in t'] | t' <-
       [:: [:: three_points t 0; three_points t 1; p];
           [:: three_points t 1; three_points t (-1); p];
           [:: three_points t (-1); three_points t 0; p]]].
Proof.
move=> t3.
(* TODO: understand why :
rewrite [in LHS](three_points_eq coords p0 t3).
does not work. *)
move=> z; set s := (_ :: _).
rewrite {2}(three_points_eq coords p0 t3); apply/imsetP/idP.
  move=> [] q /imsetP [] j _ qq ->.
  suff -> : p |: t :\ q =
         nth set0 [seq [set y in t'] | t' <- s] (nat_of_ord (j + 1)).
    by apply: mem_nth; rewrite size_map. 
  rewrite qq {qq}; rewrite (set_three_points_remove1 _ t3) //=.
  by elim/elimI3: j => /=; rewrite ?fina !set_cons setU0 !setUA cycle_set3.
by rewrite /= !set_cons setU0 !inE => /orP [/eqP -> | /orP [] /eqP ->];
  set fi := (X in three_points t X); (exists (three_points t (fi - 1));
    [apply/imsetP; exists (fi - 1); done |
     rewrite setUA (setUC [set p]) (set_three_points_remove1 (fi - 1) t3)
     /fi !fina ]).
Qed.

Lemma add_red_cons_red p b1 b2 l tr :
  ccw b2 b1 p ->
  add_red_triangles coords p [:: b1, b2 & l] tr =
  add_red_triangles coords p (b2 :: l) ([:: b2; b1; p] :: tr).
Proof. by rewrite /= => ->. Qed.

Lemma add_red_cons_blue p b1 b2 l tr :
  ~~ccw b2 b1 p ->
  add_red_triangles coords p [:: b1, b2 & l] tr = tr.
Proof. by rewrite /= => /negbTE ->. Qed.

Lemma uniq_add_red_seq p b b' b'' l (tr : seq (seq P)) :
   uniq (b :: l ++ [:: b']) -> uniq (l ++ [:: b'; b'']) -> 
   ccw b' b'' p ->
   p \notin l ++ [:: b'; b''] ->
   (forall t x, t \in [seq [set t in t_l] | t_l <- tr] ->
       p \in t -> x \in t -> x \notin (l ++ [:: b'])) ->
   uniq [seq [set t in t_l] | t_l <- tr] ->
   uniq [seq [set t in t_l] | t_l <-
    (add_red_triangles coords p ((b::l) ++ [:: b'; b'']) tr)].
Proof.
elim: l b tr => [//= | b2 l' IH] b tr uni uni' blue pnl disj utr.
  case: ifP => //.
  move/Knuth_2/negbTE: blue => -> red.
  rewrite !map_cons !cons_uniq utr andbT; apply/negP=> A.
  by move: (disj _ b' A); rewrite !subE => /(_ isT isT).
have ub2l : uniq ((b2 :: l') ++ [:: b'])
  by move: uni; rewrite cons_uniq => /andP[].
have pnl'' : p \notin (l' ++ [:: b'; b''])
  by move: pnl; rewrite !inE negb_or => /andP [].
have pnl' : p \notin (l' ++ [:: b']).
  move: pnl''; rewrite !mem_cat negb_or => /andP[] /negbTE ->.
  by rewrite !inE negb_or => /andP[] /negbTE ->.
have bnl' : b \notin (l' ++ [:: b']).
  by move: uni; rewrite /= inE negb_or => /andP [] /andP [].
have b2nl'' : b2 \notin (l' ++ [:: b'; b'']).
  by move: uni'; rewrite /= => /andP [].
have b2nl' : b2 \notin (l' ++ [:: b']).
  move: b2nl''; rewrite !mem_cat negb_or => /andP[] /negbTE ->.
  by rewrite !inE negb_or => /andP[] /negbTE ->.
have uni'' : uniq (l' ++ [:: b'; b'']).
  by move: uni'; rewrite cat_cons cons_uniq => /andP [] _.
have disj' t x :
  t \in [seq [set y in t_l] | t_l <- [:: b2; b; p] :: tr] ->
  p \in t -> x \in t -> x \notin (l' ++ [:: b']).
  rewrite map_cons inE => /= /orP [/eqP -> | ].
    rewrite !inE => _ /orP[ | /orP []] /eqP -> //.
  by move/disj => A /(A _) B /(B _); rewrite inE mem_cat negb_or => /andP [].
have [red1 | blue1] := boolP (ccw b2 b p); last by rewrite add_red_cons_blue.
rewrite 2!cat_cons add_red_cons_red //.
apply :(IH b2 ([:: b2; b; p] :: tr) ub2l) => //.
rewrite map_cons cons_uniq utr andbT.
apply/negP => A.
move : (disj [set y in [:: b2; b; p]] b2); rewrite inE A !inE !eqxx !orbT orTb.
by move=> /(_ isT isT isT).
Qed.

Lemma uniq_add_red p b chf (tr : seq (seq P)) :
  fcycle chf (orbit chf b) ->
  ~~ccw b (chf b) p -> ccw (finv chf b) b p ->
  orbit chf b \subset cover [set t in [seq [set y in l] | l <- tr]] ->
  p \notin cover [set t in [seq [set y in l] | l <- tr]] ->
  uniq [seq [set y in t_l] | t_l <- tr] ->
  uniq [seq [set y in t_l] | t_l <-
          (add_red_triangles coords p ((orbit chf b) ++ [:: b]) tr)].
Proof.
move=> cyco red blue osub pnt uni.
have ogt1 : (0 < (fingraph.order chf b).-1)%N.
  rewrite ltnNge leqn0; apply/negP => /eqP A.
  by move: blue; rewrite /finv A => /oriented_diff; rewrite eqxx. 
have : orbit chf b = b :: traject chf (chf b)
                   (fingraph.order chf b).-2 ++ [:: finv chf b].
  set rr := RHS; rewrite /orbit -orderSpred -(prednK ogt1).
  rewrite trajectS trajectSr -cats1 /rr; congr (_ :: _ ++ _).
  by rewrite -iterSr prednK.
set l := traject _ _ _ => o1_eq.
have uniq1 : uniq (b :: l ++ [:: finv chf b]).
  by rewrite -o1_eq orbit_uniq.
have uniq2 : uniq (l ++ [:: finv chf b; b]).
  move: uniq1.
  have -> : b :: l ++ [:: finv chf b] = [:: b] ++ l ++ [:: finv chf b] by [].
  by rewrite (uniq_catC [:: b]) -catA.
have pno2 : p \notin l ++ [:: finv chf b; b].
  apply/negP=> A; case/negP: pnt; rewrite (subsetP osub) //.
  move: A; rewrite (_ : [:: finv chf b; b] = [:: finv chf b] ++ [:: b]) //.
  by rewrite catA mem_cat orbC -mem_cat /= o1_eq.
have disj : forall t x, t \in [seq [set y in t_l] | t_l <- tr] ->
         p \in t -> x \in t -> x \notin l ++ [:: finv chf b].
  move=> t x tin pin xin; case/negP: pnt; apply/bigcupP; exists t => //.
  by rewrite inE.
have := (uniq_add_red_seq uniq1 uniq2 blue pno2 disj uni).
by rewrite o1_eq /= -catA /=.
Qed.

Lemma boundary_edge_subset (tr : {set {set P}}) :
  cover (boundary_edge tr) \subset cover tr.
Proof.
apply/subsetP=> x; move/bigcupP=> [e]; rewrite inE => /andP [] _.
set u := (X in pred_of_set X) => setu1 xine.
have/card_gt0P [t tin] : (0 < #|u |)%N by rewrite (eqP setu1).
move: tin; rewrite !inE => /andP [] tin esubt.
by apply/bigcupP; exists t => //; apply: (subsetP esubt).
Qed.

Lemma add_red_triangles_cat a l tr:
  add_red_triangles coords a l tr = add_red_triangles coords a l [::] ++ tr.
Proof.
elim: l tr => [ | b l IH] tr //=.
case ql : l => [// | c l'].
case: (ccw c b a) => //.
by rewrite -ql (IH ([:: c; b; a] :: tr)) (IH [:: [:: c; b; a]]) -catA.
Qed.

Lemma add_red_triangles_size tr l p :
  (forall t, t \in tr -> size t = 3) ->
  (forall t, t \in add_red_triangles coords p l tr -> size t = 3).
Proof.
move=> other; elim: l => [// | a [// | b l] IH].
move=> t.
rewrite -[add_red_triangles _ _ _ _]/
   (if (ccw b a p) then
        add_red_triangles coords p (b :: l) ([:: b; a; p] :: tr)
    else tr).
case: ifP=> _; last by apply: other.
rewrite add_red_triangles_cat => tin.
have : t \in [:: b; a; p] :: add_red_triangles coords p (b :: l) tr.
  move: tin; rewrite (add_red_triangles_cat _ _ tr) !(subE, mem_cat).
  by move=> /orP [ | /orP []]; try move=> ->; rewrite ?orbT.
by rewrite inE => /orP [/eqP -> | tin'] //; apply: IH.
Qed.

Lemma naive3_naive4 l :
  (3 <= size l)%N -> uniq l ->
  general_position [set x in l] ->
  let '(trs, f, b) := naive4 coords p0 l in
  naive3 l = ([set t in [seq [set x in t'] | t' <- trs]], f, b) /\
  uniq [seq [set y in x] | x <- trs] /\
  (forall t, t \in trs -> size t = 3).
Proof.
elim: l => [ | p l IH]; first by [].
move=> sgt2 upl gppl; move: sgt2 (upl) => /=.
have gpl : general_position [set y in l].
  apply/(general_position_sub gppl)/subsetP=> ?.
  by rewrite !inE => ->; rewrite orbT.
have gpspl : general_position (p |: [set y in l]).
  by apply/(general_position_sub gppl)/subsetP=> ?; rewrite !inE.
have [ | ]:= boolP (size l < 3)%N.
  rewrite !ltnS => lle2 lge2 /andP [pnotin ul].
  have/anti_leq/esym sl2 : (2 <= size l <= 2)%N by rewrite lle2 lge2.
  split; last first.
    split; first by apply: cons_uniq.
    by move=> t; rewrite !inE => /eqP ->.
  congr (_, _, _).
  have enuml : [set three_points [set y in p :: l] i | i : 'I_3] =
       [set x in [:: three_points [set y in p :: l] 0;
                     three_points [set y in p :: l] 1;
                     three_points [set y in p :: l] (-1)]].
    apply/setP=> x; apply/imsetP/idP.
      by move=> [j _ ->]; elim/elimI3: j; rewrite !subE.
    by rewrite !inE => /orP [ | /orP []] /eqP ->; (eexists;[ | eapply erefl ]).
  rewrite map_cons /= -enuml.  rewrite [RHS]set_cons setU0; congr [set _].
  have spl3 : #|[set y in p :: l]| = 3.
    rewrite [LHS]( _ : _ = #|p::l|); last by apply: eq_card=> x; rewrite !inE.
    by rewrite (card_uniqP upl) /= sl2.
  by rewrite -three_points_eq.
rewrite -leqNgt=> sl3 _ /andP [pnotin ul].
case rc : (naive4 coords p0 l) => [[tr chf] bp].
have size_l_q : size l = #|[set y in l]|.
  by rewrite -(card_uniqP ul); apply: eq_card => z; rewrite inE.
have [n3_eq [utr alls]]: naive3 l =
    ([set t in [seq [set x in t'] | t' <- tr]], chf, bp) /\
    uniq [seq [set y in t'] | t' <- tr] /\
    (forall t, t \in tr -> size t = 3) .
  by have {IH}:= (IH sl3 ul gpl); rewrite rc.
set tr_ref := naive_triangulate coords (pick_seq_set l) pick_triangle
    #|[set y in l]| [set y in l].
have setl3 : (2 < #|[set y in l]|)%N.
  rewrite (_ : #|[set y in l]| = #|l|); last first.
    by apply: eq_card=> z; rewrite inE.
  by have/card_uniqP ->:= ul.
have setl3' : (2 < #|[set y in l]| <= #|[set y in l]|)%N.
  by rewrite setl3 leqnn.
have [tr_eq [chfP bpP]] : [set t in [seq [set x in t'] | t' <- tr]] = tr_ref /\
  convex_hull_path [set y in l] (boundary_edge tr_ref) chf /\
  bp \in cover (boundary_edge tr_ref).
  have := naive_naive2 (pick_seq_setP l) (@pick_seq_set_in l) setl3' gpl.
  rewrite -/tr_ref; move=> [] f [] bel [] n_eq [] ch belb.
  have := (naive2_naive3 pick_set sl3 ul).
  by rewrite n3_eq /naive2' size_l_q n_eq => [[? -> ->]].
have := all_triangles_in p0 (pick_seq_setP l) (@pick_seq_set_in l)
   pick_triangle_in pick_triangleP (leqnn _) setl3 gpl.
rewrite -/tr_ref => [] [ts3 [cov cvt]].
case pteq : (pick_triangle_seq coords tr p) => [[t trmt] | ].
  move: (pick_triangle_seq_some alls utr pteq) => [pint [tintr [eqsets urem]]].
  rewrite n3_eq tr_eq.
  case p_ref: (pick_triangle tr_ref p)=> [t_ref | ]; last first.
    move: (pick_triangleP tr_ref p); rewrite p_ref.
    suff /existsP -> : exists x, (x \in tr_ref) && inside_triangle p x.
      by rewrite eqxx.
    exists [set y in t].
    by rewrite pint andbT -tr_eq inE; apply/mapP; exists t.
  move: (pick_triangle_in tr_ref p); rewrite p_ref=> /andP[] t_ref_in pint_ref.
  have t_ref_t : t_ref = [set y in t].
    have := disjoint_triangles p0 (pick_seq_setP l) (@pick_seq_set_in l)
    pick_triangle_in pick_triangleP (leqnn _) setl3 gpspl.
    rewrite inE pnotin -/tr_ref => /(_ _ _ isT t_ref_in _ pint_ref pint).
    by apply; rewrite -tr_eq inE; apply/mapP; exists t.
  set a := [:: _; _; _]; set b := [:: _; _; _]; set c := [:: _; _; _].
  have -> : [:: a, b, c & trmt] = [:: a; b; c] ++ trmt by [].
  split.
    congr (_, _, _); apply/setP => u.
    rewrite map_cat [RHS]inE mem_cat orbC [LHS]inE; congr (_ || _).
      move/setP: eqsets => eqsets; move: (eqsets u); rewrite inE => ->.
      by rewrite inE [RHS]inE t_ref_t -tr_eq.
    move/ts3: t_ref_in; rewrite t_ref_t => t_3.
    by rewrite inside_triangles_eq.
  split; last first.
    move=> t'.
    rewrite !inE => /orP [/eqP -> | /orP [/eqP -> | /orP [/eqP -> |]]] //.
    by have [_ it] := (pick_triangle_seq_size alls pteq); apply: it.
  rewrite map_cat cat_uniq; apply/andP; split.
    rewrite /= andbT !inE !negb_or.
    suff difj j :
      [set y in [:: three_points t_ref j; three_points t_ref (j + 1); p]] !=
      [set y in [:: three_points t_ref (j + 1); three_points t_ref (j - 1); p]].
      rewrite /a /b /c -t_ref_t (difj 0); move: (difj 1); rewrite !fina => ->.
      by move: (difj (-1)); rewrite !fina eq_sym => ->.
    set s':= (X in X != _); apply/negP=> /eqP A.
    have : three_points t_ref j \in s' by rewrite !inE eqxx.
    rewrite A !inE !(inj_eq (three_points_inj (ts3 _ _))) //= {A}.
    rewrite addrC (negbTE (add1_dif _)) (negbTE (addm1_dif j)) /= => /eqP A.
    suff : p \in cover tr_ref by rewrite cov inE => A'; case/negP: pnotin.
    apply/bigcupP; exists t_ref; rewrite -?A //.
    by rewrite (three_points_in coords p0 _ (ts3 _ _)).
  apply/andP; split => //=; apply/negP => /hasP => [] [z zin1].
  rewrite /= !inE => zabc.
  have pz : p \in z.
    by move: zabc=> /orP [ | /orP []] /eqP ->; rewrite !inE eqxx !orbT.
  have : z \in [set u in [seq [set x in y] | y <- trmt]] by rewrite inE.
  rewrite eqsets inE tr_eq andbC=> /andP [] A _.
  have : p \in cover tr_ref by apply/bigcupP; exists z.  
  by rewrite cov inE // => A'; move: pnotin; rewrite A'.
case fpq : (find_purple_points _ _ _ _) => [pp1 pp2].
rewrite n3_eq tr_eq.
case p_ref: (pick_triangle tr_ref p)=> [t_ref | ].
  move: (pick_triangleP tr_ref p); rewrite p_ref => /existsP[] t'.
  rewrite -tr_eq inE => /andP [] /mapP [] a t'tr -> pint'.
  move: (pick_triangle_seq_none pteq t'tr ).
  rewrite /inside_triangle_seq pint' (_ : size a = 3) ?eqxx //.
  by apply: alls.
have := find_purple_points_in_o p chf bp; rewrite fpq /= => [[o1 _]].
have besub : cover (boundary_edge tr_ref) \subset [set y in l].
  by rewrite -cov; apply: boundary_edge_subset.
have cyco : fcycle chf (orbit chf bp).
  by apply: (convex_hull_cycle' setl3 besub chfP).
have pnsl : p \notin [set y in l] by rewrite !inE.
have /negP noh : ~in_hull coords (cover (boundary_edge tr_ref)) chf p.
  move=> A.
  move: cvt; rewrite /convex_hull_triangulation => /andP [cvh].
  move=> /'forall_implyP /(_ _ chfP) /'forall_implyP /(_ _ gpspl).
  by rewrite pnsl A !implyTb {A} pick_triangleP p_ref.
have [w [wbe wred]] :
    exists w, w \in cover (boundary_edge tr_ref) /\ ~~ ccw w (chf w) p.
  move: noh; rewrite /in_hull negb_forall => /existsP [w]; rewrite negb_imply.
  move/andP => [wbe wr].
  have [pnw pnfw] : p != w /\ p != chf w.
    split; apply/negP=> /eqP A; case/negP: pnsl; rewrite (subsetP besub) ?A //.
    by apply: (convex_hull_stable chfP).
  by exists w; move: wr; rewrite pnw pnfw !implyTb.
have obp_eq : orbit chf bp =i cover (boundary_edge tr_ref).
  move: chfP => /'forall_implyP /(_ _ bpP) => /andP[] _ /eqP obp_eq.
  by apply: orbit_cover_s'.
have := blue_edge setl3 besub chfP pnsl gpspl noh => [] [bl].
rewrite -obp_eq => /andP[] blbe blP.
have wobp : w \in orbit chf bp by rewrite obp_eq.
have [pp1obp [pp2opp1 [pp1red [pp1blue [pp2red pp2blue]]]]]:= 
  find_purple_points_correct cyco wobp wred blbe blP fpq.
split; last first.
  split; last first.
    by move=> t tin; apply: (add_red_triangles_size alls tin).
  move: (chfP) => /'forall_implyP /(_ _ bpP) => /andP [] _ /eqP be.
  have pp1o : pp1 \in cover (boundary_edge tr_ref).
    rewrite -be; apply/bigcupP; exists [set pp1; chf pp1].
      by apply/imsetP; exists pp1.
    by rewrite !inE eqxx.  
  move: (chfP) => /'forall_implyP /(_ _ pp1o) => /andP [] _ /eqP be'.
  have osub : orbit chf pp1 \subset cover (boundary_edge tr_ref).
    rewrite -be'; apply/subsetP => z zin; apply/bigcupP.
    exists [set z; chf z]; first by apply/imsetP; exists z.
    by rewrite !inE eqxx.
  have cyco1 : fcycle chf (orbit chf pp1).
    by apply: (convex_hull_cycle' setl3 _ chfP).
  apply uniq_add_red => //.
    apply: (subset_trans osub); rewrite -tr_eq.
    by apply: boundary_edge_subset.
  by rewrite tr_eq cov inE.
congr (_, _, _).
(* TODO : proof done in place for convenient access to hypotheses, but
  should be in separate lemma. *)
have pp1be : pp1 \in cover (boundary_edge tr_ref) by rewrite -obp_eq.
have opp1_eq : orbit chf pp1 =i cover (boundary_edge tr_ref).
  apply: (orbit_cover_s' erefl); apply/eqP.
  by move: chfP => /'forall_implyP /(_ _ pp1be) => /andP[].
have := partition_red_blue setl3 besub chfP pnsl gpspl noh pp1be pp1red.
move=> [pp1' [i [red' [iint [reds blues]]]]].
have cyco' : fcycle chf (orbit chf pp1').
  by apply: (convex_hull_cycle' setl3 besub chfP).
have opp1'_eq : orbit chf pp1' =i cover (boundary_edge tr_ref).
  apply: (orbit_cover_s' erefl); apply/eqP.
  by move: chfP => /'forall_implyP /(_ _ red') => /andP[].
have iterm1 : forall j, (0 < j)%N ->
     iter j.-1 chf pp1' = finv chf (iter j chf pp1').
  move=> j jgt0; rewrite -[in RHS](prednK jgt0) iterS.
  by rewrite (cycle_orbit_finv_f_in cyco') // -fconnect_orbit fconnect_iter.
have pp1'q : pp1' = pp1.
  have pp1o' : pp1 \in orbit chf pp1' by rewrite opp1'_eq.
  set k := findex chf pp1' pp1.
  have kq : iter k chf pp1' = pp1.
    by apply: iter_findex; rewrite fconnect_orbit.
  have klo : (k < size (orbit chf pp1'))%N.
    by rewrite size_orbit; apply:findex_max; rewrite fconnect_orbit.
  have kli : (k < i)%N.
    rewrite ltnNge; apply/negP=> A; move: (blues k) pp1red.
    by rewrite iterS kq klo A => /(_ isT) => ->.
  have : (k <= 0)%N.
    rewrite leqNgt; apply/negP=> A.
    move: (reds k.-1); rewrite (ltn_trans _ kli); last by rewrite (prednK A).
    by rewrite (prednK A) iterm1 // kq => /(_ isT) /Knuth_2; rewrite pp1blue.
  by rewrite leqn0 -kq => /eqP ->.
move: iint reds blues cyco' iterm1; rewrite pp1'q {pp1'q opp1'_eq red' pp1'}.
move=> iint reds blues cyco1 iterm1.
have ilto : (i < fingraph.order chf pp1)%N.
  by move: iint; rewrite size_orbit => /andP [].
apply/setP=>z.
have obppp1 : (fun x => (x \in orbit chf bp) && ccw p (chf x) x) =1
              (fun x => (x \in traject chf pp1 i)).
  move=> x;apply/idP/idP.
    rewrite obp_eq -opp1_eq -fconnect_orbit -Knuth_1 => /andP [] xcpp1 xred. 
    move: (xcpp1) => /iter_findex; set k := findex _ _ _ => kidx.
    suff ki : (k < i)%N.
      have := (nth_traject chf ki pp1) => nthk. 
      by apply/(nthP pp1); exists k; rewrite ?size_traject // nthk kidx.
    have := findex_max xcpp1; rewrite -/k => klto.
    rewrite ltnNge; apply/negP=> A; move: (blues k); rewrite size_orbit klto A.
    by rewrite iterS kidx => /(_ isT) /Knuth_2; rewrite xred.
  rewrite obp_eq -opp1_eq; move/trajectP => [] k klti kidx.
  rewrite -fconnect_orbit kidx fconnect_iter andTb -Knuth_1 -iterS.
  by apply: reds.
rewrite (eq_bigl _ _ obppp1).
have dec : forall k, (k <= i)%N ->
  \bigcup_(x in traject chf pp1 i) [set [set p; chf x; x]] =
  \bigcup_(x in traject chf pp1 k) [set [set p; chf x; x]] :|: 
  \bigcup_(x in traject chf (iter k chf pp1) (i - k)) [set [set p; chf x; x]].
  elim => [ | k Ik]; first by rewrite [in RHS]big_pred0 // set0U subn0.
  move=> ki; rewrite trajectSr -cats1 -[in RHS]bigcup_seq big_cat /= -setUA.
  rewrite bigcup_seq -[X in _ :|: (_ :|: X)]bigcup_seq -big_cat /=.
  by rewrite -trajectS bigcup_seq subnSK //; apply/Ik/ltnW.
suff : forall k (tr1 : seq (seq P)), (k <= i)%N ->
  [set t in [seq [set y in t] | t <- tr1]] =
  \bigcup_(x in traject chf pp1 k) [set [set p; chf x; x]] :|:
                   [set t in [seq [set y in t] | t <- tr]] ->
  [set t in [seq [set x in t'] |
     t' <- add_red_triangles coords p 
           ((traject chf (iter k chf pp1) (fingraph.order chf pp1 - k))
           ++ [:: pp1]) tr1]] =
  tr_ref :|: \bigcup_(x in traject chf pp1 i) [set [set p; chf x; x]].
  move/(_ 0%N tr); rewrite leq0n.
  by rewrite subn0 big_pred0 //= set0U => /(_ isT erefl) ->.
move=> k tr1 klei; rewrite -(subKn klei); move: tr1.
have : (i - k <= i)%N by rewrite leq_subr.
elim: (i - k)%N => [_ | k1 Ik1 klti] tr1.
  rewrite subn0.
  have difgt0 : (0 < fingraph.order chf pp1 - i)%N.
    by rewrite subn_gt0; apply: (leq_trans _ ilto); rewrite ltnS.
  rewrite -(ltn_predK difgt0) trajectS /=.
  have [i_below | io' ]
    := boolP (0 < (fingraph.order chf pp1 - i).-1)%N; last first.
    have oi1 : i = (fingraph.order chf pp1).-1.
      move: io'.
      rewrite -leqNgt leqn0 -eqSS (prednK difgt0) -(eqn_add2r i) add1n.
      rewrite subnK; last by apply: ltnW.
      by rewrite -orderSpred eqSS eq_sym => /eqP.
    have omi1 : ((fingraph.order chf pp1 - i) = 1)%N.
      by rewrite oi1 -orderSpred /= subSnn.
    rewrite omi1 /=.
    have /Knuth_2/negbTE -> : ccw (iter i chf pp1) pp1 p.
      have := (blues i); rewrite leqnn size_orbit ilto => /(_ isT).
      by rewrite oi1 orderSpred cycle_orbit_iter_order.
    by move=> ->; rewrite tr_eq setUC.
  rewrite -(ltn_predK i_below) /=.
  move: (blues i); rewrite leqnn; move: iint=> /andP [] _ -> => /(_ isT).
  by move/Knuth_2/negbTE -> => ->; rewrite setUC tr_eq.
move=> tr1cond. 
have co : (1 < fingraph.order chf pp1 - (i - k1.+1))%N.
  by rewrite ltn_subRL addn1 subnSK // (leq_ltn_trans (leq_subr _ _)).
have difgt0 : (0 < fingraph.order chf pp1 - (i - k1))%N.
  by rewrite subn_gt0; apply: (leq_ltn_trans _ ilto); rewrite leq_subr.
suff -> : traject chf (iter (i - k1.+1) chf pp1)
           (fingraph.order chf pp1 - (i - k1.+1)) =
       [:: iter (i - k1.+1) chf pp1,
           iter (i - k1.+1).+1 chf pp1 &
           traject chf (iter (i - k1.+1).+2 chf pp1)
              (fingraph.order chf pp1 - (i - k1.+1).+2)].
  rewrite 2!cat_cons add_red_cons_red; last first.
    by apply: reds; rewrite subnSK ?leq_subr.
  rewrite -cat_cons.
  suff -> : (iter (i - k1.+1).+1 chf pp1
         :: traject chf (iter (i - k1.+1).+2 chf pp1)
          (fingraph.order chf pp1 - (i - k1.+1).+2)) =
     (traject chf (iter (i - k1.+1).+1 chf pp1)
       (fingraph.order chf pp1 - (i - k1))).
    rewrite subnSK // Ik1 //.
      by apply: ltnW.
    rewrite map_cons set_cons tr1cond setUCA setUA; congr (_ :|: _).
    have eqi := subnSK klti ; rewrite -{2} eqi trajectSr -cats1.
    rewrite -[in RHS]bigcup_seq big_cat /= bigcup_seq; congr (_ :|: _).
    rewrite big_seq1; apply/setP=> z'; rewrite !inE !set_cons setU0 setUA. 
    by rewrite -cycle_set3 -!setUA -iterS subnSK.
  rewrite -(ltn_predK difgt0) /=; congr (_ :: traject _ _ _).
  by rewrite subnSK // -orderSpred subSS subSKn.
have : (fingraph.order chf pp1 - (i - k1.+1) =
        (fingraph.order chf pp1 - (i - k1)).+1)%N.
  by rewrite -subSS subnSK //; apply/subSn/ltnW; rewrite -subn_gt0.
move=> ->; rewrite trajectS; congr (_ :: _).
rewrite -(ltn_predK difgt0) trajectS -iterS; congr (_ :: traject _ _ _).
by rewrite subnSK // -orderSpred subSS subSn //= -ltnS orderSpred -subn_gt0.
Qed.

End pick_set2.

End variables.

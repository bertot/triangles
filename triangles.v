From mathcomp Require Import all_ssreflect.
From mathcomp Require Import all_algebra.
From mathcomp Require Import fingroup perm.
(* From mathcomp Require Import finmap. *)
From mathcomp Require Import zmodp.

Require Import Psatz.

Require Import triangulation_algorithm Knuth_axiom5.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import GRing.Theory Num.Theory.

Definition subE :=
  (subUset, sub1set, inE, eqxx, orbT, andbT).

Open Scope ring_scope.

Lemma iter_orbit (T : finType) f i (x : T) : iter i f x \in orbit f x.
Proof. by rewrite -fconnect_orbit fconnect_iter. Qed.

Hint Extern 0 (is_true (iter _ _ _ \in orbit _ _)) =>
  apply iter_orbit : core.

Lemma iter_lt_order_neq (T : finType) f k l (x : T) :
  (k < l < fingraph.order f x)%N -> iter k f x <> iter l f x.
Proof.
move => /andP [kl lo] abs.
have ko : (k < fingraph.order f x)%N by apply: ltn_trans lo.
by move: (kl); rewrite ltn_neqAle -(findex_iter lo) -abs findex_iter // eqxx.
Qed.

Lemma setI1 (T : finType) (A : {set T}) (a : T) :
  (A :&: [set a] == set0) = (a \notin A).
Proof.
apply/eqP/idP.
  move=> Aa0; apply/negP => ain; suff : a \in set0 by rewrite inE.
  by rewrite -Aa0 !inE ain eqxx.
move=> ani; apply/setP => x; rewrite setIC !inE.
by have [/eqP -> | //] := boolP (x == a); rewrite (negbTE ani).
Qed.

Lemma cards3 (T : finType) (a b c : T) :
  (#|[set a; b; c]| == 3) = (a != b) && (b != c) && (c != a).
Proof.
rewrite cardsU cards1 cards2 addnS addn0.
have [/eqP -> | _ ] := boolP (a == b) => //=.
  by set u := (X in (_ - X)%N); case: u => [ | [ | ?]] //.
have [/eqP -> /= | bnc] := boolP (b == c).
  by rewrite setIUl setIid cardsU -setIA setIid cards1 addKn.
have [/eqP -> /= | cna] := boolP (c == a).
  by rewrite setIUl setIid cardsU setICA setIid addnK cards1.
suff /eqP -> : [set a; b] :&: [set c] == set0 by rewrite cards0.
by rewrite setI1 !inE negb_or cna eq_sym bnc.
Qed.

Lemma set3_cases  (T : finType) a b c (s : {set T}) w : #|s| = 3 ->
  a != b -> b != c -> c != a -> a \in s -> b \in s -> c \in s ->
  w \in s -> w = a \/ w = b \/ w = c.
Proof.
move=> s3 ab bc ca ain bin cin win.
move: (s3); have -> := cardsD1 a s; have -> := cardsD1 b (s :\ a).
have -> := cardsD1 c (s :\ a :\ b).
rewrite ain !inE bin cin ca eq_sym ab eq_sym bc /=.
case wa: (w == a); first by move/eqP: wa; left.
case wb: (w == b); first by move/eqP: wb; right; left.
case wc: (w == c); first by move/eqP: wc; right; right.
have abs : w \in s :\ a :\ b :\ c by rewrite !inE wa wb wc win.
by have -> := cardsD1 w (s :\ a :\ b :\ c); rewrite abs.
Qed.

Lemma set3_D1_dec (T : finType) (t : {set T}) a b c :
  #|t| = 3 -> t :\ c = [set a; b] -> t = [set a; b; c].
Proof.
move=> t3 tmc.
move: t3; rewrite (cardsD1 c) tmc cards2.
have [cint | ] := boolP (c \in t); have [anb | ] := boolP(a != b) => //.
by rewrite -tmc setUC setD1K.
Qed.

(* TODO : propose for addition in mathcomp *)
Lemma cardsI1 (T : finType) (t : {set T}) a : #| t :&: [set a]| = (a \in t).
Proof.
have [? | ?] := boolP (a \in t); last by apply/eqP; rewrite cards_eq0 setI1.
by have /setIidPr -> : [set a] \subset t; rewrite ?sub1set ?cards1.
Qed.

Lemma set3_D2_dec (T : finType) (t : {set T}) a b c :
  #|t| = 3 -> t :\: [set a; b] = [set c] -> t = [set a; b; c].
Proof.
move=> t3 tmab.
move: (cards1 c); rewrite -{1}tmab cardsD t3 setIUr cardsU !cardsI1.
have [a_t | ?] := boolP (a \in t); have [b_t | ?] := boolP (b \in t);
  case vc : #| _ | => [ | [ | n]] // _.
move: vc.
have := (a_t); rewrite -sub1set =>/setIidPr => ->.
have := (b_t); rewrite -sub1set =>/setIidPr => ->.
move/eqP; rewrite cardsI1 eqb0 inE => bna.
move: (set11 c); rewrite -tmab !inE negb_or=> /andP[] /andP[] cna cnb c_t.
have : [set a; b] \subset t by rewrite subUset !sub1set a_t b_t.
by move/setIidPr => vi; rewrite -[X in X :|: _ :\: _]vi setID.
Qed.

Lemma cycle_set3 (T : finType) (a b c : T) :
  [set a; b; c] = [set b; c; a].
Proof. by rewrite -(setUC [set a]) setUA. Qed.

Lemma eq_orbit (T : finType) (f g : T -> T) x : f =1 g -> orbit f x = orbit g x.
Proof.
move=> fg; rewrite /orbit; rewrite /fingraph.order.
have cfcg : (fconnect f) x =i (fconnect g) x by move=> y; apply: eq_fconnect.
  rewrite (eq_card cfcg); elim: #|fconnect g x| {-2 4}(x)  => [// | n IH y /=].
by rewrite /= fg IH.
Qed.

Lemma size_orbit_in_eq (T : finType) (S : predPredType T) (f : T -> T):
 {in S, forall x : T, f x \in S} ->
 {in S &, injective f} ->
 {in S, forall x i, size (orbit f (iter i f x)) = size (orbit f x)}.
Proof.
move=> stable inj x xin; elim=> [// | i]; rewrite !size_orbit iterS.
have := iter_in stable xin i; set y := iter _ _ _ => yin IH.
have := fconnect_sym_in stable inj yin (stable _ yin) => csym.
rewrite -IH; apply: eq_card => z; rewrite !inE.
by apply/idP/idP => c1; apply: connect_trans c1; rewrite -?csym connect1 /=.
Qed.

Lemma forall3 (T : finType) (a b c : T) PP :
  [forall w in [set a; b; c], PP w] = [&& PP a, PP b & PP c].
Proof.
apply/idP/idP.
  by move/forallP=> All; apply/andP; split;[ | apply/andP; split];
      apply: (fun x => implyP (All x)); rewrite !inE eqxx ?orbT.
move=>/andP[] ? /andP[] ? ?; apply/forallP=> w; apply/implyP.
by rewrite !inE=> /orP[/orP[] | ] /eqP ->.
Qed.

Lemma set_filterI (T : finType) (A B : {set T}) (C : pred T) :
  [set x in A :&: B  | C x] = [set x in A | C x] :&: [set x in B | C x].
Proof.
by apply/setP=> x; rewrite !inE andbA (andbAC _ (C x)) -(andbA _ (C x)) andbb.
Qed.

Lemma set_filterU (T : finType) (A B : {set T}) (C : pred T) :
  [set x in A :|: B  | C x] = [set x in A | C x] :|: [set x in B | C x].
Proof. by apply/setP=> x; rewrite !inE; rewrite -andb_orl. Qed.

Lemma set_filter0 (T : finType) (C : pred T) : [set x in set0 | C x] = set0.
Proof. by apply/setP=> x; rewrite !inE. Qed.

Lemma cards1_setU (T : finType) (A B : {set T}) (C : pred T) :
  A :&: B = set0 ->
  #|[set x in A :|: B | C x]| == 1%N =
  ((#|[set x in A | C x]| == 0%N) && (#|[set x in B | C x]| == 1%N)) ||
  ((#|[set x in A | C x]| == 1%N) && (#|[set x in B | C x]| == 0%N)).
Proof.
move=> d; rewrite set_filterU cardsU -set_filterI d set_filter0 cards0 subn0.
by case: #|[set x in A | _]|=> [ | [ | ?]]; rewrite ?eqxx ?add0n ?add1n ?orbF.
Qed.

Section abstract_over_types.

Variable P : finType.

Variable R : realDomainType.

Variable coords : P -> R * R.

Variable p0 : P.

Variable pick_set : {set P} -> option P.

Hypothesis pick_setP : forall (s : {set P}),
  reflect (exists2 x, x \in s & pick_set s = Some x) (s != set0).

Hypothesis pick_set_in : forall (s : {set P}) x,
  pick_set s = Some x -> x \in s.

Variable pick_triangle : {set {set P}} -> P -> option {set P}.

Notation inside_triangle := (inside_triangle coords).

Notation surface_mx := (surface_mx coords).

Notation "<< a ; b ; c >>" := (mkf3 a b c).

Notation general_position := (general_position coords).

Notation naive_triangulate := (naive_triangulate coords pick_set pick_triangle).

Notation three_points := (three_points coords p0).

Notation project_p := (project_p coords).

Notation separated := (separated coords).

Notation ccw := (ccw coords).

Notation Knuth_1 := (Knuth_1 coords).

Notation Knuth_4 := (Knuth_4 coords).

Hypothesis pick_triangle_in : forall tr p, 
  match pick_triangle tr p with
  | Some x => (x \in tr) && inside_triangle p x
  | _ => true end.

Hypothesis pick_triangleP : forall (tr : {set {set P}}) p,
 [exists x in tr, inside_triangle p x] = (pick_triangle tr p != None).

Lemma inside_notin t p : inside_triangle p t -> p \notin t.
Proof using. by move=> /andP [ /andP [_ it] _]; exact: it. Qed.

Definition in_hull (s' : {set P}) f p :=
  [forall q in s', (p != q) && (p != f q) ==> ccw q (f q) p].

Definition convex_hull_path (s : {set P})
   (s' : {set {set P}})
  (f : {ffun P -> P}):=
  [forall p in cover s', (f p \in cover s') &&
   [forall q in s, (q != p) && (q != f p) ==> ccw p (f p) q] &&
   ([set [set y; f y] | y in orbit f p] == s')].

Definition convex_hull (s : {set P}) s' :=
  (cover s' \subset s) && ((s == set0) == (s' == set0)) &&
  [exists f, convex_hull_path s s' f].

Definition all_triangles (tr : {set {set P}}) :=
   forall t, t \in tr -> #| t | = 3.

Definition covers_triangulation (s : {set P})
  (tr : {set {set P}}) :=
  [forall f, convex_hull_path s (boundary_edge tr) f ==>
     [forall p, general_position (p |: s) ==>
       (p \notin s) ==>
       in_hull (cover (boundary_edge tr)) f p ==>
       [exists t in tr, inside_triangle p t]]].

Definition convex_hull_triangulation (s : {set P})
  (tr : {set {set P}}) :=
  convex_hull s (boundary_edge tr) && covers_triangulation s tr.

Lemma three_points_card (s : {set _}) :
  #|s| = 3 -> #|[set three_points s i | i : 'I_3]| = 3.
Proof using.
move=> s3.
rewrite /three_points; case: pickP => [x1 /= px1 | abs ]; last first.
  have : (0 < #|s|)%N by rewrite s3.
  by move/card_gt0P => [i pi]; move: (abs i) => /=; rewrite pi.
case: pickP => [x2 /= px2 | abs]; last first.
  have : (0 < #|s :\ x1|)%N.
    by have := cardsD1 x1 s => /eqP; rewrite s3 px1 addSn add0n eqSS => /eqP <-.
  by move/card_gt0P => [i pi]; move: (abs i) => /=; rewrite pi.
case: pickP => [x3 /= px3 | abs]; last first.
  have : (0 < #|s :\ x1 :\ x2|)%N.
    have := cardsD1 x1 s => /eqP; rewrite s3 px1 addSn add0n eqSS => /eqP s2.
    have := cardsD1 x2 (s :\ x1) => /eqP.
    by rewrite -s2 px2 addSn add0n eqSS => /eqP <-.
  by move/card_gt0P => [i pi]; move: (abs i) => /=; rewrite pi.
set f := (X in #|[set X i | i : 'I_3] |).
have -> : [set f i | i :'I_3] = [set x1; x2; x3].
  apply/eqP; rewrite eqEsubset; apply/andP; split; apply/subsetP.
    by move=> x /imsetP[]; elim/elimI3=> _ ->; rewrite /f;
      case: (ccw _ _ _); rewrite ffunE !inE eqxx ?orbT.
  move=> x; rewrite !inE => /orP [/orP [/eqP -> |/eqP ->] | /eqP ->];
  apply/imsetP; rewrite /f.
      by exists 0 => //; case: (ccw _ _ _); rewrite ffunE.
    by case: (ccw _ _ _);[exists 1 | exists (-1)]; rewrite //= ffunE.
  by case: (ccw _ _ _);[exists (-1) | exists 1]; rewrite //= ffunE.
rewrite !cardsU !cards1.
rewrite !disjoint_setI0 ?cards0 // -setI_eq0 eqEsubset; apply/andP.
  split; apply/subsetP => i /=; last by rewrite inE.
  by rewrite !inE andbC =>/andP [] /eqP -> => /orP [] abs;
      move:px3; rewrite !inE abs ?andbF.
split; apply/subsetP => i /=; last by rewrite inE.
by rewrite !inE andbC => /andP [] /eqP -> => abs; move: px2; rewrite !inE abs.
Qed.

Lemma three_points_in (s : {set _}) i :
  #|s| = 3 -> three_points s i \in s.
Proof using.
move => s3.
rewrite /three_points; case: pickP => [x1 /= px1 | abs ]; last first.
  have : (0 < #|s|)%N by rewrite s3.
  by move/card_gt0P => [j pj]; move: (abs j) => /=; rewrite pj.
case: pickP => [x2 /= px2 | abs]; last first.
  have : (0 < #|s :\ x1|)%N.
    by have := cardsD1 x1 s => /eqP; rewrite s3 px1 addSn add0n eqSS => /eqP <-.
  by move/card_gt0P => [j pj]; move: (abs j) => /=; rewrite pj.
case: pickP => [x3 /= px3 | abs]; last first.
  have : (0 < #|s :\ x1 :\ x2|)%N.
    have := cardsD1 x1 s => /eqP; rewrite s3 px1 addSn add0n eqSS => /eqP s2.
    have := cardsD1 x2 (s :\ x1) => /eqP.
    by rewrite -s2 px2 addSn add0n eqSS => /eqP <-.
  by move/card_gt0P => [j pj]; move: (abs j) => /=; rewrite pj.
case: (ccw _ _ _); elim/elimI3: i; rewrite ffunE //.
      by move: px2; rewrite inE => /andP [_ ?].
    by move: px3; rewrite !inE => /andP [_ /andP [_ ?]].
  by move: px3; rewrite !inE => /andP [_ /andP [_ ?]].
by move: px2; rewrite inE => /andP [_ ?].
Qed.

Lemma three_points_eq (s : {set _}) :
  #|s| = 3 -> s = [set three_points s i | i : 'I_3].
Proof using.
move=> s3; have tpc := three_points_card s3.
apply/eqP; rewrite eq_sym eqEcard s3 tpc leqnn andbT.
by apply/subsetP => p /imsetP [] i _ => ->; apply: three_points_in.
Qed.

Lemma three_points_eq' (s : {set _}) :
  #|s| = 3 -> s = [set three_points s 0; three_points s 1; three_points s (-1)].
Proof using.
move=> s3; rewrite {1}(three_points_eq s3); apply/setP=> x; apply/imsetP/idP.
  by move=> [j _ ->]; elim/elimI3: j; rewrite !inE eqxx ?orbT.
by rewrite !inE=> /orP [/orP [] | ] /eqP ->; (eexists;[ | reflexivity]).
Qed.

Lemma three_points_eqi (s : {set _}) i :
  #|s| = 3 -> s = [set three_points s i; three_points s (i + 1);
  three_points s (i -1)].
Proof using.
move=> s3; rewrite {1}(three_points_eq s3); apply/setP=> x; apply/imsetP/idP.
  move=> [j _ ->]. rewrite -(subrK i j) (addrC (j - i)). 
  by elim/elimI3: (j - i); rewrite !inE ?addr0 ?eqxx ?orbT.
by rewrite !inE=> /orP [/orP [] | ] /eqP ->; (eexists;[ | reflexivity]).
Qed.

Lemma three_points_inj (s : {set _}) : #|s| = 3 -> injective (three_points s).
Proof using.
move => s3; have := three_points_eq s3 => s3eq.
have : #|[set (three_points s) i | i : 'I_3]| == #|'I_3|.
  by rewrite card_ord -(three_points_eq s3) s3.
by move/imset_injP => it x y; apply: it.
Qed.

Lemma three_points_oriented (s : {set _}) j :
  #|s| = 3 -> general_position s -> 
  ccw (three_points s j) (three_points s (j + 1)) (three_points s (j - 1)).
Proof using.
set tps := three_points s.
move=> s3 /forallP/(_ (tps 0))/forallP/(_ (tps 1))/forallP/(_ (tps (-1))) => C.
suff : ccw (tps 0) (tps 1) (tps (-1)).
  by elim/elimI3: j ; rewrite ?subrr ?sub0r ?add0r ?m1m1 ?p1p1
   ?(Knuth_1 (tps 0)) -?(Knuth_1 _ (tps 0)).
move: C; rewrite -three_points_eq' s3 // subxx eqxx implyTb /tps /three_points.
case: pickP => [p1 /= p1in | ]; last first.
  move=> A; suff : False by elim.
  have/card_gt0P[x xs] : (0 < #|s|)%N by rewrite s3.
  by move: (A x); rewrite /= xs.
case: pickP => [p2 /= p2in | ]; last first.
  move=> A; suff : False by elim.
  have/card_gt0P[y ys] : (0 < #|s :\ p1|)%N.
    by rewrite cardsD s3 (setIidPr (_ : [set p1] \subset s)) ?subE // cards1.
  by move: (A y); rewrite /= ys.
case: pickP => [p3 /= p3in |]; last first.
  move=> A; suff : False by elim.
  have/card_gt0P[z zs] : (0 < #|s :\ p1 :\ p2|)%N.
    rewrite !cardsD (setIidPr (_ : [set p1] \subset s)) ?subE //.
    rewrite (setIidPr (_ : [set p2] \subset s :\ p1)) ?subE //; last first.
      by move: p2in; rewrite !inE.
    by rewrite s3 !cards1.
  by move: (A z); rewrite /= zs.
case: ifP; rewrite !ffunE !eqxx /= // => /negP/negP.
by rewrite Knuth_1 => /negP A /orP [// | A']; case A'.
Qed.

Lemma all_edges_3 (s x : {set _}) :
  #|s| = 3 -> #|x| = 2 -> x \subset s ->
  exists i, x = [set three_points s i; three_points s (i + 1)].
Proof using.
move=> s3 x2 xsub.
have /inj_eq tpi : injective (three_points s) by apply: three_points_inj.
have [y ynx] : exists y, s :\: x = [set y].
  suff /eqP/cards1P [y py] : #|s :\: x| = 1%N by exists y.
  by move/setIidPr: xsub (cardsID x s) => ->; rewrite x2 s3 !addSn add0n =>[[]].
have : y \in s by have := set11 y; rewrite -ynx !inE => /andP [].
rewrite {1}(three_points_eq s3) =>/imsetP [i _ yi].
have [ip1 ip2] : three_points s (i + 1) \in x /\ three_points s (i - 1) \in x.
  have := setDDr s s x; rewrite setDv set0U ynx.
  move/setIidPr: (xsub) => -> <-; rewrite !inE yi !tpi !(three_points_in _ s3).
  by split; elim/elimI3: (i).
have i1dif2 : three_points s (i + 1) != three_points s (i + 1 + 1).
  by rewrite tpi; elim/elimI3: (i).
exists (i + 1); apply/eqP; rewrite eq_sym eqEcard cards2 i1dif2 x2 leqnn andbT.
by rewrite subUset !sub1set ip1 -(subrK 1 i) -add3 ip2.
Qed.

Lemma boundary_edge_base (s : {set _}) (s3 : #|s| = 3) :
  (boundary_edge [set s])
    = [set [set three_points s i; three_points s (i + 1)] | i : 'I_3].
Proof using.
have tpi : injective (three_points s) by apply three_points_inj.
apply/eqP; rewrite eqEsubset; apply/andP; split; apply/subsetP.
  move=> x; rewrite inE /= => /andP [/eqP x2 /cards1P [s' s'x]].
  have /(_ s' (set11 _)) : forall t, t \in [set s'] -> t = s /\ x \subset t.
    by move => t; rewrite -s'x inE in_set1 => /andP [] /eqP.
  case=> -> xsub.
  by apply/imsetP; case (all_edges_3 s3 x2 xsub) => j pj; exists j.
move=> e /imsetP [j _ pj]; rewrite inE.
have e2 : #|e| == 2 by rewrite pj cards2 (inj_eq tpi); elim/elimI3: (j).
apply/andP; split; first by [].
apply/cards1P; exists s; rewrite -setP => t; rewrite !inE.
have [/eqP ts | //]:= boolP (t == s); rewrite ts /=; apply/subsetP => q.
by rewrite pj !inE => /orP [ | ] /eqP ->; apply three_points_in.
Qed.

Lemma cover_boundary_base (s : {set P}) :
  #|s| = 3 -> cover (boundary_edge [set s]) = s.
Proof using coords p0.
move=> s3; set tps := three_points s.
apply/setP=> p; apply/bigcupP/idP.
  move=>[] e; rewrite inE => /andP [] e2 /cards1P [] t tp; move: (set11 t).
  by rewrite -tp setIdE !inE => /andP [] /eqP -> => /subsetP; apply.
rewrite [X in p \in (_ X)](three_points_eq s3)=>/imsetP [i _ pi].
set u := [set tps i; tps (i + 1)]; exists u; last by rewrite 2!inE pi eqxx.
by move/setP: (boundary_edge_base s3) => /(_ u) ->; apply/imsetP; exists i.
Qed.

Lemma convex_hull_base_not_empty (s : {set P}) :
  #|s| = 3 -> cover (boundary_edge [set s]) != set0.
Proof using coords p0.
by move => s3; rewrite cover_boundary_base // -card_gt0 s3.
Qed.

Lemma use_convex_hull_path (s : {set _}) s' f x y:
  convex_hull_path s s' f ->
  x \in cover s' -> y \in s -> y != x -> y != f x -> ccw x (f x) y.
Proof using.
move=> ch xs' ys ynx ynfx.
move:ch =>/forallP/(_ x)/implyP/(_ xs')/andP[] /andP[] _/forallP/(_ y)/implyP.
by move=>/(_ ys)/implyP; rewrite ynx ynfx=>/(_ isT) it _.
Qed.

(* TODO : find instances where this proof would have been useful. *)
Lemma convex_hull_path_iter_in (s : {set _}) s' f a i :
  (3 <= #|s|)%N -> cover s' \subset s -> convex_hull_path s s' f ->
  a \in cover s' ->
  iter i f a \in cover s'.
Proof using.
move=> s3 ss' ch ain; elim: i => [ | i IH] //=.
by move/forallP: ch=>/(_ (iter i f a)); rewrite IH implyTb => /andP [] /andP [].
Qed.

Lemma oriented_no_dup a b : ~~ccw a a b.
Proof using.
by apply/negP=> A; move: (Knuth_2 A)=> /negP; apply.
Qed.


Lemma oriented_diff c a b : ccw a b c -> a != b.
Proof using.
move=> abc; apply/negP=> /eqP A; move: abc; rewrite A.
apply/negP/oriented_no_dup.
Qed.

Lemma ch_path_no_fixpoint (s : {set _}) s' f p :
  (3 <= #|s|)%N -> cover s' \subset s -> convex_hull_path s s' f ->
  p \in cover s' -> f p != p /\ f (f p) != p.
Proof using.
move=> s3 ss ch ps.
have fps : f p \in cover s' by apply: (convex_hull_path_iter_in 1 s3 ss ch).
have/card_gt0P [q qs]: (0 < #|s :\ p :\ f p|)%N.
  move: (cardsD1 p s) (cardsD1 (f p) (s :\ p)) s3.
  rewrite !inE (subsetP ss) //= => -> ->; rewrite ltnS.
  rewrite (subsetP ss) ?andbT /=; last first.
    by move/forallP: ch => /(_ p); rewrite ps implyTb => /andP [] /andP [].
  by case : (f p != p); [rewrite ltnS | apply leq_trans].
have /andP [qnp qnfp] : (q != p) && (q != f p).
  by move: qs; rewrite !inE => /andP [-> /andP [->]].
have qs' : q \in s.
  by move: qs; rewrite !inE => /andP [_ /andP [_ ->]].
have detp := use_convex_hull_path ch ps qs' qnp qnfp.
have fpp : f p != p by rewrite eq_sym; apply: (oriented_diff detp).
split; first by [].
have [a|qff] := (boolP (q == f (f p))); first by move: qnp; rewrite (eqP a).
have defp := use_convex_hull_path ch fps qs' qnfp qff.
by apply/negP=>/eqP ffpp; move: (Knuth_2 defp); rewrite ffpp detp.
Qed.

Lemma convex_hull_path_inj (s : {set _}) s' f : (3 <= #|s|)%N ->
   cover s' \subset s -> convex_hull_path s s' f ->
   {in cover s' &, injective f}.
Proof using.
move=> s3 ss' ch x y xs' ys' ff; have [/eqP //|ynx] := boolP (y == x).
have ys : y \in s by rewrite (subsetP ss').
have xs : x \in s by rewrite (subsetP ss').
have := use_convex_hull_path ch xs' ys ynx; rewrite ff eq_sym.
have [-> _] /= := ch_path_no_fixpoint s3 ss' ch ys' => /(_ isT).
move/(Knuth_2 (P := P))=> d1.
have := use_convex_hull_path ch ys' xs; rewrite eq_sym ynx eq_sym -ff.
have [-> _] /= := ch_path_no_fixpoint s3 ss' ch xs' => /(_ isT isT) A.
by case/negP: d1; rewrite -ff Knuth_1.
Qed.

Lemma general_position_split a b c :
  general_position [set a; b; c] ->
  a != b -> b != c -> c != a -> ccw a b c || ccw b a c.
Proof using.
move=> /'forall_'forall_'forall_implyP gp ab bc ca; apply: gp.
by rewrite subxx andbT cards3 ab bc ca.
Qed.

Lemma general_position_sub (s s' : {set P}) :
  general_position s' -> s \subset s' -> general_position s.
Proof.
move=> /'forall_'forall_'forall_implyP gp ss'.
apply/'forall_'forall_'forall_implyP => a b c /andP [c3 ins]; apply: gp.
by rewrite c3 (subset_trans ins ss').
Qed.

Lemma covers_base (s : {set _}) : #|s| = 3 -> covers_triangulation s [set s].
Proof using coords p0.
move=> s3; rewrite /covers_triangulation cover_boundary_base //.
apply/forallP=> f; apply/implyP=> ch.
apply/forallP=> p; apply/implyP=> gp'; apply/implyP=> pnin.
apply/implyP=> /forallP inh.
apply/existsP; exists s; rewrite set11 andTb /inside_triangle pnin andbT.
rewrite s3 andbT.
apply/forallP => q; apply/implyP=> qin; rewrite /separated qin /=.
case:pickP=> r // rsnq; case: pickP=> r' // r'snr.
have sge3 : (3 <= #|s|)%N by rewrite leq_eqVlt s3.
have fin w : w \in s -> f w \in s.
  move => win; have := convex_hull_path_iter_in 1 sge3 _ ch => /(_ w).
  by rewrite cover_boundary_base // subxx win => /(_ isT isT).
have rin : r \in s by move: rsnq; rewrite /= inE => /andP [].
have r'in : r' \in s by move:r'snr; rewrite !inE => /andP [] _ /andP [].
have qnr : q != r by move: rsnq; rewrite /= !inE eq_sym => /andP [].
have qnr' : q != r'.
  by move: r'snr; rewrite /= !inE (eq_sym q) => /andP [] _ /andP [].
have com : forall w, q != w -> q != f w -> w \in s ->
               ~~((ccw w (f w) q && ccw (f w) w p) ||
                  (ccw (f w) w q && ccw w (f w) p)).
  move => w qnw qnf win.
  have pnw : p != w by apply: (contraNN _ pnin) => /eqP ->.
  have pnf : p != f w by apply: (contraNN _ pnin) => /eqP ->; apply: fin.
  move: (inh w); rewrite win pnw pnf !implyTb => wfp.
  rewrite (negbTE (Knuth_2 wfp)) andbF wfp.
  have := use_convex_hull_path ch; rewrite cover_boundary_base // => /(_ w q).
  rewrite qnf qnw qin win => /(_ isT isT isT isT) => wfq.
  by rewrite (negbTE (Knuth_2 wfq)).
have := ch_path_no_fixpoint _ _ ch; rewrite leq_eqVlt s3 eqxx orTb.
  rewrite (cover_boundary_base s3) => /(_ _ isT (subxx _) r'in) [frnr' _].
have fr'in : f r' \in s.
  move/'forall_implyP: (ch) => /(_ r'); rewrite cover_boundary_base //.
  by move=>/(_ r'in) /andP [] /andP [].
have pnr' : p != r'.
  by apply/negP=> /eqP pr'; case/negP: pnin; rewrite pr'.
have pnf : p != f r'.
  by apply/negP=> /eqP pf; case/negP: pnin; rewrite pf fin.
suff dir : f r = r' \/ f r' = r.
  by move: qnr qnr'; case: dir => <- qnr qnr';[ | rewrite orbC]; apply: com.
have s_cases : forall w, w \in s -> w = r \/ w = r' \/ w = q.
  move=> w win; move: r'snr; rewrite !inE eq_sym => /andP [rr' /andP [r'q _]].
  by have:= set3_cases s3 qnr rr' r'q qin rin r'in win; clear; intuition.
have s3le : (3 <= #|s|)%N by rewrite leq_eqVlt s3.
have [frx ffrx] : f r != r /\ f (f r) != r.
   apply: (ch_path_no_fixpoint s3le _ ch).
     by rewrite cover_boundary_base // subxx.
   by rewrite cover_boundary_base.
case: (s_cases _ (fin _ rin)) => [a1 | [a2 | a3]];[ | by left | ].
  by move: frx; rewrite a1 eqxx.
have [frx' ffrx'] : f r' != r' /\ f (f r') != r'.
  apply: (ch_path_no_fixpoint s3le _ ch).
    by rewrite cover_boundary_base // subxx.
  by rewrite cover_boundary_base.
case: (s_cases _ (fin _ r'in)) => [b1 | [b2 | b3]]; first by right.
  by move: frx'; rewrite b2 eqxx.
have [] : f q != q /\ f (f q) != q.
  apply: (ch_path_no_fixpoint s3le _ ch).
    by rewrite cover_boundary_base // subxx.
  by rewrite cover_boundary_base.
by case: (s_cases _ (fin _ qin)) => [c | [c | c]]; rewrite c ?(a3, b3) eqxx.
Qed.

Lemma edges_base_not_empty (s : {set P}) : #|s| = 3 ->
  (boundary_edge [set s]) != set0.
Proof using coords p0.
move=> s3; apply/negP=> A; move/negP: (convex_hull_base_not_empty s3); apply.
apply/eqP/big_pred0 => x /=; apply/negP => B.
by have : x \in set0; [rewrite -(eqP A) | by rewrite !inE].
Qed.

Lemma triangle_path a f x:
  ccw a (f a) (f (f a)) ->
  f (f (f a)) = a ->
 x \in [set a; f a; f (f a)] ->
 [set x; f x; f (f x)] = [set a; f a; f (f a)] /\
 #|[set x; f x; f (f x)]| = 3 /\
 f (f (f x)) = x /\ ccw x (f x) (f (f x)) /\ orbit f x = [:: x; f x; f (f x)].
Proof using.
move=> o f3a xin.
have collect : forall P1 P2 : Prop, (P1 /\ (P1 -> P2)) -> P1 /\ P2.
  by intros P1 ? [h1 h2]; split; [exact h1 | exact (h2 h1)].
apply: (collect); split;[ | move => sq].
  by move: xin; rewrite !inE=> /orP [/orP[] | ] /eqP ->; rewrite ?f3a //
    cycle_set3 // cycle_set3.
apply: (collect); split;[ | move => s3].
  rewrite sq; apply/eqP; rewrite cards3 (oriented_diff o).
  by do 2 (move: o; rewrite Knuth_1 => o; rewrite (oriented_diff o)).
apply: (collect); split;[ | move => f3x].
  by move: xin; rewrite !inE=> /orP[/orP[] |] /eqP ->; rewrite ?f3a //.
apply: (collect); split;[ | move => orx].
  by move: xin; rewrite !inE=> /orP[/orP[]|] /eqP ->; rewrite // f3a
  Knuth_1 // Knuth_1.
have fcx : fcycle f [:: x; f x; f (f x)] by rewrite /= f3x !eqxx.
have unx : uniq [:: x; f x; f (f x)].
  by move/eqP: (s3); rewrite cards3 !cons_uniq !inE (eq_sym x (f (f x)))
   !negb_or=> /andP[] /andP[] -> -> ->.
by rewrite /orbit (order_cycle fcx unx) ?inE ?eqxx.
Qed.

Lemma convex_hull_triangulation_base (s : {set _}) :
  general_position s -> #|s| = 3 -> convex_hull_triangulation s [set s].
Proof using coords p0.
move=> gp s3; set tps := three_points s.
have itp : injective tps by apply: three_points_inj.
rewrite /convex_hull_triangulation.
rewrite /convex_hull cover_boundary_base // subxx.
rewrite (negbTE (edges_base_not_empty _)) // -cards_eq0 s3 /=.
rewrite covers_base // andbT.
set a := tps 0; have tpj := three_points_inj s3.
have ? := three_points_eq' s3.
have [f [f_st [f_or [seta f3a]]]] : exists f, {in s, forall x, f x \in s} /\
            ccw a (f a) (f (f a)) /\ [set a; f a; f (f a)] = s /\
            f (f (f a)) = a.
  have := three_points_oriented 0 s3 gp; rewrite !add0r => ori.
  set f := [fun x => tps 0 with tps 0 |-> tps 1, tps 1 |-> tps (-1)].
  have f1 : f a = tps 1 by rewrite /f /= eqxx.
  have fana : f a != a by rewrite f1 (inj_eq tpj).
  have f2 : f (f a) = tps (-1) by rewrite /f /= eqxx !(inj_eq tpj).
  have ffana : f (f a) != a by rewrite f2 (inj_eq tpj).
  have f3 : f (f (f a)) = a by rewrite /f /= !(eqxx, (inj_eq tpj)) /=.
  have stb : {in s, forall x, f x \in s}.
    move=> y; rewrite {1}(three_points_eq s3)=> /imsetP[j _ -> ].
    by elim/elimI3: j; rewrite -?f1 -?f2 ?f3 ?f2 ?f1 /a three_points_in.
  have ffanfa : f a != f (f a) by rewrite f2 f1 (inj_eq tpj).
  by exists f; split;[ | split; [ | split]]; rewrite // f2 f1.
set ff := [ffun x => f x].
apply/existsP; exists ff; apply/forallP=> x; apply/implyP.
move: (s3); rewrite -seta=> seta3.
rewrite cover_boundary_base // => /(triangle_path f_or f3a)
    [setx [setx3 [f3x [orx orbx]]]].
move/eqP : (setx3); rewrite cards3 !ffunE=>/andP[] /andP[] ? ? ?.
rewrite -setx !inE eqxx orbT andTb setx seta; apply/andP; split; last first. 
  apply/eqP/setP=> e; rewrite -(@eq_orbit _ f); last by move=> z; rewrite ffunE.
  apply/imsetP/idP.
    move=> [z]; rewrite orbx=> zin ->.
    have zins : z \in [set x; f x; f (f x)] by move: zin; rewrite !inE orbA.
    have [setz [setz3 [f3z [orz _]]]] := triangle_path orx f3x zins.
    rewrite !inE cards2; move/eqP: (setz3).
    rewrite cards3 ffunE => /andP[]/andP[] -> _ _ /=.
    rewrite -(cards1 s); apply/eqP; congr (#|pred_of_set _|).
    apply/setP=> t; rewrite inE; apply/idP/idP; first by move/andP=>[] ->.
    by rewrite !inE => /eqP ->; rewrite eqxx andTb -seta -setx -setz !subE.
  rewrite !inE=>/andP [] ec2 /cards1P [t tq].
  move: (set11 t); rewrite -tq !inE => /andP[] /eqP ts; rewrite ts=> esubs.
  have /cards1P [y yq] : #|s :\: e| == 1%N.
    by rewrite cardsD (setIidPr esubs) (eqP ec2) s3.
  exists (f y).
    rewrite orbx inE; move: (set11 y); rewrite -yq inE=> /andP[] _.
    rewrite -seta -setx.
    by rewrite !inE=> /orP[/orP[]|] /eqP->; rewrite ?f3x eqxx ?orbT.
  have <- : s :\ y = e.
    by rewrite -yq setDDr setDv set0U (setIidPr esubs).
  rewrite -seta -setx.
  have yin : y \in [set x; f x; f (f x)].
    by move: (set11 y); rewrite -yq setx seta !inE=>/andP [].
  have [<- [/eqP sety3 [f3y A]]] := triangle_path orx f3x yin.
  move: sety3; rewrite cards3 => /andP[/andP[]] ? ? ?.
  apply/setP/subset_eqP/andP; split; apply/subsetP => z; rewrite !inE ?andB.
    by rewrite !andb_orr andNb orFb -andb_orr andbC ffunE=> /andP[] ->.
  by move=>/orP[] /eqP ->; rewrite ?ffunE eqxx ?orbT ?(eq_sym (f y) y) andbT.
apply/forallP=> q; rewrite -seta -setx; apply/implyP.
by rewrite !inE =>/orP[/orP[]|] => /eqP ->;
 rewrite ?eqxx ?andbF ?implyFb ?orx ?implybT.
Qed.

Lemma inside_triangle_edge1 a b c d e f :
  general_position ([set a; b; c; d; e; f]) ->
  ccw a b c -> ccw a b d -> ccw a b e -> ccw c d f -> ccw c f e -> ccw f d e ->
  ccw a d c -> ccw a e c -> ccw a b f.
Proof using.
set big := (_ :|: _); move=> gp abc abd abe cdf cfe fde adc aec.
have [cna [ena enc]] : c != a /\ e != a /\ e != c.
  by move: (abc) (abe) (cfe); do 3 (rewrite Knuth_1 => /oriented_diff ->).
have [dna dne] : d != a /\ d != e.
  move: abd fde.
  by rewrite (Knuth_1 a) -(Knuth_1 d); do 2 move =>/oriented_diff ->.
have [anc [cne and]] : a != c /\ c != e /\ a != d.
  by repeat split; rewrite eq_sym.
have cde : ccw c d e.
  by apply (@Knuth_axiom5.Knuth_4 _ _ _ f); do 3 rewrite // Knuth_1.
have [ecd cae] : ccw e c d /\ ccw c a e by split; do 3 rewrite // Knuth_1.
have /general_position_split/(_ and dne ena) E: general_position [set a; d; e].
  by apply/(general_position_sub gp); rewrite /big !subE.
have caf : ccw c a f by apply: (Knuth_axiom5c' _ ecd); do 3 rewrite // -Knuth_1.
move/orP: E => [ade | dae]; last first.
  apply: (Knuth_axiom5c' _ cae); do 3 rewrite // Knuth_1.
  by rewrite -Knuth_1; apply: (Knuth_axiom5c _ ecd); rewrite -Knuth_1.
have [cad dec] : ccw c a d /\ ccw d e c by split; do 3 rewrite // Knuth_1.
apply: (Knuth_axiom5c' _ cad); do 3 rewrite // Knuth_1.
by rewrite -Knuth_1; apply: (Knuth_axiom5c _ dec); rewrite Knuth_1 // Knuth_1.
Qed.

Lemma inside_triangle_left_of_edge a b c d e f :
  general_position ([set a; b; c; d; e; f]) ->
  ccw a b c -> ccw a b d -> ccw a b e -> ccw c d f -> ccw c f e -> ccw f d e ->
  ccw a b f.
Proof using.
set s := (_ :|: _); move=> gp abc abd abe cdf cfe fde.
have sgp sb := general_position_sub gp sb.
have /andP[ /andP[ad dc] ca] : (a != d) && (d !=c) && (c != a).
  move/oriented_diff: cdf abd abc; rewrite eq_sym !(eq_sym a) => ->.
  by rewrite !(Knuth_1 a) => /oriented_diff -> /oriented_diff ->.
have /andP[ /andP[ac ce] ea] : (a != c) && (c != e) && (e != a).
  move: abe cfe; rewrite !(Knuth_1 _ _ e) -(eq_sym e) (eq_sym a c) ca.
  by move=> /oriented_diff -> /oriented_diff ->.
have[sadc [sace saed]] :  [set a; d; c] \subset s /\
                 [set a; c; e] \subset s /\ [set a; e; d] \subset s.
  by rewrite !subE.
move/sgp/general_position_split/(_ ad dc ca): sadc => /orP[adc | dac].
  move/sgp/general_position_split/(_ ac ce ea): sace => /orP[ace | cae].
    rewrite /s 2!(setUAC _ _ [set e]) in gp.
    apply: (inside_triangle_edge1 gp); do 3 (rewrite // Knuth_1).
    by apply: (Knuth_axiom5c abd abc abe).
  by apply: (inside_triangle_edge1 gp); rewrite // Knuth_1.
have ed :e != d.
  by move : fde; rewrite -Knuth_1 eq_sym => /oriented_diff.
have [ae da] : a != e /\ d != a by split; do 2 rewrite // eq_sym.
move/sgp/general_position_split/(_ ae ed da): saed => /orP[aed | cae].
  rewrite /s 2!(setUAC _ [set c]) in gp.
  by apply: (inside_triangle_edge1 gp); do 3 (rewrite // Knuth_1).
rewrite /s 2!(setUAC _ _ [set e]) in gp.
apply: (inside_triangle_edge1 gp); do 3 rewrite // Knuth_1.
apply: (Knuth_axiom5c abc abd); do 2 rewrite // Knuth_1.
Qed.

(* Hoping that I can add some tools to make automatic proofs of difference
  between object. *)
Lemma diff_from_uniq  (T : eqType) (s : seq T) (a b : T) :
  (b \in s) && uniq (a::s) -> a != b.
Proof using.
by move=>/andP[] bs uas; apply/negP=> /eqP A; move: uas; rewrite A /= bs.
Qed.

Lemma use_imp (A B C: Prop) :  (A -> B) -> A -> (B -> C) -> C.
Proof using. by tauto. Qed.

Lemma separated_immediate p a b c :
  ccw a b c -> ccw b a p = separated [set a; b; c] c p.
Proof using.
move=> abc; rewrite /separated.
have [anc [bnc anb]] : a != c /\ b != c /\ a != b.
  by repeat split; apply/negP=>/eqP A; move: (abc); rewrite A;
  repeat (rewrite Knuth_1; try (move=>/oriented_diff; rewrite eqxx)).
rewrite !inE eqxx orbT andTb; case: pickP; last first.
  by move=> /(_ a) /=; rewrite !inE eqxx anc.
move=> e1 e1P; case: pickP; last first.
  have [/eqP -> | e1na ] := boolP (e1 == a).
    by move=>/(_ b) /=; rewrite !inE eqxx bnc eq_sym anb orbT.
  by move=>/(_ a) /=; rewrite !inE eqxx eq_sym e1na anc.
move=> e2; move: e1P; have [/eqP -> _ /= | /= e1na] := boolP(e1 == a).
  rewrite !inE => /andP [] /negbTE -> /andP [] /negbTE ->; rewrite orbF.
  by move/eqP ->; rewrite abc (negbTE (Knuth_2 abc)) andFb orbF.
rewrite !inE (negbTE e1na) => /andP [] /negbTE ->; rewrite orbF=> /eqP ->.
move/andP=> [] /negbTE -> /andP [] /negbTE ->; rewrite eq_sym !orbF=> /eqP <-.
by rewrite abc (negbTE (Knuth_2 abc)).
Qed.

Lemma general_position_immediate  a b c :
  general_position [set a; b; c] ->
  a != b -> b != c -> c != a -> ccw a b c || ccw b a c.
Proof using.
move=> /'forall_'forall_'forall_implyP gp anb bnc cna; apply: gp.
by rewrite subxx cards3 anb bnc cna.
Qed.

Lemma inside_triangle_immediate a b c p :
  general_position [set a; b; c; p] ->
  ccw a b c ->
  inside_triangle p [set a; b; c] =  ccw a b p && ccw b c p && ccw c a p.
Proof using.
move=> gp maint; rewrite /inside_triangle.
rewrite forall3.
have [ | pnotin ] := boolP (p \in [set a; b; c]).
  by rewrite andbF !inE !(Knuth_1 _ _ p) => /orP [/orP [] | ]/eqP ->;
  rewrite (negbTE (oriented_no_dup _ _)) ?andbF.
rewrite andbT -separated_immediate // cycle_set3 -separated_immediate;
  last by rewrite Knuth_1.
rewrite cycle_set3 -separated_immediate; last by rewrite -Knuth_1.
move/oriented_diff: (maint) => ab.
move: (maint); rewrite Knuth_1 => /oriented_diff ca.
move: (maint); rewrite -Knuth_1 => /oriented_diff bc.
have pna : p != a.
  by move: pnotin; rewrite !inE !negb_or=>/andP[]/andP[].
have bnp : b != p.
  by move: pnotin; rewrite eq_sym !inE !negb_or=>/andP[]/andP[].
have/(general_position_sub gp): [set a; b; p] \subset [set a; b; c; p].
  by rewrite !subE.
move/general_position_immediate => /(_ ab bnp pna) /orP [abp | bap]; last first.
  by rewrite (negbTE (Knuth_2 bap)) bap !andbF.
rewrite abp (Knuth_2 abp).
move : (bnp); rewrite eq_sym => pnb.
have cnp : c != p.
  by move: pnotin; rewrite eq_sym !inE !negb_or=>/andP[]/andP[].
have/(general_position_sub gp): [set b; c; p] \subset [set a; b; c; p].
  by rewrite !subE.
move/general_position_immediate => /(_ bc cnp pnb) /orP [bcp | cbp]; last first.
  by rewrite (negbTE (Knuth_2 cbp)) cbp !andbF.
rewrite bcp (Knuth_2 bcp).
move: (pna); rewrite eq_sym => anp.
move: (cnp); rewrite eq_sym => pnc.
have/(general_position_sub gp): [set c; a; p] \subset [set a; b; c; p].
  by rewrite !subE.
move/general_position_immediate => /(_ ca anp pnc) /orP [cap | acp]; last first.
  by rewrite (negbTE (Knuth_2 acp)) acp !andbF.
by rewrite cards3 ca ab bc andbT cap (Knuth_2 cap).
Qed.

(* TODO : this one should be automated. *)
Lemma inside_sub_triangle a b c d e :
  general_position [set a; b; c; d; e] ->
  uniq [:: a; b; c; d; e] ->
  inside_triangle d [set a; b; c] ->
  inside_triangle e [set a; b; d] ->
  inside_triangle e [set a; b; c].
Proof.
wlog : a b c / ccw a b c.
  set s := (a :: _); move=> main gs uniqs.
  have : [set a; b; c] \subset [set a; b; c; d; e] by rewrite /s !subE.
  move/(general_position_sub gs)/general_position_split=>it.
  apply: (use_imp it) => {it}.
    by move: uniqs; rewrite /s /= !inE !negb_or; case: (a != b).
  move=> it; apply: (use_imp it) => {it}.
     by move: uniqs; rewrite /s /= !inE !negb_or; case: (b != c);
        rewrite // ?andbF.
  move=> it; apply: (use_imp it) => {it}.
     by move: uniqs; rewrite eq_sym /s /= !inE !negb_or; case: (a != c);
        rewrite // ?andbF.
  move=> /orP [abc | bac]; first by apply: main.
  have us' : uniq [:: b; a; c; d; e].
    rewrite cons_uniq inE cons_uniq.
    move: uniqs; rewrite /s cons_uniq inE cons_uniq.
    rewrite eq_sym !(negb_or (b == a)) -2!andbA; congr (_ && _).
    by rewrite andbCA.
  rewrite (setUC [set a] [set b]).
  by apply: main;[ |rewrite -(setUC [set a] [set b]) | ].
set s := (a :: _); move=> abc gps uniqs.
rewrite inside_triangle_immediate => //; last first.
  by apply/(general_position_sub gps); rewrite !subE.
move=> /andP [] /andP [] abd bcd cad.
rewrite inside_triangle_immediate => //; last first.
  by apply/(general_position_sub gps); rewrite !subE.
move=> /andP [] /andP [] abe bde dae.
rewrite inside_triangle_immediate => //; last first.
  by apply/(general_position_sub gps); rewrite !subE.
rewrite abe andTb.
have := Knuth_axiom5c abe abd abc.
  rewrite Knuth_1 dae Knuth_1 cad => /(_ isT isT) aec.
by rewrite (Knuth_axiom5c' abc abd abe bcd bde) -Knuth_1.
Qed.

Lemma sub_triangle_unique (t : {set P}) d e j :
  #|t| = 3 ->
  general_position (t :|: [set d; e]) ->
  uniq [:: three_points t j; three_points t (j + 1); three_points t (j - 1);
           d; e] ->
  inside_triangle d t ->
  inside_triangle e [set three_points t j; three_points t (j + 1); d] ->
  ~~ inside_triangle e [set three_points t (j + 1); three_points t (j - 1); d].
Proof.
move=> t3 gp us din ein.
set tps := three_points t.
have gpt : general_position t.
  by apply/(general_position_sub gp); rewrite subsetU // subxx.
have/inside_triangle_immediate :
  general_position [set tps j; tps (j + 1); tps (j - 1); d].
  by apply/(general_position_sub gp); rewrite !(subE, three_points_in).
rewrite three_points_oriented  -?three_points_eqi // => /(_ isT) => it.
move: din; rewrite it => /andP [] /andP [] jj1d j1jm1d jm1jd .
have/inside_triangle_immediate :
  general_position [set tps j; tps (j + 1); d; e].
  by apply/(general_position_sub gp); rewrite !(subE, three_points_in) //.
move=> /(_ jj1d) => it'; move: ein; rewrite it' => /andP [] /andP [] _ j1de _.
rewrite inside_triangle_immediate => //.
  by rewrite !negb_and (Knuth_2 j1de) ?orbT.
by apply/(general_position_sub gp); rewrite !(subE, three_points_in).
Qed.

Lemma choose_sub_triangle a b c d e :
  general_position [set a; b; c; d; e] ->
  uniq [:: a; b; c; d; e] ->
  inside_triangle d [set a; b; c] ->
  inside_triangle e [set a; b; c] ->
  [|| inside_triangle e [set a; b; d], inside_triangle e [set b; c; d] |
      inside_triangle e [set c; a; d]].
Proof using.
wlog : a b c / ccw a b c.
  set s := (a :: _); move=> main gs uniqs.
  have : [set a; b; c] \subset [set a; b; c; d; e] by rewrite !subE.
  move/(general_position_sub gs)/general_position_split=> it.
  apply (use_imp it) => {it}.
    by apply: (@diff_from_uniq _ (behead s)); rewrite uniqs /= !inE eqxx ?orbT.
  move=> it; apply: (use_imp it) => {it}. 
    apply : (@diff_from_uniq _ (behead (rot 1 s))).
    by rewrite -(rot_uniq 4) uniqs /= !inE eqxx ?orbT.
  move=> it; apply: (use_imp it) => {it}.
    apply : (@diff_from_uniq _ (behead (rot 2 s))).
    by rewrite -(rot_uniq 3) uniqs /= !inE eqxx ?orbT.
  move/orP=>[abc | bac]; first by apply: main.
  rewrite (setUC [set a] [set b]) (setUC [set b] [set c]).
  rewrite (setUC [set c] [set a]).
  rewrite (orbC (inside_triangle e [set c; b; d])).
  apply: main; [ | rewrite (setUC [set b]) | ] => //.
  move: uniqs; rewrite /s 2!cons_uniq inE negb_or eq_sym.
  move => /andP [] /andP [] bna ac bc.
  by rewrite 2!cons_uniq inE negb_or bna ac bc.
set s := (a :: _); move=> abc gps uniqs.
rewrite inside_triangle_immediate //; last first.
  by apply/(general_position_sub gps); rewrite !subE.
move=> /andP[] /andP[] abd bcd cad.
rewrite inside_triangle_immediate //; last first.
  by apply/(general_position_sub gps); rewrite !subE.
move=> /andP[] /andP[] abe bce cae.
have : [set b; d; e] \subset [set a; b; c; d; e] by rewrite !subE.
move/(general_position_sub gps)/general_position_split.
move=> it; apply: (use_imp it) => {it}. 
  apply : (@diff_from_uniq _ (behead (rot 1 s))).
  by rewrite -(rot_uniq 4) uniqs /= !inE eqxx ?orbT.
move=> it; apply: (use_imp it) => {it}. 
  apply : (@diff_from_uniq _ (behead (rot 3 s))).
  by rewrite -(rot_uniq 2) uniqs /= !inE eqxx ?orbT.
move=> it; apply: (use_imp it) => {it}. 
  apply : (@diff_from_uniq _ (behead (rot 4 s))).
  by rewrite -(rot_uniq 1) uniqs /= !inE eqxx ?orbT.
move/orP=>[bde | dbe].
  have : [set c; d; e] \subset [set a; b; c; d; e] by rewrite !subE.
  move/(general_position_sub gps)/general_position_split.
  move=> it; apply: (use_imp it) => {it}. 
    apply : (@diff_from_uniq _ (behead (rot 2 s))).
    by rewrite -(rot_uniq 3) uniqs /= !inE eqxx ?orbT.
  move=> it; apply: (use_imp it) => {it}. 
    apply : (@diff_from_uniq _ (behead (rot 3 s))).
    by rewrite -(rot_uniq 2) uniqs /= !inE eqxx ?orbT.
  move=> it; apply: (use_imp it) => {it}. 
    apply : (@diff_from_uniq _ (behead (rot 4 s))).
    by rewrite -(rot_uniq 1) uniqs /= !inE eqxx ?orbT.
  move/orP=>[cde | dce].
    have : [set a; d; e] \subset [set a; b; c; d; e] by rewrite !subE.
    move/(general_position_sub gps)/general_position_split.
    move=> it; apply: (use_imp it) => {it}. 
      apply : (@diff_from_uniq _ (behead s)).
      by rewrite uniqs /= !inE eqxx ?orbT.
    move=> it; apply: (use_imp it) => {it}. 
      apply : (@diff_from_uniq _ (behead (rot 3 s))).
      by rewrite -(rot_uniq 2) uniqs /= !inE eqxx ?orbT.
    move=> it; apply: (use_imp it) => {it}. 
      apply : (@diff_from_uniq _ (behead (rot 4 s))).
      by rewrite -(rot_uniq 1) uniqs /= !inE eqxx ?orbT.
    move/orP=>[ade | dae].
      move: ade; rewrite -Knuth_1 => dea.
      move: bde; rewrite -Knuth_1 => deb.
      move: cde; rewrite -Knuth_1 => dec.
      move: abd; rewrite Knuth_1 => dab.
      move: bcd; rewrite Knuth_1 => dbc.
      have := Knuth_axiom5c dea deb dec dab dbc.
      by rewrite -Knuth_1 => /(Knuth_2 (P := P)); rewrite cad.
    rewrite inside_triangle_immediate;[ | | by rewrite abd ]; last first.
      by apply/(general_position_sub gps); rewrite !subE.
    by rewrite abe bde dae.
  have : [set a; d; e] \subset [set a; b; c; d; e] by rewrite !subE.
  move/(general_position_sub gps)/general_position_split.
  move=> it; apply: (use_imp it) => {it}. 
    apply : (@diff_from_uniq _ (behead s)).
    by rewrite uniqs /= !inE eqxx ?orbT.
  move=> it; apply: (use_imp it) => {it}. 
    apply : (@diff_from_uniq _ (behead (rot 3 s))).
    by rewrite -(rot_uniq 2) uniqs /= !inE eqxx ?orbT.
  move=> it; apply: (use_imp it) => {it}. 
    apply : (@diff_from_uniq _ (behead (rot 4 s))).
    by rewrite -(rot_uniq 1) uniqs /= !inE eqxx ?orbT.
  move/orP=>[ade | dae].
    rewrite orbA orbC.
    rewrite inside_triangle_immediate;[ | | by rewrite cad ]; last first.
      by apply/(general_position_sub gps); rewrite !subE.
    by rewrite cae ade dce.
  rewrite inside_triangle_immediate;[ | | by rewrite abd]; last first.
    by apply/(general_position_sub gps); rewrite !subE.
  by rewrite abe bde dae.
have : [set c; d; e] \subset [set a; b; c; d; e] by rewrite !subE.
move/(general_position_sub gps)/general_position_split.
move=> it; apply: (use_imp it) => {it}. 
  apply : (@diff_from_uniq _ (behead (rot 2 s))).
  by rewrite -(rot_uniq 3) uniqs /= !inE eqxx ?orbT.
move=> it; apply: (use_imp it) => {it}. 
  apply : (@diff_from_uniq _ (behead (rot 3 s))).
  by rewrite -(rot_uniq 2) uniqs /= !inE eqxx ?orbT.
move=> it; apply: (use_imp it) => {it}. 
  apply : (@diff_from_uniq _ (behead (rot 4 s))).
  by rewrite -(rot_uniq 1) uniqs /= !inE eqxx ?orbT.
move/orP=>[cde | dce].
    rewrite orbC.
    rewrite inside_triangle_immediate;[ | | by rewrite bcd ]; last first.
      by apply/(general_position_sub gps); rewrite !subE.
    by rewrite bce cde dbe.
have : [set d; a; e] \subset [set a; b; c; d; e] by rewrite !subE.
move/(general_position_sub gps)/general_position_split.
move=> it; apply: (use_imp it) => {it}. 
  apply : (@diff_from_uniq _ (behead (rot 3 s))).
  by rewrite -(rot_uniq 2) uniqs /= !inE eqxx ?orbT.
move=> it; apply: (use_imp it) => {it}. 
  apply : (@diff_from_uniq _ (behead s)).
  by rewrite uniqs /= !inE eqxx ?orbT.
move=> it; apply: (use_imp it) => {it}. 
  apply : (@diff_from_uniq _ (behead (rot 4 s))).
  by rewrite -(rot_uniq 1) uniqs /= !inE eqxx ?orbT.
move/orP=>[dae | ade].
  rewrite orbA orbC.
  move: dae dce dbe cae bce abe; rewrite !(Knuth_1 _ _ e) =>
       eda edc edb eca ebc eab.
  have := Knuth_axiom5c eda edb edc eab ebc.
  by rewrite Knuth_1 => /(Knuth_2 (P := P)); rewrite eca.
rewrite orbA orbC.
rewrite inside_triangle_immediate;[ | | by rewrite cad ]; last first.
  by apply/(general_position_sub gps); rewrite !subE.
by rewrite cae ade dce.
Qed.

(* transforms a goal that is a boolean conjunction of comparisons into
  a collection of goals that are all coq_nat comparisons. *)
Ltac to_lia := repeat (apply/andP; split); apply/leP.

(* apply lia on a goal, after converting all arithmetic operations to
  operations that lia knows about. *)
Ltac ssr_lia := rewrite -?(plusE, minusE, multE, subn1); lia.

Lemma new_path_offset (f : {ffun P -> P}) (a p : P) (i : nat) (s' : {set P}) k :
  {in s', forall x, f x \in s'} ->
  p \notin s' -> a \in s' ->
  (0 < i < fingraph.order f a)%N ->
  (0 < k <= fingraph.order f a - i + 1)%N ->
  iter k (new_path f a (iter i f a) p) p = iter (k - 1 + i) f a.
Proof using.
move => s'st pns ain io; elim: k => [ | [ | k] IH]; first by rewrite ltn0.
  rewrite /= /new_path ffunE /= eqxx subnn add0n => _.
  have /negbTE -> // : p != a.
  by apply/eqP=> abs; move/negP:pns; apply; rewrite abs.
move=> k2bnd; rewrite iterS IH ?subSS ?subn0; last first.
  by rewrite /= (leq_trans _ k2bnd).
rewrite /new_path ffunE /= (negbTE (_ : iter (k + i) f a != a)); last first.
  rewrite eq_sym; apply/eqP.
  apply/(iter_lt_order_neq (_ : (0 < k + i < fingraph.order f a)%N)).
  by to_lia; case/andP: io => [/leP i0 /leP]; move: i0 (leP k2bnd); ssr_lia.
rewrite (negbTE (_ : iter (k + i) f a != p)) //.
by apply/eqP=> abs; move/negP: pns; apply; rewrite -abs; apply: iter_in.
Qed.

Lemma new_path_a (f : {ffun P -> P}) a b p : new_path f a b p a = p.
Proof using. by rewrite ffunE /= eqxx. Qed.

Lemma new_path_p (f : {ffun P -> P}) a b p : p != a -> new_path f a b p p = b.
Proof using. by rewrite ffunE /= eqxx eq_sym => /negbTE ->. Qed.

Lemma new_path_cycle (f : {ffun P -> P}) (a p : P) (i : nat) (s' : {set P}) :
  {in s', forall x, f x \in s'} ->
  {in s' &, injective f} ->
  p \notin s' -> a \in s' ->
  (0 < i < fingraph.order f a)%N ->
  (1 < size (orbit f a))%N ->
  iter (fingraph.order f a - i + 2) (new_path f a (iter i f a) p) a = a.
Proof using.
move=> f_stable finj pnotin ain iint sz.
move: (iint) => /andP [igt0 ilto].
have int' : (0 < fingraph.order f a - i + 1 <= fingraph.order f a - i + 1)%N.
  by rewrite leqnn addnS ltnS leq0n.
rewrite addnS iterSr new_path_a.
have vv : (0 < fingraph.order f a - 1 < fingraph.order f a)%N.
  apply/andP; split; last by rewrite -orderSpred subSS subn0 ltnSn.
  by rewrite -size_orbit; apply/(leq_trans (isT : (0 < (2 - 1))%N))/leq_sub2r.
have := new_path_offset f_stable pnotin ain iint int'.
have tmp := iter_order_in f_stable finj ain.
by rewrite addnK subnK;[move=> -> | apply: leq_trans ilto; rewrite leqnSn].
Qed.

Lemma new_path_orbit_sym 
  (f : {ffun P -> P}) (a p : P) (i : nat) (s' : {set P}) x :
  {in s', forall x, f x \in s'} ->
  {in s' &, injective f} ->
  p \notin s' -> a \in s' ->
  (0 < i < fingraph.order f a)%N ->
  (1 < size (orbit f a))%N ->
  x \in orbit (new_path f a (iter i f a) p) a ->
  a \in orbit (new_path f a (iter i f a) p) x.
Proof using.
move=> f_stable finj pnotin ain iint sz.
rewrite -!fconnect_orbit=>/iter_findex <-.
have loop := new_path_cycle f_stable finj pnotin ain iint sz.
set f' := new_path _ _ _ _.
elim: (findex f' a x).+1 {-2}(findex f' a x) (ltnSn (findex f' a x)) =>
   [// | j' IH j]; rewrite ltnS => jj'.
have [jgt0 | ] := boolP (0 < j)%N; last first.
  by rewrite lt0n negbK => /eqP -> /=; apply: connect0.
have [jgeo | ] :=boolP (fingraph.order f a - i + 2 <= j)%N.
  rewrite -(subnK jgeo) iter_add loop // IH //.
  by to_lia; move: (leP jgt0) (leP jgeo) (leP jj'); ssr_lia.
rewrite -ltnNge addnS ltnS=> jlto.
rewrite -[X in fconnect _ _ X]loop -/f' addnS iterS -(subnK jlto) -iterS -addSn.
by rewrite iter_add fconnect_iter.
Qed.

Section fixed_hull.

Variable s : {set P}.

Variable s' : {set {set P}}.

Variable f : {ffun P -> P}.

Hypothesis s3 : (3 <= #|s|)%N. 

Hypothesis ss' : cover s' \subset s.

Hypothesis ch : convex_hull_path s s' f.

Variable ts : {set {set P}}.

Hypothesis ts3 : forall t, t \in ts -> #|t| = 3.

Hypothesis cover_ts : cover ts = s.

Hypothesis s'_b : s' = boundary_edge ts.

Lemma finj : {in cover s' &, injective f}.
Proof using s3 ss' ch. by apply: (convex_hull_path_inj s3). Qed.

Lemma convex_hull_stable : {in cover s', forall x, f x \in cover s'}.
Proof using ch.
move=> x xin.
by move/forallP: ch=> /(_ x); rewrite xin implyTb => /andP[]/andP[].
Qed.

Lemma oriented_triangle x q : x \in cover s' ->
   [set x; f x; q] \in ts -> ccw x (f x) q.
Proof using ch ts3 cover_ts.
move=> xin ints.
have qs : q \in s.
  rewrite -cover_ts; apply/bigcupP; exists [set x; f x; q];
  by rewrite // !inE eqxx ?orbT.
have/eqP := (ts3 ints); rewrite cards3 -?(eq_sym q) => /andP[] /andP[] _.
by move=>qnfx qnx; apply: (use_convex_hull_path ch xin qs).
Qed.

Section convex_hull_element.

Variable x : P.

Hypothesis xin : x \in cover s'.

Lemma convex_hull_size_orbit_gt2 :  (2 < size (orbit f x))%N.
Proof using xin s3 ss' ch.
set t := orbit f x.
have cfxp := ch_path_no_fixpoint s3 ss' ch; have [fxx ffxx] := cfxp _ xin.
have fxs : f x \in cover s' by apply: (convex_hull_path_iter_in 1 s3).
have /cfxp [ffxfx _] := fxs.
have fxt : f x \in t by rewrite -fconnect_orbit fconnect1.
have ffxt : f (f x) \in t by rewrite -fconnect_orbit (fconnect_iter f 2).
apply: leq_trans (card_size _); rewrite (cardD1 x [pred y | y \in t]) inE /=.
rewrite -fconnect_orbit connect0 ltnS; set t' := (X in (_ < #|X|)%N).
rewrite (cardD1 (f x) t') !inE fxx fxt ltnS; set t'' := (X in (_ < #|X|)%N).
by rewrite (cardD1 (f (f x)) t'') !inE ffxx ffxfx ffxt.
Qed.

Lemma convex_hull_cycle : f (last x (orbit f x)) = x.
Proof using s3 ss' ch xin.
have : fcycle f (orbit f x)
  by apply: (cycle_orbit_in convex_hull_stable finj xin).
by rewrite (cycle_path x) /orbit -orderSpred /= => /andP [] /eqP.
Qed.

(* This is actually a proof that for a point outside the
  convex hull, not all edges can be blue (to be confirmed). *)
Lemma convex_hull_multi_step_orientation y t :
  y \in cover s' -> (3 <= size t)%N ->
  (forall i, (i < size t)%N -> nth x t i != y) ->
  (forall i, (i < size t)%N -> nth x t i !=  f y) ->
  (forall i, (i < size t)%N -> nth x t i \in s) ->
  (forall i, (i.+1 < size t)%N ->
    ccw (nth x t i) (nth x t i.+1) y) -> ccw (nth x t 0) (last x t) y.
Proof using s3 ss' ch.
move=> yin; elim: t => [// | a t IH] st ny nfy ins hull.
have yfya : ccw y (f y) a.
  have ains : a \in s by apply: (ins 0%N).
  have any : a != y by apply: (ny 0%N).
  have anfy : a != f y by apply: (nfy 0%N).
  by apply: (use_convex_hull_path ch yin).
move: st ny nfy ins hull;  rewrite leq_eqVlt => /orP [{IH}| t3].
  case: t => [ | b [ | c [ | ? ?]]] //= _ ny nfy ins hull.
  rewrite (Knuth_1 _ _ y).
  have : ccw y b c.
    by rewrite -Knuth_1; apply: (hull 1%N).
  have : ccw y a b.
    by rewrite -Knuth_1; apply: (hull 0%N).
  have : ccw y (f y) c.
    have cins : c \in s by apply: (ins 2%N).
    have cny : c != y by apply: (ny 2%N).
    have cnfy : c != f y by apply: (nfy 2%N).
    by apply: (use_convex_hull_path ch yin).
  have : ccw y (f y) b.
    have bins : b \in s by apply: (ins 1%N).
    have bny : b != y by apply: (ny 1%N).
    have bnfy : b != f y by apply: (nfy 1%N).
    by apply: (use_convex_hull_path ch yin).
  by apply: Knuth_axiom5c.
move=> ny nfy ins hull.
have ny' : forall i, (i <  size t)%N -> nth x t i != y.
  by move=> i; exact: ny i.+1.
have nfy' : forall i, (i <  size t)%N -> nth x t i != f y.
  by move=> i; exact: nfy i.+1.
have ins' : forall i, (i <  size t)%N -> nth x t i \in s.
  by move=> i; exact: ins i.+1.
have h' : forall i, (i.+1 < size t)%N -> ccw (nth x t i) (nth x t i.+1) y.
  by move=> i; exact: hull i.+1.
have lasts : last x t = last x (a :: t) by move: t3; case: (t).
have heads : nth x (a :: t) 0 = a by [].
have := IH t3 ny' nfy' ins' h'; rewrite -lasts heads {ny' nfy' ins' h'}.
have : ccw y a (nth x t 0)
  by rewrite -Knuth_1; apply/(hull 0%N)/(leq_trans _ t3).
have : ccw y (f y) (last x t).
  have ains : last x t \in s.
    by rewrite lasts /= (last_nth x); apply: ins.
  have any : last x t != y.
    by rewrite lasts /= (last_nth x); apply ny.
  have anfy : last x t != f y.
    by rewrite lasts /= (last_nth x); apply nfy.
  by apply: (use_convex_hull_path ch yin).
have : ccw y (f y) (nth x t 0).
  have ains : nth x t 0 \in s.
    by apply/(ins 1%N)/(leq_trans _ t3).
  have any : nth x t 0 != y.
    by apply/(ny 1%N)/(leq_trans _ t3).
  have anfy : nth x t 0 != f y.
    by apply/(nfy 1%N)/(leq_trans _ t3).
  by apply: (use_convex_hull_path ch yin).
rewrite !(Knuth_1 _ _ y).
by apply: (Knuth_axiom5c (P := P)).
Qed.

Lemma convex_hull_path_orbit y : (y \in cover s') = (y \in orbit f x).
Proof using s3 ss' ch xin.
apply/idP/idP; last first.
  rewrite /orbit; move: {-1}x (connect0 (frel f) x) (xin) (fingraph.order _ _).
  move=> e xe ein n; elim: n e xe ein => [// | n IH] e xe ein; rewrite /= inE.
  move=> /orP [/eqP -> // | ]; apply: IH.
    by rewrite (connect_trans _ (fconnect1 _ _)).
  by move: (implyP (forallP ch e) ein) => /andP [] /andP [].
move=> yin.
have [// | yno] := boolP (y \in orbit f x).
set t := orbit f x.
have t3 : (3 <= size t)%N by apply: convex_hull_size_orbit_gt2.
have ny : forall i, (i < size t)%N -> nth x t i != y.
  move => i ist; apply/eqP=> tiy; move/negP: yno; apply; apply/trajectP.
  by exists i; rewrite -?tiy ?nth_traject // -size_orbit.
have s'st : forall z, z \in cover s' -> f z \in cover s'.
  by move=> z zis; apply: (convex_hull_path_iter_in 1 s3).
have iteris' : forall i, iter i f x \in cover s'.
  by move=> i; apply: (convex_hull_path_iter_in i s3).
have nthxtin : forall i, (i < size t)%N -> nth x t i \in cover s'.
  move => i ist; rewrite nth_traject; last by move:ist; rewrite size_orbit.
  by rewrite iteris'.
have nfy : forall i, (i < size t)%N -> nth x t i != f y.
  move => i ist; apply/eqP => tify; move/negP: (yno); apply.
  rewrite -(finv_f_in s'st finj yin) -tify.
  rewrite -[finv f]/(iter 1 (finv f)) (iter_finv_in s'st) //;
    last by rewrite -orderSpred.
      rewrite nth_traject; last by move: ist; rewrite size_orbit.
      by rewrite -iter_add -fconnect_orbit fconnect_iter.
    apply: finj.
  by apply: nthxtin.
have nthxtins : forall i, (i < size t)%N -> nth x t i \in s.
  by move=>i /nthxtin; apply/subsetP.
have : forall i, (i.+1 < size t)%N -> ccw (nth x t i) (nth x t i.+1) y.
  move=> i ist.
  rewrite !nth_traject -?size_orbit // ?(ltn_trans (ltnSn _) ist) //.
  rewrite iterS.
  have yni  : y != iter i f x.
    by apply/eqP => A; case/negP: yno; rewrite -fconnect_orbit A fconnect_iter.
  have ynfi : y != f (iter i f x).
    apply/eqP => A; case/negP: yno.
    by rewrite -fconnect_orbit A -iterS fconnect_iter.
  by apply: (use_convex_hull_path ch (iteris' i)); rewrite // (subsetP ss').
move/(convex_hull_multi_step_orientation yin t3 ny nfy nthxtins).
have flx : f (last x t) = x by apply: convex_hull_cycle.
rewrite nth_traject /=; last by rewrite -orderSpred.
have lin : last x t \in cover s'.
  rewrite (last_nth x) size_orbit -orderSpred /= nth_traject //.
  by rewrite -{2}orderSpred ltnSn.
have ynx : y != x.
  by apply/eqP => A; move: yno; rewrite A -fconnect_orbit connect0.
have ynl : y != last x t.
  rewrite (last_nth x) size_orbit -orderSpred /= eq_sym; apply: ny.
  by rewrite size_orbit -{2}orderSpred.
have : ccw (last x t) (f (last x t)) y.
  by apply: (use_convex_hull_path ch lin); rewrite ?flx ?(subsetP ss').
by rewrite flx => /(Knuth_2 (P := P))/negP A /A.
Qed.

Lemma convex_hull_path_unique f' : convex_hull_path s s' f' -> f x = f' x.
Proof using s3 ss' ch xin.
move=> chf'.
have [/eqP -> // | fxnf'x] := boolP(f x == f' x).
have xnf'x : f' x != x.
  by have := ch_path_no_fixpoint s3 ss' chf' xin=> [[]].
have xnfx : f x != x.
  by have := ch_path_no_fixpoint s3 ss' ch xin => [[]].
have fxs : f x \in cover s' by apply: (convex_hull_path_iter_in 1 s3 ss').
have chx := use_convex_hull_path ch xin.
have f'xs : f' x \in cover s' by apply: (convex_hull_path_iter_in 1 s3 ss').
have cfx := use_convex_hull_path chf' xin.
move/(subsetP ss'): fxs => fxs; move/(subsetP ss'): f'xs => f'xs.
move: (chx (f' x) f'xs xnf'x); rewrite eq_sym fxnf'x => /(_ isT) => A1.
move: (cfx (f x) fxs xnfx fxnf'x) => /(Knuth_2 (P := P)).
by rewrite -Knuth_1 A1.
Qed.

Lemma red_path p n : (0 < n < fingraph.order f x)%N ->
  (forall i, (i < n)%N -> ccw (iter i.+1 f x) (iter i f x) p) ->
    ccw (iter n f x) x p.
Proof using s3 ss' ch xin.
elim: n => [| [_ | n IH]] // /andP [] n0 nor red.
  by apply:red.
have iterf : forall j, iter j f x \in cover s'.
  by move=> i; apply: (convex_hull_path_iter_in i s3 ss' ch).
apply (@Knuth_axiom5.Knuth_4 _ _ _ (iter n.+1 f x)).
    rewrite Knuth_1.
    apply: (use_convex_hull_path ch); rewrite ?(subsetP ss') //.
      have /iter_lt_order_neq/eqP -> // : (0 < n.+1 < fingraph.order f x)%N.
      by rewrite /= (ltn_trans _ nor).
    rewrite -iterS.
    have /iter_lt_order_neq/eqP -> // : (0 < n.+2 < fingraph.order f x)%N.
    by rewrite /= nor.
  rewrite Knuth_1; apply: IH => //=.
    by apply: ltn_trans nor.
  by move=> i ci; apply: red; rewrite (leq_trans ci).
by rewrite -Knuth_1; apply: red.
Qed.

(* Important lemma, deserves a comment in any paper about this work. *)
Lemma sub_convex_triangle i j :
 ((0 < i < j) && (j < size (orbit f x)))%N -> ccw x (iter i f x) (iter j f x).
Proof using s3 ss' ch xin.
move=> cmps.
have chin := convex_hull_path_iter_in _ s3 ss' ch xin.
have/iter_lt_order_neq/eqP/= xnj : (0 < j < fingraph.order f x)%N.
  rewrite -size_orbit; case/andP: (cmps) => _ ->; rewrite andbT.
  by case/andP: cmps => /andP [h1 h2] _; rewrite (ltn_trans h1 h2).
have/iter_lt_order_neq/eqP/= xni : (0 < i < fingraph.order f x)%N.
  rewrite -size_orbit; case/andP: (cmps) => /andP [_ h1] h2.
  by rewrite (ltn_trans h1 h2) andbT; case/andP: cmps => /andP [].
have /= fxin := convex_hull_path_iter_in 1 s3 ss' ch.
have chx := use_convex_hull_path ch xin.
have itin : forall k, iter k f x \in s.
  by move=> k; apply/(subsetP ss')/(convex_hull_path_iter_in k s3).
move: cmps (xnj); have [/eqP -> cmps _ | in1] := boolP (i == 1)%N.
  move: (cmps); rewrite ltnSn andTb size_orbit=> /iter_lt_order_neq/eqP/= fxnj.
  by move: (chx (iter j f x) (itin j)); rewrite eq_sym xnj eq_sym fxnj; apply.
case/andP => /andP [i0 ij]; rewrite -(subnK ij) => js; move: (js).
move: (i0); rewrite leq_eqVlt eq_sym (negbTE in1) /= => igt1.
rewrite (subnK ij) in js.
have /iter_lt_order_neq/eqP fxni : (1 < i < fingraph.order f x)%N.
  by rewrite igt1 (ltn_trans ij) // -size_orbit.
elim: (j - i.+1)%N => [ | k IH].
  rewrite add0n => i1s xni1; rewrite -Knuth_1.
  by apply: (use_convex_hull_path ch (chin _)) => //; apply/(subsetP ss').
rewrite !addnS !addSn in IH * => k2s xnk2.
have k1s : ((k + i).+1 < size (orbit f x))%N by apply: ltn_trans k2s.
have /iter_lt_order_neq/eqP xnk1 : (0 < (k + i).+1 < fingraph.order f x)%N.
  by rewrite -size_orbit k1s ltn0Sn.
have /iter_lt_order_neq/eqP fxnk1 : (1 < (k + i).+1 < fingraph.order f x)%N.
  by rewrite -size_orbit k1s -addSn (leq_trans igt1) // leq_addl.
have /iter_lt_order_neq/eqP fxnk2 : (1 < (k + i).+2 < fingraph.order f x)%N.
  by rewrite -size_orbit k2s -!addSn (leq_trans igt1) // leq_addl.
have xfxi : ccw x (f x) (iter i f x).
  move: (chx (iter i f x)).
  by rewrite eq_sym xni eq_sym fxni (subsetP ss') ?chin //; apply.
have xfxk1 : ccw x (f x) (iter (k + i).+1 f x).
  move: (chx (iter (k + i).+1 f x)); rewrite eq_sym xnk1 eq_sym fxnk1.
  by rewrite (subsetP ss') ?chin //; apply.
have xfxk2 : ccw x (f x) (iter (k + i).+2 f x).
  move: (chx (iter (k + i).+2 f x)); rewrite eq_sym xnk2 eq_sym fxnk2.
  by rewrite (subsetP ss') ?chin //; apply.
have xik1 : ccw x (iter i f x) (iter (k + i).+1 f x).
  apply: IH => //.
suff xk1k2 : ccw x (iter (k + i).+1 f x) (iter (k + i).+2 f x).
  by apply: (Knuth_axiom5c xfxi xfxk1).
move/forallP/(_ (iter (k + i).+1 f x)): ch.
rewrite chin implyTb => /andP [] /andP [] _.
by move/forallP/(_ x); rewrite Knuth_1 (subsetP ss') // xnk1 xnk2.
Qed.

Lemma inside_triangle_in_hull p (t : {set P}) :
  #|t| = 3 -> t \subset s ->
  general_position (p |: s) ->
  (forall i, in_hull (cover s') f (three_points t i)) ->
  inside_triangle p t -> ccw x (f x) p.
Proof using s3 ss' ch xin.
set tps := three_points t.
move=> t3 tsubs gps inht pins.
have xnfx : x != f x.
  by have := ch_path_no_fixpoint s3 ss' ch xin; rewrite eq_sym=>[[]].
set bigset := [set x; f x; tps 0; tps 1; tps (-1); p].
have gpbig : general_position bigset.
  have : bigset \subset (p |: s).
    rewrite !subE ?orbT ?andTb 2?(subsetP ss') ?orbT//.
    rewrite !(subsetP tsubs) ?orbT  ?three_points_in //.
    by apply: (convex_hull_path_iter_in 1 s3).
  by move/(general_position_sub gps).
have chx := use_convex_hull_path ch xin.
have t_dec : t = [set tps 0; tps 1; tps (-1)].
  rewrite (three_points_eq t3); apply/setP=> z; apply/imsetP/idP.
    by move=> [j _ ->]; elim/elimI3: j; rewrite !inE eqxx ?orbT.
  by rewrite !inE => /orP[/orP [] | ] /eqP ->; (eexists;[ | reflexivity]).
have t_decj : forall j, t = [set tps j; tps (j + 1); tps (j - 1)].
  by elim/elimI3; rewrite t_dec // ?addrN -?(cycle_set3 (tps 0))
      ?(cycle_set3 _ (tps 0)) ?m1m1.
have gps' : general_position s.
 by apply/(general_position_sub gps); apply subsetUr.
have gpt : general_position t by apply/(general_position_sub gps').
have [ /andP [xnotin fxnotin] | ] := boolP ((x \notin t) && (f x \notin t)).
  have tpsnx : forall i, tps i != x.
    by move=> i; apply:(contraNN _ xnotin)=> /eqP <-; apply: three_points_in.
  have tpsnfx : forall i, tps i != f x.
    by move=> i; apply:(contraNN _ fxnotin)=> /eqP <-; apply: three_points_in.
  have xfx0 : ccw x (f x) (tps 0).
    by move: (inht 0); move/forallP=>/(_ x); rewrite xin tpsnx tpsnfx.
  have xfx1 : ccw x (f x) (tps 1).
    by move: (inht 1); move/forallP=>/(_ x); rewrite xin tpsnx tpsnfx.
  have xfxm1 : ccw x (f x) (tps (-1)).
    by move: (inht (-1)); move/forallP=>/(_ x); rewrite xin tpsnx tpsnfx.
  have := three_points_oriented 0 t3 gpt; rewrite sub0r add0r => direct.
  have gp4 : general_position [set tps 0; tps 1; tps (-1); p].
    by apply/(general_position_sub gpbig); rewrite !subE.
  move: pins; rewrite t_dec inside_triangle_immediate //.
  rewrite (Knuth_1 (tps 1) _ p) -(Knuth_1 _ _ (tps (-1))).
  move=> /andP [] /andP [] p01 p1m1 pm10.
  apply: (inside_triangle_left_of_edge gpbig xfx0 xfx1 xfxm1) => //.
have [/andP [xint fxint] _ | onlyone] := boolP ((x \in t) && (f x \in t)).
  have xfxsub : [set x; f x] \subset t by rewrite !subE xint fxint.
  have : #|t :\: [set x; f x]| == 1%N.
    rewrite cardsD (setIidPr xfxsub).
    have [j xj] : exists j, x = tps j.
      by move: xint; rewrite (three_points_eq t3)=>/imsetP[] j _ ->; exists j.
    by rewrite cards2 xnfx t3.
  move/cards1P=>[y ysubt]; move: (ysubt) => /(set3_D2_dec t3) t_decx.
  move/eqP : (t3); rewrite t_decx cards3=> /andP [] /andP [] _ fxny ynx.
  have yint : y \in t.
    by move: (set11 y); rewrite -ysubt !inE=>/andP[].
  have xfxy : ccw x (f x) y.
    by move:(chx y); rewrite (subsetP tsubs) // ynx eq_sym fxny; apply.
  have yins : y \in bigset.
    move: yint; rewrite (three_points_eq t3)=> /imsetP [j _ ->].
    by elim/elimI3: j; rewrite !inE !eqxx ?orbT.
  have gpxfxy : general_position [set x; f x; y; p].
    by apply/(general_position_sub gpbig); rewrite !(yins, subE).
  move: pins; rewrite t_decx inside_triangle_immediate //.
  by move=>/andP[] /andP[].
have tpss k : tps k \in s.
  by apply/(subsetP tsubs); rewrite (t_decj k) !inE eqxx.
rewrite negb_and !negbK => /orP [|]; rewrite (three_points_eq t3)=>
    /imsetP[j _ eqj].
  have jm1nx : tps (j - 1) != x.
    by rewrite eqj -/tps (inj_eq (three_points_inj t3)) eq_sym addm1_dif.
  have jp1nx : tps (j + 1) != x.
    by rewrite eqj -/tps (inj_eq (three_points_inj t3)) eq_sym addrC add1_dif.
  have tpsnfx : forall k, tps k != f x.
    move=> k; apply:(contraNN _ onlyone) => /eqP <-; rewrite eqj.
    by rewrite !three_points_in.
  have direct := three_points_oriented j t3 gpt.
  (* preparing a use of Knuth_axiom5' *)
  move: direct; rewrite Knuth_1 -eqj => jm1xjp1.
  have jm1xfx : ccw (tps (j - 1)) x (f x).
    move: (chx (tps (j - 1)) (tpss _) jm1nx (tpsnfx _)).
    by rewrite Knuth_1.
  have xfxjp1 : ccw x (f x) (tps (j + 1)). 
    by move: (chx (tps (j + 1)) (tpss _) jp1nx (tpsnfx _)).
  have gpjxp : general_position [set tps (j - 1); x; tps (j + 1); p].
    rewrite cycle_set3 eqj -t_decj t_dec.
    by apply/(general_position_sub gpbig); rewrite !subE.
  move: pins.
  rewrite (t_decj j) -cycle_set3 -eqj inside_triangle_immediate //.
  move=> /andP [] /andP [] jm1xp xjp1p _.
  by have := Knuth_axiom5c' jm1xfx jm1xjp1 jm1xp xfxjp1 xjp1p.
have jm1nx : tps (j - 1) != f x.
  by rewrite eqj -/tps (inj_eq (three_points_inj t3)) eq_sym addm1_dif.
have jp1nx : tps (j + 1) != f x.
  by rewrite eqj -/tps (inj_eq (three_points_inj t3)) eq_sym addrC add1_dif.
have tpsnfx : forall k, tps k != x.
  move=> k; apply:(contraNN _ onlyone) => /eqP temp.
  rewrite -[in X in X && _]temp eqj.
  by rewrite !three_points_in.
have direct := three_points_oriented j t3 gpt.
(* preparing a use of Knuth_axiom5 *)
move: direct; rewrite -eqj => fxjp1jm1.
have fxjm1x : ccw (f x) (tps (j - 1)) x.
  move: (chx (tps (j - 1)) (tpss _) (tpsnfx _) jm1nx).
  by rewrite -Knuth_1.
have fxjp1x : ccw (f x) (tps (j + 1)) x.
  move: (chx (tps (j + 1)) (tpss _) (tpsnfx _) jp1nx).
  by rewrite -Knuth_1.
have gpjxp : general_position [set f x; tps (j + 1); tps (j - 1); p].
  rewrite eqj -t_decj t_dec.
  by apply/(general_position_sub gpbig); rewrite !subE.
move: pins.
rewrite (t_decj j) -eqj inside_triangle_immediate //.
move=> /andP [] /andP [] fxjp1p xjp1p; rewrite -Knuth_1=> fxpjm1.
rewrite -Knuth_1.
by apply: (Knuth_axiom5c fxjp1p fxjp1jm1 fxjp1x).
Qed.

End convex_hull_element.

Section add_point_out.

Variable p : P.

Hypothesis pnotin : p \notin s.

Hypothesis gp : general_position (p |: s).

Hypothesis not_in_hull : ~~in_hull (cover s') f p.

Lemma pns' : p \notin cover s'.
Proof using ss' pnotin.  by apply: (contraNN _ pnotin); apply: subsetP.  Qed.

Lemma blue_edge :
  exists x, (x \in cover s') && ccw x (f x) p.
Proof using s3 ss' ch pnotin gp not_in_hull.
move: (not_in_hull); rewrite /in_hull negb_forall=> /existsP [a].
rewrite !negb_imply =>/andP [ain /andP [] /andP [] pna pnfa red].
have [ |] := boolP
    [forall i : 'I_(size (orbit f a)), ~~ ccw (iter i f a) (f (iter i f a)) p].
  move/forallP => reds.
  have sz : (0 < (size (orbit f a)).-1 < fingraph.order f a)%N.
    have := convex_hull_size_orbit_gt2 ain; rewrite -size_orbit.
    by case: (size (orbit f a)) => [ | [ | ?]] => /=.
  have := red_path ain sz; move=> /(_ p).
  have red' : forall i, (i < (size (orbit f a)).-1)%N ->
     ccw (iter i.+1 f a) (iter i f a) p.
    move => i ilt.
    have sz3 : #|[set iter i.+1 f a; iter i f a; p]| == 3.
      rewrite cards3.
      have pj : forall j, p != iter j f a.
        move=> j; apply/negP => /eqP abs; case/negP: pnotin.
        apply: (subsetP ss'); rewrite abs.
        by apply: (convex_hull_path_iter_in j s3 ss' ch).
      rewrite pj andbT andbC eq_sym pj andTb eq_sym.
      have/iter_lt_order_neq/eqP -> // : (i < i.+1 < fingraph.order f a)%N.
      by rewrite ltnSn -orderSpred ltnS -size_orbit.
    have subi : [set iter i.+1 f a; iter i f a; p] \subset p |: s.
      by apply/subsetP=> b; rewrite !inE => /orP [/orP []/eqP -> | -> //];
        apply/orP; right;
        apply/(subsetP ss')/(convex_hull_path_iter_in _ s3 ss' ch ain).
    move: (gp) => /'forall_'forall_'forall_implyP.
    move=> /(_ (iter i.+1 f a) (iter i f a) p); rewrite subi sz3.
    move=> /(_ isT) /orP [-> // | ii1a ].
    have ilt' : (i < (size (orbit f a)))%N.
      by rewrite (leq_trans ilt) // leq_pred.
    by case/negP: (reds (Ordinal ilt')).
  move=>/(_ red') foundit.
  exists (iter (size (orbit f a)).-1 f a).
  rewrite (convex_hull_path_iter_in _ s3 ss' ch ain) /=.
  have s'st : {in cover s', forall x, f x \in cover s'}.
    by move => y yin; apply: (convex_hull_path_iter_in 1 s3 ss' ch).
  have injf := convex_hull_path_inj s3 ss' ch.
  by rewrite -iterS size_orbit orderSpred (iter_order_in s'st) -?size_orbit.
rewrite negb_forall => /existsP [x]; rewrite negbK => pp; exists (iter x f a).
by rewrite (convex_hull_path_iter_in _ s3 ss' ch ain) pp.
Qed.

Lemma pnotiter a: a \in cover s' -> forall i, p != iter i f a.
Proof using s3 ss' ch pnotin.
move=> k ain; apply/negP=>/eqP A.
case/negP: pnotin; rewrite (subsetP ss') // A.
by apply: (convex_hull_path_iter_in _ s3).
Qed.

Lemma purple_point :
  exists a,
    [&& a \in cover s', ccw a (f a) p & ~~ ccw (f a) (f (f a)) p].
Proof using s3 ss' ch pnotin gp not_in_hull.
move: (not_in_hull); rewrite negb_forall => /existsP [x].
rewrite !negb_imply; case/andP => xin /andP [/andP [pnx pnfx] ].
have sz : #|[set x; f x; p]| == 3.
  rewrite cards3 pnx andbT andbC eq_sym pnfx eq_sym.
  by case: (ch_path_no_fixpoint s3 ss' ch xin) => -> _.
have sub : [set x; f x; p] \subset p |: s.
  apply/subsetP=> i; rewrite !inE => /orP [/orP [] /eqP -> | -> //];
  apply/orP; right; apply: (subsetP ss') => //.
  by apply: (convex_hull_path_iter_in 1 s3).
move: (forallP (forallP (forallP gp x) (f x)) p); rewrite sz sub.
move => /orP [ -> // | red _].
have [y /andP [yin blue]] := blue_edge.
set j := findex f y x.
have : x \in orbit f y by rewrite -convex_hull_path_orbit.
rewrite -fconnect_orbit=> /iter_findex => q; move: red; rewrite -q {q}.
elim: (findex f y x) => [ /= | i IH].
  by move=>/(Knuth_2 (P := P)); rewrite blue.
move => pi1.
have [/IH // | ] := boolP (ccw (iter i.+1 f y) (iter i f y) p).
have jin : forall j, iter j f y \in cover s'.
  by move=> k; apply: (convex_hull_path_iter_in _ s3).
have pni : forall k, p != iter k f y by apply: pnotiter.
have szi : #|[set iter i f y; iter i.+1 f y; p]| == 3.
  rewrite cards3 pni andbT andbC eq_sym pni andTb iterS eq_sym.
  by case: (ch_path_no_fixpoint s3 ss' ch (jin i)) => -> _.
have subi : [set iter i f y; iter i.+1 f y; p] \subset p |: s.
  apply/subsetP=> q; rewrite !inE => /orP [/orP [] /eqP -> | -> //];
  by apply/orP; right; apply: (subsetP ss').
move: (forallP (forallP (forallP gp (iter i f y)) (iter i.+1 f y)) p).
rewrite szi subi => /orP [blue' | -> //].
by exists (iter i f y); rewrite jin blue'; apply: Knuth_2.
Qed.

Lemma pre_contiguous_red a b c:
  a \in cover s' -> c \in cover s' -> a != b -> f a != b ->
  ccw a (f a) b -> ccw a (f a) c -> ccw (f a) b c -> ccw (f a) b (f c).
Proof using s3 ss' ch.
move=> ain cin anb fanb afab afac fabc.
have cha := use_convex_hull_path ch ain.
have [/eqP fca | fcna] := boolP (f c == a).
  by rewrite fca Knuth_1.
have fanc : f a != c.
  by move: afac; rewrite -Knuth_1 => /oriented_diff.
have anc : a != c.
  by move: afac; rewrite Knuth_1 eq_sym => /oriented_diff.
have fas : f a \in s by apply/(subsetP ss')/(convex_hull_path_iter_in 1 s3).
have fanfc : f a != f c.
  by rewrite (inj_in_eq (convex_hull_path_inj s3 ss' ch)).
have chc := use_convex_hull_path ch cin.
have fcs : f c \in s by apply/(subsetP ss')/(convex_hull_path_iter_in 1 s3).
have facfc : ccw (f a) c (f c).
  by move: (chc (f a) fas fanc fanfc); rewrite Knuth_1.
have afafc : ccw a (f a) (f c).
  by apply:cha; rewrite // (eq_sym (f c) (f a)).
by have := Knuth_axiom5c' afab afac afafc fabc facfc.
Qed.

Lemma contiguous_red a i j :
   a \in cover s' ->
  (0 < i)%N -> ccw a (f a) p -> ccw (iter i f a) (f a) p ->
  ccw (iter i f a) (iter i.+1 f a) p ->
  (i <= j <= size (orbit f a))%N -> ccw (iter j f a) (iter j.+1 f a) p.
Proof using s3 ss' ch.
move=> ain i0 afap ifafap ii1p /andP [].
have iterin := convex_hull_path_iter_in _ s3 ss' ch ain.
have sc := sub_convex_triangle.
have s'st : {in cover s', forall x, f x \in cover s'}.
  by move => y ys; apply:(convex_hull_path_iter_in 1 s3 ss' ch).
move=> ij; rewrite -(subnK ij) {ij}.
elim: (j - i)%N => [ | k IH] => // {j}; rewrite !addSn; set j := _.+1.
move: i0; rewrite leq_eqVlt => /orP [/eqP A | i1].
  by move/oriented_diff: ifafap; rewrite -A eqxx.
have ij : (i < j)%N by rewrite /j ltnS leq_addl.
have j2 : (2 < j)%N by rewrite (leq_trans _ ij).
set u := size _; rewrite leq_eqVlt => /orP [/eqP -> | js].
  by rewrite /u size_orbit iterS (iter_order_in s'st finj).
have fain : f a \in cover s'.
  by apply: (convex_hull_path_iter_in 1 s3).
have itereq := size_orbit_in_eq s'st finj ain.
have/(sc _ fain) : ((0 < i.-1 < j.-1) && (j.-1 < size (orbit f (f a))))%N.
  rewrite (itereq 1%N) -/u.
  by to_lia; move: (leP js) (leP i1) (leP ij); ssr_lia.
rewrite -!iterSr (ltn_predK i1) (ltn_predK ij) => faij.
have/(sc _ ain) afai : ((0 < 1 < i) && (i < u))%N.
  by rewrite i1 !andTb (ltn_trans ij).
have/(sc _ ain) afaj : ((0 < 1 < j) && (j < u))%N.
  by rewrite js (ltn_trans i1).
have fapj : ccw (f a) p (iter j f a).
  by apply: (Knuth_axiom5c' afap afai) => //; rewrite Knuth_1.
have/IH {IH} : (k + i <= u)%N by apply/(leq_trans _ js)/(leq_trans (leqnSn _)).
rewrite -[(k + i)%N]/j.-1 -[j.-1.+1]/j => jm1jp.
have jin : iter j f a \in cover s' by rewrite iter_in.
have round : iter (u - 1) f (iter j f a) = iter j.-1 f a.
  rewrite -iter_add [(_ + j)%N](_ : _ = (j.-1 + u)%N).
    by rewrite /u iter_add size_orbit (iter_order_in s'st) //; apply: finj.
  by move: (leP j2) (leP js); ssr_lia.
have/(sc _ jin) : ((0 < 1 < u - 1) &&
                       (u - 1 < size (orbit f (iter j f a))))%N.
  by rewrite itereq -/u; to_lia; move: (leP js) (leP j2); ssr_lia.
rewrite round -iter_add add1n Knuth_1 => jm1jj11.
have jm2p1 : j.-2.+1 = j.-1 by move: (leP j2); ssr_lia.
have/(sc _ fain) : ((0 < j.-2 < j.-1) && (j.-1 < size (orbit f (f a))))%N.
  by rewrite (itereq 1%N) -/u; to_lia; move: (leP j2) (leP js); ssr_lia.
rewrite -Knuth_1 -!iterSr (ltn_predK j2) jm2p1 => jm1jfa.
apply: (Knuth_axiom5c' _ jm1jfa) => //; last by rewrite -Knuth_1.
have/(sc _ fain): ((0 < j.-1 < j) && (j < size (orbit f (f a))))%N.
  by rewrite (itereq 1%N) -/u; to_lia; move: (leP j2) (leP js); ssr_lia.
by rewrite -!iterSr (ltn_predK j2) -Knuth_1.
Qed.

Lemma partition_red_blue :
  exists b, exists i,
  b \in cover s' /\ (0 < i < size (orbit f b))%N /\
  (forall j, (j < i)%N -> ccw (iter j.+1 f b) (iter j f b) p) /\
  (forall j, (i <= j < size (orbit f b))%N ->
     ccw (iter j f b) (iter j.+1 f b) p).
Proof using s3 ss' ch pnotin gp not_in_hull.
have [b /andP [bin /andP [bblue fbnblue]]] := purple_point.
have chin := convex_hull_path_iter_in _ s3 ss' ch bin.
have fbred : ccw (f (f b)) (f b) p.
  move : (forallP (forallP (forallP gp (f (f b))) (f b)) p).
  rewrite cards3 (pnotiter bin 2) andbT andbC andbA andbC eq_sym
    (pnotiter bin 1) andTb.
  case: (ch_path_no_fixpoint s3 ss' ch (chin 1%N)) => -> _.
  rewrite !subE !(subsetP ss') ?orbT ?(negbTE fbnblue) ?orbF ?implyTb //.
    by rewrite (chin 1%N).
  by rewrite (chin 2%N).
have main : forall i,
     exists j, (0 < i -> 0 < j)%N /\ (j <= i)%N /\ 
       (forall k, (0 < k <= j)%N -> ccw (iter k.+1 f b) (iter k f b) p) /\
       (i != j -> ccw (iter j.+1 f b) (iter j.+2 f b) p).
  elim=> [ | [ | i] [j [j1 [ji [reds IH]]]]].
      exists 0%N; rewrite eqxx leqnn.
      split;[by [] | split; [by [] | split;[ | by []]]].
      by move=> k /andP [kgt0 kle0]; move: kgt0; rewrite ltnNge kle0.
    exists 1%N; rewrite eqxx leqnn.
    split;[by [] | split; [by [] | split;[ | by []]]].
    by move=> k; rewrite -eqn_leq=> /eqP <-; exact: fbred.
  have [/eqP i1j | /IH i1nj ] := boolP (i.+1 == j); last first.
    exists j; split.
      by move=> _; apply: j1.
    split; first by apply (leq_trans ji).
    by split;[exact: reds | rewrite i1nj].
  have [pos | neg ] := boolP (ccw (iter j.+1 f b) (iter j.+2 f b) p).
    exists j; split; first by [].
    split; first by rewrite -i1j.
    by split; [exact: reds | rewrite pos].
  move: (forallP (forallP (forallP gp (iter j.+2 f b)) (iter j.+1 f b)) p).
(* big duplication to work on. *)
  have pni : forall n, p != iter n f b by apply: pnotiter.
  have szi : #|[set iter j.+2 f b; iter j.+1 f b; p]| == 3.
    rewrite cards3 pni andbT andbC eq_sym pni andTb eq_sym.
    by rewrite eq_sym; case: (ch_path_no_fixpoint s3 ss' ch (chin j.+1)) => ->.
  have subi : [set iter j.+2 f b; iter j.+1 f b; p] \subset p |: s.
    apply/subsetP=> q; rewrite !inE => /orP [/orP [] /eqP -> | -> //];
    by apply/orP; right; apply: (subsetP ss').
  rewrite szi subi (negbTE neg) orbF implyTb => redj1.
  exists j.+1; rewrite -i1j leqnn eqxx in redj1 *.
  split;[ |split; [ | split]] => //.
  move=> k /andP [k0]; rewrite leq_eqVlt => /orP [/eqP -> // | klei1].
  by apply: reds; rewrite k0 -i1j -ltnS.
have [j [j1 [jcap [ltjred purple]]]] := main (size (orbit f b)).-1.
have s'st : {in cover s', forall x, f x \in cover s'}.
  by move=> x xin; apply: (convex_hull_path_iter_in 1 s3).
have true_purple : ccw (iter j.+1 f b) (iter j.+2 f b) p.
  have [/eqP <- | /purple //] := boolP ((size (orbit f b)).-1 == j).
  by rewrite size_orbit orderSpred iterS (iter_order_in s'st) //; apply: finj.
have := convex_hull_size_orbit_gt2 bin => so2.
have j1' : (0 < j)%N.
  move: so2.
  by rewrite size_orbit -orderSpred ltnS in j1 * => A; apply/j1/(ltn_trans _ A).
have fbin : f b \in cover s'.
  by move: ch => /'forall_implyP /(_ _ bin) => /andP [] /andP [].
have sof : size (orbit f (f b)) = size (orbit f b).
  have finj := convex_hull_path_inj s3 ss' ch.
  by have := size_orbit_in_eq s'st finj bin 1%N.
exists (f b), j => {j1}.
have jint : (0 < j < fingraph.order f (f b))%N.
  rewrite -size_orbit (size_orbit_in_eq s'st finj bin 1%N).
  by rewrite j1' (leq_ltn_trans jcap) // size_orbit orderSpred leqnn.
have ltjred' : forall i, (i < j)%N ->
      ccw (iter i.+1 f (f b)) (iter i f (f b)) p.
  by move=> i ij; rewrite -!iterSr; apply: ltjred; rewrite ij.
have bigred := red_path (s'st _ bin) jint ltjred'.
rewrite s'st ?bin ?j1'; repeat split => //; last first.
  move=> n nint; rewrite -!iterSr.
  move: bigred; rewrite -iterSr => bigred.
  apply: (contiguous_red _ _ _ bigred) => //.
  by rewrite ltnS -sof.
by rewrite sof -(ltn_predK so2) ltnS.
Qed.

Section fixed_red_subpath.

Variable b : P.

Hypothesis bin : b \in cover s'.

Hypothesis bred : ccw (f b) b p.

Variable i : nat.

Let nf := new_path f b (iter i f b) p.

Hypothesis ilto : (0 < i < fingraph.order f b)%N.

Hypothesis reds :
  forall k, (k < i)%N -> ccw (iter k.+1 f b) (iter k f b) p.

Hypothesis blues :
  forall k, (i <= k < fingraph.order f b)%N ->
  ccw (iter k f b) (iter k.+1 f b) p.

Hypothesis orbit_boundary :
  [set [set x; f x] | x in orbit f b] = boundary_edge ts.

Hypothesis in_hull_s' : forall x, 
   general_position (x |: s) -> x \notin s ->
   in_hull (cover s') f x -> [exists t, (t \in ts) && inside_triangle x t].

Lemma igt0 : (0 < i)%N.
Proof.
by case/andP: ilto. Qed.

Lemma pnb : p != b.
Proof using ss' pnotin bin.
by apply: (contraNN _ pnotin) => /eqP ->; apply: (subsetP ss').
Qed.

Lemma new_path_cycle' : iter (fingraph.order f b - i + 2) nf b = b.
Proof using s3 ss' ch pnotin bin bred ilto blues.
apply : (new_path_cycle convex_hull_stable finj pns' bin ilto).
by have := convex_hull_size_orbit_gt2 bin; apply: ltn_trans.
Qed.

Lemma new_path_c2 x :
  x \in orbit nf p -> #|[set x; nf x]| = 2.
Proof using coords s3 ss' ch pnotin bin bred ilto blues.
move => xino.
have sts' : {in cover s', forall y, f y \in cover s'} := convex_hull_stable.
have int' : (0 < fingraph.order f b - i + 1 <= fingraph.order f b - i + 1)%N.
  by rewrite leqnn addnS ltnS leq0n.
have ina : iter (fingraph.order f b - i + 2) 
                (new_path f b (iter i f b) p) b = b.
  by apply: new_path_cycle'.
suff : forall k, (k <= findex nf b x)%N ->
   #|[set iter k nf b; iter k.+1 nf b]| = 2.
  move=>/(_ (findex nf b x) (leqnn _)); rewrite iterS iter_findex //.
  apply: (connect_trans (fconnect1 _ _)); rewrite /nf new_path_a.
  by rewrite fconnect_orbit.
have base_case : #|[set iter 0 nf b; iter 1 nf b]| = 2.
  rewrite /=; apply/eqP; rewrite cards2 eqSS eqb1; apply/negP=> /eqP A.
  case/negP: pns'; rewrite -(_ : nf b = p); last by apply: new_path_a.
  by rewrite -A.
elim: (findex nf b x) => [ | l IH].
  move => k; rewrite leqn0 => /eqP ->; apply: base_case.
move=> k kl; have [ large | ] := boolP (fingraph.order f b - i + 2 <= k)%N.
  rewrite iterS -(subnK large) iter_add ina; apply: IH.
  rewrite -(leq_add2r (fingraph.order f b - i + 2)) subnK // 2!addnS -addSn.
  by rewrite (leq_trans kl) // leq_addr.
rewrite -ltnNge; move: kl; case: k => [_ _| k]; first by apply: base_case.
case: k => [_ _ | k].
  apply/eqP; rewrite /= /nf new_path_a new_path_p ?pnb // cards2 eqSS eqb1.
  rewrite eq_sym; apply/negP=> /eqP A; case/negP: pns'.
  by rewrite -A; apply: (convex_hull_path_iter_in i s3 ss' ch bin).
rewrite ltnS addnS ltnS => kl klto.
have kint : (0 < k.+2 <= fingraph.order f b - i + 1)%N by rewrite ltn0Sn klto.
have := new_path_offset sts' pns' bin ilto kint.
rewrite -[X in iter k.+2 _ X](_ : nf b = p); last by apply: new_path_a.
rewrite -iterSr => -> {kint}.
have kint : (0 < k.+1 <= fingraph.order f b - i + 1)%N.
  by rewrite ltn0Sn (ltn_trans _ klto).
have := new_path_offset sts' pns' bin ilto kint.
rewrite -[X in iter k.+1 _ X](_ : nf b = p); last by apply: new_path_a.
rewrite -iterSr => -> {kint}; rewrite !subSS !subn0 addSn iterS.
apply/eqP; rewrite cards2 eqSS eqb1 eq_sym.
suff itin : iter (k + i) f b \in cover s'.
  by have [-> _] := ch_path_no_fixpoint s3 ss' ch itin.
apply: (convex_hull_path_iter_in (k + i) s3 ss' ch bin).
Qed.

(* I need a lemma about the order of new_path for any point on the new
  convex hull (the first argument of new_path would suffice. *)

Lemma orbit_new_path_subset j :
   iter j nf b = p \/
   exists k, iter j nf b = iter k f b /\ (i <= k <= fingraph.order f b)%N.
Proof using coords s3 ss' ch pnotin bin bred ilto blues.
elim: j.+1 {-2}j (ltnSn j) => {j} [// | j' IH j]; rewrite ltnS => jj'.
have [/eqP -> | jn0 ] := boolP (j == 0%N).
  right; exists (fingraph.order f b); split.
    by rewrite (iter_order_in convex_hull_stable (convex_hull_path_inj s3 _ _)).
  by move/andP: ilto => [] _ /ltnW -> ; rewrite leqnn.
have [large | ] := boolP (fingraph.order f b - i + 2 <= j)%N.
  rewrite -(subnK large) iter_add new_path_cycle'.
  apply: IH.
  to_lia; move: jn0; rewrite -lt0n => /leP; move: (leP large) (leP jj').
  by ssr_lia.
rewrite -ltnNge addnS ltnS => small.
have := new_path_offset convex_hull_stable pns' bin ilto.
rewrite -/nf => /(_ j.-1).
have [/eqP -> _ | ] := boolP (j == 1%N).
   by left; rewrite /nf /= new_path_a.
move: jn0; rewrite -lt0n => jgt0; rewrite -(prednK jgt0) eqSS /= -lt0n => jgt1.
rewrite jgt1 (leq_trans _ small) ?leq_pred // => /(_ isT) => iterjm1.
right; rewrite -iterS iterSr (_ : nf b = p); last by rewrite /nf new_path_a.
rewrite iterjm1; exists (j.-1 -1 + i)%N; split => //.
rewrite leq_addl andTb.
have it : (i <= fingraph.order f b)%N.
  by move/andP: ilto => [] _ /ltnW ->.
rewrite -(subnK it) leq_add2r -(leq_add2r 1) subnK // (leq_trans _ small) //.
by apply: leq_pred.
Qed.

Lemma iter_b_boundary k : [set iter k f b; iter k.+1 f b] \in boundary_edge ts.
Proof using orbit_boundary.
by rewrite -orbit_boundary; apply/imsetP; exists (iter k f b).
Qed.

Lemma orbit_cover_s' : orbit f b =i cover s'.
Proof using s'_b orbit_boundary.
apply/subset_eqP/andP; split; apply/subsetP=> x.
  move=> xino; rewrite s'_b -orbit_boundary.
  apply/bigcupP; exists [set x; f x]; rewrite ?inE ?eqxx //; apply/imsetP.
  by exists x.
rewrite s'_b -orbit_boundary =>/bigcupP [] e /imsetP[] y yino ->.
rewrite !inE =>/orP [] /eqP -> //; move: yino; rewrite -!fconnect_orbit.
by move=> it; apply/(connect_trans it); apply:fconnect1.
Qed.

Section new_boundary_edge_sub_orbit.

Variable t e : {set P}.

Hypothesis ec2 : #|e| == 2.

Variable tq : [set t in ts :|:
                 \bigcup_(t' in ts)
                    [set p |: t' :\ q | q in [set q in t' | separated t' q p &&
                                       (t' :\ q \in boundary_edge ts)]] |
                  e \subset t] = [set t].

Lemma esubt : e \subset t.
Proof using tq. by move: (set11 t); rewrite -tq !inE => /andP[]. Qed.

Hypothesis pint : p \in t.

Lemma tnew :
  t \in \bigcup_(t in ts) [set p |: t :\ q | q in t & separated t q p &&
           (t :\ q \in boundary_edge ts)].
Proof using pnotin cover_ts tq pint.
move: (set11 t); rewrite -tq !inE => /andP[] /orP[]; last by [].
by move=> tint; case/negP: pnotin; rewrite -cover_ts; apply/bigcupP; exists t.
Qed.

Lemma p_in_e : p \in e.
Proof using pnotin cover_ts tq pint.
move: (tnew) => /bigcupP [] t' t'ts /imsetP [] q.
rewrite inE=> /andP[] qt' /andP [] qsp bde tval.
  (* looking at an edge that does not contain p, it is shared by two
     triangles, it contradicts tq. *)
have [// | pne ] := boolP (p \in e).
suff : t' \in [set t].
  rewrite in_set1 => /eqP A; case/negP: pnotin.
  by rewrite -cover_ts; apply/bigcupP; exists t'; rewrite // A tval !inE eqxx.
rewrite -tq !inE t'ts orTb andTb; apply/subsetP=> y ye.
move/(subsetP esubt): (ye) (ye).
by rewrite tval !inE => /orP [/eqP | /andP[] //] ->; rewrite (negbTE pne).
Qed.

Lemma b_or_i : (b \in e) || (iter i f b \in e).
Proof using ts3 ss' s3 s'_b s' reds orbit_boundary ch blues
        bin pnotin cover_ts ec2 tq pint.
move: (tnew) => /bigcupP [] t' t'ts /imsetP [] q.
rewrite inE=> /andP[] qt' /andP [] qsp bde tval.
move: (bde); rewrite -orbit_boundary => /imsetP [] x xino t'mq.
have t_dec : t = [set x; f x; p] by rewrite setUC -t'mq.
set xi := findex f b x.
have xiter : x = iter xi f b.
  by move: xino; rewrite -fconnect_orbit => /iter_findex ->.
have xnp : x != p by rewrite xiter eq_sym pnotiter.
have pnfx : p != f x by rewrite xiter -iterS pnotiter.
have t'dec : t' = [set x; f x; q] by apply: set3_D1_dec; rewrite ?ts3.
have xcov : x \in cover s' by rewrite -orbit_cover_s'.
have [xilti | ] := boolP (findex f b x < i)%N; last first.
  rewrite -leqNgt => xigei.
  have/blues : (i <= findex f b x < fingraph.order f b)%N.
    by rewrite xigei findex_max // fconnect_orbit.
  rewrite iterS -xiter=> A; move: qsp.
  rewrite t'dec -separated_immediate; last first.
    by apply: oriented_triangle; rewrite // -t'dec.
  by rewrite (negbTE (Knuth_2 A)).
have [xine | xnot] := boolP(x \in e).
  have [xigt0 | ] := boolP (0 < xi)%N; last first.
    by rewrite -eqn0Ngt => /eqP xi0; move: xine; rewrite xiter xi0 => ->.
  have xpsub : [set x; p] \subset e.
    by apply/subsetP=> y; rewrite !inE=> /orP[] /eqP => -> //; apply: p_in_e.
  have xpc2 : #|[set x; p]| = 2 by apply/eqP; rewrite cards2 eqSS eqb1.
  have exp : e = [set x; p].
    have := subset_leqif_cards xpsub; rewrite (eqP ec2) xpc2 eq_sym.
    by move=>/leqif_refl/eqP.
  set e2 := [set iter xi.-1 f b; f (iter xi.-1 f b)].
  have e2_bd : e2 \in boundary_edge ts.
    rewrite -orbit_boundary; apply/imsetP.
    by exists (iter (findex f b x).-1 f b).
  move: (e2_bd); rewrite !inE => /andP[] /eqP e'2 /cards1P[] t2 t2q.
  move: (set11 t2); rewrite -t2q inE => /andP[] t2ts e2t2.
  have /eqP/cards1P [q2 q2v] : #| t2 :\: e2 | = 1%N.
    by rewrite cardsD (ts3 t2ts) (setIidPr e2t2) e'2.
  have t2_dec : t2 = e2 :|: [set q2].
    by apply: set3_D2_dec; rewrite // ?(ts3 t2ts) // -/e2.
  have im1in : iter (findex f b x).-1 f b \in cover s'.
    apply/bigcupP; exists e2; first by rewrite s'_b.
    by rewrite !inE eqxx.
  have/eqP : #|t2| = 3 by apply: ts3.
  rewrite t2_dec cards3 -iterS (prednK xigt0) -xiter=>/andP[] /andP[] _.
  move=> q2nx q2n.
  have := ch_path_no_fixpoint s3 ss' ch im1in => [] [] dif1 dif2.
  have : iter xi.-1 f b \in p |: t2 :\ q2.
    by rewrite t2_dec !inE eqxx !orTb andbT -(eq_sym q2) q2n orbT.
  move: dif1 dif2; rewrite -iterS (prednK xigt0) -xiter => dif1 dif2.
  suff : p |: t2 :\ q2 \in [set t].
    rewrite inE => /eqP ->.
    rewrite t_dec !inE -(eq_sym x) -(eq_sym (f x)) -(eq_sym p).
    by rewrite (negbTE dif1) (negbTE dif2) (negbTE (pnotiter bin _)).
  rewrite -tq inE; apply/andP; split; last first.
    by rewrite t2_dec; apply/subsetP=> y; rewrite exp !inE => /orP[]/eqP ->;
      rewrite ?(negbTE xnp) /= ?q2nx ?(negbTE q2nx) -iterS (prednK xigt0)
        -?xiter eqxx ?orbT.
  rewrite inE; apply/orP; right; apply/bigcupP; exists t2 => //.
  apply/imsetP; exists q2; rewrite // inE.
  move: (set11 q2); rewrite -q2v inE => /andP[] q2ne2 q2t2.
  rewrite q2t2 q2v andTb.
  have t2mq2 : t2 :\ q2 = e2.
    by rewrite -q2v setDDr (setIidPr e2t2) setDv set0U.
  rewrite t2mq2 e2_bd andbT t2_dec -separated_immediate; last first.
    by apply: oriented_triangle; rewrite // -t2_dec.
  by apply: reds; rewrite (prednK xigt0) leq_eqVlt xilti orbT.
have/eqP/cards1P[a aP] : #|e :\ p| = 1%N.
  by rewrite cardsD (eqP ec2) (setIidPr _) ?sub1set ?cards1 ?p_in_e.
have fxe : f x \in e.
  have ae : a \in e by move: (set11 a); rewrite -aP !inE=>/andP[].
  move: (aP); have : a \in t by apply (subsetP esubt).
  rewrite t_dec !inE => /orP[/orP [] | ] /eqP -> // A.
      by case/negP: xnot; move: (set11 x); rewrite -A !inE=>/andP[].
    by move: (set11 (f x)); rewrite -A inE=> /andP[].
  by move: (set11 p); rewrite -A !inE eqxx.
move: xilti (fxe); rewrite leq_eqVlt=> /orP [/eqP xip1i | xip1lti].
  by rewrite xiter -iterS xip1i => ->; rewrite orbT.
(* In this case  [x; f x] and [f x; f (f x)] are red, and e is [p; f x] *)
have fxcov : f x \in cover s' by rewrite xiter -iterS -orbit_cover_s'.
suff : [set f x; f (f x); p] \in [set t].
  have := ch_path_no_fixpoint s3 ss' ch xcov => [] [] dif1 dif2.
  have := ch_path_no_fixpoint s3 ss' ch fxcov => [] [] dif3 _.
  rewrite in_set1 => /eqP A.
  have : f (f x) \in t by rewrite -A !inE eqxx ?orbT.
  rewrite t_dec !inE (negbTE dif2) -!(eq_sym p) (negbTE dif3).
  by rewrite xiter -!iterS (negbTE (pnotiter _ _)).
rewrite -tq; set e2 := [set f x; f (f x)].
have : e2 \in boundary_edge ts.
  rewrite -orbit_boundary; apply/imsetP; exists (f x) => //.
  by rewrite orbit_cover_s'.
rewrite inE => /andP [] e2c2 /cards1P [] t2 t2q; move: (set11 t2).
rewrite -t2q !inE=> /andP [] t2ts e2subt2.
have t2c3 : #|t2| = 3 by apply: ts3.
have /cards1P [ q2 q2val ]: #|t2 :\: e2| == 1%N.
  by rewrite cardsD (setIidPr e2subt2) t2c3 (eqP e2c2).
have t2_dec : t2 = e2 :|: [set q2] by apply: set3_D2_dec.
apply/andP; split; last first.
  have -> : e = [set p; f x].
    have /subset_leqif_cards :
        [set p; f x] \subset e by rewrite subUset !sub1set fxe p_in_e.
    by rewrite (eqP ec2) cards2 pnfx=> /leqif_refl/eqP ->.
  by rewrite subUset !sub1set !inE ?eqxx ?orbT.
apply/orP; right.
have  t2mq2 : t2 :\ q2 = e2.
  by rewrite -q2val setDDr (setIidPr e2subt2) setDv set0U.
have q2t2 : q2 \in t2 by rewrite t2_dec !inE eqxx ?orbT.
apply/bigcupP; exists t2 => //; apply/imsetP; exists q2; last first.
  by rewrite t2mq2 setUC.
rewrite !inE t2mq2 (eqP e2c2) q2t2 t2q cards1 ?eqxx ?andTb ?andbT.
rewrite t2_dec -separated_immediate.
  by rewrite xiter -!iterS; apply: reds.
by apply: oriented_triangle; rewrite -?t2_dec.
Qed.

Lemma boundary_edge_new_triangle :
  e \in
     [set [set x; nf x] | x in orbit nf p].
Proof using ts3 ts tq t ss' s3 s'_b reds pnotin pint orbit_boundary ilto ec2
cover_ts bred ch blues bin.
move: (tnew) => /bigcupP [] t' t'ts /imsetP [] q.
rewrite inE=> /andP[] qt' /andP [] qsp bde tval.
(* looking at new edges containing p *)
have /orP [bine | iine] := b_or_i.
(* looking at the edge from b to p *)
  have bpsub : [set b; p] \subset e by rewrite !subE p_in_e bine.
  have bpc2 : #|[set b; p]| = 2 by rewrite cards2 eq_sym pnb.
  have ebp : e = [set b; p].
    have := subset_leqif_cards bpsub; rewrite (eqP ec2) bpc2 eq_sym.
    by move=>/leqif_refl/eqP.
  apply/imsetP; exists b; last by rewrite new_path_a.
  by rewrite -/nf -new_path_cycle' addnS iterSr new_path_a.
(* looking at the edge from p to (nf p) *)
have psub : [set p; iter i f b] \subset e by rewrite !subE p_in_e iine.
have bpc2 : #|[set p; iter i f b]| = 2 by rewrite cards2 pnotiter.
have ebp : e = [set p; iter i f b].
  have := subset_leqif_cards psub; rewrite (eqP ec2) (bpc2) eq_sym.
  by move=>/leqif_refl/eqP.
apply/imsetP; exists p; last by rewrite new_path_p // pnb.
by rewrite -fconnect_orbit connect0.
Qed.

End new_boundary_edge_sub_orbit.

Lemma orbit_sub_boundary_edge x :
  x \in orbit nf p -> [set x; nf x] \in
  boundary_edge (ts :|:
                 \bigcup_(t in ts)
                    [set p |: t :\ q | q in t & separated t q p &&
                            (t :\ q \in boundary_edge ts)]).
Proof using s3 ss' s'_b ts3 pnotin reds orbit_boundary ilto cover_ts ch bred
  blues bin.
move: (ilto) => /andP[] _ ilo xino.
rewrite inE new_path_c2 // eqxx andTb.
move: xino; have [/eqP -> dummy { x } | xnb ] := boolP (x == b).
  apply/cards1P; exists [set p; b; f b]; rewrite -setP=> t.
  rewrite !inE; have [/eqP -> | wrongt] := boolP (t == [set p; b; f b]).
(* probably duplicated *)
    have /negbTE -> : [set p; b; f b] \notin ts.
      apply/negP=> A; case/negP: pnotin; rewrite -cover_ts.
      by apply/bigcupP; exists [set p; b; f b] => //; rewrite !inE eqxx.
    have : [set b; f b] \in boundary_edge ts by apply: (iter_b_boundary 0%N).
    rewrite /boundary_edge inE => /andP [] bc2 /cards1P [] u uP.
    have [uts bsubu] : u \in ts /\ [set b; f b] \subset u.
        by move: (set11 u); rewrite -uP !inE => /andP [].
    have/eqP/cards1P [q qP] : #|u :\: [set b; f b]| = 1%N.
      by rewrite cardsD (ts3 uts) (setIidPr bsubu) (eqP bc2).
    have uc3 : #|u| = 3 by apply: ts3.
    have u_dec : u = [set b; f b; q] by apply: set3_D2_dec.
    have qu : q \in u by rewrite u_dec !inE eqxx !orbT.
    have qs : q \in s by rewrite -cover_ts; apply/bigcupP; exists u.
    have /andP [qnfb qnb] : (q != f b) && (q != b).
      by have/eqP := uc3; rewrite u_dec cards3 -!(eq_sym q) -andbA=> /andP[].
    have qinhull : ccw b (f b) q.
      by apply: oriented_triangle; rewrite // -u_dec.
    have umq : u :\ q = [set b; f b].
      by rewrite -qP setDDr setDv set0U (setIidPr bsubu).
    rewrite orFb; apply/andP; split; last first.
      by rewrite /nf new_path_a setUC subsetU // subxx.
    apply/bigcupP; exists u=> //; apply /imsetP; exists q; last first.
      by rewrite umq setUA.
    rewrite inE; apply/andP; split.
      by move: (set11 q); rewrite -qP !inE => /andP [].
    apply/andP; split; last by rewrite umq inE bc2 uP cards1.
    by rewrite u_dec -separated_immediate // Knuth_1.
  have [bnfb | ] := boolP ([set b; nf b] \subset t); last by rewrite andbF.
  rewrite andbT; have /negbTE -> : t \notin ts.
    apply: (contraNN _ pnotin) => A; rewrite -cover_ts; apply/bigcupP.
    exists t => //; apply: (subsetP bnfb). 
    by rewrite /nf new_path_a !inE eqxx orbT.
  rewrite orFb; apply/negP=>/bigcupP [] t' t'ts /imsetP [] q.
  rewrite 1!inE=> /andP[] qt' /andP[] qsp.
  have qin : q \in s by rewrite -cover_ts; apply/bigcupP; exists t'.
  move=> t'mqb t_dec; move: (t'mqb); rewrite -orbit_boundary.
  move/imsetP=> [] b' b'ino t'q_dec.
  have b'ins' : b' \in cover s' by rewrite -orbit_cover_s'.
  have/eqP := setD1K qt'; rewrite t'q_dec setUC eq_sym=> /eqP t'dec.
  have /andP [qnfb' qnb'] : (q != f b') && (q != b').
    have/eqP := ts3 t'ts.
    by rewrite t'dec cards3 -!(eq_sym q) -andbA=> /andP[].
  have bint' : b \in t' :\ q.
    move: bnfb; rewrite t_dec subUset sub1set andbC => /andP [] _.
    by rewrite !inE eq_sym (negbTE pnb).
  move: bint'; rewrite t'q_dec !inE => /orP [/eqP bb' | /eqP bfb'].
    by case/negP: wrongt; rewrite bb' -setUA -t'q_dec -t_dec eqxx.
  move: qsp; rewrite t'dec -separated_immediate; last first.
    by apply: oriented_triangle; rewrite // -t'dec.
  have /blues : (i <= (fingraph.order f b).-1 < fingraph.order f b)%N.
    rewrite orderSpred leqnn andbT -ltnS orderSpred.
    by move/andP: ilto => [].
  rewrite orderSpred (iter_order_in convex_hull_stable) => //; last first.
    by apply: (convex_hull_path_inj s3).
  have -> : iter (fingraph.order f b).-1 f b = b'.
    apply: (convex_hull_path_inj s3 ss' ch) => //.
      by apply: (convex_hull_path_iter_in _ s3).
    rewrite -iterS orderSpred (iter_order_in convex_hull_stable) //.
    by apply: (convex_hull_path_inj s3 ss' ch).
  by rewrite bfb'=> /(Knuth_2 (P := P)) /negbTE ->.
have [/eqP -> _ | ] := boolP (x == p).
apply/cards1P; exists [set p; nf p; iter i.-1 f b]; rewrite -setP => t.
  rewrite in_set1; have [/eqP -> | wrongt] := boolP (_ == _).
    rewrite !inE; apply/andP; split; last by rewrite subsetU // subxx.
    apply/orP; right.
    have : [set iter i.-1 f b; iter i f b] \in boundary_edge ts.
      rewrite -orbit_boundary; apply/imsetP; exists (iter i.-1 f b) => //.
      by rewrite -iterS prednK // igt0.
    rewrite inE => /andP [] c2 /cards1P [] u uP; apply/bigcupP; exists u.
      by move: (set11 u); rewrite -uP inE => /andP [].
    move: (set11 u); rewrite -uP !inE => /andP [] uts subu.
    have/eqP/cards1P [q qP]: #|u :\: [set iter i.-1 f b; iter i f b]| = 1%N.
      by rewrite cardsD (ts3 uts) (setIidPr subu) (eqP c2).
    have u_dec : u = [set iter i.-1 f b; iter i f b; q].
      by apply: set3_D2_dec => //; apply: ts3.
    have im1bin : iter i.-1 f b \in cover s'.
      by apply: (convex_hull_path_iter_in _ s3 ss' ch bin).
    have istep : iter i f b = f (iter i.-1 f b).
      by rewrite -iterS prednK // igt0.
    have qinhull : ccw (iter i.-1 f b) (iter i f b) q.
      by rewrite istep; apply: oriented_triangle; rewrite // -istep -u_dec.
    have im1red : ccw (iter i f b) (iter i.-1 f b) p.
      have /reds : (0 <= i.-1 < i)%N by rewrite prednK ?igt0 // leq0n leqnn.
      by rewrite prednK // igt0.
    have umq : u :\ q = [set iter i.-1 f b; iter i f b].
      by rewrite -qP setDDr setDv set0U (setIidPr subu).
    rewrite setUAC -setUA /nf new_path_p; last by rewrite pnb.
    rewrite -umq; apply/imsetP; exists q => //.
    rewrite inE; apply/andP; split.
      by move: (set11 q); rewrite -qP !inE => /andP [].
    apply/andP; split.
      by rewrite u_dec -separated_immediate // Knuth_1.
    by rewrite umq inE c2 uP cards1.
  rewrite !inE; have [pnfp |] := boolP ([set p; nf p] \subset t); last first.
    by rewrite andbF.
  rewrite andbT; have /negbTE -> /= : t \notin ts.
    apply: (contraNN _ pnotin)=> A; rewrite -cover_ts; apply/bigcupP.
    by exists t => //; apply: (subsetP pnfp); rewrite !inE eqxx.
  apply/negP=>/bigcupP [] t' t'ts /imsetP [] q.
  rewrite inE=> /andP[] qt' /andP[] qsp.
  move=> t'mqb t_dec; move: (t'mqb); rewrite -orbit_boundary.
  move/imsetP=> [] b' b'ino t'q_dec.
  have b'ins' : b' \in cover s' by rewrite -orbit_cover_s'.
  have t'3 : #|t'| = 3 by apply:ts3.
  have t'_dec : t' = [set b'; f b'; q] by apply: set3_D1_dec.
  have pni : p != iter i f b.
    apply/negP=> /eqP A; case/negP: pnotin; rewrite (subsetP ss') // A.
    by apply: (convex_hull_path_iter_in _ s3 ss' ch).
  have nfpnp : nf p != p by rewrite /nf new_path_p ?pnb // eq_sym.
  have bint' : iter i f b \in t' :\ q.
    move: pnfp; rewrite t_dec subUset sub1set => /andP [] _.
    by rewrite sub1set !inE (negbTE nfpnp) /nf new_path_p ?pnb // eq_sym.
  move: bint'; rewrite t'q_dec !inE => /orP [/eqP bb' | /eqP bfb']; last first.
    have im1fbb' : iter i.-1 f b = b'.
      apply: (convex_hull_path_inj s3 ss' ch) => //.
        by apply (convex_hull_path_iter_in _ s3 ss' ch).
      by rewrite -iterS prednK // igt0.
    case/negP: wrongt; rewrite new_path_p; last by rewrite pnb.
    by rewrite bfb' im1fbb' -setUA (setUC [set f b']) -t'q_dec t_dec eqxx.
  move: qsp; rewrite t'_dec -separated_immediate; last first.
    by apply: oriented_triangle; rewrite // -t'_dec.
  have /blues : (i <= i < fingraph.order f b)%N.
    by move: ilto=> /andP []; rewrite leqnn.
  by rewrite iterS bb' => /(Knuth_2 (P := P))/negbTE ->.
move=> xnp xino; apply/cards1P.
have [j [jq jint]]:
    exists j,  x = iter j f b /\ (i <= j < fingraph.order f b)%N.
  have xfc : (fconnect nf b x).
    apply: (connect_trans (fconnect1 _ _)).
    by rewrite /nf new_path_a -/nf fconnect_orbit.
  rewrite -(iter_findex xfc).
  have := orbit_new_path_subset (findex nf b x).
  rewrite -/nf (iter_findex xfc); case => [A | [j [jq /andP [ij]]]].
    by move: xnp; rewrite A eqxx.
  rewrite leq_eqVlt => /orP [/eqP A | jlto]; last first.
    by exists j; rewrite ij jlto jq.
  by move: xnb; rewrite jq A (iter_order_in convex_hull_stable finj) // eqxx.
have fxnfx : f x = nf x.
  by rewrite /nf ffunE /= (negbTE xnb) (negbTE xnp).
have : [set x; f x] \in boundary_edge ts.
  rewrite -orbit_boundary; apply/imsetP; exists x => //.
  by rewrite jq.
rewrite inE => /andP [] cx2 /cards1P => [[t tq]]; exists t.
apply/setP/subset_eqP; apply/andP; split; apply/subsetP => t'; rewrite !inE;
  last first.
  by move/eqP=> ->; move: (set11 t); rewrite -tq !inE fxnfx => /andP[] ->.
move/andP=> [] /orP [tin | /bigcupP [] t2 t2ts t2p] te.
  by rewrite -in_set1 -tq !inE tin fxnfx te.
move/imsetP: t2p=> [q]; rewrite !inE => /andP [] qt2 /andP [] qsp.
move=> /andP [] t2c t2c1 t'_dec.
have t2_enum : t2 = [set x; f x; q].
  have t2_c3 : #|t2| = 3 by apply ts3.
  have pnt2 : p \notin t2.
    apply/negP=> pint2; case/negP: pnotin; rewrite -cover_ts.
    by apply/bigcupP; exists t2.
  have pnq : p != q by apply: (contraNN _ pnt2) => /eqP ->.
  have pnfx : p != f x by rewrite jq -iterS; apply: pnotiter.
  have qnt' : q \notin t'.
    by rewrite t'_dec !inE eqxx orbF eq_sym.
  have e_c3 : #|[set x; f x; q]| = 3.
    rewrite cardsU cards1 (eqP cx2) (_ : #|_ :&: [set q]| = 0%N) //.
    apply/eqP; rewrite cards_eq0 setIUl setU_eq0 !setI1 !inE.
    by apply/andP; split; apply/negP; move=>/eqP A; case/negP: qnt';
      apply: (subsetP te); rewrite !inE A ?fxnfx ?eqxx ?orbT.
  have xfxsub2 : [set x; f x] \subset t2.
    move: te; rewrite t'_dec !subUset !sub1set !inE (negbTE xnp) -!(eq_sym p).
    rewrite -fxnfx (negbTE pnfx) !orFb=> /andP [].
    by move=> /andP [] _ -> /andP [] _ ->.
  have /subset_leqif_cards : [set x; f x; q] \subset t2.
    by rewrite subUset xfxsub2 sub1set qt2.
  by rewrite e_c3 t2_c3 => /leqif_refl/eqP ->.
move: qsp; rewrite t2_enum -separated_immediate; last first.
  apply: oriented_triangle.
    by have := convex_hull_path_iter_in j s3 ss' ch bin; rewrite -jq.
  by rewrite -t2_enum.
by move: (negbTE (Knuth_2 (blues jint))); rewrite jq -iterS=> ->.
Qed.

Lemma orbit_new_path_boundary_edge :
     [set [set x; nf x] | x in orbit nf p] =
     boundary_edge (ts :|:
                    \bigcup_(t in ts)
                        [set p |: t :\ q | q in t & separated t q p &&
                                (t :\ q \in boundary_edge ts)]).
Proof using s3 ss' s'_b ts3 pnotin reds orbit_boundary ilto cover_ts ch bred
  blues bin.
move: (ilto) => /andP[] _ ilo.
apply/setP/subset_eqP/andP; split; apply/subsetP => e.
  move=> /imsetP => [[]] x xino -> { e }.
  by apply: orbit_sub_boundary_edge.
rewrite inE => /andP [ec2] /cards1P=> [] [] t tq.
move: (set11 t); rewrite -tq !inE => /andP [] /orP [tint | tnew] esubt.
  have ebts : e \in boundary_edge ts.
    rewrite inE ec2 andTb; apply/cards1P; exists t.
    apply/setP/subset_eqP/andP; split; apply/subsetP; last first.
      by move=> t'; rewrite !inE => /eqP ->; rewrite tint esubt.
    by move=> t'; rewrite inE => /andP[] it ?; rewrite -tq !inE it.
  move: (ebts); rewrite -orbit_boundary => /imsetP [] x xino exfx.
  have/cards1P [q qP] : #|t :\: [set x; f x]| == 1%N.
    by rewrite cardsD -exfx (setIidPr esubt) (ts3 tint) (eqP ec2).
  have t_dec : t = [set x; f x; q] by apply: (set3_D2_dec (ts3 tint)).
  have [e_red | ]:= boolP (findex f b x < i)%N.
    suff A : p |: t :\ q \in [set t].
      case/negP: pnotin; rewrite -cover_ts; apply/bigcupP; exists t => //.
      by move: A; rewrite in_set1 => /eqP <-; rewrite !inE eqxx.
    rewrite -tq !inE; apply/andP; split; last first.
      have /eqP := ts3 tint; rewrite t_dec cards3 exfx -?(eq_sym q).
      move=> /andP[]/andP[] ? ? ?.
      by apply/subsetP=> y; rewrite !inE =>/orP[]/eqP->;
        rewrite eqxx ?orbT andbT -?(eq_sym q); apply/orP; right.
    apply/orP; right.
    apply/bigcupP; exists t => //; apply/imsetP; exists q; rewrite // inE.
    have -> : q \in t by rewrite t_dec !inE eqxx ?orbT.
    have tmq : t :\ q = e.
      by rewrite -qP setDDr setDv set0U -exfx (setIidPr esubt).
    have -> : t :\ q \in boundary_edge ts by rewrite tmq ebts.
    rewrite andTb andbT t_dec -separated_immediate; last first.
      have xc : x \in cover s' by rewrite -orbit_cover_s'.
      by apply: oriented_triangle; rewrite -?t_dec.
    by move: (xino); rewrite -fconnect_orbit => /iter_findex <-; apply: reds.
  rewrite -leqNgt => e_blue.
  have int : (0 < findex f b x - i + 1 <= fingraph.order f b - i + 1)%N.
    move: (xino); rewrite -fconnect_orbit => /findex_max bndi. 
    by to_lia; move: (leP igt0) (leP ilo) (leP bndi) (leP e_blue); ssr_lia.
  have := new_path_offset convex_hull_stable pns' bin ilto int.
  rewrite addnK (subnK e_blue) => offset.
  have xnb : x != b.
    by apply/negP=> /eqP A; move: e_blue; rewrite leqNgt A findex0 igt0.
  have xnp: x != p.
    apply: (contraNN _ pnotin) => /eqP <-.
    by rewrite (subsetP ss') // -orbit_cover_s'.
  apply/imsetP; exists x; last first.
    by rewrite /new_path ffunE /= (negbTE xnb) (negbTE xnp).
  by move: (xino); rewrite -fconnect_orbit => /iter_findex <-; rewrite -offset.
apply: (boundary_edge_new_triangle ec2 tq).
by move/bigcupP: tnew => [] t' _ /imsetP [] => [] q _ ->; rewrite !inE eqxx.
Qed.

(* TODO : streamline this interface to be like neighbors *)
Lemma add_point_out_new_convex_hull_path :
  convex_hull_path (p |: s) [set [set x; nf x]| x : P in orbit nf p] nf.
Proof using s3 ss' ch pnotin ilto bin bred reds blues.
move: (ilto) => /andP[] _ ilo.
apply/forallP => x.
have s'st : {in cover s', forall x, f x \in cover s'}.
  by move=> {x} x xin; apply:(convex_hull_path_iter_in 1 s3).
have chin j := convex_hull_path_iter_in j s3 ss' ch bin.
have blue : ccw (iter i f b) (iter i.+1 f b) p.
  by apply: blues; rewrite leqnn; move: ilto=> /andP[].
have i0 : (0 < i)%N.
  rewrite lt0n; apply/negP=> /eqP a; move: blue.
  by rewrite a (negbTE (Knuth_2 bred)).
apply/implyP=> /bigcupP [] e /imsetP [] x0 x0o e_is xine.
have e_sub_o : e \subset orbit nf p.
  apply/subsetP=> y; rewrite e_is !inE => /orP [] /eqP -> //.
  by rewrite -!fconnect_orbit in x0o *; apply/(connect_trans x0o)/fconnect1.
have xin : x \in orbit nf p by apply: (subsetP e_sub_o).
have -> : nf x \in cover [set [set x1; nf x1] | x1 in orbit nf p].
  move: xine; rewrite e_is !inE => /orP [] /eqP ->.
    apply/bigcupP; exists e; last by rewrite e_is !inE eqxx orbT.
    by apply/imsetP; exists x0.
  apply/bigcupP; exists [set (nf x0); nf (nf x0)]; last first.
    by rewrite !inE eqxx orbT.
  apply/imsetP; exists (nf x0) => //.
  by apply: (subsetP e_sub_o); rewrite e_is !inE eqxx orbT.
rewrite andTb; apply/andP; split; last first.
  apply/eqP/setP/subset_eqP/andP;split; apply/subsetP=> y;
    move/imsetP=>[] u; rewrite ?inE=> v1 v2; apply/imsetP; exists u=> //.
    have bop : fconnect nf p b.
      by rewrite -new_path_cycle' addnS iterSr new_path_a fconnect_iter.
    rewrite -fconnect_orbit; apply: (connect_trans bop).
    move: v1; rewrite -fconnect_orbit; apply: connect_trans.    
    move: xin; rewrite -fconnect_orbit; apply: connect_trans => //.
    by rewrite -(new_path_a f b (iter i f b) p) fconnect1.
  move: v1; rewrite -!fconnect_orbit; apply: connect_trans.
  have sz : (1 < size (orbit f b))%N.
    by apply: ltn_trans (convex_hull_size_orbit_gt2 bin).
  have xob : x \in orbit nf b.
    rewrite -fconnect_orbit (connect_trans (fconnect1 _ _)) // /nf new_path_a.
    by rewrite -/nf fconnect_orbit.
  rewrite /nf -[X in fconnect _ _ X](new_path_a f b (iter i f b) p) -/nf.
  apply: (connect_trans _ (fconnect1 nf b)); rewrite fconnect_orbit.
  by apply: (new_path_orbit_sym _ finj pns' bin ilto sz xob).
apply/forallP => q; apply/implyP=> qin.
apply/implyP=>/andP [qnx qnf].
have pn' : p \notin cover s' by apply/negP=>/(subsetP ss'); apply/negP.
have := new_path_offset s'st pn' bin ilto; rewrite -/nf => fnf.
have nfpfi : nf p = iter i f b.
  by rewrite -[nf p]/(iter 1 nf p) fnf //=; to_lia; move: (leP ilo); ssr_lia.
have flnp l : iter l f b != p by rewrite eq_sym; apply: pnotiter.
have bnfib : b <> iter i f b.
  by have/iter_lt_order_neq //: (0 < i < fingraph.order f b)%N; rewrite ilo i0.
have ito := iter_order_in s'st finj bin.
have fibnb : iter i f b != b by rewrite eq_sym; apply/eqP.
have nfps' : nf p \in cover s' by rewrite nfpfi; apply: chin.
have pxcase (px : p = x) : ccw x (nf x) q.
  have qs : q \in s by move: qin qnx; rewrite !inE -px => /orP [ ->| ].
  have nfnfp : nf (nf p) = iter i.+1 f b.
    rewrite -[nf p]/(iter 1 nf p) -iterS fnf //=.
    by to_lia; move: (leP ilo); ssr_lia.
  have c1 : ccw (nf p) (nf (nf p)) p.
    by rewrite nfnfp nfpfi; apply: blues; rewrite leqnn.
  have [/eqP -> | qnnf2p ] := boolP (q == nf (nf p)).
    by rewrite -px -Knuth_1.
  have fnfp : f (nf p) = nf (nf p).
    by rewrite nfpfi /nf /new_path ffunE /= (negbTE (flnp i)) (negbTE fibnb).
  have := (use_convex_hull_path ch nfps'); rewrite fnfp => chnfp.
  have c2 : ccw (nf p) (nf (nf p)) q.
    by move: (chnfp q qs); rewrite qnnf2p px qnf; apply.
  have im1s : iter i.-1 f b \in cover s' by apply: chin.
  have im1nff : iter i.-1 f b != nf (nf p).
    have [_]:= ch_path_no_fixpoint s3 ss' ch im1s; rewrite nfnfp eq_sym.
    by rewrite -!iterS prednK.
  have c3 : ccw (nf p) (nf (nf p)) (iter i.-1 f b).
    move: (chnfp (iter i.-1 f b)); rewrite im1nff.
    rewrite (subsetP ss' _ im1s).
    suff -> : iter i.-1 f b != nf p by apply.
    rewrite nfpfi eq_sym.
    by have [] := ch_path_no_fixpoint s3 ss' ch im1s; rewrite -iterS prednK.
  have imp1 : iter i.-1.+1 f b = nf p by rewrite prednK.
  have c5 : ccw (nf p) (iter i.-1 f b) p.
    by rewrite -imp1; apply reds; rewrite prednK // leq0n leqnn.
  have [/eqP -> | qnim1 ] := boolP (q == iter i.-1 f b).
    by rewrite -px -Knuth_1.
  have c4 : ccw (nf p) q (iter i.-1 f b).
    have := use_convex_hull_path ch im1s qs qnim1.
    by rewrite -iterS prednK // -nfpfi px qnf -Knuth_1; apply.
  by rewrite -px -Knuth_1; have := Knuth_axiom5c c2 c3 c1 c4 c5.
have nfb : iter (fingraph.order f b - i + 1) nf p = b.
  rewrite fnf; last by to_lia; move: (leP ilo); ssr_lia.
  by rewrite addnK subnK ?ito //; to_lia; move: (leP ilo); ssr_lia.
have nfp : iter (fingraph.order f b - i + 2) nf p = p.
  by rewrite addnS iterS nfb /nf /new_path ffunE /= eqxx.
have onfbnd : (fingraph.order nf p <= fingraph.order f b - i + 2)%N.
  rewrite leqNgt; apply/negP => abs.
  have/iter_lt_order_neq :
    (0 < fingraph.order f b - i + 2 < fingraph.order nf p)%N.
    by rewrite abs andbT; to_lia; move: (leP ilo); ssr_lia.
  by move/eqP; rewrite /= eq_sym nfp eqxx.
set j' := (fingraph.order f b - i)%N.
have j's : (j' + i = (j' - 1 + i).+1)%N.
  by rewrite /j'; move: (leP ilo) (leP i0); ssr_lia.
have ch' := use_convex_hull_path ch (chin (fingraph.order f b).-1).
have j'nf : iter j' nf p = iter (fingraph.order f b).-1 f b.
  rewrite fnf; last by to_lia; move:(leP i0) (leP ilo); rewrite/j'; ssr_lia.
  by congr (iter _ _ _); move:(leP i0) (leP ilo); rewrite/j'; ssr_lia.
have j'1nf : iter j'.+1 nf p = b.
  rewrite fnf; last by to_lia; move:(leP i0) (leP ilo); rewrite/j'; ssr_lia.
  rewrite subn1 /= /j' subnK ?ito //.
  by to_lia; move:(leP ilo); rewrite/j'; ssr_lia.
have j'blue : ccw (iter j' nf p) (iter j'.+1 nf p) p.
  rewrite j'nf j'1nf -[X in ccw _ X _]ito -orderSpred.
  by apply: blues; to_lia; move: (leP ilo); ssr_lia.
have bxcase (bx : b = x) : ccw x (nf x) q.
  move: (qnf); rewrite -bx /nf /new_path ffunE /= eqxx => qnp.
  have qs : q \in s by move: qin; rewrite !inE (negbTE qnp).
  have [/eqP -> | qnbm1 ] := boolP (q == iter (fingraph.order f b).-1 f b).
    by rewrite -{1}j'1nf -j'nf Knuth_1.
  have c1 : ccw (iter j' nf p) (iter j'.+1 nf p) q.
    move: (ch' q); rewrite qs j'nf qnbm1 -iterS j'1nf orderSpred ito bx qnx.
    by apply.
  have c2 : ccw (iter j' nf p) (iter j'.+1 nf p) (f b).
    have /= fbin := convex_hull_path_iter_in 1 s3 ss' ch bin.
    move: (ch' (f b)); rewrite (subsetP ss' _ fbin).
    rewrite -{1 4}ito -{1 3}orderSpred.
    have bm1in := chin (fingraph.order f b).-1.
    have [_ nfx1] := ch_path_no_fixpoint s3 ss' ch bm1in.
    rewrite iterS nfx1 -iterS orderSpred ito -j'nf -iterS j'1nf.
    by have [-> _] := ch_path_no_fixpoint s3 ss' ch bin; apply.
  have c3 : ccw (iter j'.+1 nf p) p (f b) by rewrite j'1nf // Knuth_1.
  have [/eqP -> | qnfb] := boolP(q == f b); first by rewrite Knuth_1.
  have c4 : ccw (iter j'.+1 nf p) (f b) q.
    have := use_convex_hull_path ch bin qs; rewrite qnfb bx qnx j'1nf bx.
    by apply.
  by have := Knuth_axiom5c' j'blue c2 c1 c3 c4; rewrite j'1nf.
move: (xin) qnx qnf bxcase pxcase; rewrite -fconnect_orbit => /iter_findex <-.
move: (leqnn (findex nf p x)).
move: {-2}(findex nf p x); elim: (findex nf p x) => [ | n IH] k.
  by rewrite leq_eqVlt ltn0 orbF => /eqP -> /= _ _ _; apply.
rewrite leq_eqVlt => /orP [/eqP kn1 | ]; last by rewrite ltnS; apply: IH.
move => qnk qnk1 bxcase pxcase.
set period := (fingraph.order f b - i + 2)%N.
have periodP : iter period nf p = p.
  by rewrite /period -/j' addnS addn1 iterS j'1nf /nf /new_path ffunE /= eqxx.
have period0 : (0 < period)%N by rewrite /period addn_gt0 orbT.
have [ kL | ] := boolP (period <= k)%N.
  have -> : (k = k - period + period)%N by rewrite subnK.
  rewrite iter_add periodP; apply: IH; try
    by rewrite -[X in iter _ _ X]periodP -iter_add subnK //.
  move: period0 kL kn1; clear; move => period0 kL kn1.
  by rewrite leq_subLR kn1 -addn1 addnC leq_add.
rewrite -ltnNge /period addnS addn1 ltnS leq_eqVlt => /orP [/eqP kb | knb].
  by apply: bxcase; rewrite kb -/j'; rewrite j'1nf.
rewrite -iterS fnf; last by to_lia; move: (leP knb); rewrite kn1; ssr_lia.
rewrite fnf; last by to_lia; move: (leP knb); rewrite kn1; ssr_lia.
have micmac : (k.+1 - 1 + i = (k - 1 + i).+1)%N.
  by rewrite kn1 !subSS !subn0.
rewrite micmac iterS.
have vin : iter (k - 1 + i) f b \in cover s' by apply: chin.
have [/eqP -> | qnp ] := boolP (q == p).
  by apply: blues; to_lia; move: (leP knb); rewrite kn1; ssr_lia.
have qs : q \in s by move: qin; rewrite !inE (negbTE qnp).
have := use_convex_hull_path ch vin qs.
rewrite -iterS -fnf; last by to_lia; move: (leP knb); rewrite kn1; ssr_lia.
rewrite -micmac -fnf; last by to_lia; move: (leP knb); rewrite kn1; ssr_lia.
by rewrite qnk iterS qnk1; apply.
Qed.

Lemma add_point_out_new_triangle k :
  (k < i)%N -> [set p; iter k f b; iter k.+1 f b] \in 
        (ts :|: \bigcup_(t in ts)
          [set p |: t :\ q | q in t & separated t q p &&
                (t :\ q \in boundary_edge ts)]).
Proof using ch ts3 cover_ts s'_b reds orbit_boundary.
move=> klti.
have edgein : [set iter k f b; iter k.+1 f b] \in boundary_edge ts.
  by rewrite -orbit_boundary; apply/imsetP; exists (iter k f b).
have [t [q [tts [etq [qint tme]]]]] : exists t, exists q, t \in ts /\
                  [set iter k f b; iter k.+1 f b] = t :\ q /\
                  q \in t /\
                  t :\: [set iter k f b; iter k.+1 f b]
                    = [set q].
  move: edgein; rewrite inE=>/andP[] ec2 /cards1P[] t tq.
  move: (set11 t); rewrite -tq !inE=>/andP[] tts esubt.
  have /eqP/cards1P[q vq] : #|t :\: [set iter k f b; iter k.+1 f b]| = 1%N.
    by rewrite cardsD (ts3 tts) (setIidPr esubt) (eqP ec2).
  exists t, q; split;[ | split;[ | split]]=> //.
    by rewrite -vq setDDr setDv set0U (setIidPr esubt).
  by move: (set11 q); rewrite -vq !inE=>/andP[].
  rewrite inE; apply/orP; right; apply/bigcupP; exists t => //.
  apply/imsetP; exists q; last by rewrite -setUA; congr (_ :|: _).
  have t_dec := (set3_D2_dec (ts3 tts) tme).
  rewrite inE qint -etq edgein andbT andTb t_dec.
  rewrite -separated_immediate; first by apply: reds; rewrite klti.
  move: (tts); rewrite t_dec => tts_dec.
  have k_ch : iter k f b \in cover (boundary_edge ts).
  apply/bigcupP; eexists; first by eapply edgein.
  by rewrite !inE eqxx.
by apply: oriented_triangle; rewrite // s'_b.
Qed.

Lemma edges_in_p_s : cover (boundary_edge (ts :|: \bigcup_(t in ts)
                        [set p |: t :\ q | q in t & separated t q p &&
                                (t :\ q \in boundary_edge ts)])) \subset p |: s.
Proof using cover_ts.
apply/subsetP=> x /bigcupP [] e; rewrite !inE => /andP [] _ /cards1P.
move=> [] t pt xe; move: (eqxx t); rewrite -in_set1 -pt inE andbC.
move=> /andP [] est; rewrite inE => /orP [] tin.
  apply/orP; right; rewrite -cover_ts; apply/bigcupP.
  by exists t => //; apply/(subsetP est).
move/bigcupP: tin => [] t' t'ts.
have t'subs : t' \subset s.
  rewrite -cover_ts.
    (* TODO: there should be a general theorem about subset and bigcup *)
  by apply/subsetP=> y yt'; apply/bigcupP; exists t'.
move=>/imsetP [] q; rewrite inE => /andP [] _ _ => tq.
have : x \in p |: t' :\ q by rewrite -tq; apply: (subsetP est).
rewrite !inE => /orP [/eqP -> // |/andP [] _ xt'].
  by rewrite eqxx.
by apply/orP; right; rewrite -cover_ts; apply/bigcupP; exists t'.
Qed.

(* TODO : look where this could be used. *)
Lemma bpk_oriented k : (k < i)%N -> ccw b p (iter k.+1 f b).
Proof using s3 ss' ch pnotin bin ilto reds bred blues.
move: (ilto) => /andP[] _ ilo.
elim: k => [ | k IH kp1i].
  move=> _; rewrite Knuth_1.
  by have/reds : (0 <= 0 < i)%N by rewrite igt0.
have itin k := convex_hull_path_iter_in k s3 ss' ch bin.
have fob := iter_order_in convex_hull_stable finj bin.
rewrite -[X in ccw X _ _]fob -orderSpred 2!iterS.
apply: pre_contiguous_red => //.
        by rewrite eq_sym pnotiter.
      by rewrite eq_sym -iterS pnotiter.
    have/blues // : (i <= (fingraph.order f b).-1 < fingraph.order f b)%N.
    by rewrite -ltnS orderSpred leqnn andbT ilo.
  have := (use_convex_hull_path ch (itin (fingraph.order f b).-1)
              (subsetP ss' _ (itin k.+1))).
  have/iter_lt_order_neq/eqP -> :
    (k.+1 < (fingraph.order f b).-1 < fingraph.order f b)%N.
    by rewrite orderSpred leqnn andbT (leq_trans kp1i) // -ltnS orderSpred ilo.
  rewrite -iterS orderSpred fob eq_sym.
  have/iter_lt_order_neq/eqP -> : (0 < k.+1 < fingraph.order f b)%N.
    by rewrite ltnS leq0n andTb (ltn_trans kp1i).
  by apply.
by rewrite -iterS orderSpred fob; apply/IH/(ltn_trans _ kp1i)/ltnSn.
Qed.

Lemma add_point_out_boundary_edge_subset :
  cover (boundary_edge (ts :|: \bigcup_(t in ts)
                        [set p |: t :\ q | q in t & separated t q p &&
                                (t :\ q \in boundary_edge ts)])) \subset
  (p |: s).
Proof using ts3 s3 ss' s'_b reds pnotin orbit_boundary ilto
  cover_ts ch bred blues bin.
rewrite -orbit_new_path_boundary_edge.
apply/subsetP=> x /bigcupP[] e /imsetP[] y; rewrite -fconnect_orbit => fcy.
rewrite -(iter_findex fcy) -/nf => ->.
rewrite -iterS -/nf !inE=> /orP [] /eqP ->.
  have := orbit_new_path_subset (findex nf p y).+1; rewrite -/nf.
  rewrite iterSr [nf b](_ : _ = p); last by rewrite /nf new_path_a.
  move=> [-> | [k [-> _]]]; first by rewrite eqxx.
  by rewrite (subsetP ss') ?orbT // (convex_hull_path_iter_in k s3 ss' ch bin).
have := orbit_new_path_subset (findex nf p y).+2; rewrite -/nf.
rewrite iterSr [nf b](_ : _ = p); last by rewrite /nf new_path_a.
move=> [-> | [k [-> _]]]; first by rewrite eqxx.
by rewrite (subsetP ss') ?orbT // (convex_hull_path_iter_in k s3 ss' ch bin).
Qed.

Lemma convex_hull_one_triangle x k :
  general_position (x |: (p |: s)) ->
  (k.+1 < fingraph.order f b)%N -> ccw (iter k.+1 f b) b x &&
   ccw b (iter k f b) x && ccw (iter k f b) (iter k.+1 f b) x ->
  (forall k' : nat,
   (k' < k.+1)%N -> ccw (iter k' f b) (iter k'.+1 f b) x).
Proof using s3 ss' ch bin.
move=> gpx kp1lto /andP [] /andP [] kp1bx bkx kkp1x k'.
have klto : (k < fingraph.order f b)%N by apply: (ltn_trans (ltnSn _)).
rewrite ltnS leq_eqVlt => /orP [/eqP -> // | k'lt].
have bins : b \in s by apply:(subsetP ss').
have kins' : forall k, iter k f b \in cover s'.
  by exact (fun k => convex_hull_path_iter_in k s3 ss' ch bin).
have [/eqP k0 | kn0] := boolP(k == 0)%N.
  by move: k'lt; rewrite k0 ltn0.
have kgt0 : (0 < k)%N by rewrite lt0n.
have bnk : b != iter k f b.
  have/iter_lt_order_neq/eqP// : (0 < k < fingraph.order f b)%N.
  by rewrite lt0n kn0.
have kkp1b : ccw (iter k f b) (iter k.+1 f b) b.
  have := use_convex_hull_path ch (kins' k) bins bnk; rewrite -iterS.
  have/iter_lt_order_neq/eqP -> : (0 < k.+1 < fingraph.order f b)%N.
    by rewrite ltn0Sn.
  by apply.
have bnkp1 : b != iter k.+1 f b.
  have/iter_lt_order_neq/eqP -> // : (0 < k.+1 < fingraph.order f b)%N.
  by rewrite ltn0Sn.
have := use_convex_hull_path ch (kins' k'); rewrite -iterS=> chk'.
have k'lto : (k' < fingraph.order f b)%N.
  by apply/(ltn_trans k'lt).
move: kkp1b; rewrite -Knuth_1 => kp1bk.
have chb := use_convex_hull_path ch bin.
have : ccw b (f b) (iter k.+1 f b).
  move: (chb (iter k.+1 f b)); rewrite (subsetP ss') //.
  rewrite -(eq_sym b) bnkp1 -(eq_sym (f b)).
  have/iter_lt_order_neq/eqP -> : (1 < k.+1 < fingraph.order f b)%N.
    by rewrite ltnS lt0n kn0.
  by rewrite Knuth_1 => /(_ isT isT isT).
rewrite Knuth_1=> kp1bfb.
have [/eqP k1 | kn1] := boolP (k == 1%N).
  by move: k'lt bkx; rewrite k1 ltnS leqn0=> /eqP ->.
have bfbk : ccw b (f b) (iter k f b).
  move: (chb (iter k f b)); rewrite (subsetP ss') //.
  rewrite !(eq_sym (iter k f b)).
  have/iter_lt_order_neq/eqP -> : (0 < k < fingraph.order f b)%N.
  by rewrite kgt0.
  have/iter_lt_order_neq/eqP -> : (1 < k < fingraph.order f b)%N.
    by rewrite ltn_neqAle eq_sym kn1 kgt0.
  by apply.
have bfbx : ccw b (f b) x.
  by have := Knuth_axiom5c' kp1bfb kp1bk kp1bx bfbk bkx.
have [/eqP -> // | ] := boolP(k' == 0%N);rewrite -lt0n => k'gt0.
have [k'p1k | gen ] := boolP (k'.+1 == k).
  move: kp1bk; rewrite Knuth_1 => kkp1b.
  have := use_convex_hull_path ch (kins' k); rewrite -iterS=> chk.
  have kbk' : ccw (iter k f b) b (iter k' f b).
    rewrite Knuth_1 -(eqP k'p1k); move: (chk' b).
    rewrite bins.
    have/iter_lt_order_neq/eqP -> // : (0 < k' < fingraph.order f b)%N.
      by rewrite  k'gt0 (ltn_trans k'lt).
    have/iter_lt_order_neq/eqP -> : (0 < k'.+1 < fingraph.order f b)%N.
      by rewrite ltn0Sn (eqP k'p1k).
    by apply.
  have kkp1k': ccw (iter k f b) (iter k.+1 f b) (iter k' f b).
    move: (chk (iter k' f b)); rewrite (subsetP ss') //.
    have/iter_lt_order_neq/eqP -> // : (k' < k.+1 < fingraph.order f b)%N.
      by rewrite (eqP k'p1k) leqnSn.
    have/iter_lt_order_neq/eqP -> : (k' < k < fingraph.order f b)%N.
      by rewrite (eqP k'p1k) leqnn.
    by apply.
  move: bkx; rewrite -Knuth_1 => kxb.
  have := Knuth_axiom5c kkp1x kkp1b kkp1k' kxb kbk'.
  by rewrite Knuth_1 -(eqP k'p1k).
have gp' : general_position [set iter k' f b; iter k'.+1 f b; b; iter k f b;
              iter k.+1 f b; x].
  apply/(general_position_sub gpx).
  by rewrite !subE bins !(subsetP ss') ?orbT.
move: kp1bx; rewrite -Knuth_1 => bxkp1.
move: kkp1x; rewrite Knuth_1 => xkkp1.
have k'kb : ccw (iter k' f b) (iter k f b) b.
  have trk : ((0 < k' < k) && (k < size (orbit f b)))%N.
    by rewrite size_orbit k'gt0 k'lt.
  by have := sub_convex_triangle bin trk; rewrite -Knuth_1.
have k'p1lt : (k'.+1 < k)%N.
  by move: k'lt; rewrite leq_eqVlt (negbTE gen).
have k'k'p1b : ccw (iter k' f b) (iter k'.+1 f b) b.
  move: (chk' b); rewrite bins.
  have/iter_lt_order_neq/eqP -> : (0 < k' < fingraph.order f b)%N.
    by rewrite k'gt0 (ltn_trans k'lt). 
  have/iter_lt_order_neq/eqP -> : (0 < k'.+1 < fingraph.order f b)%N.
    by rewrite ltn0Sn (ltn_trans k'p1lt) // (ltn_trans ki).
  by apply.
have k'k'p1k : ccw (iter k' f b) (iter k'.+1 f b) (iter k f b).
  move: (chk' (iter k f b)); rewrite (subsetP ss') // !(eq_sym (iter k f b)).
  have/iter_lt_order_neq/eqP -> // : (k' < k < fingraph.order f b)%N.
    by rewrite k'lt // (ltn_trans ki).
  have/iter_lt_order_neq/eqP -> // : (k'.+1 < k < fingraph.order f b)%N.
    by rewrite k'p1lt // (ltn_trans ki).
  by apply.
have k'k'p1kp1 : ccw (iter k' f b) (iter k'.+1 f b) (iter k.+1 f b).
  move: (chk' (iter k.+1 f b)); rewrite (subsetP ss') //
         !(eq_sym (iter k.+1 f b)).
  have/iter_lt_order_neq/eqP -> : (k' < k.+1 < fingraph.order f b)%N.
    by rewrite (ltn_trans _ (ltnSn k)) // (leq_ltn_trans ki).
  have/iter_lt_order_neq/eqP -> : (k'.+1 < k.+1 < fingraph.order f b)%N.
    by rewrite (ltn_trans k'p1lt (ltnSn _)) // (leq_ltn_trans ki).
  by apply.
have := inside_triangle_left_of_edge gp'.
by rewrite bkx bxkp1 xkkp1 k'k'p1b k'k'p1k k'k'p1kp1; apply.
Qed.

Lemma convex_hull_split x k :
  (k < fingraph.order f b)%N ->
  general_position (x |: (p |: s)) -> ccw b (iter k f b) x ->
  (forall u, x != iter u f b) ->
  (forall l, (k <= l < fingraph.order f b)%N -> 
         ccw (iter l f b) (iter l.+1 f b) x) ->
  forall l, (l < k)%N -> ccw (iter l f b) (iter l.+1 f b) x.
Proof using s3 ss' ch bin.
move=> klto gpx bkx xniter blues_k.
have bins : b \in s by apply:(subsetP ss').
have kins' : forall k, iter k f b \in cover s'.
  by exact (fun k => convex_hull_path_iter_in k s3 ss' ch bin).
have itero : iter (fingraph.order f b) f b = b.
  by apply: (iter_order_in convex_hull_stable finj bin).
set r := (fingraph.order f b).-1.
suff main : forall j, (j + k < fingraph.order f b)%N ->
  ccw (iter (j + k) f b) b x ->
  forall l : nat, (l < k)%N -> ccw (iter l f b) (iter l.+1 f b) x.
  have arith : ((fingraph.order f b).-1 - k + k = (fingraph.order f b).-1)%N.
    by rewrite subnK // -ltnS orderSpred.
  move: (main ((fingraph.order f b).-1 - k)%N).
  rewrite arith orderSpred leqnn; apply => //.
  rewrite -2![in X in ccw _ X _](itero, orderSpred); apply: blues_k.
  by rewrite orderSpred leqnn andbT -ltnS orderSpred.
elim => [ | j IH] kp1lto lbx l llek.
  by move/(Knuth_2 (P := P)): lbx; rewrite add0n bkx.
have jklto : (j + k < fingraph.order f b)%N.
  by apply: ltn_trans kp1lto; apply: ltnSn.
move: kp1lto; rewrite addSn => kp1lto.
have : [set b; iter (j + k) f b; x] \subset x |: (p |: s).
  by rewrite !subE bins (subsetP ss') ?orbT.
have kn0 : k != 0%N.
  by apply/negP=> /eqP A; move: llek; rewrite A ltn0.
move/(general_position_sub gpx)/general_position_split.
have/iter_lt_order_neq/eqP -> : (0 < j + k < fingraph.order f b)%N.
  by rewrite jklto andbT; move: kn0; case: (k)=> // n _; rewrite addnS ltn0Sn.
rewrite eq_sym xniter (xniter 0%N) => /(_ isT isT isT)=> /orP[bjx | jbx].
  have := convex_hull_one_triangle gpx kp1lto.
  rewrite bjx lbx blues_k; last by rewrite leq_addl jklto.
  move=> /(_ isT); apply.
  by apply: (leq_trans llek); rewrite -addSn leq_addl.
by apply: IH.
Qed.

Lemma add_point_out_covers_triangulation :
    covers_triangulation (p |: s)
        (ts :|: \bigcup_(t in ts)
                        [set p |: t :\ q | q in t & separated t q p &&
                                (t :\ q \in boundary_edge ts)]).
Proof using s3 ss' ch ts3 s'_b reds pnotin orbit_boundary in_hull_s' ilto
  cover_ts bred blues bin.
move: (ilto) => /andP [] _ ilo.
set ts' := (ts :|: _).
apply/forallP=> f'; apply/implyP=> ch'; apply/forallP=> x; apply/implyP=> gpx.
apply/implyP=> xnotin; apply/implyP=> xinh.
have ps3 : (2 < #| p |: s|)%N.
  rewrite cardsU cards1 addSn add0n setIC cardsI1 (negbTE pnotin) subn0.
  by to_lia; move: (leP s3); ssr_lia.
(* This should probably be an independent lemma. *)
have f'eqnf : forall y, y \in cover (boundary_edge ts') -> f' y = nf y.
  move=> y yin.
  move/forallP: (ch') => /(_ y)/implyP/(_ yin)/andP[] /andP[] fyin ory main.
  have yf'yb : [set y;  f' y] \in boundary_edge ts'.
    rewrite -(eqP main); apply/imsetP; exists y; rewrite //.
    by rewrite -fconnect_orbit connect0.
  have : [set y; f' y] \in [set [set a; nf a] | a in orbit nf p].
    by rewrite orbit_new_path_boundary_edge.
  move/imsetP=>[a aP1]=> yfya.
  have annfa : a != nf a.
    by move: yf'yb; rewrite yfya !inE cards2 eqSS eqb1=>/andP[].
  move:(yfya); have [/eqP -> | yna ] := boolP (y == a).
    move/setP/subset_eqP/andP=>[] _; rewrite subUset !sub1set => /andP[] _.
    by rewrite !inE => /orP [] /eqP A; move: annfa; rewrite // A eqxx.
  move=> /setP/subset_eqP/andP[];
    rewrite !subUset !sub1set !inE (eq_sym a y) (negbTE yna) =>
    /andP[] /eqP ynfa => /orP [] /eqP A _; last first.
    by move: yf'yb; rewrite A ynfa !inE cards2 eqxx.
  have ab : a \in cover (boundary_edge ts').
    apply/bigcupP; exists [set a; nf a]; first by rewrite -yfya.
    by rewrite !inE eqxx.
  move: add_point_out_new_convex_hull_path.
  rewrite orbit_new_path_boundary_edge => new_ch.
  have nf2a : nf (nf a) \in cover (boundary_edge ts').
  by have := convex_hull_path_iter_in 2 ps3 edges_in_p_s new_ch ab.
  have:= ch_path_no_fixpoint ps3 edges_in_p_s new_ch ab => [[]] _ ch2na.
  have nfab : nf a \in cover (boundary_edge ts').
    by have := convex_hull_path_iter_in 1 ps3 edges_in_p_s new_ch ab.
  have:= ch_path_no_fixpoint ps3 edges_in_p_s new_ch nfab => [[]] ch2ncha _.
  have nf2s : nf (nf a) \in p |: s by rewrite (subsetP edges_in_p_s).
  move/forallP: ory=> /(_ (nf (nf a))) /implyP/(_ nf2s).
  rewrite A ynfa ch2ncha ch2na implyTb=> /(Knuth_2 (P := P)).
  move/forallP: (new_ch) => /(_ a); rewrite ab implyTb andbC=>/andP[] _.
  move=>/andP[] _ /forallP/(_ (nf (nf a)))/implyP/(_ nf2s).
  by rewrite ch2na ch2ncha implyTb => ->.
move: (xinh) => /forallP xh'.
have sorbit1 : (1 < size (orbit f b))%N.
  by have := convex_hull_size_orbit_gt2 bin; apply: ltn_trans.
have nfb_new_orbit : [set b; nf b] \in [set [set x; nf x] | x in orbit nf p].
  apply/imsetP; exists b => //; rewrite -new_path_cycle' addnS iterSr.
  by rewrite  new_path_a.
have b_new_cover : b \in cover (boundary_edge ts').
  apply/bigcupP; exists [set b; nf b].
    by rewrite -orbit_new_path_boundary_edge.
  by rewrite !inE eqxx.
have nfb_new_cover : nf b \in cover (boundary_edge ts').
  apply/bigcupP; exists [set b; nf b].
    by rewrite -orbit_new_path_boundary_edge.
  by rewrite !inE eqxx orbT.
have be : [set [set x; f x] | x in orbit f b] = boundary_edge ts.
  by move/forallP:ch =>/(_ b)/implyP/(_ bin)/andP[] _; rewrite s'_b=>/eqP.
have eints : [set b; f b] \in boundary_edge ts.
  by rewrite -be; apply/imsetP; exists b; rewrite // -fconnect_orbit connect0.
have esubs : [set b; f b] \subset p |: s.
  apply/(subset_trans _ (subsetUr _ _))/(subset_trans _ ss'); rewrite s'_b.
  by apply: (bigcup_max _ eints); rewrite subxx.
have xnb : x != b.
  apply: (contraNN _ xnotin) => /eqP ->.
  by rewrite (subsetP esubs) // !inE eqxx.
have xnnfb : x != nf b.
  apply: (contraNN _ xnotin) => /eqP ->.
  by rewrite /nf new_path_a !inE eqxx.
have leftmost : ccw b p x.
  move:(xh' b); rewrite b_new_cover f'eqnf //.
  by rewrite xnb xnnfb !implyTb /nf new_path_a.
have kins' : forall k, iter k f b \in cover s'.
  by exact (fun k => convex_hull_path_iter_in k s3 ss' ch bin).
have xniter : forall k, x != iter k f b.
  move=> k; apply/(contraNN _ xnotin) => /eqP ->.
  by rewrite inE; apply/orP; right; have:= (kins' k); apply/(subsetP ss').
have := xniter i; rewrite eq_sym => inx.
have bni : b != iter i f b.
  have/iter_lt_order_neq : (0 < i < fingraph.order f b)%N by rewrite igt0 ilo.
  by move/eqP.
have bins : b \in s by apply:(subsetP ss').
have iins : iter i f b \in s by apply:(subsetP ss').
have bix_sub: [set b; iter i f b; x] \subset (x |: (p |: s)).
  by rewrite !subE bins iins !orbT.
have nfi : nf (iter i f b) = (iter i.+1 f b).
  rewrite /nf /new_path ffunE /= eq_sym (negbTE bni) eq_sym.
  by rewrite (negbTE (pnotiter bin i)).
have nfp_new_cover : nf p \in cover (boundary_edge ts').
  rewrite -orbit_new_path_boundary_edge; apply/bigcupP; exists [set p; nf p].
    by apply/imsetP; exists p; rewrite // -fconnect_orbit connect0.
  by rewrite !inE eqxx orbT.
have nfp_eq : nf p = iter i f b by rewrite /nf new_path_p // pnb.
have nf2p_eq : nf (nf p) = iter i.+1 f b by rewrite nfp_eq nfi.
have blues_x k (kint : (i <= k < fingraph.order f b)%N):
     ccw (iter k f b) (iter k.+1 f b) x.
  have [k_new_cover k_eq] : iter k f b \in cover (boundary_edge ts') /\
                     nf (iter k f b) = iter k.+1 f b.
    elim: k kint => [/andP [] ile0 _ | k IH /andP[] ilek klto].
      by move: ile0; rewrite leqNgt igt0.
    have klto' : (k < fingraph.order f b)%N by apply: (ltn_trans (ltnSn _)).
    move: ilek; rewrite leq_eqVlt=> /orP[/eqP <- | ].
      by rewrite -nfp_eq.
    rewrite ltnS => ileK.
    move: IH; rewrite klto' ileK=>/(_ isT)[] IH1 IH2.
    rewrite -IH2.
    move/forallP:add_point_out_new_convex_hull_path=>/(_ (iter k f b)).
    rewrite orbit_new_path_boundary_edge -/ts' -/nf IH1 implyTb.
    move=>/andP[]/andP[] ->; split;[by [] | rewrite IH2 ].
      rewrite /nf ffunE /= -!iterS.
      have /negbTE -> : iter k.+1 f b != b.
        have/iter_lt_order_neq/eqP : (0 < k.+1 < fingraph.order f b)%N.
          by rewrite klto.
        by rewrite eq_sym.
      by rewrite eq_sym (negbTE (pnotiter bin k.+1)).
  move: (xh' (iter k f b)); rewrite k_new_cover implyTb !xniter.
  by rewrite f'eqnf // k_eq xniter.
have fkkp k : (k < i)%N -> ccw (iter k.+1 f b) (iter k f b) p.
  by move=> klti; have/reds : (0 <= k < i)%N by rewrite klti.
have kpfk k : (k < i)%N -> ccw (iter k f b) p (iter k.+1 f b).
  by move=> klti; rewrite Knuth_1 fkkp.
have xnp : x != p by move: xnotin; rewrite !inE negb_or=>/andP[].
have main: forall k, (k <= i)%N -> inside_triangle x [set b; p; iter k f b] ->
   [exists t in ts', inside_triangle x t] \/
   (forall k', (k' < k)%N -> ccw (iter k' f b) (iter k'.+1 f b) x).
  elim=>[ | k IH]; first by move=> _ _ ; right.
  have [/eqP -> ki insx| kn0 ki xins] := boolP(k == 0)%N.
    left; apply/existsP; exists [set p; b; f b].
    apply/andP; split; first by apply: (add_point_out_new_triangle igt0). 
    by rewrite (setUC [set p]). 
(* preparatory work for choose_sub_triangle. *)
  have : [set b; p; iter k.+1 f b; iter k f b; x] \subset (x |: (p |: s)).
    by rewrite !subE bins !(subsetP ss') ?orbT.
  move/(general_position_sub gpx) => gp1.
  have bnk : b != iter k f b.
    have/iter_lt_order_neq/eqP// : (0 < k < fingraph.order f b)%N.
      by rewrite lt0n kn0 (ltn_trans ki).
  have bnkp1 : b != iter k.+1 f b.
    have/iter_lt_order_neq/eqP -> // : (0 < k.+1 < fingraph.order f b)%N.
    by rewrite ltn0Sn (leq_ltn_trans ki).
  have knkp1 : iter k f b != iter k.+1 f b.
    have/iter_lt_order_neq/eqP// : (k < k.+1 < fingraph.order f b)%N.
    by rewrite ltnSn (leq_ltn_trans ki).
  have bpk : ccw b p (iter k f b).
    move/forallP : add_point_out_new_convex_hull_path => /(_ b).
    rewrite orbit_new_path_boundary_edge b_new_cover implyTb=>/andP[] /andP[] _.
    move/forallP=>/(_ (iter k f b)); rewrite !(eq_sym (iter k f b)).
    by rewrite bnk new_path_a pnotiter // !inE (subsetP ss') // orbT !implyTb.
  have bpkp1 : ccw b p (iter k.+1 f b).
    move/forallP : add_point_out_new_convex_hull_path => /(_ b).
    rewrite orbit_new_path_boundary_edge b_new_cover implyTb=>/andP[] /andP[] _.
    move/forallP=>/(_ (iter k.+1 f b)); rewrite !(eq_sym (iter k.+1 f b)).
    by rewrite bnkp1 new_path_a pnotiter // !inE (subsetP ss') // orbT !implyTb.
  have kkp1b : ccw (iter k f b) (iter k.+1 f b) b.
    move/forallP: (ch) => /(_ (iter k f b)); rewrite kins' implyTb.
    move=> /andP [] /andP [] _ /forallP /(_ b).
    rewrite bins bnk -iterS.
    have/iter_lt_order_neq/eqP -> // : (0 < k.+1 < fingraph.order f b)%N.
    by rewrite ltn0Sn (leq_ltn_trans ki).
  have kins : inside_triangle (iter k f b) [set b; p; iter k.+1 f b].
    rewrite inside_triangle_immediate // ?bpk ?bpkp1; last first.
      apply/(general_position_sub gpx).
      by rewrite !subE bins !(subsetP ss') // ?orbT.
    rewrite (Knuth_1 _ b (iter k f b)) kkp1b andTb andbT.
    by rewrite -(Knuth_1 _ _ p); apply: reds; rewrite ki.
  have kkp1p : ccw (iter k.+1 f b) (iter k f b) p.
    by apply: reds; rewrite ki.
  have := choose_sub_triangle gp1.
  rewrite (uniq_catC [:: b; p; iter k.+1 f b; iter k f b] [:: x]) cat1s.
  rewrite !cons_uniq !inE !negb_or xnb xnp !xniter in_nil bnk !pnotiter //.
  rewrite (eq_sym b p) pnb (eq_sym (iter k.+1 f b)) bnkp1 knkp1 xins kins.
  have klei : (k <= i)%N by rewrite leq_eqVlt ki orbT.
  have kgt0 : (0 < k)%N by rewrite lt0n.
  move => /(_ isT isT isT)=>/orP[insbpk | /orP [inpkp1k|]].
      have gp' : general_position [set b; p; iter k f b; x].
        apply/(general_position_sub gpx).
        by rewrite !subE bins (subsetP ss') // !orbT.
      move: (insbpk); rewrite inside_triangle_immediate ?bpk ?gp' //.
      move=> /andP[] /andP[] bpx pkx kbx.
      move: (insbpk)=>/(IH klei) []; first by left.
      move=> inpath.
      suff kkp1x : ccw (iter k f b) (iter k.+1 f b) x.
        right=> k'; rewrite leq_eqVlt=>/orP[k'kp1 | k'kp1].
          by move: k'kp1; rewrite eqSS=> /eqP ->.
        by move: k'kp1; rewrite ltnS; apply: inpath.
      have chm1 q :
        [&& (q \in s), (q != iter k.-1 f b) & (q  != iter k f b)] ->
        ccw (iter k.-1 f b) (iter k f b) q.
        move=> /andP[] qs cq.
        move/forallP: (ch) => /(_ (iter k.-1 f b)); rewrite kins' implyTb.
        move=> /andP [] /andP [] _ /forallP /(_ q).
        by rewrite  qs -iterS (prednK kgt0) cq !implyTb.
      have km1kkp1 : ccw (iter k.-1 f b) (iter k f b) (iter k.+1 f b).
        apply: chm1.
        rewrite (subsetP ss') // andTb !(eq_sym (iter k.+1 f b)).
        have /iter_lt_order_neq/eqP -> : (k.-1 < k.+1 < fingraph.order f b)%N.
          rewrite (leq_ltn_trans ki ilo) andbT (ltn_trans _ (ltnSn _)) //. 
          by rewrite prednK ?leqnn.
        have /iter_lt_order_neq/eqP -> // : (k < k.+1 < fingraph.order f b)%N.
        by rewrite (leq_ltn_trans ki ilo) // ltnSn.
      have km1kx : ccw (iter k.-1 f b) (iter k f b) x.
        rewrite -[in X in ccw _ X _](prednK kgt0); apply: inpath.
        by rewrite prednK ?leqnn.
      have km1kb : ccw (iter k.-1 f b) (iter k f b) b.
        apply: chm1; rewrite bins.
        have/iter_lt_order_neq/eqP// -> : (0 < k < fingraph.order f b)%N.
          by rewrite kgt0 ?(ltn_trans _ ilo).
        rewrite andTb andbT; apply/negP=>/eqP A.
        move: (inpath 0%N) kbx=> /(_ kgt0).
        by rewrite -(prednK kgt0) !iterS -A /= => /(Knuth_2 (P := P))/negbTE ->.
      by apply: (Knuth_axiom5c' km1kkp1 km1kb).
    left; apply/existsP; exists [set p; iter k f b; iter k.+1 f b].
      by rewrite add_point_out_new_triangle // setUAC inpkp1k.
(* start of case above the b--iter k f b line *)
  rewrite inside_triangle_immediate;
   [ move=> trp | | by rewrite Knuth_1]; last first.
    apply/(general_position_sub gpx).
    by rewrite !subE bins !(subsetP ss') // !orbT.
  have kp1lto : (k.+1 < fingraph.order f b)%N by rewrite (leq_ltn_trans ki).
  have := convex_hull_one_triangle gpx kp1lto trp => it; right; exact it.
(* end of case above the b--iter k b line *)
have nfbp : nf b = p by rewrite /nf new_path_a.
have bpi : ccw b p (iter i f b).
  move/forallP: (ch') => /(_ b); rewrite b_new_cover implyTb.
  move=>/andP[] /andP[] _ /forallP /(_ (nf (nf b))) it orbitb.
  move: it; rewrite (f'eqnf b b_new_cover).
  rewrite (subsetP add_point_out_boundary_edge_subset); last first.
    by rewrite nfbp nfp_new_cover.
  have := add_point_out_new_convex_hull_path.
    rewrite orbit_new_path_boundary_edge -/ts' -/nf => chn.
  have := ch_path_no_fixpoint ps3 add_point_out_boundary_edge_subset chn.
  rewrite -/ts' -/nf =>nofix.
  move:(nofix _ b_new_cover) => [] _ ->.
  by move:(nofix _ nfb_new_cover) => [] -> _; rewrite !implyTb nfbp nfp_eq.
suff main'' :
 (forall k' : nat, (k' < i)%N -> ccw (iter k' f b) (iter k'.+1 f b) x) ->
  [exists t in ts', inside_triangle x t].
  have : [set iter i f b; b; x] \subset x |: (p |: s).
    by rewrite !subE bins (subsetP ss') ?orbT.
  move/(general_position_sub gpx).
  have bnx : b != x.
    by rewrite -(iter_order_in convex_hull_stable finj bin) eq_sym xniter.
  move/general_position_split; rewrite bnx xniter eq_sym bni.
  move=> /(_ isT isT isT)=> /orP[ibx | bix].
    have : [set b; p; iter i f b; x] \subset (x |: (p |: s)).
      by rewrite !subE bins (subsetP ss') ?orbT.
    move/(general_position_sub gpx) => gp'.
    have lower : inside_triangle x [set b; p; iter i f b].
      rewrite inside_triangle_immediate => //.
      have bpx : ccw b p x.
        rewrite -nfbp; move/forallP : xinh => /(_ b).
         by rewrite b_new_cover implyTb xnb (f'eqnf _ b_new_cover) nfbp xnp.
      have pix : ccw p (iter i f b) x.
      rewrite -nfp_eq -nfbp.
      move/forallP : xinh => /(_ (nf b)); rewrite nfb_new_cover implyTb.
        by rewrite nfbp xnp -nfbp (f'eqnf _ nfb_new_cover) nfbp nfp_eq xniter.
      by rewrite bpx pix ibx.
    case: (main i (leqnn _) lower).
    by [].
  by [].
apply: main''.
by have := convex_hull_split ilo gpx bix xniter blues_x.
(* don't loose the last part. *)
move => main'.
  have gpxs : general_position (x |: s).
    apply/(general_position_sub gpx).
    by rewrite !subE subsetU // subsetUr orbT.
  have xnotins : x \notin s.
    by apply/(contraNN _ xnotin); rewrite !inE orbC=> ->.
  have /(in_hull_s' gpxs xnotins)/existsP[t tP] : in_hull (cover s') f x.
    apply/forallP=> y; rewrite s'_b -orbit_boundary; apply/implyP.
    move/bigcupP=>[]e /imsetP [] z zP1 -> yzfz.
    have : y \in orbit f b.
      move: yzfz; rewrite !inE => /orP [] /eqP -> //.
      move: zP1; rewrite -!fconnect_orbit => bfz.
      by apply: (connect_trans bfz); apply:fconnect1.
    rewrite -fconnect_orbit => yb; apply/implyP => _.
    rewrite -(iter_findex yb); set k:= (findex f b y); rewrite -iterS.
    have klto : (k < fingraph.order f b)%N by apply/findex_max.
    have [ ilek | ] := boolP (i <= k)%N.
      by apply blues_x; rewrite ilek.
    by rewrite -ltnNge => klti; apply main'.
  move/andP: tP => [] tts xint; apply/existsP; exists t; rewrite xint andbT.
  by rewrite inE tts.
Qed.

Lemma red_index_max b' : b' \in orbit f b -> ccw (f b') b' p ->
    (findex f b b' < i)%N.
Proof.
rewrite -fconnect_orbit=> /iter_findex.
set k := findex f b b' => ki; rewrite -ki => cc.
rewrite ltnNge; apply/negP=> A; case/(Knuth_2 (P := P))/negP: cc.
by apply: blues; rewrite A findex_max // -ki fconnect_iter.
Qed.

(* The condition on q has to be added because inside_triangle_in_hull cannot
  be used in this section. *)
Lemma pre_new_triangle_unique q t t' :
  ccw q p (iter i f b) ->
  general_position (q |: (p |: s)) ->
  t \in \bigcup_(t in ts)
        [set p |: t :\ q | q in t & separated t q p &&
                                (t :\ q \in boundary_edge ts)] ->
  t' \in \bigcup_(t in ts)
        [set p |: t :\ q | q in t & separated t q p &&
                                (t :\ q \in boundary_edge ts)] ->
  inside_triangle q t -> inside_triangle q t' ->
  t = t'.
Proof.
move: (ilto) => /andP [] _ ilo.
move=> qcond gpqps.
move=> /bigcupP [] t1 t1ts; move=> /imsetP [] r; rewrite inE => /andP [] rt1.
move=> /andP [] seprp t1mrb tq.
move: t1mrb; rewrite -orbit_boundary=> /imsetP [] bp bpo t1mro.
move=> /bigcupP [] t2 t2ts; move=> /imsetP [] r'; rewrite inE => /andP [] rt2.
move=> /andP [] sepr'p /imsetP [] bp2 bp2o t2mro t'q qint; move: (qint).
move: (ts3 t1ts) => t1_3.
move: (set3_D1_dec t1_3 t1mro) => t1q.
have bpfr : ccw bp (f bp) r.
  apply: (use_convex_hull_path ch) => //.
        by rewrite -orbit_cover_s'.
      by rewrite -cover_ts; apply/bigcupP; exists t1.
    by move/eqP: (t1_3); rewrite t1q cards3 => /andP [].
  by rewrite eq_sym; move/eqP: (t1_3); rewrite t1q cards3 => /andP [] /andP [].
move: (ts3 t2ts) => t2_3.
move: (set3_D1_dec t2_3 t2mro) => t2q.
have a_sub_s u : u \in ts -> u \subset s.
  by move=> ?; apply/subsetP=> z ?; rewrite -cover_ts; apply/bigcupP; exists u.
have [t1sub_s t2sub_s] : t1 \subset s /\ t2 \subset s by split; apply: a_sub_s.
have bp2fr : ccw bp2 (f bp2) r'.
  apply: (use_convex_hull_path ch) => //.
        by rewrite -orbit_cover_s'.
      by rewrite -cover_ts; apply/bigcupP; exists t2.
    by move/eqP: (t2_3); rewrite t2q cards3 => /andP [].
  by rewrite eq_sym; move/eqP: (t2_3); rewrite t2q cards3 => /andP [] /andP [].
move: tq; rewrite t1mro setUA => tq; move: t'q; rewrite t2mro setUA => t'q.
have fbpp : ccw (f bp) bp p by rewrite (separated_immediate _ bpfr) -t1q.
have fbp2p : ccw (f bp2) bp2 p by rewrite (separated_immediate _ bp2fr) -t2q.
have/(general_position_sub gpqps) tmp :
    [set f bp; bp; p; q] \subset (q |: (p |: s)).
  by rewrite !subE !(subsetP t1sub_s) ?t1q ?subE.
rewrite tq setUAC cycle_set3 (inside_triangle_immediate tmp fbpp) {tmp}.
have/(general_position_sub gpqps) tmp :
    [set f bp2; bp2; p; q] \subset (q |: (p |: s)).
  by rewrite !subE !(subsetP t2sub_s) ?t2q ?subE.
rewrite t'q setUAC cycle_set3 (inside_triangle_immediate tmp fbp2p).
move=> /andP [] /andP [] fbppq bppq pfbpq /andP [] /andP [] fbp2q bp2pq pfbp2q.
suff out k : forall l, ccw (iter (l + k).+1 f b) (iter (l + k) f b) p ->
          ccw (iter k.+1 f b) (iter k f b) p ->
          (l + k < i)%N ->
          ccw (iter k.+1 f b) q p -> ccw (iter (l + k).+1 f b) q p.
  set k := findex f b bp; set k' := findex f b bp2.
  have kmax : (k < i)%N by apply: red_index_max.
  have k'max : (k' < i)%N by apply: red_index_max.
  have ki : iter k f b = bp by rewrite iter_findex ?fconnect_orbit.
  have k'i : iter k' f b = bp2 by rewrite iter_findex ?fconnect_orbit.
  have [ | C] := boolP (k <= k')%N.
    rewrite leq_eqVlt -ki -k'i => /orP [/eqP -> // | C].
    have C' : (k <= k'.-1)%N by rewrite -ltnS (ltn_predK C).
    case/(Knuth_2 (P := P))/negP: bp2pq.
    rewrite -k'i -(ltn_predK C) -(subnK C') -Knuth_1.
    apply: out.
          rewrite subnK //; apply: reds.
          by rewrite (ltn_trans (ltnSn _)) // (ltn_predK C).
        by rewrite iterS ki.
      by rewrite subnK // (ltn_trans _ k'max) // -(ltn_predK C) ltnSn.
    by rewrite iterS ki Knuth_1.
  move: C; rewrite -ltnNge => C.
  have C' : (k' <= k.-1)%N by rewrite -ltnS (ltn_predK C).
  case/(Knuth_2 (P := P))/negP: bppq.
  rewrite -ki -(ltn_predK C) -(subnK C') -Knuth_1.
  apply: out.
        rewrite subnK //; apply: reds.
        by rewrite (ltn_trans (ltnSn _)) // (ltn_predK C).
      by rewrite iterS k'i.
    by rewrite subnK // (ltn_trans _ kmax) // -(ltn_predK C) ltnSn.
  by rewrite iterS k'i Knuth_1.
elim=> [// | l IH].
move=> {fbppq bppq pfbpq fbp2q bp2pq pfbp2q tmp tq t'q bpo bp2o}.
move=> {fbp2p fbpp bp2fr t2sub_s t2q bpfr t1q t1_3 t2mro bp2 sepr'p rt2 r'}.
move=> {t1sub_s t2ts t2_3 t2 t1mro bp seprp rt1 r t1ts t1}.
rewrite Knuth_1; move=> l1red kred C or1.
have C' : (l + k < i)%N by rewrite (leq_trans _ C).
have /(fun w => IH w kred C' or1) {IH}:
  ccw (iter (l + k).+1 f b) (iter (l + k) f b) p.
  by apply: reds; rewrite (leq_trans _ C).
rewrite Knuth_1 => IH'.
have := add_point_out_new_convex_hull_path.
set no := [set [set x; nf x] | x in _] => nch.
have pno : p \in cover no.
  apply/bigcupP; exists [set p; nf p]; rewrite ?inE ?eqxx//.
  by apply/imsetP; exists p => //; rewrite -fconnect_orbit connect0.
have iterins n : iter n f b \in s.
  by apply/(subsetP ss')/(convex_hull_path_iter_in n s3 ss' ch).
have iterinps n : iter n f b \in p |: s.
  by rewrite inE; apply/orP; right.
have iternp n : iter n f b != p.
  by apply/negP=> /eqP A; case/negP: pnotin; rewrite -A.  
have iternnp n (cn : (n < i)%N) : iter n f b != iter i f b.
  apply/negP=> /eqP A.
  have/(Knuth_2 (P := P)): (ccw (iter n.+1 f b) (iter n f b) p).
    by apply: reds; rewrite cn.
  by case/negP; rewrite /= A -iterS; apply: blues; rewrite leqnn ilo.
have pnb : p != b.
  by apply/negP => /eqP A; case/negP: pnotin; rewrite A; apply: (iterins 0%N).
have redin : forall n, (n < i)%N -> ccw p (iter i f b) (iter n f b).
  move=> n cn; move: nch => /'forall_implyP /(_ p pno) => /andP [] /andP [] _.
  move/'forall_implyP=> /(_ (iter n f b) (iterinps _)).
  by rewrite (iternp _) /nf new_path_p // (iternnp _ cn) implyTb.
rewrite Knuth_1; move: qcond; rewrite -Knuth_1 => qcond.
have [/eqP -> // | noti ] := boolP((l.+1 + k).+1 == i)%N.
move: (C); rewrite leq_eqVlt (negbTE noti) orFb => C1.
apply: (Knuth_axiom5c (redin _ C1) (redin _ C) qcond l1red IH').
Qed.

End fixed_red_subpath.

End add_point_out.

Section add_point_in.

Variable p : P.

Hypothesis pnotin : p \notin s.

Hypothesis gpp : general_position (p |: s).

Variable t : {set P}.

Hypothesis tts : t \in ts.

Hypothesis pinst : inside_triangle p t.

Hypothesis boundary_edge_not_empty : boundary_edge ts != set0.

Hypothesis all_in_hull : forall q, q \in s -> in_hull (cover s') f q.

Lemma add_point_in_new_boundary :
  boundary_edge (ts :\ t :|: [set p |: t :\ q | q in t]) =
  boundary_edge ts.
Proof using coords p0 ts3 cover_ts pnotin tts.
apply/setP=> e; rewrite !inE.
have [ ec2 /= | //=] := boolP (#|e| == 2).
rewrite [in RHS](_ : ts = ts :\ t :|: [set t]); last by rewrite setUC setD1K.
rewrite !cards1_setU; last 2 first.
    by apply/setP=> x; rewrite !inE andbC andbA (andbC (x == t)) andNb.
  apply/eqP; rewrite -subset0; apply/subsetP=> x; rewrite !inE.
  move=>/andP[] /andP[] xnt xts /imsetP [q qt xq]; case/negP: pnotin.
  by rewrite -cover_ts; apply/bigcupP; exists x; rewrite // xq !inE eqxx.

have pnotints : #|[set x in (ts :\ t)| e \subset x]| = 1%N -> p \notin e.
  move/eqP/cards1P=> [t' t'q].
  move : (set11 t'); rewrite -t'q !inE => /andP[] /andP [] _ t'ts esub'.
  apply: (contraNN _ pnotin) => pine; rewrite -cover_ts; apply/bigcupP.
  by exists t'; rewrite // (subsetP esub').
have pnt : p \notin t.
  apply: (contraNN _ pnotin)=> pint; rewrite -cover_ts; apply/bigcupP.
  by exists t.
case h : #| _ | => [ | [ | ? //]]; rewrite !eqxx !andFb ?orbF ?orFb !andTb.
  have [pine | pn] := boolP (p \in e).
    have ent : ~ (e \subset t).
      move=> A; case/negP: pnotin; rewrite -cover_ts; apply/bigcupP.
      by exists t; rewrite // (subsetP A).
    have/cards1P [q1 qq] : #|e :\ p| == 1%N.
      by rewrite cardsD (eqP ec2) (setIidPr _) ?cards1 // sub1set.
    have [q1e qnp] : q1 \in e /\ q1 != p.
      by move: (set11 q1); rewrite -qq !inE=>/andP[].
    have [q1t | q1n ] := boolP (q1 \in t); last first.
      have -> : [set x in [set t] | e \subset x] = set0.
        apply/eqP; rewrite -subset0; apply/subsetP=> x; rewrite !inE=> /andP[].
        by move=>/eqP -> esubt; case/negP: pnt; rewrite // (subsetP esubt).
      rewrite cards0; apply/negbTE/negP=> /cards1P [t' t'q].
      move: (set11 t'); rewrite -t'q !inE=> /andP[]/imsetP[q qt t'v] esub'.
      have : q1 \in t' by apply: (subsetP esub'). 
      by rewrite t'v !inE (negbTE q1n) andbF (negbTE (_ : q1 != p)).
    have [j jq1] : exists j, q1 = three_points t j.
      by move: q1t; rewrite [X in _ \in (_ X)]
            (three_points_eq (ts3 tts))=> /imsetP[j _ jq]; exists j.
    set tps := three_points t.
    set tt := (X in (#|(_ X)| == 1%N) = _).
    have ev : e = [set p; q1].
      have/subset_leqif_cards: [set p; q1] \subset e by rewrite !subE pine.
      by rewrite (eqP ec2) cards2 eq_sym qnp eq_sym=>/leqif_refl/eqP.
    have /subset_leq_card :
        [set (p |: [set tps j; tps (j + 1)]); (p |: [set tps j; tps (j - 1)])]
              \subset tt.
      rewrite !(subE,ev, jq1); apply/andP; split.
        apply/imsetP; exists (tps (j - 1)).
          by rewrite (three_points_in _ (ts3 tts)).
        congr (_ :|: _); rewrite (three_points_eq (ts3 tts)).
        have/subset_leqif_cards : [set tps j; tps (j + 1)]
             \subset [set tps i | i : 'I_3] :\ tps (j - 1).
          by apply/subsetP=> x; rewrite !inE=> /orP[] /eqP ->;
          rewrite (inj_eq (three_points_inj (ts3 tts))) ?addm1_dif
            ?add1m1_dif; apply/imsetP; eapply ex_intro2; eapply erefl.
        rewrite cardsD (three_points_card (ts3 tts)) cards2
          (inj_eq (three_points_inj (ts3 tts))) -(addrC 1) add1_dif
          cardsI1 -(three_points_eq (ts3 tts)) (three_points_in _ (ts3 tts)).
        by move/leqif_refl/eqP.
      apply/imsetP; exists (tps (j + 1)).
        by rewrite (three_points_in _ (ts3 tts)).
      congr (_ :|: _); rewrite (three_points_eq (ts3 tts)).
      have/subset_leqif_cards : [set tps j; tps (j - 1)]
             \subset [set tps i | i : 'I_3] :\ tps (j + 1).
        by apply/subsetP=> x; rewrite !inE=> /orP[] /eqP ->;
          rewrite (inj_eq (three_points_inj (ts3 tts))) -(addrC 1) ?add1_dif
            ?(eq_sym (j - 1)) ?(addrC 1) ?add1m1_dif; apply/imsetP;
            eapply ex_intro2; eapply erefl.
      rewrite cardsD (three_points_card (ts3 tts)) cards2
        (inj_eq (three_points_inj (ts3 tts))) addm1_dif
        cardsI1 -(three_points_eq (ts3 tts)) (three_points_in _ (ts3 tts)).
      by move/leqif_refl/eqP.
    rewrite cards2 (_ : (p |: [set tps j; tps (j + 1)]) !=
                        (p |: [set tps j; tps (j - 1)%R])); last first.
      apply/negP => /eqP A.
      have : tps (j + 1) \in p |: [set tps j; tps (j + 1)].
        by rewrite !inE eqxx ?orbT.
      rewrite A !inE !(inj_eq (three_points_inj (ts3 tts))).
      rewrite -(eq_sym j) (negbTE (add1m1_dif _)) -(addrC 1).
      rewrite (negbTE (add1_dif _)) ?orbF=> /eqP A'.
      case/negP: pnotin; rewrite -cover_ts; apply/bigcupP; exists t=> //.
      by rewrite -A' (three_points_in _ (ts3 tts)).
    rewrite ltnNge leq_eqVlt negb_or=> /andP[] /negbTE -> _.
    apply/esym/negP=>/cards1P [t' t'q]; move: (set11 t'); rewrite -t'q !inE.
    by move => /andP[] /eqP -> A; case ent.
  apply/idP/idP => /cards1P [t' t'q]; move: (set11 t'); rewrite -t'q !inE=>
     /andP[] w1 esub'.
    move/imsetP: w1 => [q qt qq].
    have : e \subset t :\ q.
      apply/subsetP=> x xe.
      move/(subsetP esub'): (xe); rewrite qq inE=> /orP[|-> // ].
      by rewrite inE=>/eqP A; case/negP: pn; rewrite -A.
    move=> esubtmq; apply/cards1P; exists t; apply/setP=> x; apply/idP/idP.
      by rewrite !inE=> /andP[] ->.          
    rewrite !inE => /eqP ->; rewrite eqxx; apply: (subset_trans esubtmq).
    by apply/subsetP=> y; rewrite !inE=> /andP[] _ ->.
  move: esub'; rewrite (eqP w1) => esub' {w1 t'q t'}.
  have/cards1P [q qq] : #|t :\: e| == 1%N.
    by rewrite cardsD (setIidPr esub') (ts3 tts) (eqP ec2).
  have tmq : t :\ q = e by rewrite -qq setDDr setDv set0U (setIidPr esub').
  apply/cards1P; exists (p |: e); apply/setP=> t'; apply/idP/idP.
    rewrite !inE=>/andP[] /imsetP [q' q't qq'] esub''.
    have [/eqP qisq' | qnq']:= boolP (q == q'); first by rewrite -tmq qq' qisq'.
    have/subset_leqif_cards : p |: e \subset t'.
      by rewrite subE esub'' qq' !subE.
    have/eqP eIp : [set p] :&: e == set0 by rewrite setIC setI1.
    have -> : #| t' | = 3.
      rewrite qq' cardsU cardsD (setIidPr (_ : [set q'] \subset t)); last first.
        by rewrite subE.
      have/eqP -> : [set p] :&: (t :\ q') == set0. 
        by rewrite setIC setI1 inE (negbTE pnt) andbF.
      by rewrite (ts3 tts) !cards1 cards0.
    by rewrite cardsU eIp (eqP ec2) cards1 cards0 eq_sym=>/leqif_refl.
  rewrite !inE=> /eqP ->; rewrite subsetUr andbT; apply/imsetP.
  have qt : q \in t by move: (set11 q); rewrite -qq !inE=>/andP[].
  by exists q; rewrite // tmq.
apply/idP/idP; rewrite !cards_eq0=> cnd; rewrite -subset0; apply/subsetP.
  move=>x ; rewrite !inE => /andP[] /eqP -> esubt.
  have/cards1P [q qq] : #| t :\: e | == 1%N.
    by rewrite cardsD (ts3 tts) (setIidPr esubt) (eqP ec2).
  have tmq : t :\ q = e by rewrite -qq setDDr setDv set0U (setIidPr esubt).
  suff : p |: t :\q \in set0 by rewrite inE.
  rewrite -(eqP cnd) !inE tmq subsetUr andbT; apply/imsetP; exists q.
    by move: (set11 q); rewrite -qq !inE=> /andP[].
  by rewrite tmq.
move: (pnotints h) => pn.
move=> x; rewrite !inE=> /andP[] /imsetP [q qt qq] esubx.
move: esubx; rewrite qq.
rewrite -subDset (_ : e :\ p = e); last first.
  apply/setP => y; rewrite !inE; apply/idP/idP; first by move/andP=>[].
  move=> yine; rewrite yine andbT; apply/negP=> yp; case/negP: pn.
  by rewrite -(eqP yp).
move=> esubtmq; have esubt : e \subset t.
  by apply/(subset_trans esubtmq)/subsetP=> y; rewrite !inE=>/andP[].
suff : t \in set0 by rewrite inE.
by rewrite -(eqP cnd) !inE eqxx esubt.
Qed.

Lemma add_point_in_convex_hull_path :
  convex_hull_path (p |: s) s' f.
Proof using all_in_hull boundary_edge_not_empty ch coords cover_ts f gpp
  p0 pinst s'_b s3 ss' ts3 tts.
have [b bin] : exists b, b \in cover (boundary_edge ts).
  have [e ein] : exists e, e \in boundary_edge ts by apply/set0Pn.
  move: ein; rewrite !inE =>/andP [ec2 eP].
  have/card_gt0P [b bine] : (0 < #|e|)%N by rewrite (eqP ec2).
  by exists b; apply/bigcupP; exists e; rewrite // !inE ec2 eP.    
have := use_convex_hull_path ch; rewrite s'_b=> /(_ _ _ bin) => univ.
move/forallP: (ch) => /(_ b); rewrite s'_b bin implyTb=>/andP[] /andP [] _.
move=> _ /eqP orbit_boundary.
apply/forallP=> y; apply/implyP=> ycover.
have bs' : b \in cover s' by rewrite s'_b.
have ys' : y \in cover s' by rewrite s'_b.
have := ys'; rewrite (convex_hull_path_orbit bs') => yo.
have := bs'; rewrite (convex_hull_path_orbit ys') => bo.
have edge_step v2 w v1 :
   v1 \in [set v2; f v2] -> v2 \in orbit f w -> v1 \in orbit f w.
  move=> v1in; rewrite -!fconnect_orbit => v2in; apply: (connect_trans v2in).
  by move: v1in; rewrite !inE=>/orP[]/eqP ->; rewrite ?connect0 // fconnect1.
have fy_edge : f y \in [set y; f y] by rewrite !inE eqxx orbT.
have fyo : f y \in orbit f b.
  by apply: (edge_step _ _ _ (_ : f y \in [set y; f y])).
have fyb : f y \in cover (boundary_edge ts).
  apply/bigcupP; exists [set y; f y] => //.
  by rewrite -orbit_boundary; apply/imsetP; exists y; rewrite // inE.
rewrite fyb andTb; apply/andP; split; last first.
  rewrite -orbit_boundary; apply/eqP/setP/subset_eqP/andP;split; apply/subsetP;
  move=> z /imsetP[w wino zinw]; apply/imsetP; exists w; rewrite //.
    by move: yo wino; rewrite -!fconnect_orbit; apply: connect_trans.
  by move: bo wino; rewrite -!fconnect_orbit; apply:connect_trans.
apply/forallP=> q; apply/implyP=>qin; apply/implyP=>/andP[qny qnfy].
move: qin; rewrite !inE=>/orP[/eqP -> | qs ]; last first.
  move/forallP: ch => /(_ y); rewrite ys' implyTb => /andP[] /andP[] _.
  by move/forallP=> /(_ q); rewrite qs qny qnfy.
have  t3 : #|t| = 3 by apply: ts3.
have tsubs : t \subset s by rewrite -cover_ts; apply: (bigcup_sup t).
apply: (inside_triangle_in_hull ys' t3 tsubs gpp) => //.
by move=> i; apply all_in_hull; apply/(subsetP tsubs)/three_points_in.
Qed.

Lemma add_point_in_covers_triangulation :
  covers_triangulation s ts ->
  covers_triangulation (p |: s)
    (ts :\ t :|: [set p |: t :\ q  | q in t]).
Proof using tts ts3 s'_b pnotin pinst p0 cover_ts.
move=> csts; apply/forallP=> f'; rewrite add_point_in_new_boundary -s'_b.
apply/implyP=> ch'; apply/forallP=> x; apply/implyP=> gpx.
apply/implyP=> xnotin; apply/implyP=> xinh.
have chs' : convex_hull_path s s' f'.
  apply/forallP=> y; apply/implyP=> yin.
  move: (ch')=>/forallP/(_ y); rewrite yin implyTb=>/andP[] /andP[] -> ch'y ->.
  rewrite andbT andTb; apply/forallP=> q; apply/implyP=> qs.
  by move: ch'y => /forallP/(_ q); rewrite inE qs orbT implyTb.
have gp : general_position (x |: s).
  apply/(general_position_sub gpx).
  by rewrite !subE subsetU // subsetUr orbT.
have xns : x \notin s.
  by apply:(contraNN _ xnotin); rewrite inE => ->; rewrite orbT.
move: (csts)=>/forallP/(_ f'); rewrite -s'_b chs' implyTb=>/forallP/(_ x).
rewrite gp xns xinh=>/existsP[t' /andP[t'ts xint']].
have [/eqP t't | t'nt ] := boolP( t' == t); last first.
  by apply/existsP; exists t'; rewrite !inE t'nt t'ts xint'.
move: xint' {t'ts}; rewrite t't=> xint.
set tps := three_points t.
have tsubs : t \subset s by rewrite -cover_ts; apply: bigcup_sup.
have ct3 := ts3 tts.
have gp5 : general_position [set tps 0; tps 1; tps (-1); p; x].
  apply/(general_position_sub gpx).
  by rewrite !subE !(subsetP tsubs) ?orbT // !(three_points_in _ ct3).
have pnx : p != x.
  by apply: (contraNN _ xnotin)=> /eqP ->; rewrite !inE eqxx.
have pxnt : (p \notin t) && (x \notin t).
  apply/andP; split; first by apply:(contraNN _ pnotin); apply/subsetP.
  by apply:(contraNN _ xns); apply/subsetP.
have tpit i : tps i \in t by apply: (three_points_in _ ct3).
have un5 : uniq [:: tps 0; tps 1; tps (-1); p; x].
  rewrite (uniq_catC [:: tps 0; tps 1; tps (-1)] [:: p; x]) !cat_cons cat0s.
  rewrite 2!cons_uniq in_cons negb_or pnx.
  have -> : uniq [:: tps 0; tps 1; tps (-1)].
    by rewrite !cons_uniq !in_cons /= !(inj_eq (three_points_inj ct3)).
  by rewrite !in_cons !negb_or ?andbT ?andTb !andbA; repeat (apply/andP; split);
  apply/negP=>/eqP A; move: pxnt; rewrite A tpit ?andbF ?andFb.
set tpv := [set tps 0; tps 1; tps (-1)].
have tpvt : tpv = t.
  have/subset_leqif_cards : tpv \subset t by rewrite !subE !tpit.
  have /eqP -> : #|[set tps 0; tps 1; tps (-1)]| == 3.  
    by rewrite cards3 !(inj_eq (three_points_inj ct3)).
  by rewrite ct3=>/leqif_refl/eqP.
have := choose_sub_triangle gp5 un5; rewrite -/tpv tpvt=> /(_ pinst xint).
rewrite -!(setUC [set p]).
have ctmi i : #| t :\ tps i| = 2.
  have si : [set tps i] \subset t by rewrite subE (three_points_in _ ct3).
  by rewrite cardsD (setIidPr si) ct3 cards1.
by move=>/orP[ | /orP []] it;
  (apply/existsP; eexists; apply/andP;split;[ | eapply it]); rewrite !inE;
  apply/orP; right; apply/imsetP;
    [exists (tps (-1)) | exists (tps 0) | exists (tps 1)];
    rewrite ?(three_points_in _ ct3) //; (congr (_ :|: _)); set e := (LHS);
    set e' := (RHS);
    (have ce2 : #|e| == 2 by rewrite cards2 (inj_eq (three_points_inj ct3)));
    (have/subset_leqif_cards : e \subset e' by
      rewrite !subE !(inj_eq (three_points_inj ct3)) 
         !(three_points_in _ ct3)); rewrite (eqP ce2) /e' ctmi=>
       /leqif_refl/eqP.
Qed.    

End add_point_in.

End fixed_hull.

Lemma pick_set_none (s : {set P}) : pick_set s = None -> s = set0.
Proof using pick_setP.
case s0P : (s == set0); first by move=> _; apply/eqP.
by move/negbT/pick_setP: s0P=> [x x1 ->].
Qed.

Lemma all_triangles_in n (s : {set _}) :
  (#|s| <= n)%N -> (3 <= #|s|)%N -> general_position s ->
  (forall t, t \in naive_triangulate n s -> #|t| = 3) /\
  cover (naive_triangulate n s) = s /\
  convex_hull_triangulation s (naive_triangulate n s).
Proof using pick_triangle_in pick_triangleP pick_set_in pick_setP
  p0.
elim: n s => [ | n IH] s sn s3 gps; first by move: (leq_trans s3 sn).
rewrite /=; case psv: (pick_set _) => [p | ]; last first.
  by move/pick_set_none: psv s3=> ->; rewrite cards0.
move/pick_set_in: psv => ps.
case: ifP => [s3' | /negbT ].
  have s3'' : #|s| = 3 by apply/eqP; rewrite eqn_leq s3'.
  split; first by move=> t; rewrite inE => /eqP ->; apply/eqP; rewrite s3''.
  split; first by rewrite /cover big_set1.
  by apply: convex_hull_triangulation_base.
set ts := naive_triangulate n (s :\ p).
have pnotin : p \notin (s :\ p) by rewrite !inE eqxx.
have addprem : p |: s :\ p = s by rewrite setD1K.
have gp_addp : general_position (p |: s :\ p) by rewrite addprem.
rewrite -ltnNge => s3'.
have [smpn smp2] : (#|s :\ p| <= n)%N /\ (2 < #|s :\ p|)%N.
  by move: (cardsD1 p s) sn s3'; rewrite ps => ->.
have smps : s :\ p \subset s by apply: subD1set.
move: {IH} (IH (s :\ p) smpn smp2 (general_position_sub gps smps)) => IH.
move: IH => [IH1 [IH2 /andP[/andP[/andP[IH3 IH6] /existsP [chf IH5]] IH4]]].
move/forallP:(IH4)=> /(_ chf)/implyP/(_ IH5)/forallP IH4'.
case ptv : (pick_triangle _ _) => [t | ]; last first.
  move:(IH4' p); fold ts.
  rewrite pnotin gp_addp /=; move=> h_ex.
  have n_ex' : ~~ [exists t in ts, inside_triangle p t].
    by apply/negP; rewrite pick_triangleP ptv.
  move: h_ex; rewrite implybE (negbTE n_ex') orbF => pnot_in_hull.
  set ch := \bigcup_(e in boundary_edge ts) e.
  have [pp0] := purple_point smp2 IH3 IH5 pnotin gp_addp pnot_in_hull.
  fold ts => /andP [pp0P /andP [pp0P2 pp0P3]].
(*
  set pp1 := chf pp0.
  have pp1in : pp1 \in cover (boundary_edge ts).
    by move/forallP: IH5 => /(_ pp0); rewrite pp0P implyTb => /andP [] /andP [].
*)
  have [pp [i [pp_ch [ilto [reds blues]]]]] :=
    partition_red_blue smp2 IH3 IH5 pnotin gp_addp pnot_in_hull.
  move: (ilto) => /andP [] igt0 ilo.
  split.
    move=> t; rewrite inE => /orP [tts | ]; first by apply: IH1.
    move/bigcupP => [h hts /imsetP [q]].
    rewrite !inE => /andP [qh _] ->.
    rewrite cardsU1 !inE (negbTE (_ : p \notin h)) ?andbF; last first.
      apply/negP=> a; case /negP: pnotin.
      by rewrite -IH2; apply/bigcupP; exists h; rewrite ?hts.
    by rewrite -[~~false]/true -qh -cardsD1; apply: IH1.
  have := IH5=> /'forall_implyP IH5'.  
  move: (IH5' pp pp_ch) => /andP [] pp1_bnd be.
  split.
    apply/setP/subset_eqP/andP; split; apply/subsetP=> x.
      move/bigcupP=> [t]; rewrite !inE => /orP [ tts xt | /bigcupP [t' t's]].
      rewrite -addprem inE -IH2; apply/orP.
        by right; apply/bigcupP; exists t.
      move/imsetP=> [q]; rewrite !inE => /andP [qt'] _ ->; rewrite !inE.
      case/orP=> [/eqP -> // | /andP [_ xint']].
      by rewrite -addprem -IH2 inE; apply/orP; right; apply/bigcupP; exists t'.
    rewrite -addprem inE ![in X in X || _]inE => /orP [/eqP -> | ]; last first.
      rewrite -IH2 => /bigcupP [t tts xint]; apply/bigcupP; exists t => //.
      by rewrite inE tts.
    have ppin : pp \in orbit chf pp by rewrite -fconnect_orbit connect0.
    set tp := [set pp; chf pp; p].
    apply/bigcupP; exists tp; last by rewrite !inE eqxx ?orbT.
    rewrite inE; apply/orP; right.
    have : [set pp; chf pp]
          \in boundary_edge (naive_triangulate n (s :\ p)).
      by rewrite -(eqP be); apply/imsetP; exists pp.
    rewrite inE => /andP [] c2 /cards1P [] tp' tp'P.
    move: (set11 tp'); rewrite -tp'P inE => /andP [tp'ts esubtp'].
    apply/bigcupP; exists tp' => //.
    have [q pq] : exists q, tp' :\: [set pp; chf pp] = [set q].
      suff /cards1P : #|tp' :\: [set pp; chf pp]| == 1%N by [].
      by rewrite cardsDS // IH1 // (eqP c2).
    have qtp' : q \in tp'.
      by move: (set11 q); rewrite -pq !inE andbC => /andP [].
    apply/imsetP; exists q; last first.
(* TODO : this should be in math-comp *)
      by rewrite -pq setDDr (setIidPr esubtp') setDv set0U setUC.
    have tp'mq : tp' :\ q = [set pp; chf pp].
      by rewrite -pq setDDr (setIidPr esubtp') setDv set0U.
    rewrite inE qtp' andTb; apply/andP; split; last first.
      by rewrite -(eqP be); apply/imsetP; exists pp; rewrite // inE.
    rewrite /separated qtp' andTb; case: (pickP _) => [w1 /= |]; last first.
      by rewrite tp'mq => /(_ (chf pp)); rewrite !inE eqxx ?orbT.
    rewrite tp'mq !inE => /orP [] /eqP -> { w1 }.
      case: (pickP _) => [w2 /= |]; last first.
        move=>/(_ (chf pp)); rewrite !inE eqxx ?orbT eq_sym.
        by move: c2; rewrite cards2 eqSS eqb1 => ->.
      rewrite !inE => /andP [] /negbTE ->; rewrite orFb => /eqP -> { w2 }.
      move/andP: pp1_bnd => [] _ /forallP/(_ q).
      have -> : q \in s :\ p  by rewrite -IH2; apply/bigcupP; exists tp'.
      have := eqxx q; rewrite -in_set1 -pq !inE negb_or => /andP [] /andP [].
      move=> -> -> _ /= => detq; rewrite detq.
      by move: (reds 0%N); move: (ilto) => /andP [] -> _ => ->.
    case: (pickP _) => [w2 /= |]; last first.
      move=>/(_ pp); rewrite !inE eqxx orTb.
      by move: c2; rewrite cards2 eqSS eqb1 => ->.
    rewrite !inE => /andP [] /negbTE ->; rewrite orbF => /eqP -> { w2 }.
    move/andP: pp1_bnd => [] _ /forallP/(_ q).
    have -> : q \in s :\ p  by rewrite -IH2; apply/bigcupP; exists tp'.
    have := eqxx q; rewrite -in_set1 -pq !inE negb_or => /andP [] /andP [].
    by move: (reds 0%N); rewrite /= orbC igt0=> /(_ isT) -> -> -> _ /= ->.
  (* TODO: move this declaration earlier. *)
  set ts' := ts :|: _.
  rewrite/convex_hull_triangulation andbC.
  have pp'_red : ccw (chf pp) pp p.
      by apply (reds 0%N); rewrite igt0.
  have bts_st: {in cover (boundary_edge ts),
                 forall x, chf x \in cover (boundary_edge ts)}.
    by apply: (convex_hull_stable IH5).
  have chf_inj : {in cover (boundary_edge ts) &, injective chf}.
    by apply: (convex_hull_path_inj smp2 IH3 IH5).
  move: ilto; rewrite size_orbit => ilto'.
  have := orbit_new_path_boundary_edge smp2 IH3 IH5 IH1 IH2
        erefl pnotin pp_ch pp'_red ilto' reds. 
  rewrite -size_orbit=> /(_ blues (eqP be)).
  set chf' := new_path _ _ _ _; rewrite -/ts' => new_boundary.
  have      := add_point_out_new_convex_hull_path smp2 IH3 IH5 pnotin pp_ch
          pp'_red ilto' reds.
  rewrite -size_orbit=> /(_ blues)=> new_chf.
(* aie, aie, je ne sais pas finir, ici. *)
  have ppnp : pp != p.
    apply/negP=> A; case/negP: pnotin; apply: (subsetP IH3).
    by rewrite -[X in X \in _](eqP A).
  have pp1np : pp != p.
    apply: (contraNN _ pnotin) => /eqP {1}<-; apply: (subsetP IH3).
    rewrite -(eqP be).
    apply/bigcupP; exists [set pp; chf pp]; rewrite ?inE ?eqxx ?orbT //.
    by apply/imsetP; exists pp; rewrite -?fconnect_orbit ?connect0.
  rewrite /convex_hull; have /negbTE -> : s != set0.
    by apply/set0Pn; exists p.
  have /negbTE -> : boundary_edge ts' != set0.
    apply/set0Pn; exists [set p; chf' p].
    rewrite -new_boundary; apply/imsetP; exists p; rewrite // -fconnect_orbit.
    by rewrite connect0.
  rewrite eqxx andbT.
  have edges_in_s : cover (boundary_edge ts') \subset s.
    apply/subsetP=> x /bigcupP [] e; rewrite !inE => /andP [] _ /cards1P.
    move=> [] t pt xe; move: (eqxx t); rewrite -in_set1 -pt inE andbC.
    move=> /andP [] est; rewrite inE => /orP [] tin.
      apply/(subsetP (subsetDl _ [set p])); rewrite -IH2; apply/bigcupP.
      by exists t => //; apply/(subsetP est).
    move/bigcupP: tin => [] t' t'ts.
    have t'subs : t' \subset s.
      apply: subset_trans _ (subsetDl s [set p]); rewrite -IH2.
    (* TODO: there should be a general theorem about subset and bigcup *)
      by apply/subsetP=> y yt'; apply/bigcupP; exists t'.
    move=>/imsetP [] q; rewrite inE => /andP [] _ _ => tq.
    have : x \in p |: t' :\ q by rewrite -tq; apply: (subsetP est).
    rewrite !inE => /orP [/eqP -> // |/andP [] _ xt'].
    apply/(subsetP (subsetDl _ [set p])); rewrite -IH2; apply/bigcupP.
    by exists t'.
  apply/andP; split; last first.
    apply/andP; split => //.
    apply/existsP; exists chf'.
    by move: new_chf; rewrite -/chf' new_boundary (setD1K ps).
  have hullc : forall x, general_position (x |: s :\p) -> x \notin s :\ p ->
    in_hull (cover (boundary_edge ts)) chf x ->
         [exists t, (t \in ts) && (inside_triangle x t)].
    by move=> x gp xns xi; move:(IH4' x); rewrite gp xi xns.
  have := add_point_out_covers_triangulation smp2 IH3 IH5 IH1 IH2 erefl pnotin
     pp_ch pp'_red ilto' reds _ (eqP be) hullc.
     rewrite -size_orbit => /(_ blues).
  by rewrite addprem.
move: (pick_triangle_in ts p); rewrite ptv => /andP [tint pinst].
(* start of the case where a triangle contains the new point *)
split.
  move=> t'; rewrite !inE. move=>/orP[/andP[_ /IH1 //] | ].
  move/imsetP=>[q qt t'qtmp]; rewrite t'qtmp cardsU.
  have : p \notin t :\ q.
    apply/(contraNN _ pnotin); rewrite inE -IH2=> /andP [_ pt].
    by apply/bigcupP; exists t.
  rewrite setIC -setI1 => /eqP ->; rewrite cards0 subn0.
  rewrite [#|[set p]|](_ : #|[set p]| = (q \in t)); last by rewrite cards1 qt.
  by rewrite -cardsD1 IH1.
split.
  apply/setP/subset_eqP/andP; split; apply/subsetP=> x.
    move=>/bigcupP [t']; rewrite !inE=> /orP [/andP [] tnt' t'ts | ] xin'.
      by apply: (subsetP smps); rewrite -IH2 -/ts; apply/bigcupP; exists t'.
    move/imsetP: xin'=> [q qt t'tmqp]; rewrite t'tmqp !inE orbC.
    move=>/orP [/andP[xq xt] | /eqP -> //].
    by apply: (subsetP smps); rewrite -IH2; apply/bigcupP; exists t.
  rewrite -addprem; rewrite 2!inE=>/orP[/eqP -> | ].
    have : (0 < #|t|)%N  by rewrite IH1.
    move/card_gt0P=> [q' qt]; apply/bigcupP; exists (p |: t :\ q').
      by rewrite inE; apply/orP; right; apply/imsetP; exists q'.
    by rewrite !inE eqxx.
  rewrite -IH2=>/bigcupP[t' t'ts xin'].
  have t'3 : #|t'| = 3 by apply: IH1.
  have sxt' : [set x] \subset t' by rewrite subE.
  have/card_gt0P[q1 q1t'] : (0 < #|t' :\ x|)%N.
    by rewrite cardsD IH1 // (setIidPr sxt') cards1.
  have q1t'' : q1 \in t' by move: q1t'; rewrite !inE=>/andP[].
  have q1nx : q1 != x.
    by move: q1t'; rewrite !inE=>/andP[].
  have sxq1t' : [set x; q1] \subset t' by rewrite !subE xin'.
  have/cards1P[q2 tmxq1] : #| t' :\: [set x; q1] | == 1%N.
    by rewrite cardsD IH1 // (setIidPr sxq1t') cards2 (eq_sym x) q1nx.
  move/(set3_D2_dec t'3): tmxq1 => t'_dec.
  have [/eqP -> | tnt'] := boolP (t == t').
    apply/bigcupP; exists (p |: t' :\ q1).
      rewrite !inE; apply/orP; right.
      by apply/imsetP; exists q1.
    by rewrite t'_dec !inE eqxx orbC (eq_sym x) q1nx.
  by apply/bigcupP; exists t'; rewrite // !inE (eq_sym t') t'ts tnt'.
have smpsubs :  s :\ p \subset s by apply: subsetDl.
have boundary_n0 : boundary_edge ts != set0.
  by move: IH6; rewrite -cards_eq0 eqn0Ngt (ltn_trans _ smp2).
have nb := (add_point_in_new_boundary IH1 IH2 pnotin tint).
apply/andP; split.
  apply/andP; split.
    apply/andP; split.
      apply/subsetP=> x /bigcupP [e eb xine].
      move: (eb); rewrite !inE=>/andP[ec2] /cards1P [t' t'q].
      move: (set11 t'); rewrite -t'q inE => /andP [ct' esubt'].
      have xin' : x \in t' by rewrite (subsetP esubt').
      move : ct'; rewrite !inE => /orP [ /andP [] _ xinsmp | ].
        apply (subsetP smpsubs).
        rewrite -IH2 -/ts; apply: (subsetP (_ : t' \subset _)) => //.
        by apply: (bigcup_sup t').
      move/imsetP=>[] q qint t'v; move: xin'; rewrite t'v !inE => /orP[].
        by move=> /eqP ->.
      move/andP=>[] _; apply/subsetP; apply: subset_trans smpsubs.
      by rewrite -IH2; apply: (bigcup_sup t).
    by rewrite nb (negbTE boundary_n0) -cards_eq0 eqn0Ngt (ltn_trans _ s3').
  apply/existsP; exists chf.
  have inh : forall q, q \in s :\ p -> in_hull (cover (boundary_edge ts)) chf q.
    move=> q qsp; apply/forallP=> x; apply/implyP=> xb; apply/implyP.
    move=> /andP[] qnx qnfx.
    move: IH5 => /forallP/(_ x)/implyP/(_ xb)/andP[]/andP[] _/forallP/(_ q).
    by rewrite qsp qnx qnfx !implyTb.
  have := add_point_in_convex_hull_path smp2 IH3 IH5 IH1 IH2 erefl gp_addp
        tint pinst boundary_n0 inh.
  by rewrite setD1K // nb.
have := add_point_in_covers_triangulation IH1 IH2 erefl pnotin tint pinst
          IH4.
by rewrite setD1K.
Qed.

(* BUG to report : why is triangulation_algorithm.P not visible as simply P ?
   previous versions of the code. *)
Lemma allpoints n (s : {set _}) :
  (#|s| <= n)%N -> general_position s -> cover (naive_triangulate n s) = s.
Proof using p0 pick_triangle_in pick_set_in pick_triangleP pick_setP.
move=> sn gp.
have [s3 | ] := boolP(2 < #|s|)%N.
  by have[_ [it _]] := all_triangles_in sn s3 gp.
rewrite -leqNgt => csle2.
have css : cover [set s] = s.
  apply/setP/subset_eqP/andP; split.
    by apply/subsetP=> x => /bigcupP [y]; rewrite inE=>/eqP ->.
  by apply: bigcup_sup; rewrite inE.
case: (n) => //=.
move=> k; case psv: (pick_set _) => [x | ]; last by [].
have -> // : (#|s| <= 3)%N by apply: (leq_trans _ (leqnSn _)).
Qed.

Lemma disjoint_triangles n (s : {set P}) t t' p :
  (#|s| <= n)%N ->
  (3 <= #|s|)%N -> general_position (p |: s) -> p \notin s ->
  t \in naive_triangulate n s -> t' \in naive_triangulate n s -> 
  inside_triangle p t -> inside_triangle p t' ->
  t = t'.
Proof.
move=> sn s3 gpps pns tin tin' pint pint'.
elim: n s sn s3 gpps pns t t' tin tin' pint pint' => 
  [| n IH] s sn s3 gpp pns /= t t' tin tin' pint pint'.
  by move: tin tin'; rewrite !inE => /eqP -> /eqP ->.
have gps : general_position s.
  by apply/(general_position_sub gpp)/subsetUr.
have [ts3 [_ /andP [_ cvt]]]:= all_triangles_in sn s3 gps.
move: ts3 cvt tin tin'; rewrite /=.
case psq: (pick_set s) => [p' | ]; last first.
  by rewrite !inE => _ _ /eqP -> /eqP ->.
case: ifP.
  by rewrite !inE => _ _ _ /eqP -> /eqP ->.
move/negbT; rewrite -ltnNge => s3' ts3' cvt.
have pnsmp : p \notin s :\ p'.
  by apply/negP=> A'; case/negP: pns; apply/(subsetP (subsetDl _ [set p'])).
have pnp' : p != p'.
  by apply/negP=> /eqP A; case/negP: pns; rewrite A pick_set_in.
have sn' : (#| s :\ p' | <= n)%N by move: sn; rewrite (cardsD1 p') pick_set_in.
have smp3 : (2 < #|s :\ p'|)%N by move: s3'; rewrite (cardsD1 p') pick_set_in.
have gp : general_position s.
  by apply/(general_position_sub gpp)/subsetU; rewrite subxx orbT.
have gp' : general_position (s :\ p').
  by apply/(general_position_sub gp)/subsetDl.
have gpp' : general_position (p |: s :\ p').
  apply/(general_position_sub gpp)/subsetP=> q; rewrite inE => /orP [].
    by rewrite !inE => ->.
  move=> qin; rewrite inE; apply/orP; right.
  by rewrite (subsetP (subsetDl _ [set p'])).
have [ts3 [cov ch]] := all_triangles_in sn' smp3 gp'.
have tsubs u (uin : u \in naive_triangulate n (s :\ p')) : u \subset (s :\ p').
  rewrite -cov; apply/subsetP=> z; rewrite (three_points_eq (ts3 _ uin)).
  move/imsetP=> [] j _ ->.
  by apply/bigcupP; exists u => //; rewrite three_points_in ?ts3.
have gpt u (uin : u \in naive_triangulate n (s :\ p')) :
  general_position (u :|: [set p'; p]).
    apply/(general_position_sub gpp); rewrite !subE pick_set_in // orbT andbT.
    apply: (subset_trans (tsubs _ uin)); apply/subsetP=> z; rewrite !inE.
    by move=> /andP [] _ ->; rewrite orbT.
move: ts3' cvt.
case ptq : (pick_triangle _ p') => [tp | ] ts3' cvt'.
  move: (pick_triangle_in (naive_triangulate n (s :\ p')) p').
  rewrite ptq => /andP [] tpint p'ins.
  set tps := three_points tp.
  have tpsin : forall j, tps j \in s.
    move=> j; rewrite (subsetP (subsetDl _ [set p'])) //.
    rewrite -cov; apply/bigcupP; exists tp => //.
    by rewrite /tps three_points_in // ts3.
  have tpsnq : forall q j, q \notin (s :\ p') -> tps j != q.
    move=> q j /negP qn; apply/negP=> /eqP A; case: qn; rewrite -A.
    rewrite -cov; apply/bigcupP; exists tp => //.
    by rewrite /tps three_points_in // ts3.
  have tpsnp : forall j, tps j != p'.
    by move=> j; apply: tpsnq; rewrite !inE negb_and eqxx.
  have tpsnp' : forall j, tps j != p.
    by move=> j; apply: tpsnq.
  have subtt : (naive_triangulate n (s :\ p') :\ tp \subset
          naive_triangulate n (s :\ p')) by rewrite subsetDl.
  have tpinj : injective tps.
    by rewrite /tps; apply/three_points_inj/ts3.
  have tp0 : tp = [set tps 0; tps 1; tps (-1)].
    by rewrite -three_points_eq' // ts3.
  have tpj j : tp = [set tps j; tps (j + 1); tps (j - 1)].
    by apply/three_points_eqi/ts3.
   have tpmqj j : tp :\ tps j = [set tps (j + 1); tps (j - 1)].
      rewrite (tpj j) -/tps; apply/setP => z.
      apply/idP/idP; rewrite !inE andbC.
        by move=> /andP [] /orP [/orP [] | ] /eqP ->; rewrite eqxx ?orbT.
      by move=> /orP [] /eqP ->; rewrite eqxx ?orbT andTb (inj_eq tpinj);
         elim/elimI3: (j).
  rewrite inE => /orP [] placet.
    rewrite inE => /orP [] placet'.
      by apply: (IH (s :\ p')); rewrite 1?(subsetP subtt) //.
    move: placet; rewrite !inE => /andP [/eqP A placet]; case A.
    suff : inside_triangle p tp.
      by apply: (IH (s :\ p')).
    move/imsetP: placet'=> [q qin t'q].
    move : qin; rewrite {1}(three_points_eq (ts3 _ tpint)).
    move/imsetP=> [j _ qq].
    have tpmq : tp :\ q = [set tps (j + 1); tps (j - 1)].
      by rewrite qq tpmqj.
    move: pint'; rewrite t'q tpmq (setUC [set p']) => pint'.
    move: p'ins; rewrite {1}(tpj j) cycle_set3 => p'ins.
    rewrite (tpj j) cycle_set3; apply: (inside_sub_triangle _ _ p'ins pint').
    apply/(general_position_sub gpp); rewrite !subE !tpsin //.
      by rewrite pick_set_in // ?orbT.
    by rewrite !cons_uniq !inE !(inj_eq tpinj) !negb_or !add1m1_dif
      !(eq_sym (j + 1)) ?(addrC j 1) !add1_dif !tpsnp !tpsnp' 
      !(eq_sym (j - 1)) !addm1_dif ?(andTb, andbT) (eq_sym p' p) pnp'.
  rewrite inE => /orP [] placet'.
    move: placet'; rewrite !inE => /andP [/eqP A placet']; case A.
    suff : inside_triangle p tp by apply: (IH (s :\ p')).
    move/imsetP: placet=> [q qin tq].
    move : qin; rewrite {1}(three_points_eq (ts3 _ tpint)).
    move/imsetP=> [j _ qq].
    have tpmq : tp :\ q = [set tps (j + 1); tps (j - 1)].
      by rewrite qq tpmqj.
    move: pint; rewrite tq tpmq (setUC [set p']) => pint.
    move: p'ins; rewrite {1}(tpj j) cycle_set3 => p'ins.
    rewrite (tpj j) cycle_set3; apply: (inside_sub_triangle _ _ p'ins pint).
    apply/(general_position_sub gpp); rewrite !subE !tpsin //.
      by rewrite pick_set_in // ?orbT.
    by rewrite !cons_uniq !inE !(inj_eq tpinj) !negb_or !add1m1_dif
      !(eq_sym (j + 1)) ?(addrC j 1) !add1_dif !tpsnp !tpsnp' 
      !(eq_sym (j - 1)) !addm1_dif ?(andTb, andbT) (eq_sym p' p) pnp'.
  move/imsetP: placet; rewrite (three_points_eq (ts3 tp _)) //. 
  move=> [q /imsetP [j _ qj]]; rewrite -(three_points_eq (ts3 tp _)) //.
  rewrite qj tpmqj => tval {qj q}.
  move/imsetP: placet'; rewrite (three_points_eq (ts3 tp _)) //. 
  move=> [q /imsetP [j' _ qj]]; rewrite -(three_points_eq (ts3 tp _)) //.
  rewrite qj tpmqj => tval' {qj q}.
  have un5 i : uniq [:: tps i; tps (i + 1); tps (i - 1); p'; p].
    by rewrite !cons_uniq !inE !(inj_eq tpinj) !negb_or !add1m1_dif
       (addrC i 1) !add1_dif !tpsnp !tpsnp' (eq_sym p' p) pnp' addm1_dif.
  move: tval tval'; rewrite -(subrK j j'); elim/elimI3: (j' - j) => tval tval'.
      by rewrite tval tval' add0r.
    move: pint; rewrite tval {2}(add3 j) addrK (setUC [set p']) => pint.
    have := sub_triangle_unique (ts3 _ tpint) (gpt _ tpint) (un5 _) p'ins pint.
    by rewrite (addrC j) -(setUC [set p']) -tval' pint'.
  move: pint'; rewrite tval' (addrC (-1)) subrK -addrA m1m1.
  rewrite (setUC [set p']) => pint'.
  have := sub_triangle_unique (ts3 _ tpint) (gpt _ tpint) (un5 _) p'ins pint'.
  by rewrite -(setUC [set p']) -tval pint.
(* finished the case where p' was in hull *)
have [_ [cov' /andP [/andP [/andP [be1sub be] AA] _]]] :=
    all_triangles_in sn s3 gp.
move/andP: ch => [] /andP [] /andP [] cvb b0 /existsP [] f ch cvt.
(* TODO: bedec should be a general purpose theorem. *)
have bedec e : e \in boundary_edge (naive_triangulate n (s :\ p')) ->
   exists2 bp, e = [set bp; f bp] &
          bp \in cover (boundary_edge (naive_triangulate n (s :\ p'))).
  have/set0Pn [E eb]: boundary_edge (naive_triangulate n (s :\ p')) != set0.
  by rewrite -(eqP b0) -cards_eq0 -lt0n; apply: ltn_trans smp3.
  move: (eb); rewrite inE => /andP [] /eqP e2 _.
  have/card_gt0P[bp bpe]: (0 < #|E|)%N by rewrite e2.
  have bpb : bp \in cover (boundary_edge (naive_triangulate n (s :\ p'))).
    by apply/bigcupP; exists E.
  move: (ch) => /forallP /(_ bp) /implyP /(_ bpb) /andP [] _ /eqP/esym beq.
  rewrite beq => /imsetP [it it1 it2]; exists it => //.
  apply/bigcupP; exists e => //; last by rewrite it2 !inE eqxx.
  by apply/imsetP; exists it.
rewrite inE => /orP [t_old | t_new].
  rewrite inE => /orP [t'_old | t'_new].
    by apply: (IH (s :\ p')).
  move/bigcupP: t'_new => [] t1 t1in /imsetP [] q; rewrite inE.
  move=> /andP [] qin /andP [] sep /bedec [] bp t1qq bpb.
  have := set3_D1_dec (ts3 _ t1in) t1qq => t1dec; rewrite t1qq setUA => t'q.
  have/(separated_immediate p') : ccw bp (f bp) q.
    have qinsmp' : q \in s :\ p' by apply: (subsetP (tsubs _ t1in)).
    have qnbp : q != bp /\ q != f bp.
      move: (ts3 _ t1in) => /eqP; rewrite t1dec cards3 => /andP [] /andP [] _.
      by rewrite eq_sym => -> ->.
    by have := use_convex_hull_path ch bpb qinsmp' (proj1 qnbp) (proj2 qnbp).
  rewrite -t1dec sep=> /inside_triangle_immediate=> /(_ p).
  move=> /(_ (general_position_sub (gpt _ t1in) _)); rewrite t1dec !subE.
  move=> /(_ isT); rewrite (setUC [set f bp]) -cycle_set3 -t'q pint'=> /esym.
  move=> /andP [] /andP [] fbp _ _.
  (* TODO: Check with other uses of ..._in_hull for possible duplication. *)
  move/(Knuth_2 (P := P))/negP: fbp; case.
  apply: (inside_triangle_in_hull smp3 cvb ch bpb
               (ts3 _ t_old) (tsubs _ t_old) gpp' _ pint).
  move=> i; apply/'forall_implyP=> bp1 bp1b; apply/implyP=> /andP [] A B.
  have tpiin : three_points t i \in s :\ p'.
    by apply/(subsetP (tsubs _ t_old))/three_points_in/ts3.
  by apply: (use_convex_hull_path ch).
rewrite inE => /orP [t'_old | t'_new].
  move/bigcupP: t_new => [] t1 t1in /imsetP [] q; rewrite inE.
  move=> /andP [] qin /andP [] sep /bedec [] bp t1qq bpb.
  have := set3_D1_dec (ts3 _ t1in) t1qq => t1dec; rewrite t1qq setUA => tq.
  have/(separated_immediate p') : ccw bp (f bp) q.
    have qinsmp' : q \in s :\ p' by apply: (subsetP (tsubs _ t1in)).
    have qnbp : q != bp /\ q != f bp.
      move: (ts3 _ t1in) => /eqP; rewrite t1dec cards3 => /andP [] /andP [] _.
      by rewrite eq_sym => -> ->.
    by have := use_convex_hull_path ch bpb qinsmp' (proj1 qnbp) (proj2 qnbp).
  rewrite -t1dec sep=> /inside_triangle_immediate=> /(_ p).
  move=> /(_ (general_position_sub (gpt _ t1in) _)); rewrite t1dec !subE.
  move=> /(_ isT); rewrite (setUC [set f bp]) -cycle_set3 -tq pint => /esym.
  move=> /andP [] /andP [] fbp _ _.
  (* TODO: Check with other uses of ..._in_hull for possible duplication. *)
  move/(Knuth_2 (P := P))/negP: fbp; case.
  apply: (inside_triangle_in_hull smp3 cvb ch bpb
               (ts3 _ t'_old) (tsubs _ t'_old) gpp' _ pint').
  move=> i; apply/'forall_implyP=> bp1 bp1b; apply/implyP=> /andP [] A B.
  have tpiin : three_points t' i \in s :\ p'.
    by apply/(subsetP (tsubs _ t'_old)) /three_points_in/ts3.
  by apply: (use_convex_hull_path ch).
have p'nin : p' \notin s :\ p' by rewrite !inE negb_and eqxx.
have gpsDK : general_position (p' |: s :\ p') by rewrite setD1K ?pick_set_in //.
have [red0 red0b red0P] : exists2 red,
  red \in cover (boundary_edge (naive_triangulate n (s :\ p'))) &
  ~~ ccw red (f red) p'.
  have/bigcupP[t1 t1in /imsetP [q]]:= t_new.
  rewrite inE => /andP [] qt1 /andP [] sep /bedec [bp bp1 bp2].
  rewrite bp1 setUA => tq.
  exists bp => //; apply: Knuth_2.
  have := set3_D1_dec (ts3 _ t1in) bp1 => t1dec.
  have/(separated_immediate p') -> : ccw bp (f bp) q.
    have qinsmp' : q \in s :\ p' by apply: (subsetP (tsubs _ t1in)).
    have qnbp : q != bp /\ q != f bp.
      move: (ts3 _ t1in) => /eqP; rewrite t1dec cards3 => /andP [] /andP [] _.
      by rewrite eq_sym => -> ->.
    by have := use_convex_hull_path ch bp2 qinsmp' (proj1 qnbp) (proj2 qnbp).
  by rewrite -t1dec.
have p'nred0 i: p' != iter i f red0.
  apply/negP=> /eqP A; case/negP: p'nin; rewrite (subsetP cvb) // {1}A.
  by apply: (convex_hull_path_iter_in _ smp3 cvb ch).
have nih: ~~in_hull (cover (boundary_edge (naive_triangulate n (s :\ p'))))
              f p'.
  rewrite negb_forall; apply/existsP; exists red0; rewrite red0b (p'nred0 0%N).
  by rewrite (p'nred0 1%N).
have [pp [i [ppb [iint [reds blues]]]]] :=
  partition_red_blue smp3 cvb ch p'nin gpsDK nih.
(* TODO: cleanup the mess between orbit size and order. *)
have /andP [igt0 ilto] := iint.
have ofpp : [set [set x; f x] | x in orbit f pp] =
       boundary_edge (naive_triangulate n (s :\ p')).
  by move: ch => /'forall_implyP /(_ _ ppb) /andP [] _ /eqP.
have gpp3 : general_position (p |: (p' |: s :\ p')).
  by rewrite setD1K ?pick_set_in.
have := orbit_new_path_boundary_edge smp3 cvb ch ts3 cov erefl p'nin
   ppb (reds 0%N igt0) _ reds _ ofpp; rewrite -size_orbit => /(_ iint blues).
set nf := new_path _ _ _ _; set new_ts := (X in (_ :|: X)) => onf.
have t3 : #|t| = 3 by apply: ts3'; rewrite inE t_new orbT.
have qp'npp' : ccw p p' (nf p').
  have :=
   add_point_out_new_convex_hull_path smp3 cvb ch p'nin ppb (reds _ igt0) _
         reds _; rewrite -size_orbit => /(_ iint blues).
  rewrite -/nf; rewrite setD1K ?pick_set_in // => chnf.
have tsub_bigs : t \subset s.
  move: t_new => /bigcupP [] t1 t1in /imsetP [] q _ ->.
  apply/subsetP=> z; rewrite !inE => /orP [/eqP -> | /andP [] _ ].
    by rewrite pick_set_in.
  by move=> /(subsetP (tsubs _ t1in)); rewrite !inE => /andP [] _.
  have tinh:
     forall i', in_hull (cover [set [set x; nf x] | x in orbit nf p'])
                    nf (three_points t i').
    move=> i'; apply /'forall_implyP=> p1 pinh; apply/implyP=> /andP [df1 df2].
    move: chnf => /'forall_implyP /(_ p1).
    rewrite pinh => /(_ isT) /andP [] /andP [] _ /'forall_implyP.
    have tpis : three_points t i' \in s.
      by apply/(subsetP tsub_bigs)/three_points_in.
    by rewrite ?pick_set_in // => /(_ _ tpis); rewrite df1 df2 /= => ->.
  have := orbit_new_path_subset smp3 cvb ch p'nin ppb (reds _ igt0).
  rewrite -size_orbit => /(_ _ iint blues) => sub0.
  have cvnfs : cover [set [set x; nf x] | x in orbit nf p'] \subset s.
    apply/subsetP=> z /bigcupP [] e /imsetP [] y yo ->; rewrite !inE.
    move=> /orP vs.
    have /iter_findex <- : fconnect nf p' z.
      by move: vs => [] /eqP ->;
         rewrite ?(connect_trans _ (fconnect1 _ _)) // fconnect_orbit //.
    set i' := findex _ _ _; case: (sub0 i'.+1); rewrite iterSr new_path_a.
      by rewrite -/nf => ->; rewrite pick_set_in //.
    move=> [] k []; rewrite -/nf => -> _ {z vs i'}.
    have := convex_hull_path_iter_in k smp3 cvb ch ppb.
    apply/subsetP; apply/(subset_trans cvb)/subsetP=> z.
    by rewrite !inE => /andP [].
  rewrite -Knuth_1.
  apply: (inside_triangle_in_hull s3 cvnfs chnf _ t3 tsub_bigs gpp
          tinh pint).
  apply/bigcupP; exists [set p'; nf p']; rewrite ?inE ?eqxx //.
  by apply/imsetP; exists p'; rewrite -?fconnect_orbit ?connect0.
move: qp'npp'; rewrite /nf new_path_p; last first.
  by move: (reds _ igt0); rewrite -Knuth_1 eq_sym => /oriented_diff.
move=> pp'i.
by apply: (pre_new_triangle_unique smp3 cvb ch ts3 cov erefl p'nin ppb
     (reds _ igt0) _ reds _ ofpp pp'i gpp3 t_new t'_new pint pint');
  rewrite -size_orbit.
Qed.

End abstract_over_types.

Require Import QArith.

Open Scope Q_scope.

(* This small file contains the basic geometric proof that will guarantee
that visibility walks in a Delaunay triangulation terminate.  The closest
reference to this proof that I was able to find is the article by
Devillers and Hemsley "the worst visibility walk in a random Delaunay
triangulation is O(\sqrt{n})", jocg, Vol. 7, No. 1, 2016. *)

Definition det3x3 a b c
                  d e f
                  g h i := a * e * i + b * f * g + d * h * c -
                           a * h * f - g * e * c - d * b * i.

Definition triangle_area (A B C : Q * Q) : Q :=
  let (x1, y1) := A in let (x2, y2) := B in let (x3, y3) := C in 
  det3x3 1 x1 y1 1 x2 y2 1 x3 y3.

Definition incircle_fun (A B C D : Q * Q) :=
  let (x1, y1) := A in let (x2, y2) := B in let (x3, y3) := C in 
  let (x4, y4) := D in
  - (x1 ^ 2 + y1 ^2) * det3x3 1 x2 y2 1 x3 y3 1 x4 y4 +
  (x2 ^ 2 + y2 ^2) * det3x3 1 x1 y1 1 x3 y3 1 x4 y4 -
  (x3 ^ 2 + y3 ^2) * det3x3 1 x1 y1 1 x2 y2 1 x4 y4 +
  (x4 ^ 2 + y4 ^2) * triangle_area A B C.

(* incircle_fun A B C D computes the power of D with
   respect to the circle A B C, multiplied by the signed triangle
   area of A B C. *)

Lemma power_decrease A B C D E:
(* power A D B E  - power A B C E =
   power A D B C * triangle_area A B E / triangle_area A B C *)
 triangle_area A D B * incircle_fun A B C E -
 triangle_area A B C * incircle_fun A D B E ==
   - incircle_fun A D B C * triangle_area A B E.
Proof.
revert A B C D E; intros [x1 y1][x2 y2][x3 y3][x4 y4][x5 y5].
unfold incircle_fun, triangle_area, det3x3; ring.
Qed.

(* The value that we wish to show decreases at each step is actually
  incircle t.1 t.2 t.3 q / triangle_area t1. t.2 t.3 *)

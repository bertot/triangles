From mathcomp Require Import all_ssreflect.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Lemma fconnect_step (T : finType) (f : T -> T) (x y : T) :
  fconnect f x y -> (x == y) || (fconnect f (f x) y).
Proof.
move/iter_findex; case: (findex f x y) => [| i] <-; first by rewrite eqxx.
by rewrite iterSr fconnect_iter orbT.
Qed.

Lemma cycle_orbit_iter_order (T : finType) (f : T -> T) x :
  fcycle f (orbit f x) -> iter (fingraph.order f x) f x = x.
Proof.
rewrite /orbit -orderSpred trajectS /=.
elim: (fingraph.order f x).-1 {1 2 6} x => [ | k IH] /= u.
  by rewrite andbT => /eqP.
by rewrite eqxx andTb=>/IH; rewrite -!iterS -!iterSr.
Qed.

Lemma cycle_orbit_iter_order_in (T : finType) (f : T -> T) x y:
  fcycle f (orbit f x) ->
  y \in orbit f x -> iter (fingraph.order f x) f y = y.
Proof.
move=> cyco; rewrite -fconnect_orbit=>/iter_findex <-.
by rewrite -iter_add addnC iter_add cycle_orbit_iter_order.
Qed.

Lemma cycle_orbit_injective (T : finType) (f : T -> T) (x : T) :
  fcycle f (orbit f x) -> {in orbit f x &, injective f}.
Proof.
move=> cyco y z yin zin fq.
rewrite -(cycle_orbit_iter_order_in cyco yin) -(cycle_orbit_iter_order_in cyco zin).
by rewrite -orderSpred !iterSr fq.
Qed.

Lemma orbit_stable (T : finType) (f : T -> T) (x : T) :
  {in orbit f x, forall y, f y \in orbit f x}.
Proof.
 move=> y; rewrite -!fconnect_orbit => yin; apply: (connect_trans yin). 
by rewrite fconnect1.
Qed.

Lemma cycle_orbit_mem (T : finType) (f : T -> T) (x y : T) :
  fcycle f (orbit f x) -> y \in orbit f x ->
  orbit f y =i orbit f x.
Proof.
by move=> C /(orbit_rot_cycle C (orbit_uniq _ _)) [i ->]; apply: mem_rot.
Qed.

Lemma cycle_orbit_order_eq (T : finType) (f : T -> T) x y :
  fcycle f (orbit f x) -> y \in orbit f x ->
  fingraph.order f y = fingraph.order f x.
Proof.
have ? := orbit_uniq f x; move=> cyco ?; rewrite !(order_cycle cyco) //.
by rewrite -fconnect_orbit connect0.
Qed.

Lemma fconnect_step_cycle_orbit (T : finType) (f : T -> T) x :
  fconnect f (f x) x -> fcycle f (orbit f x).
Proof.
move=> c; move/iter_findex: (c); set k := findex _ _ _ => kq.
have oeq : fingraph.order f x = (fingraph.order f (f x)).
  apply: eq_card=> y; rewrite !inE; apply/idP/idP; apply/connect_trans => //.
  by apply/fconnect1.
suff ko : k.+1 = fingraph.order f x.
  rewrite /orbit -orderSpred trajectS /= -cats1 cat_path fpath_traject andTb.
  by rewrite last_traject -ko /= -iterS iterSr kq eqxx.
move/findex_max: (c); rewrite -/k -oeq leq_eqVlt => /orP[/eqP -> // |].
move: (orbit_uniq f x); rewrite /orbit -orderSpred ltnS=> ou A; move: ou.
rewrite trajectS cons_uniq andbC => /andP [] _ /negP; case; apply/trajectP.
by exists k; rewrite ?kq.
Qed.

Lemma order_step (T : finType) (f : T -> T) (x : T) :
  fingraph.order f x = fingraph.order f (f x) /\
    iter (fingraph.order f x) f x = x \/
  fingraph.order f x = (fingraph.order f (f x)).+1.
Proof.
have [/fconnect_step_cycle_orbit cyc | /negP noc] :=
  boolP (fconnect f (f x) x); last first.
  right; rewrite /fingraph.order; set M := fconnect f (f x); set N := pred1 x.
  have -> : #|fconnect f x| = #|[predU M & N]|.
    apply: eq_card=> z; rewrite !inE; apply/idP/idP.
      by move=> ?; rewrite orbC eq_sym; apply: fconnect_step.
    move=> /orP [ | /eqP -> ]; last by rewrite connect0.
    by apply/connect_trans/fconnect1.
  rewrite -[LHS]addn0 -[0%N](_ : #|[predI M & N]| = _) ?cardUI ?card1 ?addn1 //.
  by rewrite eq_card0 // => z; rewrite !inE andbC; apply/negP=>/andP [/eqP ->].
left; split; last by apply: cycle_orbit_iter_order.
by apply/esym/cycle_orbit_order_eq; rewrite // -fconnect_orbit fconnect1.
Qed.

Lemma findex_step (T : finType) (f : T -> T) (x y : T) :
  fconnect f x y -> y != x -> findex f x y = (findex f (f x) y).+1.
Proof.
rewrite /findex fconnect_orbit /orbit -orderSpred /= inE => /orP [-> //| ].
rewrite eq_sym; move=> yin /negbTE ->.
by have [[oq _] | noc] := order_step f x;
  rewrite ?noc // -(orderSpred _ (f x)) trajectSr -cats1 index_cat -oq yin.
Qed.

Lemma findex_eq0 (T : finType) (f : T -> T) (x y : T) :
  (findex f x y == 0%N) = (x == y).
Proof.
apply/idP/idP; last by move=> /eqP ->; rewrite findex0.
by rewrite /findex /orbit -orderSpred /=; case: (x == y).
Qed.

Lemma cycle_orbit_finv_f_in (T : finType) (f : T -> T) (x : T) :
  fcycle f (orbit f x) -> {in orbit f x, cancel f (finv f)}.
Proof.
move => cyco; apply: finv_f_in; first by apply: orbit_stable.
by apply: cycle_orbit_injective.
Qed.

Lemma cycle_orbit_cycle (T : finType) (f : T -> T) (x y : T) :
  fcycle f (orbit f x) -> y \in orbit f x -> fcycle f (orbit f y).
Proof.
move=> cyco yin; rewrite /orbit -(orderSpred f y) /=.
rewrite rcons_path fpath_traject last_traject /= -iterS orderSpred.
by rewrite (cycle_orbit_order_eq cyco yin) (cycle_orbit_iter_order_in cyco yin).
Qed.


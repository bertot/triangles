Require Import concrete_alg ZArith List String.

Open Scope Z_scope.

Definition sample := 
  (142, 49)::(207, 10)::(201,147)::(90, 87)::(30, 61)::(20, 80)::(10,10)::
  (0, 50)::(48,5)::(42,53)::(100,20)::nil.

Definition sample1 := 
  (10,10)::(0, 50)::(48,5)::(42,53)::(100,20)::nil.

Definition sample2 := 
  (0,50)::(48,5)::(42,53)::(100,20)::nil.

Fixpoint check_all_oriented (l : list triangle) :=
  forallb (fun t => 0 <? dsurf (tp1 t) (tp2 t) (tp3 t)) l.

Open Scope string_scope.

Fixpoint positive_to_postscript (p : positive) :=
  match p with
    xH => " 1 "
  | xO p' => append (positive_to_postscript p') " 2 mul "
  | xI p' => append (positive_to_postscript p') "2 mul 1 add "
  end.

Definition Z_to_postscript (x : Z) :=
  match x with
  | Z0 => "0 "
  | Zpos p => positive_to_postscript p
  | Zneg p => append " 0 " (append (positive_to_postscript p) " sub ")
  end.

Definition eol := String (Ascii.ascii_of_nat 10) EmptyString.

Definition triangle_to_postscript_string (t : triangle) :=
  let (p1, p2, p3) := t in
  let (x1, y1) := p1 in
  let (x2, y2) := p2 in
  let (x3, y3) := p3 in
  concat eol
   ((append (append (Z_to_postscript x1) (Z_to_postscript y1))
       "moveto")::
   (append (append (Z_to_postscript x2) (Z_to_postscript y2))
       "lineto")::
   (append (append (Z_to_postscript x3) (Z_to_postscript y3))
       "lineto")::
   (append (append (Z_to_postscript x1) (Z_to_postscript y1))
       "lineto")::nil).

(* To produce a postscript file, just process the output of coqc on this
   file with the command:
   perl -e 'while(<>) {if($take == 1) {print};if(/showpage/){$take = 0};' \
        -e 'if(/Start/){$take = 1}}' *)

Compute append (append "Start here" eol)
    (append (append "%!PS" eol)
    (append (concat eol
    (List.map triangle_to_postscript_string 
                      (fst (naive_concrete sample))))
    (append " stroke showpage" 
      (append eol "End here")))).

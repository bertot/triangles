# Triangulations, from abstract to concrete

Using finite sets and their enumerations, one can write algorithms that build
triangulations.  These algorithms are not efficient, but can progressively be
refined into algorithms that make efficient use of precomputed information.
At a first level, the precomputed information is the convex hull.

Finite functions are used to represent pointers.

# compact proof

In august 2019, file triangles3.v contains a compact proof of correctness,
directly for naive5 without including refinement steps for all the other
steps, as was done initially in triangles.v triangles2.v and ref.v

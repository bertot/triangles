From mathcomp
  Require Import all_ssreflect all_algebra fingroup perm zmodp.
From CoqEAL Require Import hrel param refinements binnat binint.
From triangles Require Import triangulation_algorithm Knuth_axiom5 triangles
    triangles2 fingraph_complements.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import Refinements.Op GRing.Theory Num.Theory.

(* TODO : move to fingraph.complements and potentially to math-comp. *)
Lemma orbit_ext (T : finType)(f g : T -> T) (x : T) :
  {in orbit f x,f =1 g} -> orbit f x = orbit g x.
Proof.
move=> main; rewrite /orbit.
have xo : x \in orbit f x by rewrite -fconnect_orbit connect0.
suff -> : (fingraph.order f x) = (fingraph.order g x).
  move: {-2} x (fingraph.order g x) xo => e n; elim: n e => //= n IH.
  move=> e eino; congr (_ :: _); move/main: (eino) => <-.
  apply: IH; rewrite -fconnect_orbit (connect_trans _ (fconnect1 _ _)) //.
  by rewrite fconnect_orbit.
rewrite /fingraph.order; apply: eq_card => y.
have pathp : forall p e, e \in orbit f x -> fpath f e p = fpath g e p.
  elim => // a p IH e eo /=; move/main: (eo) => /eqP q.
  have [fea | fena] := boolP (f e == a).
    rewrite -(eqP q) fea; apply: IH; rewrite -(eqP fea).
    rewrite -fconnect_orbit (connect_trans _ (fconnect1 _ _)) //.
    by rewrite fconnect_orbit.
  by rewrite -(eqP q) (negbTE fena).
by apply/idP/idP; move/connectP => [] p pp yp; apply/connectP;
  (exists p; last by apply: yp); rewrite 1?pathp // -1?pathp.
Qed.

Section ZZMap.

Variables (P : finType) (R : realDomainType) (p0 : P) (coords : P -> R * R)
  (pick_triangle : {set {set P}} -> P -> option {set P}).

Variables (map : Type) (map0 : map)
  (evalmap : map -> P -> P) (update : map -> P -> P -> map)
  (remove : map -> P -> map) (mapsize : map -> nat).

Hypothesis eval_update1 :
  forall m x y, evalmap (update m x y) x = y.

Hypothesis eval_update_dif :
  forall m x x' y, x <> x' -> evalmap (update m x y) x' = evalmap m x'.

Hypothesis eval_remove :
  forall m x x', x <> x' -> evalmap (remove m x) x' = evalmap m x'.

Hypothesis mapsizeP : 
  forall m p, (fingraph.order (evalmap m) p <= mapsize m)%N
.
Notation inside_triangle := (inside_triangle coords).

Hypothesis pick_triangle_in : forall tr p,
  match pick_triangle tr p with
  | Some x => (x \in tr) && inside_triangle p x
  | _ => true end.

Hypothesis pick_triangleP : forall (tr : {set {set P}}) p,
 [exists x in tr, inside_triangle p x] = (pick_triangle tr p != None).

Definition map_compat (x : P) (m : map) (f : {ffun P -> P}) :=
  [forall y in orbit f x, f y == evalmap m y].

Definition permutations (T : Type) 
 (x:T)(l : seq T) : seq (seq T) :=
 [seq [seq (nth x l ((p : 'S_ _) i)) | i <- enum 'I_(size l)] |
  p <- enum 'S_(size l)].

Lemma permutationsP (T : eqType) (x : T) (l : seq T) (p : pred (seq T)) :
  reflect (exists2 l' : seq T, perm_eq l l' & p l')
  (has p (permutations x l)).
Proof.
apply (iffP idP).
  move/hasP => [] l' pl'; exists l' => //.
  move: pl'=> /mapP /= [] pi _ l'l2.
  rewrite -(in_tupleE l) perm_eq_sym; apply/tuple_perm_eqP; exists pi.
  have sz : size l' = size l by rewrite l'l2 size_map size_enum_ord.
  apply: (eq_from_nth (x0 := x)).
    by rewrite /= sz size_map size_enum_ord.
  rewrite sz; move=> i ilt /=.
  rewrite (nth_map (Ordinal ilt)) ?size_enum_ord //= (tnth_nth x).
  by rewrite l'l2 (nth_map (Ordinal ilt)) ?size_enum_ord.
move=> [l' ll' pl'].
move: ll'; rewrite -(in_tupleE l) perm_eq_sym => /tuple_perm_eqP[] pi /= piP.
apply/hasP; exists l'; rewrite /permutations //=; apply/mapP.
exists pi=> //; first by rewrite mem_enum.
rewrite piP /=; apply: (eq_from_nth (x0 := x)); first by rewrite !size_map.
rewrite size_map size_enum_ord => i ilt /=.
rewrite -[i]/(val (Ordinal ilt)).
rewrite (nth_map (Ordinal ilt)) ?size_enum_ord // nth_ord_enum.
rewrite (nth_map (Ordinal ilt)) ?size_enum_ord //nth_ord_enum.
by rewrite (tnth_nth x).
Qed.

Definition same_triangles (s1 s2 : seq (seq P)) :=
  has [pred l : seq (seq P) |
         [forall i : 'I_(size l), perm_eq (nth [::] l i) (nth [::] s2 i)]
         && (size l == size s2) ]
   (permutations [::] s1).

(*
Definition same_triangles (s1 s2 : seq (seq P)) :=
  (size s2 == size s1) &&
  [exists p : 'S_(size s1),
      [forall i, perm_eq (nth [::] s1 (p i)) (nth [::] s2 i)]].
*)

Lemma orbit_three_points (s : {set P}) (i : 'I_3): #|s| = 3 ->
  orbit (cycle3 (three_points coords p0 s))
    (three_points coords p0 s i) = 
  [:: three_points coords p0 s i; three_points coords p0 s (i + 1);
      three_points coords p0 s (i - 1)].
Proof.
move=> s3.
set c := cycle3 _; set p := _ :: _.
have ocy : fcycle c p.
  by rewrite /= /c !cycle3_add1 // -!addrA p1p1 addNr addr0 !eqxx.
have uc : uniq p.
  rewrite /p /= !inE !(inj_eq (three_points_inj s3)) !negb_or.
  by rewrite addm1_dif add1m1_dif addrC add1_dif.
have : fingraph.order c (three_points coords p0 s i) = 3.
  by rewrite (order_cycle ocy) // !subE.
by rewrite /orbit => ->; rewrite !trajectS /= /c !cycle3_add1 // -addrA p1p1.
Qed.

Lemma orbit_cycle3_seq s e : size s = 3 -> uniq s -> e \in [set y in s] ->
   orbit (cycle3 (three_points coords p0 [set y in s])) e =i s.
Proof.
case: s => [| a [ | b [ | c [ | ]]]] // _ uns.
wlog : e a b c uns/ (e = a).
  move=> main; rewrite !inE => /orP [/eqP -> | ].
    by apply: main => //; rewrite !inE eqxx.
  move=> /orP [/eqP -> | ].
    rewrite [[set y in _]](_ : _ = [set y in [:: b; c; a]]);last first.
      by apply/setP=> z; rewrite !inE orbC orbA.
    move=> z; rewrite -mem_rcons /=; move: z; apply: main=> //.
      by rewrite (uniq_catC [:: b; c] [:: a]).
    by rewrite !subE.
  move=> /eqP ->.
  rewrite [[set y in _]](_ : _ = [set y in [:: c; a; b]]); last first.
    by apply/setP=> z; apply /esym; rewrite !inE orbC orbA.
  move=> z; rewrite -mem_rcons /= -mem_rcons /=; move: z; apply: main => //.
    by rewrite (uniq_catC [:: c] [:: a; b]).
  by rewrite !subE.
move=> -> _.
have sabc : #|[set y in [:: a; b; c]]| = 3.
  by move/card_uniqP: uns => /= <-; apply:eq_card=> z; rewrite inE.
have [j vj] : exists j, a = three_points coords p0 [set y in [:: a; b; c]] j.
  have := three_points_eq coords p0 sabc => tmp.
  have : a \in [set y in [:: a; b; c]] by rewrite !subE.
  by rewrite [X in a \in pred_of_set X]tmp => /imsetP [] j _; exists j.
move=> z; have -> : (z \in [:: a; b; c]) = (z \in [set y in [:: a; b; c]]).
  by rewrite !inE.  
rewrite [in RHS](three_points_eqi coords p0 j sabc) [X in orbit _ X]vj.
by rewrite orbit_three_points // !inE orbA.
Qed.

Lemma pick_triangle_none_cat tr tr' a :
  (pick_triangle_seq coords tr a == None) &&
  (pick_triangle_seq coords tr' a == None) =
  (pick_triangle_seq coords (tr ++ tr') a == None).
Proof.
elim: tr => [ // | t1 tr IH].
rewrite /=; case: (inside_triangle_seq coords t1 a).
  by [].
move: IH; case: (pick_triangle_seq coords tr a).
  move => [v1 v2] /=; case: (pick_triangle_seq coords (tr ++ tr') a).
    by move=> [v1' v'2] _.
  by [].
rewrite /= => ->; case : (pick_triangle_seq coords (tr ++ tr') a) => //.
by move=> [v w].
Qed.

Lemma pick_triangle_invariant_none tr tr' a :
  perm_eq tr tr' ->
  (pick_triangle_seq coords tr a == None) =
  (pick_triangle_seq coords tr' a == None).
Proof.
elim: tr tr' => [ | t1 tr IH] tr'.
  by rewrite perm_eq_sym =>/perm_eq_nilP ->.
move/perm_eq_consP => [k [tl [pk pk']]].
have [kls | kges] := boolP (k < size tr')%N.
  have kles : (k <= size tr')%N by rewrite ltnW.
  have <- : rot (size tr' - k) (t1 :: tl) = tr'.
    by rewrite -pk -rot_addn subnK // rot_size.
  rewrite /rot -pick_triangle_none_cat andbC pick_triangle_none_cat.
  rewrite cat_take_drop /=.
  have [// | ] := boolP (inside_triangle_seq coords t1 a).
  move: (IH _ pk'); case h: (pick_triangle_seq coords tr a) => [[v w] |].
    by rewrite /=; case h' : (pick_triangle_seq coords tl a) => [[y z] |].
  by rewrite -[None == None]/true => /esym /eqP => ->.
move: kges pk; rewrite -leqNgt => kges; rewrite rot_oversize // => ->.
rewrite /=.
have [// | ] := boolP (inside_triangle_seq coords t1 a).
move: (IH _ pk'); case h: (pick_triangle_seq coords tr a) => [[v w] |].
  by rewrite /=; case h' : (pick_triangle_seq coords tl a) => [[y z] |].
by rewrite -[None == None]/true => /esym /eqP => ->.
Qed.

Lemma perm_eq_inside_triangle_seq t1 t2 a :
  perm_eq t1 t2 ->
  inside_triangle_seq coords t1 a =
  inside_triangle_seq coords t2 a.
Proof.
move=> t1t2.
rewrite /inside_triangle_seq.
have -> : [set y in t1] = [set y in t2].
  by apply/setP=> y; rewrite !inE; apply/perm_eq_mem.
by congr (_ && _); move/perm_eq_size: t1t2 =>  ->.
Qed.

Lemma pick_triangle_seq_inv_none tr tr' a :
  same_triangles tr tr' ->
  (pick_triangle_seq coords tr a == None) =
  (pick_triangle_seq coords tr' a == None).
Proof.
move/permutationsP => [] l l1 /= /andP [] /forallP l2 /eqP l3.
rewrite (pick_triangle_invariant_none a l1); move: l tr' l2 l3 {l1}.
elim=> [ | e u IH] [// | e' tr' ] peq /esym/eqP.
  by rewrite size_eq0 => /eqP ->.
rewrite /= eqSS /eqP => sizes.
have pp : perm_eq e e' by apply: (peq ord0).
rewrite (perm_eq_inside_triangle_seq a pp) /=.
case: (inside_triangle_seq coords e' a) => //.
have pp2 : forall i : 'I_(size u),  perm_eq (nth [::] u i) (nth [::] tr' i).
  move=> i; have := (peq (inord (i.+1))) => /=.
  have prf : (i.+1 < (size u).+1)%N by rewrite ltnS.
  by rewrite (inordK (_ : (i.+1 < (size u).+1)%N)).
move: (IH _ pp2 (esym (eqP sizes))).
by case: (pick_triangle_seq coords u a) => [[v w]|];
  case: (pick_triangle_seq coords tr' a) => [[x y]|] //.
Qed.

Lemma pick_triangle_some_perm_eq tr tr' a t1 :
  pick_triangle_seq coords tr a = Some(t1, tr') ->
  perm_eq tr (t1::tr').
Proof.
elim: tr t1 tr' => [ // | t tr IH /= t1 tr'].
have [ ins | outs ] := boolP(inside_triangle_seq coords t a).
  by move=> [ -> ->].
case rec : (pick_triangle_seq coords tr a) => [ [t2 tr2] | //].
  move=> [q1 q2]; rewrite -q1 -q2.
have : perm_eq [:: t, t2 & tr2] [:: t2, t & tr2].
  by rewrite (perm_catCA [:: t] [:: t2] tr2).
by apply: perm_eq_trans; rewrite perm_cons; apply: IH.
Qed.

Definition no_intersection (tr : seq (seq P)) :=
  forall x t1 t2,
    general_position coords 
       (x |: cover [set u in [seq [set y in t] | t <- tr]]) ->
   x \notin cover [set u in [seq [set y in t] | t <- tr]] ->
   t1 \in tr -> t2 \in tr ->
   inside_triangle x [set y in t1] ->
   inside_triangle x [set y in t2] ->
   index t1 tr = index t2 tr.

Lemma pick_triangle_seq_someP tr p t1 tr' :
  pick_triangle_seq coords tr p = Some (t1, tr') <->
  exists bef aft, tr = bef ++ [:: t1 & aft] /\
    pick_triangle_seq coords bef p = None /\
    inside_triangle_seq coords t1 p /\
    tr' = bef ++ aft.
Proof.
split.
  elim: tr tr' => [// | t2 tr IH] tr' /=.
  case hp: (inside_triangle_seq coords t2 p).
    by move=> [<- trtr']; exists [::], tr; repeat split.
  case hr : (pick_triangle_seq coords tr p) => [[v w]|//] [vq wq].
  move: hr; rewrite vq => /IH [bef [aft [dec [bn [inst1 dec']]]]].
  exists (t2 :: bef), aft; split; first by rewrite dec.
  split; first by rewrite /=; rewrite hp bn. 
  split; first by [].
  by rewrite -wq dec'.
move=> [pre [post [-> [bn [inst1 ->]]]]].
elim: pre bn => [_ | t2 pre IH]; first by rewrite /= inst1.
rewrite /=; case: (inside_triangle_seq coords t2 p) => //.
case hp: (pick_triangle_seq coords pre p) => [[v w] |] // _.
by move: hp => /IH ->.
Qed.

Lemma pick_triangle_seq_some_perm_eq tr tr' tr2 tr2' t1 t2 p:
  general_position coords
     (p |: cover [set u in [seq [set y in t] | t <- tr]]) ->
  no_intersection tr ->
  perm_eq tr tr' ->
  p \notin cover [set u in [seq [set y in t] | t <- tr]] ->
  pick_triangle_seq coords tr p = Some (t1, tr2) ->
  pick_triangle_seq coords tr' p = Some (t2, tr2') ->
  t1 = t2 /\ perm_eq tr2 tr2'.
Proof.
move=> gps n_i trtr' pnotin /pick_triangle_seq_someP
  [bef [aft [dec [bn [inst1 dec']]]]].
move=> /pick_triangle_seq_someP [be2 [af2 [de2 [bn2 [inst2 de2']]]]].
have t2in : t2 \in tr.
  by rewrite (perm_eq_mem trtr') de2 mem_cat inE eqxx orbT.
have t1in: t1 \in tr.
  by rewrite dec mem_cat inE eqxx orbT.
have t1t2 : t1 = t2.
  move/andP: (inst1) => [] inst1' _.
  move/andP: (inst2) => [] inst2' _.
  move: n_i => /(_ p _ _ gps pnotin t1in t2in inst1' inst2') => iq.
  by rewrite -(nth_index [::] t1in) -(nth_index [::] t2in) iq.
split => //.
rewrite dec' de2' -(perm_cons t1) (perm_catCA [:: t1] bef aft) perm_eq_sym.
rewrite [X in X :: _ ++ _]t1t2 (perm_catCA [::t2] be2 af2) perm_eq_sym /=.
by rewrite -dec -de2.
Qed.

(* TODO : suggest an addition in seq. *)
Lemma nth_lt_index (T : eqType) (def x : T) (s : seq T) (i : nat) :
  (i < index x s)%N -> nth def s i != x.
Proof. by move=>/(before_find def) /= ->. Qed.

Lemma all_points_naive4 s tr chf bp:
  (2 < #|[set y in s]|)%N ->
  general_position coords [set y in s] ->
  uniq s ->
  naive4 coords p0 s = (tr, chf, bp) ->
  cover [set u in [seq [set y in t] | t <- tr]] = [set y in s].
Proof.
move=> s3 gps uns.
have intc : (2 < #|[set y in s]| <= #|[set y in s]|)%N by rewrite s3 leqnn.
have [ch [bo [qn2 [chP boP]]]]:=
   naive_naive2 p0 pick_triangle_in pick_triangleP
    (pick_seq_setP p0 s) (@pick_seq_set_in _ p0 s) intc gps.
have sizes : size s = #|[set y in s]|.
  by move: (uns)=> /card_uniqP => <-; apply/eq_card => z; rewrite !inE.
have s3' : (2 < size s)%N by rewrite sizes.
have := naive2_naive3 coords p0 pick_triangle (@pick_set P) s3' uns.
rewrite /naive2' => n3n2.
move: qn2; rewrite -sizes -n3n2.
have := naive3_naive4 p0 pick_triangle_in pick_triangleP s3' uns gps.
case qn4 : (naive4 coords p0 s) => [[trs f] bo'] [qn3 untr].
rewrite qn3 => [[trq chq] boq] [trq' chq'] boq'.
move: qn4 qn3 untr trq chP; rewrite -trq' -?chq chq' {trq' tr chq chq' f ch}.
move: boP; rewrite -boq boq' {boq boq' bo' bo}.
move=> boP qn4 qn3 untr trq chP.
move: qn3; rewrite trq => qn3.
have allp := allpoints p0 (pick_seq_setP p0 s) (@pick_seq_set_in _ p0 s)
  pick_triangle_in pick_triangleP (leqnn _) gps.
by rewrite sizes.
Qed.

Lemma all_triangles_naive4 s tr chf bp :
  (2 < #|[set y in s]|)%N ->
  general_position coords [set y in s] ->
  uniq s ->
  naive4 coords p0 s = (tr, chf, bp) ->
  (forall t, t \in tr -> size t = 3 /\ uniq t) /\
  convex_hull_path coords [set y in s]
     (boundary_edge [set u in [seq [set y in t] | t <- tr]]) chf /\
  bp \in cover (boundary_edge [set u in [seq [set y in t] | t <- tr]]) /\
  convex_hull_triangulation coords [set y in s]
      [set u in [seq [set y in t] | t <- tr]] /\
  cover [set u in [seq [set y in t] | t <- tr]] = [set y in s] /\
  no_intersection tr.
Proof.
move=> s3 gps uns.
have intc : (2 < #|[set y in s]| <= #|[set y in s]|)%N by rewrite s3 leqnn.
have [ch [bo [qn2 [chP boP]]]]:=
   naive_naive2 p0 pick_triangle_in pick_triangleP
    (pick_seq_setP p0 s) (@pick_seq_set_in _ p0 s) intc gps.
have sizes : size s = #|[set y in s]|.
  by move: (uns)=> /card_uniqP => <-; apply/eq_card => z; rewrite !inE.
have s3' : (2 < size s)%N by rewrite sizes.
have := naive2_naive3 coords p0 pick_triangle (@pick_set P) s3' uns.
rewrite /naive2' => n3n2.
move: qn2; rewrite -sizes -n3n2.
have := naive3_naive4 p0 pick_triangle_in pick_triangleP s3' uns gps.
case qn4 : (naive4 coords p0 s) => [[trs f] bo'] [qn3 untr].
rewrite qn3 => [[trq chq] boq] [trq' chq'] boq'.
move: qn4 qn3 untr trq chP; rewrite -trq' -?chq chq' {trq' tr chq chq' f ch}.
move: boP; rewrite -boq boq' {boq boq' bo' bo}.
move=> boP qn4 qn3 [] untr alls trq chP.
have := all_triangles_in p0 (pick_seq_setP p0 s) (@pick_seq_set_in _ p0 s)
          pick_triangle_in pick_triangleP (leqnn _) s3 gps.
have cs3 : #|[set y in s]| = size s.
  by rewrite -(card_uniqP uns); apply: eq_card=> z; rewrite inE.
move: qn3; rewrite trq cs3 => qn3 all_in; split.
  move=> t tt.
  move: (all_in) => [] it _; move: it; rewrite -trq => /(_ [set y in t])=> it.
  have := alls t tt => t3; split => //.
  have :#|[set y in t]| = 3.
     by apply:it; rewrite inE; apply:map_f.
  have -> : #|[set y in t]| = #|t| by apply: eq_card => z; rewrite inE.
  by rewrite -t3; move/card_uniqP.
split.
  by move: chP; rewrite cs3.
split.
  by move: boP; rewrite cs3.
split.
  by move: all_in => [] _ [].
split.
  by move: all_in => [] _ [].
rewrite /no_intersection.
move=> p t1 t2 gpsp pnotin t1t t2t inst1 inst2.
have allp := allpoints p0 (pick_seq_setP p0 s) (@pick_seq_set_in _ p0 s)
  pick_triangle_in pick_triangleP (leqnn _) gps.
have gps2 : general_position coords (p |: [set y in s]).
  by rewrite -allp -sizes -trq. 
have pnotin' : p \notin [set y in s].
  by rewrite -allp -sizes -trq.
have disj := disjoint_triangles p0 (pick_seq_setP p0 s)
  (@pick_seq_set_in _ p0 s)
  pick_triangle_in pick_triangleP (leqnn _) s3.
have := disj [set y in t1] [set y in t2] _ gps2 pnotin'.
rewrite -sizes -trq; rewrite !inE.
have t1t' : [set y in t1] \in [seq [set x in t'] | t' <- trs].
  by apply: map_f.
have t2t' : [set y in t2] \in [seq [set x in t'] | t' <- trs].
  by apply: map_f.
move=> /(_ t1t' t2t' inst1 inst2) t1t2.
have [/eqP // | abs] := boolP(index t1 trs == index t2 trs).
have it1le : (index t1 trs < size trs)%N.
  by rewrite index_mem.  
have it2le : (index t2 trs < size trs)%N.
  by rewrite index_mem.  
have /eqP : nth set0 [seq [set y in t] | t <- trs] (index t1 trs) =
       nth set0 [seq [set y in t] | t <- trs] (index t2 trs).
  by rewrite !(nth_map [::]) // !nth_index.
rewrite nth_uniq // ?size_map //.
by move/eqP.
Qed.

Lemma big_and_zipE (T : Type) (d : T) (l1 l2 : seq T) (p : T -> T -> bool):
  \big[andb/true]_(e <- zip l1 l2) p e.1 e.2 =
  [forall i: 'I_(minn (size l1) (size l2)), p (nth d l1 i) (nth d l2 i)].
Proof.
elim: l1 l2 => [| a l1 IH] [|b l2] /=; try
  solve[rewrite big_nil; apply/esym/forallP => [] []; by []].
rewrite big_cons; rewrite IH.
case vp : (p a b); last first.
  rewrite andFb; apply/esym/negbTE/negP=> /forallP.
  by rewrite minnSS => /(_ ord0); rewrite vp.
rewrite andTb.
have [/forallP ae | ] := boolP([forall i : 'I_(minn (size l1) (size l2)),
           p (nth d l1 i) (nth d l2 i)]); last first.
   rewrite negb_forall => /existsP [w pw].
   apply/esym/negbTE; rewrite negb_forall.
   have w1w : (w.+1 < minn (size l1).+1 (size l2).+1)%N.
     by rewrite minnSS; case: w {pw} => w pw /=; rewrite ltnS.
  by apply/existsP; exists (Ordinal w1w) => /=.
apply/esym/forallP.
move=> [ [ | x] px] //=.
have px' : (x < minn (size l1) (size l2))%N.
  by move: px; rewrite minnSS ltnS.
by apply: (ae (Ordinal px')).
Qed.

Lemma pick_triangle_seq_some_same_triangles tr tr' tr2 tr2' t1 t2 a:
   general_position coords 
      (a |: cover [set u in [seq [set y in t] | t <- tr]]) ->
   no_intersection tr ->
   same_triangles tr tr' ->
   a \notin cover [set u in [seq [set y in t] | t <- tr]] ->
   pick_triangle_seq coords tr a = Some (t1, tr2) ->
   pick_triangle_seq coords tr' a = Some (t2, tr2') ->
   perm_eq t1 t2 /\ same_triangles tr2 tr2'.
Proof.
move=> gps n_i stt anotin picktr picktr'.
move/permutationsP: stt => /= [] l l1 /andP [] l2 l3.
case picki:
(pick_triangle_seq coords l a) => [[ti tri]|]; last first.
  by have := pick_triangle_invariant_none a l1 ; rewrite picktr picki.
have := pick_triangle_seq_some_perm_eq gps n_i l1 anotin picktr picki. 
move => [t1i trtri]; move: picki picktr'; rewrite -t1i {t1i ti}=> pick picktr'.
have sztri : size tr2 = size tri by apply: perm_eq_size.
suff : perm_eq t1 t2 /\ same_triangles tri tr2'.
  move=> [] t1t2 /permutationsP [] l' tril' /= /andP [] pp'.
  move=> /eqP szltr2'.
  split; first by [].
  have sztritr2' : size tri = size tr2'.
    by rewrite (perm_eq_size tril') szltr2'.   
  apply/permutationsP; exists l' => //=.
    by apply: perm_eq_trans tril'.
  by apply/andP; split=> //; apply/eqP.
move: l tr' tr2' tri l2 l3 pick picktr' {l1 trtri sztri}.
elim => [// | b u IH] [// | b' tr'] tr2' tri /forallP pp /=.
rewrite eqSS => /eqP sz cmp1 cmp2; have := (pp ord0) => /= bb'.
move: cmp2; rewrite -(perm_eq_inside_triangle_seq a bb').
move: cmp1; case : (inside_triangle_seq coords b a).
  move=> [bt1 utri] [b't2 tr'tr2'].
  rewrite -b't2 -bt1 bb' -tr'tr2'; split; first by [].
  apply/permutationsP; exists tri => //=; apply/andP; split; last first.
    by rewrite -utri sz.
  apply/forallP => x.
  have := pp (inord x.+1).
  by rewrite (inordK (_ : (x.+1 < (size u).+1)%N)) //=; rewrite utri // ltnS.
case cmp1' : (pick_triangle_seq coords u a) => [[t1' l]| //] [tt1' bltri].
move: cmp1'; rewrite tt1' {tt1' t1'} => cmp1'.
case cmp2' : (pick_triangle_seq coords tr' a) => [[t2' l']| //] [tt2' bltri'].
move: cmp2'; rewrite tt2' {tt2' t2'} => cmp2'.
have pp2 : [forall i : 'I_(size u),
               perm_eq (nth [::] u  i) (nth [::] tr' i)].
  by apply/forallP=> i; move: (pp (inord (i.+1))); rewrite inordK // ltnS.
move/eqP: sz=> sz.
move: (IH _ _ _ pp2 sz cmp1' cmp2') => [] t1t2 /permutationsP [] l2 /= pl2.
move=> /andP [] /forallP pp3 /eqP sz3.
split=> //; apply/permutationsP; exists (b:: l2) => /=.
  by rewrite -bltri; rewrite perm_cons.
apply/andP; split;  rewrite -bltri'; last by rewrite sz3.
apply/forallP => [] [[// | x]] /=.
by rewrite ltnS => ilt; apply: (pp3 (Ordinal ilt)).
Qed.

Lemma same_triangles_cat t1 t2 t3 t4 :
  same_triangles t1 t3 -> same_triangles t2 t4 ->
  same_triangles (t1 ++ t2) (t3 ++ t4).
Proof.
move=>/permutationsP /= [l l1 /andP [l2 l3]].
move=>/permutationsP /= [k k1 /andP [k2 k3]].
apply/permutationsP; exists (l++k).
  apply: perm_eq_trans (_ : perm_eq (t1 ++ k) (l ++ k)).
    by rewrite perm_cat2l.
  by rewrite perm_cat2r.
apply/andP; split; rewrite !size_cat (eqP k3) (eqP l3) //.
apply/forallP=> /= x; have [[i ilt] ix| i ix] := splitP x.
  rewrite ix !nth_cat /=; move: (ilt) l2.
  rewrite -[X in (_ < X)%N](eqP l3)=> ilt'; rewrite ilt' => l2.
  by apply (forallP l2 (Ordinal ilt')).
rewrite ix !nth_cat /= !(eqP l3).
have/negbTE -> : ~~(size t3 + i < size t3)%N.
  by rewrite -leqNgt leq_addr.
by rewrite addKn; move: i {ix}; rewrite -(eqP k3); exact (forallP k2).
Qed.

Lemma same_triangles_refl l : same_triangles l l.
Proof.
by apply/permutationsP; exists l => //=; rewrite eqxx andbT; apply/forallP=> x.
Qed.

Lemma perm_swap2_3 (T : eqType)(x y z : T) : perm_eq [:: x; y; z] [:: y; x; z].
Proof. by rewrite (perm_catCA [:: x] [::y] [::z]). Qed.

Lemma perm_swap2_2 (T : eqType)(x y : T) : perm_eq [:: x; y] [:: y; x].
Proof. by rewrite (perm_catC [:: x] [::y]). Qed.

Lemma perm_cyc_3 (T :eqType)(x y z : T) : perm_eql [:: x; y; z] [:: y; z; x].
Proof. by move=> p; rewrite -(perm_rot 2 [:: y; z; x]) /rot.  Qed.

Lemma perm_eq_same_triangles a b c :
  perm_eq a b -> same_triangles a c -> same_triangles b c.
Proof.
move=> ab /permutationsP [d /= ad dd].
apply/permutationsP; exists d => //.
by apply: perm_eq_trans ad; rewrite perm_eq_sym.
Qed.

Lemma auto_same1 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 :
    perm_eq [:: x1; x2] [:: x7; x8] &&
  perm_eq [:: x3; x4] [:: x9; x10] &&
  perm_eq [:: x5; x6] [:: x11; x12] ->
  same_triangles [:: [:: x1; x2; x13]; [:: x3; x4; x13];[:: x5; x6; x13]]
     [:: [:: x7; x8; x13]; [::x9; x10; x13]; [:: x11; x12; x13]].
Proof.
Proof.
move=> facts; apply/permutationsP; eexists; [apply: perm_eq_refl | ].
rewrite /= andbT; apply/forallP => [] [[ | [ | [|] ] ] ?] //=;
rewrite -!(perm_cyc_3 x13) perm_eq_sym -!(perm_cyc_3 x13) ?perm_cons //;
rewrite perm_eq_sym; move: facts => /andP [] /andP [] //.
Qed.

Lemma auto_same2 x1 x2 x3 x4 x5 x6:
same_triangles [::x1; x2; x3] [:: x4; x5; x6] =
same_triangles [::x2; x3; x1] [:: x4; x5; x6].
Proof.
revert x1 x2 x3 x4 x5 x6.
have main x1 x2 x3 x4 x5 x6: same_triangles [::x1; x2; x3] [:: x4; x5; x6] ->
             same_triangles [::x2; x3; x1] [:: x4; x5; x6].
  by move=> *; rewrite (perm_eq_same_triangles (perm_eqlE (perm_cyc_3 _ _ _))).
move=> x1 x2 x3 x4 x5 x6.
by apply/idP/idP;[apply/main |move=> ?; apply/main/main].
Qed.

Lemma auto_same3 x1 x2 x3 x4 x5 x6:
same_triangles [::x1; x2; x3] [:: x4; x5; x6] =
same_triangles [::x2; x1; x3] [:: x4; x5; x6].
Proof.
revert x1 x2 x3 x4 x5 x6.
have main x1 x2 x3 x4 x5 x6: same_triangles [::x1; x2; x3] [:: x4; x5; x6] ->
             same_triangles [::x2; x1; x3] [:: x4; x5; x6].
  by move=> *; rewrite (perm_eq_same_triangles (perm_swap2_3 _ _ _)).
move=> x1 x2 x3 x4 x5 x6.
by apply/idP/idP; apply/main.
Qed.

Lemma traject_connect (T : finType) (f : T -> T) x e n :
  e \in traject f x n -> fconnect f x e.
Proof.
elim: n x => [// | n IH] x /=.
rewrite inE => /orP [/eqP -> // | ein].
by apply: (connect_trans (fconnect1 _ _)); apply: IH.
Qed.

Lemma find_next_purple_mapP a p e purple b n n' chfm chf :
   {in orbit chf a, chf =1 evalmap chfm} ->
   (findex chf (chf e) (chf purple) < n' <= n)%N ->
   ccw coords e (chf e) p = ~~b ->
   ccw coords purple (chf purple) p = ~~b ->
   ccw coords (chf purple) (chf (chf purple)) p = b ->
   chf purple \in traject chf (chf e) n' ->
   e \in orbit chf a ->
   find_next_purple_map coords evalmap p chfm b n (chf e) =
   find_next_purple coords p chf b n' (chf e).
Proof.
move=> ext nint opposite pp1 pp2.
elim: n n' e opposite nint=> [ // | n IH] n' e opposite nint.
  by move: nint; case: (n') => [ | k] // /andP [] //.
move=> pin ein.
have [i_n' n'_n] : (findex chf (chf e) (chf purple) < n' /\ n' <= n.+1)%N.
  by move: nint=> /andP.
rewrite -(ltn_predK i_n') => /=.
have fin : chf e \in orbit chf a.
  rewrite -fconnect_orbit (connect_trans _ (fconnect1 _ _)) //.
  by rewrite fconnect_orbit.
rewrite -(ext _ fin); case : ifP=> // /negbT opposite'.
have dif : chf purple != chf e.
  apply/eqP=> abs; move: pp2; rewrite abs => /eqP.
  by rewrite (negbTE opposite').
apply: IH.
      by move: opposite'; case: (b); case: (ccw _ _ _ _).
    apply/andP; split; last first.
      by rewrite -ltnS (ltn_predK i_n').
    move: nint; rewrite findex_step //; last first.
      by apply: traject_connect pin.
    by move/andP=> []; rewrite -(ltn_predK i_n') ltnS /=.
  by move: pin; rewrite -(ltn_predK i_n') /= inE (negbTE dif) orFb.
by rewrite -fconnect_orbit (connect_trans _ (fconnect1 _ _)) // fconnect_orbit.
Qed.

Lemma map_compat_orbit (f : {ffun P -> P}) fm a b :
  b \in orbit f a -> map_compat a fm f -> map_compat b fm f.
Proof.
move=> bin cm; apply/forallP => x; apply/implyP=> xin.
apply: (implyP (forallP cm x)).
by move: bin xin; rewrite -!fconnect_orbit; apply: connect_trans.
Qed.

Lemma purple_unique (s : {set P}) s' chf p a x y vb:
  (2 < #|s|)%N -> cover s' \subset s ->
  convex_hull_path coords s s' chf ->
  p \notin s ->
  general_position coords (p |: s) ->
  ~~ in_hull coords (cover s') chf p ->
  a \in cover s' ->
  ~~ ccw coords a (chf a) p ->
   x \in cover s' -> y \in cover s' ->
  ccw coords x (chf x) p = ~~vb ->
  ccw coords (chf x) (chf (chf x)) p = vb ->
  ccw coords y (chf y) p = ~~vb ->
  ccw coords (chf y) (chf (chf y)) p = vb -> x = y.
Proof.
move=> s3 ss' ch pnotin gps ninh ab afared xin yin prex atx prey aty.
have := partition_red_blue s3 ss' ch pnotin gps ninh ab afared.
move=> [b [i [bin [iint [reds blues]]]]].
have cov_orbit := (convex_hull_path_orbit s3 ss' ch ab).
move/(convex_hull_cycle' s3 ss' ch): (ab) => cyca.
have boa : b \in orbit chf a by rewrite -cov_orbit.
move: (cycle_orbit_mem cyca boa) => obpoa.
have un1: forall z, z \in cover s' ->
  ccw coords z (chf z) p = true ->
  ccw coords (chf z) (chf (chf z)) p = false ->
  z = finv chf b.
  move=> z zin; move: (zin); rewrite cov_orbit -obpoa -fconnect_orbit => zco.
  move/iter_findex: (zco) => zq.
  move: (findex_max zco); rewrite leq_eqVlt => /orP [| up].
    by rewrite -orderSpred eqSS=> /eqP iq; rewrite -zq iq.
  have [ | ] := boolP(findex chf b z < i)%N.
    by move=> it; move: (reds _ it); rewrite iterS zq=>/Knuth_2/negbTE ->.
  rewrite -leqNgt=> it; have := (blues (findex chf b z).+1).
  by rewrite (leq_trans it) // size_orbit up !iterS zq => /(_ isT) ->.
have un2 : forall z, z \in cover s' ->
  ccw coords z (chf z) p = false ->
  ccw coords (chf z) (chf (chf z)) p = true ->
  z = iter i.-1 chf b.
  move=> z zin; move: (zin); rewrite cov_orbit -obpoa -fconnect_orbit => zco.
  move/iter_findex: (zco) => zq.
  have [ | ] := boolP(findex chf b z < i)%N; last first.
    rewrite -leqNgt=> ile.
    have :z \in orbit chf b by rewrite obpoa -cov_orbit.
    rewrite -fconnect_orbit => zb.
    move: (blues (findex chf b z)); rewrite ile size_orbit (findex_max zb).
    by rewrite iterS zq => /(_ isT) => ->.
  rewrite leq_eqVlt=> /orP [/eqP it |]; first by rewrite -zq -it.
  by move/reds/Knuth_2/negbTE; rewrite !iterS zq=> ->.
move: prex atx prey aty.
case: vb; move=> prex atx prey aty.
  by rewrite (un2 _ xin prex atx); rewrite (un2 _ yin prey aty).
by rewrite (un1 _ xin prex atx); rewrite (un1 _ yin prey aty).
Qed.

Lemma find_purple_points_mapP p (* a *) (s : {set P}) (s' : {set {set P}})
  (chf : {ffun P -> P}) chfm bp bp' :
  (2 < #|s|)%N -> cover s' \subset s ->
  convex_hull_path coords s s' chf ->
  (fingraph.order chf bp <= mapsize chfm)%N ->
  p \notin s ->
  general_position coords (p |: s) ->
  ~~ in_hull coords (cover s') chf p ->
  bp \in cover s' ->
  bp' \in orbit chf bp ->
  map_compat bp chfm chf ->
  find_purple_points_map coords evalmap mapsize p chfm bp' = 
  find_purple_points coords p chf bp.
Proof.
move=> s3 ss' ch condo pnotin gps ninh bpb bp'p cm.
have [a ab afared] : exists2 a, a \in cover s' & ~~ccw coords a (chf a) p.
  move: ninh; rewrite negb_forall=> /existsP [] a; rewrite !negb_imply.
  by move=> /andP [] ab /andP[] _ cc; exists a.
have := partition_red_blue s3 ss' ch pnotin gps ninh ab afared.
move=> [b [i [bb [iint [reds blues]]]]].
(* Why is it so hard to prove that the orbit of b is the cover s' *)
(* just search is not useful enough. *)
have cov_orbit := (convex_hull_path_orbit s3 ss' ch ab).
move/(convex_hull_cycle' s3 ss' ch): (ab) => cyca.
have orbits : {in orbit chf a, forall y, orbit chf y =i orbit chf a}.
  by move=> y yoa; exact: (cycle_orbit_mem cyca yoa).
have chf_oa_stable : {in orbit chf a, forall y, chf y \in orbit chf a}.
  move=> y yoa.
  rewrite -fconnect_orbit; apply/(connect_trans _ (fconnect1 _ _)).
  by rewrite fconnect_orbit.
have boa : b \in orbit chf a by rewrite -cov_orbit.
set pre_b := finv chf b.
have pre_boa : pre_b \in orbit chf a.
  rewrite -fconnect_orbit; apply/(connect_trans _ (fconnect_finv _ _)).
  by rewrite fconnect_orbit -cov_orbit. 
have := f_finv_in chf_oa_stable (cycle_orbit_injective cyca) boa.
  rewrite -/pre_b => fpre.
have pre_blue : ccw coords pre_b (chf pre_b) p = ~~false.
  rewrite /=.
  have pre_int : (i <= (fingraph.order chf b).-1 < size (orbit chf b))%N.
    rewrite -ltnS orderSpred size_orbit leqnn andbT -size_orbit.
    by move/andP: iint => [] _ ->.
  by move: (blues _ pre_int); rewrite -[X in ccw _ X _ _]/pre_b.
have fboa : chf b \in orbit chf a by rewrite mem_orbit1.
have := cycle_orbit_cycle cyca boa => cycb.
have fbb : chf b \in orbit chf b by rewrite -fconnect_orbit fconnect1.
have := cycle_orbit_order_eq cycb fbb => orderfbb.
have prebb : pre_b \in cover s' by rewrite cov_orbit.
have pre_red : ccw coords (chf pre_b) (chf (chf pre_b)) p = false.
  rewrite fpre; have := reds 0%N; move/andP: iint => [] -> _ /(_ isT) /=.
  by move/Knuth_2/negbTE.
have ordereq : {in orbit chf a, forall x, fingraph.order chf x =
                       fingraph.order chf (chf x)}.
  move=> x xoa; have := cycle_orbit_cycle cyca xoa => cycx.
  have fxx : chf x \in orbit chf x by rewrite -fconnect_orbit fconnect1.
  by rewrite (cycle_orbit_order_eq cycx fxx).
have bint : forall x y, x \in orbit chf a -> y \in orbit chf a ->
    chf y \in traject chf (chf x) (fingraph.order chf x).
  move=> x y xoa yoa; rewrite ordereq // -/(orbit chf (chf x)).
  have [k ->]:= orbit_rot_cycle cyca (orbit_uniq chf _) (chf_oa_stable _ xoa).
  by rewrite mem_rot mem_orbit1. 
set pur2 := iter i.-1 chf b.
have ispred : i.-1.+1 = i by move: iint=> /andP[] /ltn_predK.
have /Knuth_2/negbTE pur2false : ccw coords (chf pur2) pur2 p.
  by apply: reds; rewrite -ispred.
have pur2true : ccw coords (chf pur2) (chf (chf pur2)) p.
  rewrite /pur2 -iterS ispred.
  by apply: blues; rewrite leqnn; move: iint=>/andP[].
have b_red : ccw coords b (chf b) p = false.
  by move: pre_red; rewrite fpre.
have purob : pur2 \in orbit chf b by apply: iter_orbit.
have puroa : pur2 \in orbit chf a by rewrite -(orbits _ boa).
have fpuroa : chf pur2 \in orbit chf a by rewrite mem_orbit1.
have purb : pur2 \in cover s' by rewrite cov_orbit.

(* Here we start listing properties that depend on bpa only for
  the extensional equality of chf and evalmap chfm. *)
have bpa : bp \in orbit chf a by rewrite -(cov_orbit _).
have obpoa := orbits _ bpa.
have exteq : {in orbit chf a, chf =1 evalmap chfm}.
  by move=> y Y; have :=(implyP (forallP cm y)); rewrite obpoa=> /(_ Y)/eqP.
have nint : forall x y, x \in orbit chf a -> y \in orbit chf a ->
        (findex chf (chf x) (chf y) <
             fingraph.order chf x <= mapsize chfm)%N.
  move=> x y xoa yoa; rewrite ordereq // findex_max ?andTb; last first.
    apply: (@connect_trans _ _ a); last first.
      by rewrite fconnect_orbit chf_oa_stable.
    rewrite (fconnect_cycle cyca); last by rewrite chf_oa_stable.
    by rewrite -fconnect_orbit connect0.
  rewrite -size_orbit.
  have [k ob]:= orbit_rot_cycle cyca (orbit_uniq chf _) (chf_oa_stable _ xoa).
  by rewrite ob size_rot (orbit_ext exteq) size_orbit mapsizeP.
have mP := fun h h' => find_next_purple_mapP exteq (nint _ _ h h') _ _ _
    (bint _ _ h h') h.
(* end of properties relying on extensional equality *)
(*
have pnotoa : forall x, x \in orbit chf a -> p != x.
  move=> x xoa; apply/eqP=> abs; move: pnotin.
(* end not useful *)
  by rewrite (subsetP ss') // cov_orbit abs.
*)

have bp'a : bp' \in orbit chf a.
  rewrite -fconnect_orbit.
  by apply (@connect_trans _ _ bp); rewrite fconnect_orbit.
have fbp'a : chf bp' \in orbit chf a by apply: mem_orbit1.
have fbpa: chf bp \in orbit chf a.
  rewrite -fconnect_orbit (connect_trans _ (fconnect1 _ _)) //.
  by rewrite fconnect_orbit.
have obfpoa := orbits _ fbpa.
have cm' : {in orbit chf a, chf =1 evalmap chfm}.
  by move=> y; rewrite -obpoa=>ybp; move: (implyP (forallP cm y) ybp)=>/eqP.
have := cycle_orbit_cycle cyca bpa => cycbp.
have := cycle_orbit_cycle cyca bp'a => cycbp'.
have fbp'bp' : chf bp' \in orbit chf bp'.
  by rewrite -fconnect_orbit fconnect1.
have := cycle_orbit_order_eq cycbp' fbp'bp' => orderfbp'bp'.
have main1 : forall u, u \in orbit chf a -> ccw coords u (chf u) p ->
       find_next_purple coords p chf false (fingraph.order chf u) (chf u) = b.
  move=> u uoa ublue.
  have fua : chf u \in orbit chf a by rewrite mem_orbit1.
  set aliasb := find_next_purple _ _ _ false _ _.
  have := find_next_purple_fconnect coords p chf false (chf u)
            (fingraph.order chf u); rewrite -/aliasb => alco.
  have bou : b \in orbit chf u by rewrite (orbits _ uoa).
  have := find_next_purpleP (cycle_orbit_cycle cyca uoa) bou
          (ublue : _ = ~~false) b_red erefl.
  rewrite -/aliasb=>[][] aliasbP1 aliasbP2.
  have aliasboa : aliasb \in orbit chf a.
    by rewrite -(orbits _ fua) -fconnect_orbit.
  have prealb : (finv chf aliasb) \in cover s'.
    rewrite cov_orbit -(orbits _ fua) -fconnect_orbit.
    by apply: (connect_trans alco (fconnect_finv _ _)).
  have:= (purple_unique s3 ss' ch pnotin gps ninh ab afared prealb prebb).
  have fprea :=
      f_finv_in chf_oa_stable (cycle_orbit_injective cyca) aliasboa.
  rewrite fprea aliasbP1 aliasbP2 pre_red pre_blue.
  move => /(_ false erefl erefl erefl erefl) => step.
  by rewrite -fprea -fpre step.
have main2:forall u, u \in orbit chf a -> ccw coords u (chf u) p = false ->
       find_next_purple coords p chf true
       (fingraph.order chf u) (chf u) = chf pur2.

  move=> u uoa ured.
  have fua : chf u \in orbit chf a by rewrite mem_orbit1.
  set aliasi := find_next_purple _ _ _ true _ _.
  have := find_next_purple_fconnect coords p chf true (chf u)
            (fingraph.order chf u); rewrite -/aliasi => alco.
  have bou : chf pur2 \in orbit chf u by rewrite (orbits _ uoa).
  have := find_next_purpleP (cycle_orbit_cycle cyca uoa) bou
          (ured : _ = ~~true) pur2true erefl.
  rewrite -/aliasi=>[][] aliasiP1 aliasiP2.
  have aliasioa : aliasi \in orbit chf a.
    by rewrite -(orbits _ fua) -fconnect_orbit.
  have prealb : (finv chf aliasi) \in cover s'.
    rewrite cov_orbit -(orbits _ fua) -fconnect_orbit.
    by apply: (connect_trans alco (fconnect_finv _ _)).
  have:= (purple_unique s3 ss' ch pnotin gps ninh ab afared prealb purb).
  have fprea :=
      f_finv_in chf_oa_stable (cycle_orbit_injective cyca) aliasioa.
  rewrite fprea aliasiP1 aliasiP2 pur2false pur2true.
  move => /(_ true erefl erefl erefl erefl) => step.
  by rewrite -step fprea.
have -> : find_purple_points_map coords evalmap mapsize p chfm bp' =
        (b, chf pur2).
  rewrite /find_purple_points_map.
  (case: ifP; rewrite -exteq //) => [bp'blue | bp'red].
    rewrite (mP _ _ _ false bp'blue pre_blue pre_red bp'a pre_boa).
    rewrite (main1 _ bp'a bp'blue).
    rewrite -exteq // (mP _ _ _ true b_red pur2false pur2true boa puroa).
    by rewrite main2.
  rewrite (mP _ _ _ true bp'red pur2false pur2true bp'a puroa).
  rewrite (main2 _ bp'a bp'red).
  rewrite -exteq //(mP _ _ _ false pur2true pre_blue pre_red fpuroa pre_boa).
  by rewrite main1.
rewrite /find_purple_points; case: ifP => [bpblue | bpred].
  rewrite (main1 _ bpa bpblue).
  have:= (main2 _ boa b_red).
  rewrite (cycle_orbit_order_eq cyca boa)(cycle_orbit_order_eq cyca bpa).
  by move=> ->.
rewrite (main2 _ bpa bpred).
have:= (main1 _ fpuroa pur2true).
rewrite (cycle_orbit_order_eq cyca fpuroa)(cycle_orbit_order_eq cyca bpa).
by move=> ->.
Qed.

Lemma new_mapP f fm a b p :
  map_compat b fm f ->
  map_compat p (new_map update fm a b p) (new_path f a b p).
Proof.
move=>/forallP cm; apply/forallP => x; apply/implyP=> xo.
have [/eqP -> | ] := boolP (x == a).
  by rewrite new_path_a /new_map eval_update1.
have [/eqP -> | ] := boolP (x == p).
  move=> pna; rewrite new_path_p // eval_update_dif; last first.
    by apply/eqP; rewrite eq_sym pna.
  by rewrite eval_update1.
move=> xnp xna; rewrite /new_path ffunE /= (negbTE xnp) (negbTE xna).
rewrite eval_update_dif; last by apply/eqP; rewrite eq_sym xna.
rewrite eval_update_dif; last by apply/eqP; rewrite eq_sym xnp.
move: (cm x) => /implyP; apply.
move: xo xna xnp; rewrite -fconnect_orbit => /iter_findex.
set n := findex _ _ _; move: n; move=> n; move: n x.
have [/eqP -> | pna ] := boolP(p == a).
  have dummy : forall n, iter n (new_path f a b a) a = a.
    by elim=> [ | n IH]; rewrite //= IH new_path_a.
  by move=> n x; rewrite dummy => <-; rewrite eqxx.
elim => [| n IH] x /=; first by move ->; rewrite eqxx.
have [/eqP toa | nota ] := boolP(iter n (new_path f a b p) p == a)%N.
  by rewrite toa new_path_a => ->; rewrite eqxx.
have [/eqP top | notp] := boolP(iter n (new_path f a b p) p == p).
  by rewrite top new_path_p //; move=> ->; rewrite -fconnect_orbit connect0.
rewrite ffunE /= (negbTE nota) (negbTE notp) => <- _ _.
  by apply: mem_orbit1; apply: IH.
Qed.

Lemma map_orbitP n e chfm (chf : {ffun P -> P}) :
  map_compat e chfm chf ->
  fcycle chf (orbit chf e) ->
  (size (orbit chf e) <= n)%N ->
  map_orbit evalmap chfm e e n [::e] =
  orbit chf e ++ [:: e].
Proof.
move=> cm cyc sz.
have := orbit_uniq chf e; move: sz cyc.
rewrite /path.cycle /orbit -orderSpred /= size_traject.
have : e \in orbit chf e.
  by rewrite -fconnect_orbit connect0.
move: (fingraph.order chf e).-1 => k.
move: {-2 5 6 10 11 14}e.
elim: k n => [ /= | k IH] n.
  move=> e'; rewrite andbT => e'o /ltn_predK => <- /=.
  by rewrite (eqP (implyP (forallP cm e') e'o)) => ->.
move=> e' e'o kltn; have := ltn_predK kltn => nSpred; move: kltn.
rewrite -nSpred ltnS /= => kltn /andP [] _ fp /andP [] enotin /andP[] _ un1.
move: enotin; rewrite inE negb_or eq_sym => /andP[] /negbTE chfe'ne enotin.
rewrite -(eqP (implyP (forallP cm e') e'o)) chfe'ne.
have chfe'o : chf e' \in orbit chf e.
  rewrite -fconnect_orbit (connect_trans _ (fconnect1 _ _)) //.
  by rewrite fconnect_orbit.
by have := IH (n.-1) (chf e') chfe'o kltn fp; rewrite enotin un1 =>/(_ isT) ->.
Qed.

Lemma add_red_triangles_cat a l tr:
  add_red_triangles coords a l tr = add_red_triangles coords a l [::] ++ tr.
Proof.
elim: l tr => [ | b l IH] tr //=.
case ql : l => [// | c l'].
case: (ccw coords c b a) => //.
by rewrite -ql (IH ([:: c; b; a] :: tr)) (IH [:: [:: c; b; a]]) -catA.
Qed.

Lemma naive4_naive5 s :
  (2 < size s)%N -> uniq s -> general_position coords [set y in s] ->
  let v := naive4 coords p0 s in
  let v' := naive5 coords p0 map0 evalmap update mapsize s in
  (same_triangles v.1.1 v'.1.1) &&
  (v'.2 \in orbit v.1.2 v.2) &&
  map_compat v.2 v'.1.2 v.1.2.
Proof.
elim: s => [ | a s]; first by [].
move: s => [ | b [ | c [ | d s']]] //.  
  move => _ _ uns gps; rewrite /=.
  set tps := three_points coords p0 [set y in [:: a; b; c]].
  have cabc : #|[set y in [:: a; b; c]]| = 3.
    by move/card_uniqP: uns => /= <-; apply/eq_card=> z; rewrite !subE.
  have tin i := three_points_in coords p0 i cabc.
  set tr := [:: _; _; _]; set c3 := cycle3 _.
  have ain : a \in [set y in [:: a; b; c]] by rewrite !subE.
  have obs := orbit_cycle3_seq (eqP (isT : size [:: a; b; c] == 3)) uns ain.
  have in0 := three_points_in coords p0 0 cabc.
  have untr : uniq tr.
    by rewrite /tr /= !subE !negb_or !(inj_eq (three_points_inj cabc)).
  apply/andP; split.
    apply/andP; split.
      apply/permutationsP; exists [:: tr]; first by [].
      apply/andP; split; last by [].
      have uns': uniq [:: a; c; b].
        by  move: uns; rewrite /= !inE (eq_sym b c) orbC.
      apply/forallP=> [] [ [ | i] ilt] //=.
      rewrite /tr -/tps.
      by have := tin 0; rewrite -/tps !inE => /orP [ | /orP []] /eqP tps0;
      move: untr; rewrite /tr -/tps tps0 /= negb_or orbF andbT inE;
        have := tin 1; rewrite -/tps !inE => /orP [ | /orP []] /eqP tps1;
        rewrite tps1 ?eqxx ?andbF //;
          by have := tin (-1); rewrite -/tps !inE => /orP [| /orP[]] /eqP tpm1;
          rewrite tpm1 ?eqxx ?andbF //; rewrite -1?(perm_rcons b [:: a; c])
              ?(perm_rcons a [::_; _]) -?(perm_rcons c [:: a; b]);
           case: ccw; rewrite // perm_cons -perm_rcons.
    have -> : forall t, head p0 (if t then [:: a; b; c] else [:: a; c; b]) = a.
     by case.
    rewrite (orbit_three_points 0 cabc).
    have : a \in [set y in [:: a; b; c]] by rewrite !subE.
    by rewrite [Z in a \in _ Z](three_points_eq' coords p0 cabc) !subE orbA.
  apply/forallP=> x; apply/implyP.
  move: in0; rewrite -/tps => in0.
  have ao i : a \in orbit c3 (tps i).
    have sabc : #|[set y in [:: a; b; c]]| = 3.
      by move/card_uniqP: uns => /= <-; apply/eq_card => z; rewrite inE.
    have : a \in [set y in [:: a; b; c]] by rewrite !subE.
    by rewrite orbit_cycle3_seq // ?tin ?inE.
  have [j pj] : exists j, a = tps j.
    have:= ao 0; rewrite orbit_three_points -/tps // !inE.
    by move=> /orP [ | /orP[]] /eqP ->; eexists; eauto.
  have unj : uniq [:: tps j; tps (j + 1); tps (j - 1)].
    rewrite /= !subE !(inj_eq (three_points_inj cabc)) negb_or addm1_dif.
    by rewrite add1m1_dif addrC add1_dif.
  have [abc | acb] := boolP(ccw coords a b c).
    have tmp : b = c3 a.
      rewrite pj /c3 /tps.
    have := three_points_oriented p0 j cabc gps; rewrite -/tps.
      move: unj.
      rewrite cycle3_add1 //.
      have := tin (j + 1); rewrite !inE => /orP [|/orP[]]/eqP ->;
        rewrite -pj /= !inE ?eqxx //=; have:= tin(j - 1); rewrite /= !inE =>
        /orP [|/orP[]]/eqP ->; rewrite ?eqxx ?orbT ?andbF // => _.
      by move/Knuth_2: (abc); rewrite 2!Knuth_1 => /negbTE ->.
    have tm2 : c = c3 b.
      have := three_points_oriented p0 j cabc gps; rewrite -/tps.
      move: tmp unj.
      rewrite /c3 pj cycle3_add1 // -/tps => vb; rewrite vb cycle3_add1 //.
      rewrite -addrA p1p1 -/tps.
      by have := tin (j - 1); rewrite /= !inE -pj -vb=> /orP [|/orP[]]/eqP ->;
        rewrite ?eqxx ?orbT ?andbF.
    have tm3 : a = c3 c.
      move: tmp tm2.
      rewrite /c3 pj !cycle3_add1 // -/tps => vb; rewrite vb cycle3_add1 //.
      by move=> ->; rewrite cycle3_add1 //; rewrite -add3.
    by rewrite (orbit_cycle3_seq) // !inE => /orP [ |/orP []] /eqP ->;
    apply/eqP; rewrite /map_cycle3; rewrite ?(eval_update1, eval_update_dif) //;
    apply/eqP; move: uns; rewrite /= !inE negb_or andbT => /andP [] /andP [] //.
  have bac : ccw coords b a c.
    move: gps; rewrite /general_position => /forallP /(_ a) /forallP /(_ b).
    move=> /forallP /(_ c); rewrite !subE (negbTE acb) orFb => /implyP; apply.
    by apply/eqP; rewrite -cabc; apply eq_card=> z; rewrite !inE orbA.
  have tmp : c = c3 a.
    rewrite pj /c3 /tps.
    have := three_points_oriented p0 j cabc gps; rewrite -/tps.
      move: unj.
      rewrite cycle3_add1 //.
      have := tin (j + 1); rewrite !inE => /orP [|/orP[]]/eqP ->;
        rewrite -pj /= !inE ?eqxx //=; have:= tin(j - 1); rewrite /= !inE =>
        /orP [|/orP[]]/eqP ->; rewrite ?eqxx ?orbT ?andbF // => _.
      by move/Knuth_2: (bac); rewrite 2!Knuth_1 => /negbTE ->.
    have tm2 : b = c3 c.
      have := three_points_oriented p0 j cabc gps; rewrite -/tps.
      move: tmp unj.
      rewrite /c3 pj cycle3_add1 // -/tps => vb; rewrite vb cycle3_add1 //.
      rewrite -addrA p1p1 -/tps.
      by have := tin (j - 1); rewrite /= !inE -pj -vb=> /orP [|/orP[]]/eqP ->;
        rewrite ?eqxx ?orbT ?andbF.
    have tm3 : a = c3 b.
      move: tmp tm2.
      rewrite /c3 pj !cycle3_add1 // -/tps => vb; rewrite vb cycle3_add1 //.
      by move=> ->; rewrite cycle3_add1 //; rewrite -add3.
    rewrite (orbit_cycle3_seq) // !inE => /orP [ |/orP []] /eqP ->;
    apply/eqP; rewrite /map_cycle3; rewrite ?(eval_update1, eval_update_dif) //;
    apply/eqP; move: uns; rewrite /= !inE negb_or andbT => /andP [] /andP [] //.
    by rewrite (eq_sym b c).
move=> IH _ un gps.
have hyp1 : (2 < size [:: b, c, d & s'])%N by [].
have hyp2 : uniq [:: b, c, d & s'].
  by move: un; rewrite cons_uniq => /andP [] _.
have hyp3 : general_position coords [set y in [:: b, c, d & s']].
  apply: (general_position_sub gps); apply/subsetP => z; rewrite 1!inE => zin.
  by rewrite 2!inE zin orbT.
set l' := [:: b, c, d & s'].
rewrite [naive4 coords p0 (a :: l')](_ : _ =
    let '(tr, chf, bp) := naive4 coords p0 l' in
      match pick_triangle_seq coords tr a with
      | Some (t, trmt) =>
        let s' := [set y in t] in
        let tps := three_points coords p0 s' in
        ([:: [:: tps 0; tps 1; a], [:: tps 1; tps (-1); a],
             [:: tps (-1); tps 0; a] & trmt], chf, bp)
      | None =>
        let (a', b) := find_purple_points coords a chf bp in
            (add_red_triangles coords a (orbit chf a' ++ [:: a']) tr,
            new_path chf a' b a, a)
      end); last by [].
rewrite [naive5 _ _ _ _ _ _ _](_ : _ =
    let '(tr, chf, bp) := naive5 coords p0 map0 evalmap update mapsize l' in
      match pick_triangle_seq coords tr a with
      | Some (t, trmt) => (inside_triangles t a ++ trmt, chf, bp)
      | None =>
        let (a', b) := find_purple_points_map coords evalmap mapsize a chf bp in
        (add_red_triangles coords a
          (map_orbit evalmap chf a' a' (mapsize chf) [:: a']) tr,
          new_map update chf a' b a, a)
      end); last by [].
case h4: (naive4 coords p0 l') => [[tr chf] bp].
case h5 : (naive5 _ _ _ _ _ _ l') => [[tr5 chf5] bp5].
have un' : (uniq l') by move: un; rewrite cons_uniq => /andP [].
have gps' : general_position coords [set y in l'].
  by apply : (general_position_sub gps); rewrite (set_cons a) subsetUr.
move: (IH isT un' gps'); rewrite h4 h5 => /andP [] /andP [].
rewrite -[same_triangles _ _]/(_ tr tr5) -[map_compat _ _ _]/(_ bp chf5 chf).
rewrite -[_ \in orbit _ _]/(bp5 \in orbit chf bp) => tr5P bp5P chf5P.
have szl' : (2 < #|[set y in l']|)%N.
  rewrite (_ : #|[set y in l']| = size l') //.
  by move/card_uniqP : (un') => <-; apply: eq_card=> z; rewrite inE.
(*
have n_i :no_intersection tr.
  by apply: (disjoint_triangles_naive4 szl' gps' un' h4).
*)
have allp := all_points_naive4 szl' gps' un' h4.
have gpsal' : general_position coords
    (a |: cover [set u in [seq [set y in t] | t <- tr]]).
  rewrite allp; apply: (general_position_sub gps).
  by rewrite (set_cons a) subUset subsetUl subsetUr.
have [all3 [WW [bbo [cht [cov n_i]]]]] := all_triangles_naive4 szl' gps' un' h4.
have := pick_triangle_seq_inv_none a tr5P.
case picktr : (pick_triangle_seq coords tr a) => [[t1 tr']|];
  [case picktr5 : (pick_triangle_seq coords tr5 a) => [[t2 tr5']|];[ | by []] |
   case picktr5 : (pick_triangle_seq coords tr5 a) => [[t2 tr5']|];[by [] |] ].
  move=> _.
  have anotin : a \notin [set y in l'].
    move: un; rewrite cons_uniq => /andP[] it _; rewrite inE; exact it.
  have pt1t2 : perm_eq t1 t2.
    have := pick_triangle_seq_some_same_triangles gpsal' n_i tr5P.
    by rewrite allp anotin => /(_ _ _ _ _ isT picktr picktr5) [].
  rewrite /=; set tps := three_points coords p0 [set y in t1].
  have t1in : t1 \in tr.
    move/pick_triangle_seq_someP: picktr => [bef [aft [P1 _]]].
    by rewrite P1 mem_cat inE eqxx orbT.
  move: (all3 t1 t1in) => [] t1_3 unt1.
  have ct1_3 : #|[set y in t1]| = 3.
    move/card_uniqP: unt1.
    by rewrite t1_3 => <-; apply/eq_card=>z; rewrite !inE.
  have t2_3 : size t2 = 3.
    by rewrite -t1_3; apply: perm_eq_size; rewrite perm_eq_sym.
  have unt2 : uniq t2 by rewrite -(perm_eq_uniq pt1t2).
  move : (t2_3).
  case t2_eq : t2 => [ | x [ | y [ | z [ | ]]]] // _.
  have -> : same_triangles [:: [:: tps 0; tps 1; a],[::tps 1; tps (-1); a],
                               [:: tps (-1); tps 0; a] & tr']
                  ((inside_triangles [:: x; y ; z] a) ++ tr5').
    rewrite -[_ :: _]/([:: [:: tps 0; tps 1; a]; [:: tps 1; tps (-1); a];
                          [:: tps (-1); tps 0; a]] ++ tr').
      rewrite same_triangles_cat // /inside_triangles; last first.
      have := pick_triangle_seq_some_same_triangles gpsal' n_i tr5P.
      by rewrite allp anotin => /(_ _ _ _ _ isT picktr picktr5) [].
    move: (three_points_in coords p0 0 ct1_3); rewrite -/tps inE.
    rewrite (perm_eq_mem pt1t2) t2_eq !inE.
    by move=> /orP [| /orP []] => tp0;
      move: (three_points_in coords p0 1 ct1_3) (tp0); rewrite -/tps inE;
      rewrite (perm_eq_mem pt1t2) t2_eq !inE => /orP [| /orP []] => tp1;
      rewrite -(eqP tp1) ?(inj_eq (three_points_inj ct1_3)) // => _;
      move: (tp0)(tp1);
      move: (three_points_in coords p0 (-1) ct1_3); rewrite -/tps inE;
      rewrite (perm_eq_mem pt1t2) t2_eq !inE => /orP [| /orP []] => tp2;
      rewrite -(eqP tp2) ?(inj_eq (three_points_inj ct1_3)) // => _ _;
       rewrite (eqP tp0);
      do 3 (rewrite ?same_triangles_refl //;
        try solve [rewrite auto_same1 // 6?(perm_eq_refl, perm_swap2_2) //];
        rewrite auto_same2); rewrite auto_same3;
      do 3 (rewrite ?same_triangles_refl //;
        try solve [rewrite auto_same1 // 6?(perm_eq_refl, perm_swap2_2) //];
        rewrite auto_same2).
  by rewrite bp5P chf5P.
move => _.
have := boundary_edge_subset [set u in [seq [set y in t] | t <- tr]].
rewrite allp => bs.
have chc := convex_hull_cycle' szl' bs WW bbo => {bs}.
(* map_compat_order *)
have orderq : fingraph.order (evalmap chf5) bp5 = fingraph.order chf bp.
  have [k ob] := orbit_rot_cycle chc (orbit_uniq chf bp) bp5P.
  rewrite -!size_orbit.
  have chf5P' : {in orbit chf bp5, forall y, chf y = evalmap chf5 y}.
    move=> x; move: chf5P; rewrite /map_compat=> /forallP /(_ x)/implyP.
    by rewrite ob mem_rot=> it xo; apply/eqP: (it xo).
  by have <- := orbit_ext chf5P'; rewrite ob size_rot.
have orderle : (fingraph.order chf bp <= mapsize chf5)%N.
  by rewrite -orderq.
have anotin : a \notin [set y in l'].
  by move: un; rewrite cons_uniq=> /andP [] it _; rewrite inE.
have gpsa' : general_position coords (a |: [set y in l']).
  apply: (general_position_sub gps); rewrite !subE /=.
  by rewrite [X in _ \subset pred_of_set X]set_cons subsetUr.
have ninh : ~~ in_hull coords
      (cover (boundary_edge [set u in [seq [set y in t] | t <- tr]])) chf a.
  apply/negP=> inh.
  move/andP: cht=> [] _ cht.
  have cht' := (implyP (forallP (implyP (forallP cht chf) WW) a) gpsa').
  have /existsP [t /andP [tl aint]] := (implyP (implyP cht' anotin) inh).
  have:= pick_triangle_seq_none picktr.
  move: tl; rewrite inE=> /mapP [] t' t'l tt' /(_ t' t'l).
  by rewrite /inside_triangle_seq -tt' aint; move: (all3 t' t'l)=>[] ->.
have := boundary_edge_subset [set u in [seq [set y in t] | t <- tr]].
  rewrite cov => besub.
have cov_orbit := (convex_hull_path_orbit szl' besub WW bbo).
have bp5P' :
    bp5 \in cover (boundary_edge [set u in [seq [set y in t] | t <- tr]]).
  by rewrite cov_orbit.
have mapp := find_purple_points_mapP szl' _ WW orderle anotin gpsa'
  ninh bbo.
rewrite mapp=> //.
have  :=(find_purple_points_in_o coords a chf bp).
case heq : (find_purple_points coords a chf bp) => [ba bb] /= [aino bino].
apply/andP; split; last first.
  by apply: new_mapP; apply: map_compat_orbit chf5P.
apply/andP; split; last by rewrite -fconnect_orbit connect0.
have chfbaP := map_compat_orbit aino chf5P.
rewrite (map_orbitP chfbaP).
    rewrite (add_red_triangles_cat a _ tr) (add_red_triangles_cat a _ tr5).
    by apply: same_triangles_cat; rewrite // same_triangles_refl.
  by apply: (cycle_orbit_cycle chc aino).
have [k ob] := orbit_rot_cycle chc (orbit_uniq chf bp) aino.
rewrite ob size_rot.
  have chf5P' : {in orbit chf bp, forall y, chf y = evalmap chf5 y}.
  move=> x; move: chf5P; rewrite /map_compat=> /forallP /(_ x)/implyP.
  move=> it xo; exact (eqP (it xo)).
rewrite (orbit_ext chf5P').
by rewrite size_orbit; apply: mapsizeP.
Qed.

End ZZMap.
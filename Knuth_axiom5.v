From mathcomp Require Import ssreflect ssrbool ssrnat eqtype ssrfun seq tuple.
From mathcomp Require Import choice path finset finfun fintype bigop fingraph.
From mathcomp Require Import ssralg ssrnum matrix mxalgebra perm fingroup.
From mathcomp Require Import all_algebra.
From mathcomp Require Import zmodp.

Require Import triangulation_algorithm.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import GRing.Theory Num.Theory.

Open Scope ring_scope.

Lemma elimI3 (P : 'I_3 -> Prop): P 0 -> P 1 -> P (-1) -> forall i, P i.
Proof.
move=> p0 p1 p2 [[ | [ | [ | ?]]] ci] //.
    by have /eqP -> : Ordinal ci == 0.
  by have /eqP -> : Ordinal ci == 1.
by have /eqP -> : Ordinal ci == (-1).
Qed.

Lemma p1p1 : (1 + 1 : 'I_3) = -1.
Proof. by apply/eqP. Qed.

Lemma m1m1 : (-1 - 1 : 'I_3) = 1.
Proof. by apply/eqP. Qed.

Lemma add3 (i : 'I_3) : i = i + 1 + 1 + 1.
Proof. by rewrite -!addrA [X in i + X](_ : _ = 0) ?addr0 //; apply/eqP. Qed.

Lemma add1_dif (i : 'I_3) : i != 1 + i.
Proof. by elim/elimI3: i. Qed.

Lemma addm1_dif (i : 'I_3) : i != i - 1.
Proof. by elim/elimI3: i. Qed.

Lemma add1m1_dif (i : 'I_3) : i + 1 != i - 1.
Proof. by elim/elimI3: i. Qed.

Section abstract_over_types.

Variable P : Type.

Variable R : comUnitRingType.

Variable coords : P -> R * R.

Variable p0 : P.

Notation "<< a ; b ; c >>" := (mkf3 a b c).

Lemma expand_det_surface  (a b c : R * R):
  \det (surface_mx << a; b; c >>) =
   b.1 * c.2 - b.2 * c.1 - (a.1 * c.2 - a.2 * c.1) + a.1 * b.2 - a.2 * b.1.
Proof.
repeat rewrite (expand_det_col _ ord0) /cofactor /row' /col' !big_ord_recr
   big_ord0 /= add0r !(mxE, ffunE, addn0, expr0, expr1, expr2, mxE, ffunE, 
   det_mx00, mul1r, mulNr, mulrN, opprK, mulr1, addrA) /=.
by rewrite !(mulrC c.1, mulrC b.1 a.2, mulrC c.2, mulrC b.2 a.1).
Qed.

Definition ptb (t : R) (p : R * R) :=
  (p.1 + t * p.2, p.2 + t * (p.1 ^ 2 + p.2 ^ 2)).

Definition ptb' '(a, b) : R * R := (a, a ^ 2 + b ^ 2).

Definition ptb'' '(a, b) : R * R := (b, a ^ 2 + b ^ 2).

Let R' := (R : Type).

Let mul : R' -> R' -> R' := @GRing.mul _.
Let add : R' -> R' -> R' := @GRing.add _.
Let sub : R' -> R' -> R' := (fun x y => x - y).
Let opp : R' -> R' := @GRing.opp _.
Let zero : R' := 0.
Let one : R' := 1.

Let R2_theory :=
  @mk_rt R' zero one add mul sub opp
   (@eq R')
   (@add0r R) (@addrC R) (@addrA R) (@mul1r R) (@mulrC R)
     (@mulrA R) (@mulrDl R) (fun x y : R => erefl (x - y)) (@addrN R).

Add Ring R2_Ring : R2_theory.

Ltac mc_ring :=
rewrite ?mxE /= ?(expr0, exprS, mulrS, mulr0n) -?[@GRing.add _]/add
   -?[@GRing.mul _]/mul
   -?[@GRing.opp _]/opp -?[1]/one -?[0]/zero;
match goal with |- @eq ?X _ _ => change X with R' end;
ring.

Lemma expand_ptb (t : R) (a b c : R * R):
\det (surface_mx << ptb t a; ptb t b; ptb t c >>) =
\det (surface_mx << a; b; c>>) +
\det (surface_mx << ptb' a; ptb' b; ptb' c >>) * t +
\det(surface_mx << ptb'' a;  ptb'' b; ptb'' c>>) * t ^ 2.
Proof.
rewrite !expand_det_surface /ptb /=.
rewrite /exprz !exprS !mulr1 /ptb' /ptb''.
case: a b c=> [a1 a2][b1 b2][c1 c2] /=.
rewrite /exprz; mc_ring.
Qed.

(* TODO : This lemma needs renaming. *)
Lemma step (a b c : R * R) :
  \det (surface_mx << a; b; c >>) = 0 ->
  (b.1 - a.1) * c.2 = (b.2 - a.2) * c.1 - a.1 * b.2 + a.2 * b.1.
Proof.
rewrite expand_det_surface  opprB !addrA 2!(addrAC _ _ (- (a.1 * c.2))) -mulrBl.
rewrite -!addrA=> /eqP; rewrite addr_eq0=> /eqP ->.
mc_ring.
Qed.

Lemma det_lin3 (a1 a2 b1 b2 c1 c2 v1 v2 v3 v4 : R) :
  \det (surface_mx << (a1, a2 + v4 * v1); (b1, b2 + v4 * v2);
          (c1, c2 + v4 * v3) >>) =
  \det (surface_mx << (a1, a2); (b1, b2); (c1, c2) >>) +
  v4 * \det (surface_mx << (a1, v1); (b1, v2); (c1, v3) >>).
Proof.
rewrite !expand_det_surface /=; mc_ring.
Qed.

Lemma det_swap23 (a1 a2 b1 b2 c1 c2 : R) :
 \det (surface_mx << (a1, a2); (b1, b2); (c1, c2) >>) =
 -\det (surface_mx << (a2, a1); (b2, b1); (c2, c1) >>).
Proof.
rewrite !expand_det_surface /=; mc_ring.
Qed.

Lemma det_D3 (a1 a2 b1 b2 c1 c2 v1 v2 v3 : R) :
  \det (surface_mx << (a1, a2 + v1); (b1, b2 + v2);
          (c1, c2 + v3) >>) =
  \det (surface_mx << (a1, a2); (b1, b2); (c1, c2) >>) +
  \det (surface_mx << (a1, v1); (b1, v2); (c1, v3) >>).
Proof.
rewrite !expand_det_surface /=; mc_ring.
Qed.

Lemma det_M3 (a1 a2 b1 b2 c1 c2 v : R) :
  \det (surface_mx << (a1, v * a2); (b1, v * b2); (c1, v * c2) >>) =
  v * \det (surface_mx << (a1, a2); (b1, b2); (c1, c2) >>).
Proof.
rewrite !expand_det_surface /=; mc_ring.
Qed.

Lemma det_013 (a1 b1 c1 : R) :
  \det (surface_mx << (a1, a1); (b1, b1); (c1, c1) >>) = 0.
Proof.
rewrite !expand_det_surface /=; mc_ring.
Qed.

Lemma det_003 (a1 b1 c1 k : R) :
  \det (surface_mx << (a1, k); (b1, k); (c1, k) >>) = 0.
Proof.
rewrite !expand_det_surface /=; mc_ring.
Qed.

Lemma expand_van_der_monde (a b c : R) :
 \det(surface_mx << (a, a ^ 2); (b, b ^ 2); (c, c ^ 2) >>) =
 (a - b) * (b - c) * (c - a).
Proof.
rewrite expand_det_surface /exprz /=; mc_ring.
Qed.

Lemma expand_ptb' (a b c : R * R) :
  (b.1 - a.1) * c.2 = (b.2 - a.2) * c.1 - a.1 * b.2 + a.2 * b.1 ->
  (b.1 - a.1) ^ 2 * \det (surface_mx << ptb' a; ptb' b; ptb' c >>) =
  ((b.1 - a.1) ^ 2 + (b.2 - a.2) ^ 2) * 
  (b.1 - a.1) * (c.1 - b.1) * (c.1 - a.1).
Proof.
rewrite /ptb'.
case: a b c=>[a1 a2][b1 b2][c1 c2] /= => cnd.
rewrite det_D3 mulrDr -[X in _ + X]det_M3.
have tmp : forall x, (b1 - a1) ^ 2 * x ^ 2 = ((b1 - a1) * x) ^ 2.
  move=> x; rewrite /exprz; mc_ring.
rewrite !tmp {tmp}; rewrite cnd.
have -> : (b1 - a1) * a2 = (b2 - a2) * a1 - a1 * b2 + a2 * b1 by mc_ring.
have -> : (b1 - a1) * b2 = (b2 - a2) * b1 - a1 * b2 + a2 * b1 by mc_ring.
have -> : ((b2 - a2) * a1 - a1 * b2 + a2 * b1) ^ 2 =
          (b2 - a2) ^ 2 * a1 ^ 2 + (2%:R * (b2 - a2) * (a2 * b1 - a1 * b2)) * a1 +
          (a2 * b1 - a1 * b2) ^ 2.
  by rewrite /exprz mulr2n; mc_ring.
have -> : ((b2 - a2) * b1 - a1 * b2 + a2 * b1) ^ 2 =
          (b2 - a2) ^ 2 * b1 ^ 2 + (2%:R * (b2 - a2) * (a2 * b1 - a1 * b2)) * b1 +
          (a2 * b1 - a1 * b2) ^ 2.
  by rewrite /exprz mulr2n; mc_ring.
have -> : ((b2 - a2) * c1 - a1 * b2 + a2 * b1) ^ 2 =
          (b2 - a2) ^ 2 * c1 ^ 2 + (2%:R * (b2 - a2) * (a2 * b1 - a1 * b2)) * c1 +
          (a2 * b1 - a1 * b2) ^ 2.
  by rewrite /exprz mulr2n; mc_ring.
rewrite det_D3 det_lin3 det_003 det_M3 det_013 !mulr0 !addr0.
rewrite -mulrDl -!mulrA; congr (_ * _).
rewrite expand_van_der_monde; mc_ring.
Qed.

Lemma expand_ptb'' (a b c : R * R) :
  (b.1 - a.1) * c.2 = (b.2 - a.2) * c.1 - a.1 * b.2 + a.2 * b.1 ->
  (b.2 - a.2) ^ 2 * \det (surface_mx << ptb'' a; ptb'' b; ptb'' c >>) =
  ((b.1 - a.1) ^ 2 + (b.2 - a.2) ^ 2) * 
  (b.2 - a.2) * (c.2 - b.2) * (c.2 - a.2).
Proof.
rewrite /ptb''.
case: a b c=>[a1 a2][b1 b2][c1 c2] /= => cnd.
have cnd' : (b2 - a2) * c1 = (b1 - a1) * c2 + a1 * b2 - a2 * b1.
  by rewrite cnd; mc_ring.
rewrite det_D3 mulrDr -[X in X + _]det_M3.
have tmp : forall x, (b2 - a2) ^ 2 * x ^ 2 = ((b2 - a2) * x) ^ 2.
  move=> x; rewrite /exprz; mc_ring.
rewrite !tmp {tmp}; rewrite cnd'.
have -> : (b2 - a2) * a1 = (b1 - a1) * a2 + a1 * b2 - a2 * b1 by mc_ring.
have -> : (b2 - a2) * b1 = (b1 - a1) * b2 + a1 * b2 - a2 * b1 by mc_ring.
have -> : ((b1 - a1) * a2 + a1 * b2 - a2 * b1) ^ 2 =
          (b1 - a1) ^ 2 * a2 ^ 2 + (2%:R * (a1 - b1) * (a2 * b1 - a1 * b2)) * a2 +
          (a2 * b1 - a1 * b2) ^ 2.
  by rewrite /exprz mulr2n; mc_ring.
have -> : ((b1 - a1) * b2 + a1 * b2 - a2 * b1) ^ 2 =
          (b1 - a1) ^ 2 * b2 ^ 2 + (2%:R * (a1 - b1) * (a2 * b1 - a1 * b2)) * b2 +
          (a2 * b1 - a1 * b2) ^ 2.
  by rewrite /exprz mulr2n; mc_ring.
have -> : ((b1 - a1) * c2 + a1 * b2 - a2 * b1) ^ 2 =
          (b1 - a1) ^ 2 * c2 ^ 2 + (2%:R * (a1 - b1) * (a2 * b1 - a1 * b2)) * c2 +
          (a2 * b1 - a1 * b2) ^ 2.
  by rewrite /exprz mulr2n; mc_ring.
rewrite det_D3 det_lin3 det_003 det_M3 det_013 !mulr0 !addr0.
rewrite -mulrDl -!mulrA; congr (_ * _).
rewrite expand_van_der_monde; mc_ring.
Qed.

Lemma cramer (a b c d e : R * R) :
       \det (surface_mx << a; b; d >>) *
       \det (surface_mx << a; c; e >>) =
       \det (surface_mx << a; b; c >>) *
       \det (surface_mx << a; d; e >>) +
       \det (surface_mx << a; b; e >>) *
       \det (surface_mx << a; c; d >>).
Proof.
rewrite !expand_det_surface.
mc_ring.
Qed.

Lemma cramer' (a b c d e : R * R) :
       \det (surface_mx << a; b; d >>) *
       \det (surface_mx << b; c; e >>) =
       \det (surface_mx << a; b; c >>) *
       \det (surface_mx << b; d; e >>) +
       \det (surface_mx << a; b; e >>) *
       \det (surface_mx << b; c; d >>).
Proof.
rewrite !expand_det_surface.
mc_ring.
Qed.

Lemma Knuth_4_main (a b c d : R * R) :
  \det (surface_mx << b; c; d >>) -
  \det (surface_mx << a; c; d >>) +
  \det (surface_mx << a; b; d >>) -
  \det (surface_mx << a; b; c >>) = 0.
Proof.
rewrite !expand_det_surface.
mc_ring.
Qed.

Definition shift_cycle_val := [ffun i : 'I_3 => 1 + i].

Lemma shift_cycle_proof : injectiveb [ffun i : 'I_3 => 1 + i].
Proof using. by apply/perm_proof/addrI. Qed.

Open Scope group_scope.

Lemma cycle_det_surface1 (a b c : R * R) :
  \det (surface_mx << b; c; a >>) = \det (surface_mx << a; b; c>>).
Proof using.
have shift_cycle_even : (Perm shift_cycle_proof : bool) = false.
  have <- : (tperm 1%R (-1)%R) * (tperm 0%R 1%R) = Perm shift_cycle_proof.
    by apply/permP; elim/elimI3; rewrite permE /=;
    try (rewrite [X in tperm _ _ X]tpermD; [| by [] | by []]);
      rewrite permE /= ?(tpermR, tpermL) // ?tpermD // unlock ffunE; apply/eqP.
  by rewrite odd_permM !odd_tperm.
set BM := (X in _ = \det X).
rewrite [surface_mx _](_ : _ = perm_mx (Perm shift_cycle_proof) *m BM).
  by rewrite detM det_perm shift_cycle_even expr0 mul1r.
rewrite -row_permE; apply/matrixP => k l; rewrite !mxE !ffunE.
by elim/elimI3: k => //=; rewrite unlock ffunE.
Qed.

Lemma cycle_det_surface (f : (R * R)%type ^ 3) (i : 'I_3) :
  \det (surface_mx <<f i; f (i + 1%R); f (i - 1%R)>>) =
  \det (surface_mx <<f 0; f 1%R; f (-1%R)>>).
Proof using.
elim/elimI3: i;
  [ | rewrite cycle_det_surface1 | rewrite -cycle_det_surface1];
  congr (\det(surface_mx << f _; f _; f _ >>)); apply/eqP => //.
Qed.

Close Scope group_scope.

Lemma swap_det_surface (a b c : R * R) :
  \det (surface_mx << a; b; c >>) = - \det (surface_mx << b; a; c >>).
Proof using.
have m_eq U V W : surface_mx << V; U; W >> =
    perm_mx (tperm 0 1) *m surface_mx << U ; V; W >>.
  apply/matrixP=> i j; rewrite -row_permE !mxE.
  by elim/elimI3: i; rewrite !ffunE /= ?(tpermL, tpermR) //= tpermD.
by rewrite m_eq detM det_perm odd_tperm /= expr1 mulNr mul1r.
Qed.

End abstract_over_types.

Section abstract_over_types'.

Variable P : Type.

Variable R : realDomainType.

Variable coords : P -> R * R.

Variable p0 : P.

Notation "<< a ; b ; c >>" := (mkf3 a b c).

Lemma Knuth_axiom5 (a b c d e : R * R) :
  0 < \det (surface_mx << a; b; c >>) ->
  0 < \det (surface_mx << a; b; d >>) ->
  0 < \det (surface_mx << a; b; e >>) ->
  0 < \det (surface_mx << a; c; d >>) ->
  0 < \det (surface_mx << a; d; e >>) ->
  0 < \det (surface_mx << a; c; e >>).
Proof.
move => abc abd abe acd ade.
by rewrite -(pmulr_lgt0 _ abd) mulrC cramer addr_gt0 // mulr_gt0.
Qed.

Lemma Knuth_axiom5c a b c d e :
  ccw coords a b c -> ccw coords a b d -> ccw coords a b e ->
  ccw coords a c d -> ccw coords a d e -> ccw coords a c e.
Proof. exact: Knuth_axiom5. Qed.

Lemma Knuth_axiom5' (a b c d e : R * R) :
  0 < \det (surface_mx << a; b; c >>) ->
  0 < \det (surface_mx << a; b; d >>) ->
  0 < \det (surface_mx << a; b; e >>) ->
  0 < \det (surface_mx << b; c; d >>) ->
  0 < \det (surface_mx << b; d; e >>) ->
  0 < \det (surface_mx << b; c; e >>).
Proof.
move => abc abd abe acd ade.
by rewrite -(pmulr_lgt0 _ abd) mulrC cramer' addr_gt0 // mulr_gt0.
Qed.

Lemma Knuth_axiom5c' a b c d e :
  ccw coords a b c -> ccw coords a b d -> ccw coords a b e ->
  ccw coords b c d -> ccw coords b d e -> ccw coords b c e.
Proof. exact: Knuth_axiom5'. Qed.

Definition fina := (addrN, addNr, add0r).

Lemma Knuth_4 d a b c:
  ccw coords a b d -> ccw coords b c d -> ccw coords c a d -> ccw coords a b c.
Proof using.
rewrite /ccw /ccw0.
have main:= Knuth_4_main (coords a) (coords b) (coords c) (coords d).
move=> c1 c2 c3; rewrite -oppr0 -main -[X in _ < X]add0r !opprD !opprK.
rewrite ltr_add2r subr_lt0 (ltr_trans _ c1) // addrC subr_lt0.
by rewrite (ltr_trans _ c2) //; rewrite swap_det_surface oppr_lt0.
Qed.

Lemma Knuth_1 a b c : ccw coords a b c = ccw coords c a b.
Proof.
by rewrite /ccw /ccw0 cycle_det_surface1.
Qed.

Lemma Knuth_2 a b c : ccw coords a b c -> ~~ccw coords b a c.
Proof.
rewrite /ccw /ccw0 swap_det_surface oppr_gt0 ltrNge ler_eqVlt negb_or.
by move => /andP[].
Qed.

End abstract_over_types'.
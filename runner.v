From mathcomp Require Import all_ssreflect ssralg ssrnum ssrint.

From triangles Require Import triangles3 to_ccw_system.

Open Scope ring_scope.

Require Import ZArith.

Definition Zccw :=
   ccw BinInt.Z.eqb BinInt.Z.ltb BinInt.Z.add
             BinInt.Z.mul BinInt.Z.opp Z0.

Definition Z2_eqb (x y : Z * Z) :=
  (x.1 =? y.1)%Z && (x.2 =? y.2)%Z.

Definition Z2mf : map_funs (Z * Z) := a_list_map_funs Z2_eqb.


Definition Z20 : Z * Z := (0, 8)%Z.

Definition Z2naive : seq (Z * Z) -> _ := naive Z20 Zccw Z2mf.

Open Scope Z_scope.

Definition input :=
  [:: (120, 140); (200, 100); (100, 200); (100, 100); 
          (160, 160); (130, 120)].

Definition result1 := Eval vm_compute in Z2naive input.

Definition triangles := Eval vm_compute in result1.1.1.
Definition map := Eval vm_compute in result1.1.2.
Definition point_on_boundary :=
  Eval vm_compute in result1.2.

Definition complete_boundary :=
  map_iter cons nil result1.1.2 result1.2.

Compute complete_boundary.

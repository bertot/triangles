From mathcomp Require Import all_ssreflect all_algebra fingroup perm zmodp.
From triangles Require Import triangles3 triangulation_algorithm Knuth_axiom5.

Require Import Psatz.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Import GRing.Theory Num.Theory.

Section perturbation.

Variable R : idomainType.

Definition ptb : R * R -> {poly R} * {poly R} :=
  fun '(a, b) => Knuth_axiom5.ptb 'X (a%:P, b%:P).

End perturbation.

Section ccw_def.

Variable (R : Type) (eq lt : R -> R -> bool) (add mul : R -> R -> R)
  (opp : R -> R) (zero : R).

Notation "x * y" := (mul x y) : ccw_scope.
Notation "x + y" := (add x y) : ccw_scope.
Notation "x - y" := (add x (opp y)) : ccw_scope.
Notation "0" := zero : ccw_scope.
Notation "x < y" := (lt x y) : ccw_scope.
Notation "x < y < z" := (andb (lt x y) (lt y z)) : ccw_scope.
Notation "x == y" := (eq x y) : ccw_scope.
Delimit Scope ccw_scope with CCW.

Open Scope ccw_scope.

Definition surface (a b c : (R * R)) :=
  (b.1 * c.2 - b.2 * c.1 - (a.1 * c.2 - a.2 * c.1) + a.1 * b.2 - a.2 * b.1).

Definition ccw (a b c : R * R) :=
  (0 < surface a b c) ||
  (if (0 == surface a b c)%CCW then
    if (a.1 < b.1) then
       (b.1 < c.1) || (c.1 < a.1)
    else
       if (b.1 < a.1) then
          (b.1 < c.1 < a.1)
       else if (a.2 < b.2) then
               (b.2 < c.2) || (c.2 < a.2)
            else if (b.2 < a.2) then (b.2 < c.2 < a.2) else false
    else false).

End ccw_def.

Section perturbed_setting.

Variable R : realDomainType.

Variable P : Type.

Variable coords : P -> (R * R).

Notation ccw := (ccw eq_op <%R +%R *%R (@GRing.opp _) 0%R).
Notation surface := (surface +%R *%R (@GRing.opp _)).

Definition ptbs (a b c : R * R) : {poly R} :=
  \det(surface_mx (mkf3 (ptb a) (ptb b) (ptb c))).

Lemma surface_value (a b c : R * R) :
  surface a b c = \det(surface_mx (mkf3 a b c)).
Proof. by rewrite expand_det_surface. Qed.

Lemma ptbd_value (a b c : R * R) :
  ptbs a b c =
  (\det(surface_mx (mkf3 (ptb'' a) (ptb'' b) (ptb'' c))))%:P * 'X ^ 2 +
  (\det(surface_mx (mkf3 (ptb' a) (ptb' b) (ptb' c))))%:P * 'X +
  (\det(surface_mx (mkf3 a b c)))%:P.
Proof.
rewrite /ptbs /ptb; destruct a as [a1 a2] eqn:va; destruct b as [b1 b2] eqn:vb.
destruct c as [c1 c2] eqn:vc.
rewrite expand_ptb -!det_map_mx addrC -addrA; congr (_ * 'X ^ 2 + _).
  congr (\det( _ )).
  apply/matrixP=> i j; rewrite /surface_mx !mxE /project_p !ffunE.
  now repeat case:ifP=> // ?; rewrite ?rmorphD ?rmorphX.
rewrite addrC; congr (_ * 'X + _).
  congr (\det( _ )).
  apply/matrixP=> i j; rewrite /surface_mx !mxE /project_p !ffunE.
  now repeat case:ifP=> // ?; rewrite ?rmorphD ?rmorphX.
congr (\det( _ )).
apply/matrixP=> i j; rewrite /surface_mx !mxE /project_p !ffunE.
now repeat case:ifP=> // ?; rewrite ?rmorphD ?rmorphX.
Qed.

Definition tail_coef (p : {poly R}) := head 0 [seq x <- p | x != 0].

Lemma tail_coef0 (p : {poly R}) :
  p`_0 != 0 -> tail_coef p = p`_0.
Proof.
by rewrite /tail_coef; case: (polyseq p) => [// | a tl /=] ->.
Qed.

Lemma tail_coefc (p : {poly R}) c :
  c != 0 -> tail_coef (p * 'X + c%:P) = c.
Proof.
move=> cn0; rewrite /tail_coef -cons_poly_def polyseq_cons.
by case: ifP=> /=; rewrite ?cn0 // ?polyseqC //= cn0 /= cn0.
Qed.

Lemma tail_coefr (p : {poly R}) : tail_coef (p * 'X) = tail_coef p.
Proof.
rewrite -(addr0 (p * 'X)) -cons_poly_def /tail_coef polyseq_cons.
case: ifP=> /= cp; first by rewrite eqxx.
move: cp; case: (polyseq p) => [ | //=] _.
by rewrite polyseqC eqxx.
Qed.

Lemma tail_coef_eq0 (p : {poly R}) : (tail_coef p == 0) = (p == 0).
Proof.
apply/idP/idP; last first.
  by move=> /eqP ->; rewrite /tail_coef polyseq0.
rewrite -(coefK p).
move: {p} (size (polyseq p)) {-2} (polyseq p) (erefl (size (polyseq p))).
elim=> [ | n IH] s sz.
  have : size (\poly_(i < size s) s`_i) == 0%N by rewrite sz -leqn0 size_poly.
  by rewrite size_poly_eq0.
case: s sz => [ // | a s]; move=> /eqP /=; rewrite eqSS=> sz.
rewrite poly_def big_ord_recl expr0 alg_polyC /=.
rewrite (eq_bigr (fun i : 'I_(size s) => (s`_i *: 'X^i) * 'X)); last first.
  by move=> i _; rewrite /bump leq0n /= exprD scalerAr mulrC.
rewrite -big_distrl /= addrC.
have [/eqP -> | an0] := boolP (a == 0); last by rewrite tail_coefc (negbTE an0).
by rewrite addr0 tail_coefr -poly_def mulf_eq0 polyX_eq0 orbF; apply/IH/eqP.
Qed.

Lemma tail_coefC (a: R) : tail_coef (a%:P) = a.
Proof.
have [a0| an0] := boolP (a%:P == 0).
  move: (a0); rewrite -tail_coef_eq0=> /eqP ->.
  by move: a0; rewrite polyC_eq0=> /eqP ->.
move: (an0); rewrite polyC_eq0 => an0'; rewrite /tail_coef polyseqC -nth0.
by rewrite filter_nseq an0' /=.
Qed.

Lemma tail_coefM (p1 p2 : {poly R}) :
   tail_coef (p1 * p2) = tail_coef p1 * tail_coef p2.
Proof.
have lem1 : forall p c, tail_coef (c%:P * p) = c * tail_coef p.
  elim/poly_ind => [| p a IH] c; first by rewrite mulr0 !tail_coefC mulr0.
  have [/eqP c0 |cn0] :=boolP(c == 0).
    by rewrite c0 !mul0r tail_coefC.
  have [/eqP a0 |an0] :=boolP(a == 0).
    by rewrite a0 addr0 mulrA !tail_coefr IH.
  by rewrite mulrDr -polyC_mul mulrA !tail_coefc // mulf_eq0 (negbTE cn0) /=.
elim/poly_ind: p1 p2 => [| p1 a IH] p2.
  by rewrite mul0r tail_coefC mul0r.
have [/eqP -> | an0] := boolP(a == 0).
  by rewrite !addr0 mulrAC !tail_coefr; apply IH.
elim/poly_ind : p2 => [ | p2 b IH2].
  by rewrite mulr0 tail_coefC mulr0.
have [/eqP -> | bn0] := boolP(b == 0).
  by rewrite mulrDr mulr0 !addr0 mulrA !tail_coefr IH2.
rewrite mulrDr (mulrDl _ _ (b%:P)) mulrA (mulrAC _ 'X) addrA -mulrDl -polyC_mul.
by rewrite !tail_coefc // mulf_eq0 negb_or an0 bn0.
Qed.

Lemma tail_coefZ (p : {poly R}) (x : R):
  tail_coef (x *: p) = x * tail_coef p.
Proof.
by rewrite -mul_polyC tail_coefM tail_coefC.
Qed.

Lemma tail_coefN (p : {poly R}) :
  tail_coef (-p) = -tail_coef p.
Proof.
by rewrite -[X in tail_coef (-X)]scale1r -scaleNr !tail_coefZ mulNr mul1r.
Qed.

Lemma tail_coefD_gt0 (p1 p2 : {poly R}) :
 0 < tail_coef p1 -> 0 < tail_coef p2 -> 0 < tail_coef (p1 + p2).
Proof.
elim/poly_ind: p1 p2=> [ | p1 a IH]; first by rewrite tail_coefC ltrr.
  elim/poly_ind=> [ | p2 b _]; first by rewrite tail_coefC ltrr.
have [/eqP a0 | an0] := boolP (a == 0).
  have [/eqP b0 | bn0] := boolP (b == 0).
    by rewrite a0 b0 !addr0 -mulrDl !tail_coefr; apply: IH.
  by move=> _; rewrite a0 !addr0 addrA -mulrDl !tail_coefc.
have [/eqP b0 | bn0] := boolP (b == 0).
  by move=> ca _; move: ca; rewrite b0 !addr0 addrAC -mulrDl !tail_coefc.
rewrite addrA (addrAC _ a%:P) -mulrDl -addrA -polyC_add 2?tail_coefc //.
move=> agt0 bgt0; have abgt0 : 0 < a + b by rewrite addr_gt0.
by rewrite tail_coefc // lt0r_neq0.
Qed.

Let pair_eqE' (A B : eqType) (p1 p2 : A * B) :
  (p1 == p2) = (p1.1 == p2.1) && (p1.2 == p2.2).
Proof. by rewrite -pair_eqE. Qed.

Definition uniq_rules := (inE, pair_eqE', eqxx, andbF, orbT).

Lemma ccw_tail_coef a b c :
 uniq [:: a; b; c] ->
 ccw a b c <-> 0 < tail_coef (ptbs a b c).
Proof.
move=> uabc; rewrite /ccw ptbd_value surface_value eq_sym.
case vD1 : (0 < \det _) => /=.
  rewrite !mulrA -mulrDl tail_coefc.
    by rewrite vD1.
  by move: vD1; rewrite ltr_neqAle eq_sym=> /andP[].
set D2 := \det (_ (_ (ptb' a) _ _)).
move/negP/negP: vD1; rewrite -lerNgt ler_eqVlt => /orP[/eqP vD1|].
  rewrite vD1 eqxx addr0.
  have straight:= (step vD1); move: (expand_ptb' straight) => cn0.
  case: ifP=> [a1ltb1|].
    move: (a1ltb1) (cn0); rewrite -subr_gt0 => b1ma1.
    have sq : 0 < (b.1 - a.1) ^ 2 by rewrite exprn_even_gt0 //= lt0r_neq0.
    have fgt0 : 0 < (b.1 - a.1) ^ 2 + (b.2 - a.2) ^ 2.
      by rewrite -(add0r 0) ltr_le_add // ?sqr_ge0.
    have b1ma1n0 : b.1 - a.1 != 0.
      by rewrite subr_eq0; move: a1ltb1; rewrite ltr_neqAle eq_sym=>/andP[].
    have [bltc /= | cleb] := boolP(b.1 < c.1).
      split=> // _; have D2gt0 : 0 < D2.
        rewrite /D2 -(pmulr_rgt0 _ sq) cn0 !mulr_gt0 // subr_gt0 //.
        by rewrite (ltr_trans _ bltc).
      by rewrite !mulrA -mulrDl tail_coefr tail_coefc // lt0r_neq0.
    have [clta /= | alec] := boolP(c.1 < a.1).
      split=> // _;  have D2gt0 : 0 < D2.
        rewrite /D2 -(pmulr_rgt0 _ sq) cn0.
        rewrite -(opprB b.1 c.1) mulrN mulNr -mulrN opprB.
        rewrite !mulr_gt0 // subr_gt0 //.
        by rewrite (ltr_trans _ a1ltb1).
      by rewrite !mulrA -mulrDl tail_coefr tail_coefc // lt0r_neq0.
    split => //= => tcgt0; exfalso; move: tcgt0; apply/negP; rewrite -lerNgt.
  suff : D2 < 0.
    rewrite ltr_neqAle => /andP [d2n0 d2le0].
    by rewrite !mulrA -mulrDl tail_coefr tail_coefc.
  have c1na1 : c.1 != a.1.
    apply/negP=> /eqP c1a1; move: straight; rewrite c1a1 (mulrBl a.1).
    rewrite (addrAC _ _ (- (a.1 * b.2))) (mulrC b.2) addrN add0r [RHS]addrC.
    rewrite -mulrBr (mulrC a.2)=> /(mulfI b1ma1n0)=>c2a2.
    by move: uabc; rewrite /= !(uniq_rules, c1a1, c2a2).
  have c1nb1 : c.1 != b.1.
    apply/negP=> /eqP c1b1; move: straight; rewrite c1b1 (mulrBl b.1).
    rewrite (addrAC _ _ (a.2 * b.1)) addrNK (mulrC b.2).
    rewrite -mulrBl=> /(mulfI b1ma1n0)=>c2b2.
    by move: uabc; rewrite /= !(uniq_rules, c1b1, c2b2).
  have cltb : c.1 < b.1.
    by move: cleb; rewrite -lerNgt ler_eqVlt (negbTE c1nb1).
  have altc : a.1 < c.1.
    by move: alec; rewrite -lerNgt ler_eqVlt eq_sym (negbTE c1na1).
  rewrite /D2 -(pmulr_rlt0 _ sq) cn0.
  by rewrite -!mulrA (pmulr_rlt0 _ fgt0) (pmulr_rlt0 _ b1ma1) pmulr_llt0
    ?subr_lt0 // ?subr_gt0.
  move/negbT; rewrite -lerNgt ler_eqVlt => /orP[/eqP b1a1| b1lta1].
    have a2b2: a.2 != b.2.
      by apply/negP=>/eqP ab2; move: uabc; rewrite /= !(uniq_rules, b1a1, ab2).
    have a2mb2 : a.2 - b.2 != 0 by rewrite subr_eq0.
    move: (straight); rewrite b1a1 subrr mul0r -addrA (addrC (- _)) (mulrC a.1).
    rewrite -opprB -mulrBl; move/eqP; rewrite eq_sym addrC mulNr subr_eq0.
    move/eqP; move/(mulfI a2mb2) => a1c1.
    have D20 : D2 = 0; rewrite /D2.
      rewrite /ptb' (surjective_pairing a) (surjective_pairing b).
      rewrite (surjective_pairing c) /= det_D3 !expand_van_der_monde.
      rewrite b1a1 -a1c1 subrr !mul0r add0r expand_det_surface /=.
      rewrite !(opprB, opprD, addrA) 2!(addrAC _ (a.2 ^ 2 * _)) addrK.
      by rewrite addrAC -(mulrC a.1) addrNK addrN.
    rewrite -/D2 D20 mul0r addr0 !mulrA !tail_coefr.
    rewrite tail_coefC /ptb'' (surjective_pairing a) (surjective_pairing b).
    rewrite (surjective_pairing c) /= det_D3 b1a1 -a1c1 det_003 add0r.
    rewrite expand_van_der_monde -opprB mulNr -mulrN opprB.
    rewrite ltr_neqAle eqxx /=; case: ifP => [a2ltb2 | /negbT].
      have [bltc /= | /=] := boolP(b.2 < c.2).
        split => //= _; rewrite pmulr_rgt0.
          by rewrite subr_gt0 (ltr_trans a2ltb2).
        by rewrite pmulr_rgt0 subr_gt0.
      rewrite -lerNgt ler_eqVlt=> /orP[/eqP c2b2 | c2b2 ].
        by move: uabc; rewrite /= !(uniq_rules, c2b2, b1a1, a1c1).
      have [c2a2 | ]:= boolP(c.2 < a.2).
        split => //= _; rewrite -(opprB b.2) mulrN mulNr -mulrN opprB.
        rewrite pmulr_rgt0; first by rewrite subr_gt0.
        by rewrite pmulr_rgt0 subr_gt0.
      rewrite -lerNgt ler_eqVlt=> /orP[/eqP |] a2c2; split => //=.
        by move: uabc; rewrite /= !(uniq_rules, a2c2, a1c1).
      move : a2c2; rewrite -subr_gt0 => a2c2; rewrite (pmulr_lgt0 _ a2c2).
      move : a2ltb2; rewrite -subr_gt0 => a2b; rewrite (pmulr_rgt0 _ a2b).
      by rewrite subr_gt0 ltrNge ler_eqVlt c2b2 orbT.
    rewrite -lerNgt ler_eqVlt eq_sym (negbTE a2b2) /= => b2lta2; rewrite b2lta2.
    split.
      move=> /andP[bc ca]; rewrite -(opprB a.2) !mulNr -mulrN opprB.
      by rewrite !pmulr_rgt0 ?subr_gt0 // (ltr_trans bc).
    rewrite -mulrA -opprB mulNr -mulrN pmulr_rgt0 ?subr_gt0 //.
    have [clta | ] := boolP (c.2 < a.2).
      by rewrite -mulrN opprB pmulr_lgt0 subr_gt0 ?andbT.
    rewrite -lerNgt ler_eqVlt=> /orP[/eqP c2a2 | altc].
      by move: uabc; rewrite /= !(uniq_rules, c2a2, a1c1).
    rewrite -mulNr opprB pmulr_lgt0 subr_gt0 // ltrNge ler_eqVlt.
    by rewrite (ltr_trans b2lta2 altc) orbT.
  have sq : 0 < (b.1 - a.1) ^ 2.
    by rewrite exprn_even_gt0 //= ltr0_neq0 ?subr_lt0.
  have fgt0 : 0 < (b.1 - a.1) ^ 2 + (b.2 - a.2) ^ 2.
    by rewrite -(add0r 0) ltr_le_add // ?sqr_ge0.
  rewrite b1lta1 -/D2 !mulrA -mulrDl tail_coefr.
  have [bltc| ]:= boolP(b.1 < c.1).
    have [clta /= | ] := boolP(c.1 < a.1).
      have D2gt0 : 0 < D2.
        rewrite -(pmulr_rgt0 _ sq); rewrite cn0 -(opprB a.1 c.1) mulrN -mulNr.
        rewrite pmulr_lgt0 ?subr_gt0 // -mulNr pmulr_lgt0 ?subr_gt0 // -mulrN.
        by rewrite opprB pmulr_lgt0 ?subr_gt0.
      by rewrite tail_coefc ?lt0r_neq0.
    have bnc : b.1 - c.1 != 0 by rewrite ltr0_neq0 // subr_lt0.
    rewrite -lerNgt ler_eqVlt => /orP [/eqP ac | altc].
      move: straight; rewrite ac (mulrBl c.1) (mulrC b.2).
      rewrite (addrAC _ _ (- _)) addrN add0r (addrC (- _)) -mulrBr (mulrC a.2).
      by move=>/(mulfI bnc) ca; move: uabc; rewrite /= !(uniq_rules, ca, ac).
    have D2lt0 : D2 < 0.
      rewrite -(pmulr_rlt0 _ sq) cn0 -(opprB a.1 c.1) mulrN -mulNr.
      rewrite pmulr_rlt0 ?subr_lt0 // -mulNr pmulr_rgt0 ?subr_gt0 //.
      by rewrite -mulrN opprB pmulr_rgt0 ?subr_gt0.
    have D2n0 : D2 != 0 by apply: ltr0_neq0.
    by rewrite tail_coefc ?ltrNge ?ler_eqVlt ?(negbTE D2n0) ?D2lt0.
  rewrite -lerNgt ler_eqVlt => /orP[/eqP bc |].
    move: straight; rewrite bc (mulrBl b.1) (mulrC b.2).
    rewrite (addrAC _ _ (a.2 * b.1)) addrNK -mulrBl.
    have bma : b.1 - a.1 != 0 by rewrite ltr0_neq0 // subr_lt0.
    by move/(mulfI bma)=> cb; move: uabc; rewrite /= !(uniq_rules, bc, cb).
  move=> cltb; have D2lt0 : D2 < 0.
    rewrite -(pmulr_rlt0 _ sq) cn0 pmulr_rlt0 ?subr_lt0 ?(ltr_trans cltb) //.
    rewrite -(opprB b.1 c.1) mulrN -mulNr pmulr_rgt0 ?subr_gt0 //.
    by rewrite -mulrN opprB pmulr_rgt0 ?subr_gt0.
  have D2n0 : D2 != 0 by apply: ltr0_neq0.
  by rewrite tail_coefc ?ltrNge ?ler_eqVlt ?(negbTE D2n0) ?D2lt0.
move=> D0lt0.
have D0n0 : \det(surface_mx (mkf3 a b c)) != 0 by apply: ltr0_neq0.
rewrite (negbTE (ltr0_neq0 D0lt0)) !mulrA -mulrDl.
by rewrite tail_coefc // ltrNge ler_eqVlt (negbTE D0n0) D0lt0.
Qed.

Lemma non_zero_polynomial a b c (uabc: uniq [:: a; b; c]) : ptbs a b c != 0.
Proof.
have [/eqP d00 | ?] := boolP(\det(surface_mx(mkf3 a b c)) == 0); last first.
  have at0n0 : (ptbs a b c).[0] != 0.
    by rewrite ptbd_value !hornerE /= ?mulr0 ?add0r.
  by apply/negP=> /eqP abs; case/negP: at0n0; rewrite abs hornerE.
move/eqP: (step d00); have [/eqP c2m0 | c2mn0] := boolP(b.1 == a.1).
  rewrite c2m0 subrr mul0r; have c1mn0 : b.2 != a.2.
    by apply/eqP=> b2a2; move: uabc; rewrite /= !(uniq_rules, c2m0, b2a2).
  rewrite [X in _ == X](_ : _ = (b.2 - a.2) * (c.1 - a.1)); last first.
    rewrite mulrBr -addrA -mulrN (mulrC a.1) -mulrDl (addrC (- _)).
    by rewrite -(opprB b.2) mulNr -mulrN -mulrDr.
  rewrite eq_sym mulf_eq0 !subr_eq0 (negbTE c1mn0) => /eqP c1a1.
  rewrite ptbd_value d00 addr0 !mulrA -mulrDl.
  rewrite mulf_neq0 // ?polyX_eq0 //; set D1 := (X in _ + X).
  have D10 : D1 = 0.
    rewrite /D1 /ptb' (surjective_pairing a) (surjective_pairing b).
    rewrite (surjective_pairing c) /= det_D3 expand_van_der_monde c2m0 c1a1.
    rewrite subrr !mul0r add0r expand_det_surface /= opprB !addrA !(mulrC a.1).
    by rewrite 2!(addrAC _ _ (b.2 ^ 2 * a.1)) addrNK addrAC addrK subrr.
  rewrite D10 addr0 mulf_eq0 polyX_eq0 /=.
  move: (expand_ptb'' (step d00)); rewrite c2m0 subrr /exprz (expr2 0).
  rewrite mul0r add0r -(mulrA _ _ (c.2 - a.2)) -(mulrA (_ ^ 2) _ _) /exprz orbF.
  have fn0 : (b.2 - a.2) ^+ 2 != 0 by rewrite expf_neq0 // subr_eq0.
  move=>/(mulfI fn0) ->; rewrite polyC_eq0.
  by rewrite !mulf_neq0 // subr_eq0; apply/negP=> /eqP A; move: uabc;
    rewrite /= !(uniq_rules, c2m0, c1a1, A).
move: (c2mn0); rewrite -subr_eq0=> ba1n0.
move/eqP/expand_ptb'/eqP; rewrite eq_sym => main.
rewrite ptbd_value d00 addr0 !mulrA -mulrDl mulf_eq0.
rewrite polyX_eq0 orbF /=; set p' := (X in X == _); suff vn0 : p'.[0] != 0.
  by apply/negP=>/eqP abs; case/negP: vn0; rewrite abs hornerE.
rewrite /p' !hornerE; apply/negP=> /eqP A; move: main; rewrite A mulr0.
have smn0 : (b.1 - a.1) ^ 2 + (b.2 - a.2) ^ 2 != 0.
  apply: lt0r_neq0; rewrite -(addr0 0) ltr_le_add // /exprz ?sqr_ge0 //.
  by rewrite exprn_even_gt0 /=.
rewrite -!mulrA !mulf_eq0 (negbTE ba1n0) (negbTE smn0) !subr_eq0 /= {A}.
by move =>/orP[] /eqP A; move: (step d00) uabc; rewrite /= A (mulrBl _ b.2)
  (mulrC b.2)(addrAC (_ * b.2)) ?addrNK ?addrN ?add0r ?(addrC (- _)) -?mulrBl
  -?mulrBr ?(mulrC _ (_ - _))=>/(mulfI ba1n0) B; rewrite !(uniq_rules, A, B).
Qed.

Lemma surface_aab (a b : R * R) :
  \det(surface_mx (mkf3 a a b)) = 0.
Proof.
set X := (LHS); suff : 2%:R * X == 0.
  by rewrite mulf_eq0 pnatr_eq0 /= => /eqP.
by rewrite mulr2n mulrDl mul1r /X [X in _ + X]swap_det_surface subrr eqxx.
Qed.

Lemma surface_aba (a b : R * R) :
  \det(surface_mx (mkf3 a b a)) = 0.
Proof.
by rewrite cycle_det_surface1 surface_aab.
Qed.

Lemma surface_baa (a b : R * R) :
  \det(surface_mx (mkf3 b a a)) = 0.
Proof.
by rewrite -cycle_det_surface1 surface_aab.
Qed.

Lemma ccw_uniq (a b c : R * R) :
  ccw a b c -> uniq [:: a; b; c].
Proof.
rewrite /ccw; have [] := boolP(uniq [:: a; b; c]) => //.
rewrite /= /ccw !(negb_and, negbK, orbF, inE) => /orP[/orP []|] /eqP ->;
set s := surface _ _ _; rewrite /s surface_value
  !(surface_aab, surface_aba, surface_baa,
   eqxx ,ltrr, orbF, orFb, andFb, andbF) //; try (case:ifP;[move=> _ -> //|]);
  (case: ifP; [move=> abs; rewrite ltrNge ler_eqVlt abs orbT //|];
  case: ifP => //= _ _; case: ifP => /= ab2; [ |case: ifP=> //];
  by rewrite ltrNge ler_eqVlt ab2 orbT).
Qed.

Lemma Kn1 (a b c : R * R) : ccw a b c -> ccw b c a.
Proof.
move=> abc; have uabc := ccw_uniq abc; move: abc.
rewrite ccw_tail_coef //.
have /ccw_tail_coef -> : uniq [:: b; c; a].
  move: uabc; rewrite /= !(inE, negb_or, (eq_sym c), (eq_sym b a), andbT).
  by move=> /andP[/andP[]] -> -> ->.
by rewrite /ptbs -cycle_det_surface1.
Qed.

Lemma Kn2 (a b c : R * R) : ccw a b c -> ~~ccw b a c.
Proof.
move=> abc; apply/negP=> bac; move: (abc) (bac).
rewrite (ccw_tail_coef (ccw_uniq abc)) (ccw_tail_coef (ccw_uniq bac)).
rewrite /ptbs swap_det_surface tail_coefN oppr_gt0=> abs.
by rewrite ltrNge ler_eqVlt abs orbT.
Qed.

Lemma Kn3 (a b c : R * R) : uniq [:: a; b; c] ->
  ccw a b c \/ ccw b a c.
Proof.
move=> uabc; have ptbn0 := non_zero_polynomial uabc.
rewrite !ccw_tail_coef //; last first.
  move: uabc; rewrite /= !(inE, negb_or, andbT, (eq_sym b a)).
  by move=> /andP[/andP[]]-> -> ->.
rewrite /ptbs (swap_det_surface (ptb b)) -/(ptbs a b c).
have [ | ] := boolP(0 < tail_coef(ptbs a b c)).
  by left.
rewrite -lerNgt ler_eqVlt tail_coef_eq0 (negbTE ptbn0) /= tail_coefN oppr_gt0.
by right.
Qed.

Lemma Kn4 (a b c d : R * R) :
  ccw a b d -> ccw b c d -> ccw c a d -> ccw a b c.
Proof.
move=> abd bcd cad.
have uabd := ccw_uniq abd.
have ubcd := ccw_uniq bcd.
have ucad := ccw_uniq cad.
have uabc : uniq [:: a; b; c].
  move: (uabd) (ubcd) (ucad); rewrite /= !(inE, negb_or) -!andbA (eq_sym a c).
  by move=> /andP[] -> _ /andP[] -> _ /andP[] -> _.
rewrite ccw_tail_coef //.
have -> : ptbs a b c = ptbs b c d + ptbs c a d + ptbs a b d.
  have := Knuth_4_main (ptb a) (ptb b) (ptb c) (ptb d).
  rewrite -/(ptbs a b c) -/(ptbs a b d) -/(ptbs b c d) swap_det_surface.
  by rewrite -/(ptbs c a d) opprK => /eqP; rewrite subr_eq0 eq_sym => /eqP.
by rewrite !tail_coefD_gt0 // -ccw_tail_coef.
Qed.

Lemma Kn5 (a b c d e : R * R):
  ccw a b c -> ccw a b d -> ccw a b e ->
  ccw a c d -> ccw a d e -> ccw a c e.
Proof.
move=> abc abd abe acd ade.
have uabc := ccw_uniq abc.
have uabd := ccw_uniq abd.
have uabe := ccw_uniq abe.
have uacd := ccw_uniq acd.
have uade := ccw_uniq ade.
have uace : uniq [:: a; c; e].
  move: (uabc) (uabe); rewrite /= !(inE, negb_or) -!andbA.
  rewrite !andbT !andTb => /andP[] _ /andP[] -> _ /andP[] _ /andP[] -> _ /=.
  by apply/negP=> ce; move: (Kn2 (Kn1 (Kn1 ade))); rewrite -(eqP ce) acd.
rewrite ccw_tail_coef //.
have pabd : 0 < tail_coef (ptbs a b d) by rewrite -ccw_tail_coef //.
rewrite -(pmulr_rgt0 _ pabd) -tail_coefM.
have := @cramer _ (ptb a) (ptb b) (ptb c) (ptb d) (ptb e).
rewrite -/(ptbs a b c) -/(ptbs a b d) -/(ptbs a b e) -/(ptbs a c d)
 -/(ptbs a d e) -/(ptbs a c e) => ->.
by rewrite tail_coefD_gt0 // tail_coefM mulr_gt0 // -ccw_tail_coef.
Qed.

Lemma Kn5' (a b c d e : R * R):
  ccw a b c -> ccw a b d -> ccw a b e ->
  ccw b c d -> ccw b d e -> ccw b c e.
Proof.
move=> abc abd abe bcd bde.
have uabc := ccw_uniq abc.
have uabd := ccw_uniq abd.
have uabe := ccw_uniq abe.
have ubcd := ccw_uniq bcd.
have ubde := ccw_uniq bde.
have ubce : uniq [:: b; c; e].
  move: (uabc) (uabe); rewrite /= !(inE, negb_or) -!andbA.
  rewrite !andbT !andTb => /andP[] _ /andP[] _ -> /andP[] _ /andP[] _ -> /=.
  by apply/negP=> ce; move: (Kn2 (Kn1 (Kn1 bde))); rewrite -(eqP ce) bcd.
rewrite ccw_tail_coef //.
have pabd : 0 < tail_coef (ptbs a b d) by rewrite -ccw_tail_coef //.
rewrite -(pmulr_rgt0 _ pabd) -tail_coefM.
have := @cramer' _ (ptb a) (ptb b) (ptb c) (ptb d) (ptb e).
rewrite -/(ptbs a b c) -/(ptbs a b d) -/(ptbs a b e) -/(ptbs b c d)
 -/(ptbs b d e) -/(ptbs b c e) => ->.
by rewrite tail_coefD_gt0 // tail_coefM mulr_gt0 // -ccw_tail_coef.
Qed.

Definition ptb_cc_system : cc_system ccw :=
  @Build_cc_system (prod_eqType R  R)%type ccw Kn1 Kn2 Kn3 Kn4 Kn5 Kn5'.

End perturbed_setting.



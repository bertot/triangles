Require Import ZArith List Bool.

Open Scope Z_scope.

Definition P := (Z * Z)%type.

Definition det33 (a b c
                  d e f
                  g h i : Z) := a * e * i + d * h * c + b * f * g -
                                g * e * c - b * d * i - h * f * a.

Definition dsurf (p1 p2 p3 : Z * Z) :=
  det33 1 (fst p1) (snd p1) 1 (fst p2) (snd p2) 1 (fst p3) (snd p3).

Record triangle := mkt { tp1 : P; tp2 : P; tp3 : P}.

Record edge := mke {ep1 : P; ep2 : P}.

Definition is_inside (t : triangle) (p : P) :=
  (0 <? dsurf (tp1 t) (tp2 t) p) &&
  (0 <? dsurf (tp2 t) (tp3 t) p) &&
  (0 <? dsurf (tp3 t) (tp1 t) p).

Definition find_triangle (tr : list triangle) (p : P) :=
  find (fun t => is_inside t p) tr.

Definition red_edge (e : edge) (p : P) :=
  0 <? dsurf (ep2 e) (ep1 e) p.

Fixpoint find_red_prefix (b : list edge) (p : P) : list edge * list edge :=
  match b with
  | nil => (nil, nil)
  | e::tl =>
    if red_edge e p then
      let (reds, rest) := find_red_prefix tl p in
        (e::reds, rest)
    else
      (nil, e::tl)
   end.

Fixpoint find_blue_prefix (b : list edge) (p : P) : list edge * list edge :=
  match b with
  | nil => (nil, nil)
  | e::tl =>
    if negb (red_edge e p) then
      let (blues, rest) := find_blue_prefix tl p in
        (e::blues, rest)
    else
      (nil, e::tl)
   end.

Definition find_red_edges (boundary : list edge) (p : P) :
  list edge * list edge :=
  match boundary with
  | nil => (nil, nil)
  | e :: _ =>
    if red_edge e p then
      let (first_reds, rest) := find_red_prefix boundary p in
      let (blues, other_reds) := find_blue_prefix rest p in
      (other_reds++first_reds, blues)
    else
      let (first_blues, rest) := find_blue_prefix boundary p in
      let (reds, other_blues) := find_red_prefix rest p in
      (reds, other_blues++first_blues)
  end.

Fixpoint new_triangles_aux (default : edge) (red_edges : list edge) 
    (triangulation : list triangle) (p : P) : edge * list triangle :=
  match red_edges with 
    nil => (default, triangulation)
  | a::nil => (mke p (ep2 a), mkt p (ep2 a) (ep1 a)::triangulation)
  | a::tl => 
      new_triangles_aux a tl (mkt p (ep2 a) (ep1 a) :: triangulation) p
  end.

Definition new_triangles_and_boundary (red_edges blue_edges : list edge) 
  (triangulation : list triangle) (p : P) :
  list triangle * list edge :=
  match red_edges with
  | nil => (triangulation, blue_edges)
  | a::tl => 
    let new_edge1 := mke (ep1 a) p in
    let (new_edge2, triangles) :=
    new_triangles_aux a red_edges triangulation p in
    (triangles, new_edge1::new_edge2::blue_edges)
  end.

Definition list_to_triangles_and_edges (s : list P)
   : list triangle * list edge :=
  match s with
  | a::b::c::_ => 
    if 0 <? dsurf a b c then
      (mkt a b c::nil, mke a b::mke b c:: mke c a::nil)
    else
      (mkt a c b::nil, mke b a:: mke a c:: mke c b::nil)
  | _ => (nil, nil)
  end.

Definition triangle_eq_dec (t1 t2 : triangle) : {t1 = t2} + {t1 <> t2}.
case t1; intros [p11x p11y] [p12x p12y] [p13x p13y].
case t2; intros [p21x p21y] [p22x p22y] [p23x p23y].
case (Z.eq_dec p11x p21x);
  [intros ? | intros N; right; intros E; injection E; intros; case N; auto].
case (Z.eq_dec p11y p21y);
  [intros ? | intros N; right; intros E; injection E; intros; case N; auto].
case (Z.eq_dec p12x p22x);
  [intros ? | intros N; right; intros E; injection E; intros; case N; auto].
case (Z.eq_dec p12y p22y);
  [intros ? | intros N; right; intros E; injection E; intros; case N; auto].
case (Z.eq_dec p13x p23x);
  [intros ? | intros N; right; intros E; injection E; intros; case N; auto].
case (Z.eq_dec p13y p23y);
  [intros ? | intros N; right; intros E; injection E; intros; case N; auto].
now left; subst.
Defined.    

Fixpoint naive_concrete (s : list P) : list triangle * list edge :=
  match s with
  | a::tl =>
    if (length tl <=? 2)%nat then
      list_to_triangles_and_edges s
    else
    let (trs, eds) := naive_concrete tl in
    match find_triangle trs a with
    | Some (mkt p1 p2 p3) => 
      (mkt p1 p2 a::mkt p2 p3 a::mkt p3 p1 a::
         remove triangle_eq_dec (mkt p1 p2 p3) trs, eds)
    | None =>
      let (red_edges, blue_edges) := find_red_edges eds a in
      new_triangles_and_boundary red_edges blue_edges trs a
    end
  | nil => (nil, nil)
  end.

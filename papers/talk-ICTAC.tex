\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{alltt}
\usepackage{color}
\newcommand{\coqor}{{\tt\char'134/}}
\newcommand{\coqand}{{\tt/\char'134}}
\newcommand{\coqnot}{\mbox{\~{}}}
\newcommand{\ssrin}{{\tt\char'134{}in}}

\setbeamertemplate{footline}[frame number]
\title{Formal Verification of a Geometry Algorithm}
\author{Yves Bertot}
\date{October 2018}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{Outline}
\begin{itemize}
\item Triangulation algorithm
\item Abstract presentation and successive refinements
\item Symmetries of the triangle and the convex hull
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Triangulations and formal proofs}
\begin{itemize}
\item Bigger context: robot motion
\begin{itemize}
\item Vorono\"{\i} diagrams and Delaunay triangulation
\end{itemize}
\item Dufourd\&Bertot10 starts from an existing triangulation
\item Dufourd\&Bertot10 uses a specific data-structure
\begin{itemize}
\item darts: edge extremities
\item two darts to make an edge, \(\alpha_0\)
\item darts around a point are connected, \(\alpha_1\)
\item A third permutation \((\alpha_0 \circ \alpha_1)^{-1}\) enumerates facets
\end{itemize}
\item Astonishingly handy for implementation in C-like languages
\begin{itemize}
\item \(\alpha_0\) and \(\alpha_1\) are just pointers
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Drawing with darts}
\begin{center}
\includegraphics[scale=0.5,trim=0cm 18cm 0cm 2cm, clip=true]{triangles_darts.pdf}
\end{center}
\begin{itemize}
\item \(d_0, d_1, d_2\) and \(d_3, d_4, d_5\) form triangles
\item unnamed darts form the outer boundary
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{A more abstract approach to triangulations}
\begin{itemize}
\item Fix a finite set of points
\item Each point has a pair of coordinates
\begin{itemize}
\item all operations on coordinates are deemed exact
\item Only ring operations are necessary
\end{itemize}
\item Describe a triangulation as a set of triangles
\item Describe each triangle as a three-point set
\item The formalized set theory we use has all basic set operations
\begin{itemize}
\item set difference, singletons, cardinal, union, intersection
\item all sets are naturally finite and all operations are decidable 
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Crucial geometry property}
\begin{itemize}
\item Given a segment \([a, b]\), does \(c\) lie to the left or to the right?
\item Knuth (1992) proposes to abstract this question as a single predicate \(ccw\) (counter-clockwise)
\item 5 basic properties (called {\em Knuth axioms})
\begin{enumerate}
\item \(ccw(a,b,c) = ccw(b,c,a)\)
\item \(ccw(a,b,c) \Rightarrow \neg ccw(b,a,c)\)
\item \(card\{a,b,c\} = 3 \Rightarrow ccw(a,b,c) \vee ccw(b,a,c)\)
\item \(ccw(a,b,d) \wedge ccw(b,c,d) \wedge ccw(c,a,d) \Rightarrow ccw(a,b,c)\)
\item \(ccw(a,b,c) \wedge ccw(a,b,d) \wedge ccw(a,b,e) \wedge ccw(a,c,d) \wedge ccw(a,d,e) \Rightarrow ccw(a,c,e)\)
\end{enumerate}
\item Axiom 3 is a property of the data: no three points aligned
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Illustration of Axiom 4}
\begin{center}
\includegraphics[scale=0.5,trim={3cm 16cm 7cm 7cm}, clip]{axiom4.pdf}
\end{center}
\end{frame}
\begin{frame}
\frametitle{Illustration of Axiom 5}
\begin{center}
\includegraphics[scale=0.7,trim={4cm 16cm 4cm 4cm}, clip]{ax5.pdf}
\end{center}
\end{frame}
\begin{frame}
\frametitle{Implementing and proving \(ccw\)}
\begin{itemize}
\item To every triangle we can associate an \(oriented surface\)
\item \(ccw\) just says that this oriented surface is positive
\item the oriented surface is computed by a determinant
\item \(ccw(a,b,c) = \left(0 < \left|\begin{array}{ccc}1&x_a&y_a\\
1&x_b&y_b\\
1&x_c&y_c
\end{array}\right|\right)\)
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Discussion on numerical issues}
\begin{itemize}
\item The assumption of exact numerical computations is reasonable
\begin{itemize}
\item Operations for computing the counter-clockwise operation rely on
polynomials with degree 2
\item Limiting the precision for the inputs and using double precision numbers is enough
\item alternatively using 32-bit integers for inputs and 64 bit integers for
results
\item In Coq itself, all computations are exact by default
\item We have to use a {\em discrete} number ring to be algorithmically
relevant
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Derived predicates}
\begin{itemize}
\item Whether point \(d\) is inside a triangle \(abc\) can be described using \(ccw\)
\item The convex hull of a set \(A\) of points can be described using \(ccw\)
\begin{itemize}
\item a sequence \(s_0, \ldots, s_n\) such that, for every \(i\) and \(x\) different
from \(s_i, s_{i+1}\), \(ccw(s_i, s_{i+1}, x)\) and similarly for \(ccw(s_n, s_0)\)
\item it can also be described by giving \(s_0\) and the function \(f\) that maps \(s_i\) to \(s_{i+1}\) and \(s_n\) to \(s_0\)
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Convex hull illustration}
\begin{center}
\includegraphics[scale=1, trim={5cm 18cm 10cm 3cm}]{convex_hull.pdf}
\end{center}
\end{frame}
\begin{frame}
\frametitle{A naive triangulation algorithm}
\begin{itemize}
\item Add points one by one
\item When you have three points make the first triangle
\item When the new point is in an existing triangle
\begin{itemize}
\item remove that one
\item add 3 new triangles made with the edges of the old triangle and the new point
\end{itemize}
\item Otherwise, find the boundary edges that are {\em separating}
\begin{itemize}
\item For each such separating boundary edge, make a new triangle by adding the new point
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Naive algorithm illustration}
\begin{center}
\includegraphics[scale=1, trim={5cm 18cm 10cm 3cm}]{naive_step.pdf}
\end{center}
\end{frame}
\begin{frame}
\frametitle{Naive algorithm illustration}
\begin{center}
\includegraphics[scale=1, trim={5cm 18cm 10cm 3cm}]{naive_step1.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Naive algorithm illustration}
\begin{center}
\includegraphics[scale=1, trim={5cm 18cm 10cm 3cm}]{naive_step2.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Naive algorithm illustration}
\begin{center}
\includegraphics[scale=1, trim={5cm 18cm 10cm 3cm}]{naive_step3.pdf}
\end{center}
\end{frame}
\begin{frame}
\frametitle{Mathematical formulation}
\begin{itemize}
\item Blue step: find \(t \in T\) such that \(p\) is inside \(t\)
\item \(T \backslash \{t\} \cup \{ t \backslash q \cup \{p\}\, |\, q \in t \}\)
\item Red step:
\item \(T \cup \bigcup_{t \in T} \bigcup_{\left\{\begin{array}{c}
q \in t,\\
{\rm boundary} (t \backslash q)\\
{\rm separated} (t \backslash q, p, q)
\end{array}\right.} \{p\} \cup (t \backslash q)\)
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Proving properties of this algorithm}
\begin{itemize}
\item First main specification
\begin{itemize}
\item All elements of the triangulation have 3 points
\item The union of all triangles is the input set
\item The whole convex hull is covered by triangles
\end{itemize}
\item Need Geometry proofs
\item Re-use Knuth's approach to the convex Hull
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Examples of proofs}
\begin{itemize}
\item A point inside a triangle left of a segment is also left of the segment
\end{itemize}
\begin{alltt}
Lemma inside_triangle_left_of_edge a b c d e f :
  general_position ([set a; b; c; d; e; f]) ->
  ccw a b c -> ccw a b d -> ccw a b e ->
  ccw c d f -> ccw c f e -> ccw f d e ->
  ccw a b f.
\end{alltt}
\begin{itemize}
\item Many cases, but also many symmetries
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Illustration}
\begin{center}
\includegraphics[scale=1,trim={7cm 22cm 8cm 3cm}]{inside_triangle_leftof.pdf}
\end{center}
\end{frame}
\begin{frame}
\frametitle{Symmetries in this figure}
\begin{itemize}
\item The statement does not say which of \(c\), \(d\), or \(e\) is the
the leftmost point with respect to \(a\)
\item If we assume \(c\) to be the leftmost, it does not say whether
({\tt ccw} \(a\) \(d\) \(e\)) holds or not.
\item There are 6 cases in the proof, each case require 3 uses of
Knuth's 5th axiom
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Illustration}
\begin{center}
\includegraphics[scale=1,trim={7cm 22cm 8cm 3cm}]{inside_triangle_leftof2.pdf}
\end{center}
\end{frame}
\begin{frame}[fragile]
\frametitle{Key points concerning the convex hull}
\begin{itemize}
\item In the red case, red edges are grouped together
\item There are only two points adjacent to a blue and a red edge
\item This requires a proof by induction (80 lines of dense Coq script)
\end{itemize}
\begin{small}
\begin{alltt}
Lemma partition_red_blue a: a \ssrin cover s' -> ccw (f a) a p ->
  exists b, exists i,
  b \ssrin cover s' \coqand{} (0 < i < size (orbit f b))%N \coqand{}
  (forall j, (j < i)%N ->
      ccw (iter j.+1 f b) (iter j f b) p) \coqand{}
  (forall j, (i <= j < size (orbit f b))%N ->
     ccw (iter j f b) (iter j.+1 f b) p).
\end{alltt}
\end{small}
\end{frame}
\begin{frame}
\frametitle{A critique of the mathematical algorithm}
\begin{itemize}
\item Complexity is terrible
\item Generic operations on sets of sets
\item Refinements
\begin{itemize}
\item Caching data
\item Sparse representation for sets
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Refinements}
\begin{enumerate}
\item Return the convex hull with the triangulation
\item Take as input a list of points (instead of a subset)
\item Output a list of triangles (instead of a subset)
\end{enumerate}
\end{frame}
\begin{frame}
\frametitle{Convex Hull refinement}
\begin{itemize}
\item Refine the output type from {\tt \{set P\}} to\\ {\tt \{set P\} *
    (P \(\rightarrow\) P) * P}
\begin{itemize}
\item The first component is the same as for the previous algorithm
\item The second component is a function circulating the convex hull
\item The third component is a point on the convex hull
\end{itemize}
\item The convex hull does not change in the blue case
\item In the red case some computation must be done
\begin{itemize}
\item Detect and suppress red edges, keep blue edges
\item Add two new edges from and two purple points
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Convex hull computation in the red case}
\begin{center}
\includegraphics[scale=1, trim={5cm 18cm 10cm 3cm}]{red_convex_hull.pdf}
\end{center}
\end{frame}
\begin{frame}[fragile]
\frametitle{Defining the new convex hull in the red case}
\begin{itemize}
\item I chose to use the datatype of ``finite functions''
\item Just define the new convex hull as a variation over the old one
\item Proofs about this new path
\begin{itemize}
\item values for all blue points are the same as the old path function
\item Also true for the last purple point
\item The new path constitutes the new convex hull
\end{itemize}
\end{itemize}
\begin{small}
\begin{alltt}
Definition new_path (f : \{ffun P -> P\}) (a b np : P) :=
  finfun [fun x => f x with a |-> np, np |-> b].
\end{alltt}
\end{small}
\end{frame}
\begin{frame}
\frametitle{Proofs about the new convex hull}
\begin{itemize}
\item The new path is a cycle
\item The orbit of the new point describes the new boundary edges
\begin{itemize}
\item 400 lines of proof
\end{itemize}
\item The new path has the convex hull property
\begin{itemize}
\item 120 lines of proof
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Correctness statement for the first refinement}
\begin{alltt}
Lemma naive_naive2 n (s : \{set P\}) :
  (3 <= #|s| <= n)%N -> general_position s ->
  exists chf b,
    naive2 n s = (naive n s, chf, b) \coqand{}
    convex_hull_path coords s
        (boundary_edge (naive n s)) chf \coqand{}
    b \ssrin cover (boundary_edge (naive n s)).
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Input refinement}
\begin{itemize}
\item Input points from a list instead of a set
\item Move from a mathematical concept to a computer science one
\item Simple recursion scheme for lists (none for sets)
\item Need to replace a non-deterministic choice with a deterministic one
\item All proofs about the initial algorithm need to be abstract wrt the {\tt pick} function
\begin{itemize}
\item Lists that are equal up to permutation may lead to different triangulations
\item Introduce a function {\tt pick\_seq\_set} that chooses elements in a set according to the order given by a list, when applicable
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Code for the second refinement}
\begin{alltt}
Definition pick_seq_set (s : seq P) (s' : \{set P\}) :
   option P :=
  if (find (mem s') s < size s)%N then
    Some (nth p0 s (find (mem s') s))
  else pick_set s'.

Definition naive2' s n s' :=
  triangulation_algorithm.naive2 coords p0
    (pick_seq_set s) pick_triangle n s'.

Lemma naive2_naive3 (s : seq P) :
  (3 <= size s)%N -> uniq s ->
  naive3 s = naive2' s (size s) [set y in s].
\end{alltt}
\begin{itemize}
\item This proof is short: 25 lines
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{abstractions in the refinement ladder}
\begin{itemize}
\item {\tt naive} and {\tt naive2} are shown to return the same triangulation
when using the same {\tt pick\_set} function
\item {\tt naive2} and {naive3} are shown to return the same triangulation
when naive2 is using {\tt pick\_seq\_set}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Third refinement: lists of triangles}
\begin{itemize}
\item Instead of returning a set of sets, return a list of lists
\item Need to show that lists have no duplicates
\begin{itemize}
\item Each list representing a triangle has cardinal 3
\item The list of triangles has no duplicates as sets
\end{itemize}
\item This proof requires an extra result about the naive algorithm
\begin{itemize}
\item For a point inside the convex hull, there is {\bf exactly} one triangle
from the triangulation that contains it (270 lines of proofs)
\end{itemize}
\item Proving the refinement itself takes another 300 lines of proofs
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{}
\begin{alltt}
Lemma naive3_naive4 l :
  (3 <= size l)%N -> uniq l ->
  general_position [set x in l] ->
  let '(tr, f, b) := naive4 coords p0 l in
  naive3 l =
  ([set t in [seq [set x in t'] | t' <- tr]], f, b) \coqand{}
  uniq [seq [set y in x] | x <- tr].
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{More refinements}
\begin{itemize}
\item Work in progress: finding the right triangles can be improved using
Delaunay
\item The convex hull path can also be refined
\begin{itemize}
\item into a cycle of pointers in C-like implementations
\item into a list for a function implentation (run inside Coq)
\end{itemize}
\item The {\em general position} condition could be refined, too.
\begin{itemize}
\item lazy verification of the constraints
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Conclusion}
\begin{itemize}
\item Advertising the power of interactive theorem proving
\item Refinement is a big thing in the B method 
\item Libraries are important
\item The set abstraction provided by Mathematical components is a big help 
\item Logical properties (specifications) as a support for collaboration
\end{itemize}
\end{frame}


\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
